package faktura.FakturaSprzedazyComponent.Test;

import faktura.FakturaSprzedazyComponent.Contract.IFakturaSprzedazy;
import faktura.FakturaSprzedazyComponent.Implementation.FakturaSprzedazy;
import faktura.FakturaSprzedazyComponent.Model.FakturaSprzedazyModel;
import faktura.ListaKontrahentowComponent.Model.AdresModel;
import faktura.ListaKontrahentowComponent.Model.KontrahentModel;
import magazyn.ListaTowarowComponent.Contract.IListaTowarow;
import magazyn.ListaTowarowComponent.Implementation.ListaTowarow;
import magazyn.ListaTowarowComponent.Model.TowarModel;
import glowne.BazaDanychComponent.Implementation.BazaDanych;
import test.resetBazy.ResetBazy;
import org.junit.Assert;
import org.junit.Test;
import java.util.Date;
import pk.epub.Creator.Creator;

/**
 * @author Katarzyna Romanowska
 * @version 1.0.1.0
 */
public class FakturaSprzedazyTest {

    @SuppressWarnings("deprecation")
    private static String GenerujBazeTowarow() {
        ResetBazy.usunBaze("BazaTowarow.yap", false);
        ResetBazy.usunBaze("BazaFakturSprzedazy.yap", true);
        String wynik = "Towar został pomyślnie dodany do bazy danych.";
        IListaTowarow listaTowarow = new ListaTowarow(new BazaDanych());
        wynik = listaTowarow.dodajTowar(new TowarModel(1, "nazwa1", "producent1", 11, 1.00, 0.1, new Date(2013, 0, 31), "pkwiu1", 0.1, "Produkt Testowy1", false, false));
        wynik = listaTowarow.dodajTowar(new TowarModel(2, "nazwa2", "producent2", 11, 1.00, 0.1, new Date(2013, 0, 31), "pkwiu1", 0.1, "Produkt Testowy1", false, false));
        wynik = listaTowarow.dodajTowar(new TowarModel(3, "nazwa3", "producent3", 7, 1.00, 0.1, new Date(2013, 0, 12), "pkwiu1", 0.1, "Produkt Testowy1", false, false));
        wynik = listaTowarow.dodajTowar(new TowarModel(4, "nazwa4", "producent4", 5, 1.00, 0.1, new Date(2013, 0, 14), "pkwiu1", 0.1, "Produkt Testowy1", false, false));
        wynik = listaTowarow.dodajTowar(new TowarModel(5, "nazwa5", "producent5", 8, 1.00, 0.1, new Date(2013, 0, 31), "pkwiu1", 0.1, "Produkt Testowy1", false, false));
        wynik = listaTowarow.dodajTowar(new TowarModel(6, "nazwa6", "producent6", 4, 1.00, 0.1, new Date(2013, 1, 1), "pkwiu1", 0.1, "Produkt Testowy1", false, false));
        wynik = listaTowarow.dodajTowar(new TowarModel(7, "nazwa7", "producent7", 78, 1.00, 0.1, new Date(2013, 1, 28), "pkwiu1", 0.1, "Produkt Testowy1", false, false));
        wynik = listaTowarow.dodajTowar(new TowarModel(8, "nazwa8", "producent8", 43, 1.00, 0.1, new Date(2013, 1, 15), "pkwiu1", 0.1, "Produkt Testowy1", false, false));
        wynik = listaTowarow.dodajTowar(new TowarModel(9, "nazwa9", "producent9", 0, 1.00, 0.1, new Date(2013, 1, 27), "pkwiu1", 0.1, "Produkt Testowy1", false, false));
        wynik = listaTowarow.dodajTowar(new TowarModel(10, "nazwa10", "producent10", 0, 1.00, 0.1, new Date(2013, 2, 1), "pkwiu1", 0.1, "Produkt Testowy1", false, false));
        wynik = listaTowarow.dodajTowar(new TowarModel(11, "nazwa11", "producent11", 0, 1.00, 0.1, new Date(2013, 2, 31), "pkwiu1", 0.1, "Produkt Testowy1", false, false));
        wynik = listaTowarow.dodajTowar(new TowarModel(12, "nazwa12", "producent12", 0, 1.00, 0.1, new Date(2013, 2, 12), "pkwiu1", 0.1, "Produkt Testowy1", false, false));
        wynik = listaTowarow.dodajTowar(new TowarModel(13, "nazwa13", "producent13", 5, 1.00, 0.1, new Date(2013, 2, 18), "pkwiu1", 0.1, "Produkt Testowy1", false, false));
        wynik = listaTowarow.dodajTowar(new TowarModel(14, "nazwa14", "producent14", 10, 1.00, 0.1, new Date(2013, 2, 21), "pkwiu1", 0.1, "Produkt Testowy1", false, false));
        wynik = listaTowarow.dodajTowar(new TowarModel(15, "nazwa15", "producent15", 0, 1.00, 0.1, new Date(2013, 0, 1), "pkwiu1", 0.1, "Produkt Testowy1", false, true));
        wynik = listaTowarow.dodajTowar(new TowarModel(16, "nazwa16", "producent16", 1, 1.00, 0.1, new Date(2013, 0, 31), "pkwiu1", 0.1, "Produkt Testowy1", true, false));
        wynik = listaTowarow.dodajTowar(new TowarModel(17, "nazwa17", "producent17", 1, 1.00, 0.1, new Date(2013, 0, 16), "pkwiu1", 0.1, "Produkt Testowy1", true, true));
        return wynik;
    }

    @SuppressWarnings("deprecation")
    @Test
    public void testKomponentu() {
        GenerujBazeTowarow();
        IFakturaSprzedazy fakturaSprzedazy = new FakturaSprzedazy(new BazaDanych(), new Creator());

        //faktura
        FakturaSprzedazyModel faktura = new FakturaSprzedazyModel(new Date(2013, 10, 30), new Date(2013, 10, 29), "Jan", "Kowalski", 1);

        //kontrahent
        KontrahentModel kontrahent = new KontrahentModel("Firma2", "2", "Tadeusz", "Nowak", new AdresModel("Ladna", "22", "2", "22-222", "Poznan", "Polska"));
        kontrahent.setKontrahentID(1);

        String wiadomosc1 = fakturaSprzedazy.dodajKontrahentaDoFaktury(faktura, kontrahent);
        Assert.assertEquals(1, faktura.getKontrahentID());
        Assert.assertEquals("Kontrahent został pomyślnie dodany", wiadomosc1);

        //towar
        TowarModel towar1 = new TowarModel(1, "nazwa1", "producent1", 11, 1.00, 0.1, new Date(2013, 0, 31), "pkwiu1", 0.1, "Produkt Testowy1", false, false);
        TowarModel towar2 = new TowarModel(2, "nazwa2", "producent2", 11, 1.00, 0.1, new Date(2013, 0, 31), "pkwiu1", 0.1, "Produkt Testowy1", false, false);
        TowarModel towar3 = new TowarModel(3, "nazwa3", "producent3", 8, 1.00, 0.1, new Date(2013, 0, 12), "pkwiu1", 0.1, "Produkt Testowy1", false, false);

        //dodawanie
        String wiadomosc2 = fakturaSprzedazy.dodajTowarDoFaktury(faktura, towar1, 7);
        Assert.assertEquals(6.93, faktura.getFakturaKwota(), 0.0000001);
        Assert.assertEquals(1, faktura.getFakturaTowary().size());
        Assert.assertEquals("nazwa1", faktura.getFakturaTowary().get(0).getTowarnNazwa());
        Assert.assertEquals(7, faktura.getFakturaTowary().get(0).getTowarIlosc(), 0);
        Assert.assertEquals("Towar został pomyślnie dodany", wiadomosc2);

        String wiadomosc3 = fakturaSprzedazy.dodajTowarDoFaktury(faktura, towar1, 2);
        Assert.assertEquals(8.91, faktura.getFakturaKwota(), 0.0000001);
        Assert.assertEquals(1, faktura.getFakturaTowary().size());
        Assert.assertEquals("nazwa1", faktura.getFakturaTowary().get(0).getTowarnNazwa());
        Assert.assertEquals(9, faktura.getFakturaTowary().get(0).getTowarIlosc(), 0);
        Assert.assertEquals("Liczebność towaru została zaktualizowana.", wiadomosc3);

        String wiadomosc4 = fakturaSprzedazy.dodajTowarDoFaktury(faktura, towar2, 12);
        Assert.assertEquals(8.91, faktura.getFakturaKwota(), 0.0000001);
        Assert.assertEquals(1, faktura.getFakturaTowary().size());
        Assert.assertEquals("nazwa1", faktura.getFakturaTowary().get(0).getTowarnNazwa());
        Assert.assertEquals(9, faktura.getFakturaTowary().get(0).getTowarIlosc(), 0);
        Assert.assertEquals("Błąd! Nie można dodać określonej ilości towaru.", wiadomosc4);

        String wiadomosc5 = fakturaSprzedazy.dodajTowarDoFaktury(faktura, towar3, 7);
        Assert.assertEquals(15.84, faktura.getFakturaKwota(), 0.0000001);
        Assert.assertEquals(2, faktura.getFakturaTowary().size());
        Assert.assertEquals("nazwa3", faktura.getFakturaTowary().get(1).getTowarnNazwa());
        Assert.assertEquals(7, faktura.getFakturaTowary().get(1).getTowarIlosc(), 0);
        Assert.assertEquals("Towar został pomyślnie dodany", wiadomosc5);

        //usuwanie
        String wiadomosc6 = fakturaSprzedazy.usunTowarZFaktury(faktura, towar1);
        Assert.assertEquals(6.93, faktura.getFakturaKwota(), 0.0000001);
        Assert.assertEquals(1, faktura.getFakturaTowary().size());
        Assert.assertEquals("nazwa3", faktura.getFakturaTowary().get(0).getTowarnNazwa());
        Assert.assertEquals(7, faktura.getFakturaTowary().get(0).getTowarIlosc(), 0);
        Assert.assertEquals("Towar został pomyślnie usunięty", wiadomosc6);

        String wiadomosc7 = fakturaSprzedazy.usunTowarZFaktury(faktura, towar2);
        Assert.assertEquals(6.93, faktura.getFakturaKwota(), 0.0000001);
        Assert.assertEquals(1, faktura.getFakturaTowary().size());
        Assert.assertEquals("nazwa3", faktura.getFakturaTowary().get(0).getTowarnNazwa());
        Assert.assertEquals(7, faktura.getFakturaTowary().get(0).getTowarIlosc(), 0);
        Assert.assertEquals("Błąd! Nie znaleziono towaru.", wiadomosc7);

        //sprawdzenie towarow
        Assert.assertEquals(11, towar1.getTowarIlosc(), 0);
        Assert.assertEquals(11, towar2.getTowarIlosc(), 0);
        Assert.assertEquals(8, towar3.getTowarIlosc(), 0);

        //dodanie faktury
        String wiadomosc8 = fakturaSprzedazy.dodajFaktureSprzedazy(new FakturaSprzedazyModel(new Date(2013, 9, 1), new Date(2013, 9, 1), "Janusz", "Kwiatkowski", 1));
        String wiadomosc9 = fakturaSprzedazy.dodajFaktureSprzedazy(new FakturaSprzedazyModel(new Date(2013, 10, 30), new Date(2013, 10, 29), "Janek", "Kowalski", 1));
        String wiadomosc10 = fakturaSprzedazy.dodajFaktureSprzedazy(faktura);
        Assert.assertEquals(1, faktura.getFakturaTowary().size());
        Assert.assertEquals("nazwa3", faktura.getFakturaTowary().get(0).getTowarnNazwa());
        Assert.assertEquals(7, faktura.getFakturaTowary().get(0).getTowarIlosc(), 0);
        Assert.assertEquals("Faktura sprzedazy została pomyślnie dodana do bazy danych.", wiadomosc8);
        Assert.assertEquals("Faktura sprzedazy została pomyślnie dodana do bazy danych.", wiadomosc9);
        Assert.assertEquals("Faktura sprzedazy została pomyślnie dodana do bazy danych.", wiadomosc10);
    }
}
