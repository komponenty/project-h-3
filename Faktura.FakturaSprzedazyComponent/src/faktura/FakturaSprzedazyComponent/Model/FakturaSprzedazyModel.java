package faktura.FakturaSprzedazyComponent.Model;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import magazyn.ListaTowarowComponent.Model.TowarModel;

/**
 * @author Katarzyna Romanowska
 * @version 1.0.1.3
 */
public class FakturaSprzedazyModel {

    //<editor-fold defaultstate="collapsed" desc="Pola">
    private String fakturaID;
    private Date fakuraDataWystawienie;
    private Date fakuraDataZaplata;
    private double fakturaKwota;
    private String fakturaOsobaWystawiajacaImie;
    private String fakturaOsobaWystawiajacaNazwisko;
    private int fakturaOsobaWystawiajacaID;
    private int kontrahentID;
    private List<TowarModel> fakturaTowary;
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Wlasciwosci">
    public void setFakturaID(String fakturaID) {
        String oldValue = this.fakturaID;
        this.fakturaID = fakturaID;
        changeSupport.firePropertyChange("fakturaID", oldValue, fakturaID);
    }

    public String getFakturaID() {
        return this.fakturaID;
    }

    public void setFakuraDataWystawienie(Date fakuraDataWystawienie) {
        Date oldValue = this.fakuraDataWystawienie;
        this.fakuraDataWystawienie = fakuraDataWystawienie;
        changeSupport.firePropertyChange("fakuraDataWystawienie", oldValue, fakuraDataWystawienie);
    }

    public Date getFakuraDataWystawienie() {
        return this.fakuraDataWystawienie;
    }

    public void setFakuraDataZaplata(Date fakuraDataZaplata) {
        Date oldValue = this.fakuraDataZaplata;
        this.fakuraDataZaplata = fakuraDataZaplata;
        changeSupport.firePropertyChange("fakuraDataZaplata", oldValue, fakuraDataZaplata);
    }

    public Date getFakuraDataZaplata() {
        return this.fakuraDataZaplata;
    }

    public void setFakturaKwota(double fakturaKwota) {
        double oldValue = this.fakturaKwota;
        this.fakturaKwota = fakturaKwota;
        changeSupport.firePropertyChange("fakturaKwota", oldValue, fakturaKwota);
    }

    public double getFakturaKwota() {
        return this.fakturaKwota;
    }

    public void setFakturaOsobaWystawiajacaImie(String fakturaOsobaWystawiajacaImie) {
        String oldValue = this.fakturaOsobaWystawiajacaImie;
        this.fakturaOsobaWystawiajacaImie = fakturaOsobaWystawiajacaImie;
        changeSupport.firePropertyChange("fakturaOsobaWystawiajacaImie", oldValue, fakturaOsobaWystawiajacaImie);
    }

    public String getFakturaOsobaWystawiajacaImie() {
        return this.fakturaOsobaWystawiajacaImie;
    }

    public void setFakturaOsobaWystawiajacaNazwisko(String fakturaOsobaWystawiajacaNazwisko) {
        String oldValue = this.fakturaOsobaWystawiajacaNazwisko;
        this.fakturaOsobaWystawiajacaNazwisko = fakturaOsobaWystawiajacaNazwisko;
        changeSupport.firePropertyChange("fakturaOsobaWystawiajacaNazwisko", oldValue, fakturaOsobaWystawiajacaNazwisko);
    }

    public String getFakturaOsobaWystawiajacaNazwisko() {
        return this.fakturaOsobaWystawiajacaNazwisko;
    }

    public void setFakturaOsobaWystawiajacaID(int fakturaOsobaWystawiajacaID) {
        int oldValue = this.fakturaOsobaWystawiajacaID;
        this.fakturaOsobaWystawiajacaID = fakturaOsobaWystawiajacaID;
        changeSupport.firePropertyChange("fakturaOsobaWystawiajacaID", oldValue, fakturaOsobaWystawiajacaID);
    }

    public int getFakturaOsobaWystawiajacaID() {
        return this.fakturaOsobaWystawiajacaID;
    }

    public void setKontrahentID(int kontrahentID) {
        int oldValue = this.kontrahentID;
        this.kontrahentID = kontrahentID;
        changeSupport.firePropertyChange("kontrahentID", oldValue, kontrahentID);
    }

    public int getKontrahentID() {
        return this.kontrahentID;
    }

    public void setFakturaTowary(List<TowarModel> fakturaTowary) {
        List<TowarModel> oldValue = this.fakturaTowary;
        this.fakturaTowary = fakturaTowary;
        changeSupport.firePropertyChange("fakturaTowary", oldValue, fakturaTowary);
    }

    public List<TowarModel> getFakturaTowary() {
        return this.fakturaTowary;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Konstruktory">
    public FakturaSprzedazyModel() {
        this.fakturaTowary = new ArrayList<TowarModel>();
    }

    public FakturaSprzedazyModel(int kontrahentID) {
        this.kontrahentID = kontrahentID;
    }

    public FakturaSprzedazyModel(String fakturaID) {
        this.fakturaID = fakturaID;
    }

    public FakturaSprzedazyModel(Date fakuraDataWystawienie) {
        this.fakuraDataWystawienie = fakuraDataWystawienie;
        this.fakturaTowary = new ArrayList<TowarModel>();
    }

    public FakturaSprzedazyModel(Date fakuraDataWystawienie, Date fakuraDataZaplata, String fakturaOsobaWystawiajacaImie, String fakturaOsobaWystawiajacaNazwisko, int fakturaOsobaWystawiajacaID) {
        this.fakuraDataWystawienie = fakuraDataWystawienie;
        this.fakuraDataZaplata = fakuraDataZaplata;
        this.fakturaOsobaWystawiajacaImie = fakturaOsobaWystawiajacaImie;
        this.fakturaOsobaWystawiajacaNazwisko = fakturaOsobaWystawiajacaNazwisko;
        this.fakturaOsobaWystawiajacaID = fakturaOsobaWystawiajacaID;
        this.fakturaTowary = new ArrayList<TowarModel>();
    }
    
    public FakturaSprzedazyModel(FakturaSprzedazyModel fakturaSprzedazyModel) {
        this.fakuraDataWystawienie = fakturaSprzedazyModel.getFakuraDataWystawienie();
        this.fakuraDataZaplata = fakturaSprzedazyModel.getFakuraDataZaplata();
        this.fakturaOsobaWystawiajacaImie = fakturaSprzedazyModel.getFakturaOsobaWystawiajacaImie();
        this.fakturaOsobaWystawiajacaNazwisko = fakturaSprzedazyModel.getFakturaOsobaWystawiajacaNazwisko();
        this.fakturaOsobaWystawiajacaID = fakturaSprzedazyModel.getFakturaOsobaWystawiajacaID();
        this.fakturaTowary = fakturaSprzedazyModel.getFakturaTowary();
        this.fakturaID = fakturaSprzedazyModel.getFakturaID();
        this.kontrahentID = fakturaSprzedazyModel.getKontrahentID();
        this.fakturaKwota = fakturaSprzedazyModel.getFakturaKwota();
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="PropertyChange">
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="toString method">
    @Override
    public String toString() {
        return this.fakturaID; 
    }
    //</editor-fold>
}
