package faktura.FakturaSprzedazyComponent.Implementation;

import java.util.Date;
import java.util.List;
import magazyn.ListaTowarowComponent.Model.TowarModel;
import faktura.FakturaSprzedazyComponent.Contract.IFakturaSprzedazy;
import faktura.FakturaSprzedazyComponent.Model.FakturaSprzedazyModel;
import faktura.ListaKontrahentowComponent.Model.KontrahentModel;
import glowne.BazaDanychComponent.Contract.IBaza;
import glowne.KonfiguracjaComponent.Model.PrzedsiebiorstwoModel;
import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import pk.epub.Creator.Contracts.ICreator;

/**
 * @author Katarzyna Romanowska
 * @version 1.0.1.4
 */
public class FakturaSprzedazy implements IFakturaSprzedazy {

    private IBaza bazaDanych;
    private ICreator creator;

    /**
     * Konstruktor zapewniajacy podstawowe funkcjonalnosci komponentu 
     *
     * @param bazaDanych repozytorium używane do przeprowadzania operacji nad bazą
     * @param creator komponent używany do zrzutu faktury do pliku
     */
    public FakturaSprzedazy(IBaza bazaDanych, ICreator creator) {
        this.bazaDanych = bazaDanych;
        this.creator = creator;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String dodajFaktureSprzedazy(FakturaSprzedazyModel nowaFaktura) {
        //generacja ID na podstawie wystawionych w danym dniu faktur oraz daty wystawienia faktry
        String noweId = GenerujFakturaID(nowaFaktura.getFakuraDataWystawienie());
        if (noweId == null) {
            return "Błąd bazy! Skonsultuj się z dostawcą oprogramowania!";
        } else {
            nowaFaktura.setFakturaID(noweId);
        }

        //usuwanie danej ilosci towaru z magazynu
        if (!AktualizujIloscTowarow(nowaFaktura.getFakturaTowary())) {
            return "Błąd bazy! Skonsultuj się z dostawcą oprogramowania!";
        }

        //dodawanie faktury do bazy danych
        boolean wynikOperacji = bazaDanych.dodajObiekt("BazaFakturSprzedazy.yap", true, nowaFaktura);
        if (wynikOperacji) {
            return "Faktura sprzedazy została pomyślnie dodana do bazy danych.";
        } else {
            return "Błąd bazy! Skonsultuj się z dostawcą oprogramowania!";
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String dodajKontrahentaDoFaktury(FakturaSprzedazyModel nowaFaktura, KontrahentModel kontrahent) {
        nowaFaktura.setKontrahentID(kontrahent.getKontrahentID());
        return "Kontrahent został pomyślnie dodany";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String dodajTowarDoFaktury(FakturaSprzedazyModel nowaFaktura, TowarModel towar, double ilosc) {
        for (TowarModel towarZFaktury : nowaFaktura.getFakturaTowary()) {
            if (towarZFaktury.getTowarID() == towar.getTowarID() && ilosc + towarZFaktury.getTowarIlosc() <= towar.getTowarIlosc()) {
                towarZFaktury.setTowarIlosc(towarZFaktury.getTowarIlosc() + ilosc);
                double cenaZRabatem = towarZFaktury.getTowarCenaNetto() - towarZFaktury.getTowarCenaNetto() * towarZFaktury.getTowarRabat();
                double cenaZVATem = nowaFaktura.getFakturaKwota() + ilosc * (cenaZRabatem + cenaZRabatem * towarZFaktury.getTowarVAT());
                double cena = Math.round(cenaZVATem * 100.0) / 100.0;
                nowaFaktura.setFakturaKwota(cena);
                return "Liczebność towaru została zaktualizowana.";
            } else {
                return "Błąd! Nie można dodać określonej ilości towaru.";
            }
        }

        if (ilosc <= towar.getTowarIlosc()) {
            List<TowarModel> listaTowarow = nowaFaktura.getFakturaTowary();
            listaTowarow.add(new TowarModel(towar, ilosc));
            nowaFaktura.setFakturaTowary(listaTowarow);

            double cenaZRabatem = towar.getTowarCenaNetto() - towar.getTowarCenaNetto() * towar.getTowarRabat();
            double cenaZVATem = nowaFaktura.getFakturaKwota() + ilosc * (cenaZRabatem + cenaZRabatem * towar.getTowarVAT());
            double cena = Math.round(cenaZVATem * 100.0) / 100.0;
            nowaFaktura.setFakturaKwota(cena);
            return "Towar został pomyślnie dodany";
        }
        return "Błąd! Nie można dodać określonej ilości towaru.";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String usunTowarZFaktury(FakturaSprzedazyModel nowaFaktura, TowarModel towar) {
        for (TowarModel towarZFaktury : nowaFaktura.getFakturaTowary()) {
            if (towarZFaktury.getTowarID() == towar.getTowarID()) {
                double cenaZRabatem = towarZFaktury.getTowarCenaNetto() - towarZFaktury.getTowarCenaNetto() * towarZFaktury.getTowarRabat();
                double nowaKwota = nowaFaktura.getFakturaKwota() - towarZFaktury.getTowarIlosc() * (cenaZRabatem + cenaZRabatem * towar.getTowarVAT());
                double cena = Math.round(nowaKwota * 100.0) / 100.0;
                nowaFaktura.setFakturaKwota(cena);
                nowaFaktura.getFakturaTowary().remove(towarZFaktury);
                return "Towar został pomyślnie usunięty";
            }
        }
        return "Błąd! Nie znaleziono towaru.";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void doPlikuFaktureSprzedazy(FakturaSprzedazyModel fakturaDoPliku, String sciezkaDoPliku) {
        FakturaSprzedazyModel faktura = bazaDanych.znajdzObiekt("BazaFakturSprzedazy.yap", true, new FakturaSprzedazyModel(fakturaDoPliku)).get(0);
        KontrahentModel kontrahent = bazaDanych.znajdzObiekt("BazaKontrahentow.yap", true, new KontrahentModel(fakturaDoPliku.getKontrahentID())).get(0);
        PrzedsiebiorstwoModel przedsiebiorstwo;
        
        List<PrzedsiebiorstwoModel> lista = bazaDanych.znajdzObiekt("DanePrzedsiebiorstwa.yap", false, new PrzedsiebiorstwoModel());
        
        if (lista == null || lista.isEmpty()) {
            przedsiebiorstwo = new PrzedsiebiorstwoModel();
        }
        else {
            przedsiebiorstwo = lista.get(0);
        }

        creator.setDir(sciezkaDoPliku);
        creator.setTitle("Faktura sprzedazy");
        creator.setLanguage("pl");
        creator.addIdentifier("faktura_sprzedazy", faktura.getFakturaID());

        String style = "@charset \"utf-8\";\n"
                + "body { width: 100%; }\n"
                + ".pp { margin: 20px; }\n"
                + "table { margin-bottom:50px; }\n"
                + ".ppp { font-size:22px; margin: 20px; }";

        creator.setStyle(style);

        String nIP = "";
        if (kontrahent.getKontrahentNazwaFirmy() == null) {
            nIP = "";
        } else {
            nIP = kontrahent.getKontrahentNIP();
        }
        
        String html = "<html>\n"
                + "	<head>\n"
                + "		<link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\" />\n"
                + "	</head>\n"
                + "	<body>\n"
                + "    <table width=\"800px\" border=\"1\">\n"
                + "		<tr>\n"
                + "			<td width=\"400px\" height=\"150px\" colspan=\"2\" valign=\"bottom\" align=\"center\">Pieczęć sprzedawcy</td>\n"
                + "                     <td width=\"400px\" colspan=\"2\"><p class=\"pp\"><b>FAKTURA SPRZEDAŻY</b></br>\n"
                + "        		" + faktura.getFakturaID() + "</br></br>\n"
                + "        		Data wystawienia: " + new SimpleDateFormat("dd-MM-yyyy").format(faktura.getFakuraDataZaplata()) + "</br>\n"
                + "                     Data zapłaty: " + new SimpleDateFormat("dd-MM-yyyy").format(faktura.getFakuraDataWystawienie()) + "</p></td>\n"
                + "		</tr>\n"
                + "             <tr>\n"
                + "			<td width=\"400px\" colspan=\"2\"><p class=\"pp\"><b>SPRZEDAWCA</b></br>\n"
                + "        		" + przedsiebiorstwo.getPrzedsiebiorstwoNazwa() + "</br>\n"
                + "        		Adres: " + przedsiebiorstwo.getPrzedsiebiorstwoAdres() + "</br></br>\n"
                + "        		NIP: " + przedsiebiorstwo.getPrzedsiebiorstwoNIP() + "</p></td>\n"
                + "			\n"
                + "            <td width=\"400px\" colspan=\"2\"><p class=\"pp\"><b>NABYWCA</b></br>\n"
                + "        		" + kontrahent.getKontrahentImie() + " " + kontrahent.getKontrahentNazwisko() + "</br>\n"
                + "        		Adres: " + kontrahent.getKontrahentAdres() + "</br></br>\n"
                + "        		NIP: " + nIP + "</p></td>\n"
                + "		</tr>\n"
                + "	</table>\n"
                + " \n"
                + "	<table width=\"800px\" border=\"1\">\n"
                + "		<tr>\n"
                + "			<td width=\"300px\" valign=\"middle\" align=\"center\"><b>Nazwa towaru</b></td>\n"
                + "            <td width=\"100px\" valign=\"middle\" align=\"center\"><b>Ilosć</b></td>\n"
                + "            <td  width=\"150px\" valign=\"middle\" align=\"center\"><b>Cena netto</b></td>\n"
                + "            <td width=\"50px\"valign=\"middle\" align=\"center\"><b>VAT</b></td>\n"
                + "            <td width=\"50px\" valign=\"middle\" align=\"center\"><b>Rabat</b></td>\n"
                + "            <td width=\"150px\" valign=\"middle\" align=\"center\"><b>Cena brutto</b></td>\n"
                + "		</tr>\n";

        for (TowarModel towar : faktura.getFakturaTowary()) {

            double cenaZRabatem = towar.getTowarCenaNetto() - towar.getTowarCenaNetto() * towar.getTowarRabat();
            double cenaZVATem = towar.getTowarIlosc() * (cenaZRabatem + cenaZRabatem * towar.getTowarVAT());
            double cena = Math.round(cenaZVATem * 100.0) / 100.0;

            html += "       <tr>\n"
                    + "            <td width=\"300px\" valign=\"middle\" align=\"center\">" + towar.getTowarnNazwa() + "</td>\n"
                    + "            <td width=\"100px\" valign=\"middle\" align=\"center\">" + Math.round(towar.getTowarIlosc() * 100.0) / 100.0 + "</td>\n"
                    + "            <td  width=\"150px\" valign=\"middle\" align=\"center\">" + Math.round((towar.getTowarIlosc() * towar.getTowarCenaNetto()) * 100.0) / 100.0 + "</td>\n"
                    + "            <td width=\"50px\"valign=\"middle\" align=\"center\">" + Math.round(towar.getTowarVAT() * 100.0) / 100.0 + "</td>\n"
                    + "            <td width=\"50px\" valign=\"middle\" align=\"center\">" + Math.round(towar.getTowarRabat() * 100.0) / 100.0 + "</td>\n"
                    + "            <td width=\"150px\" valign=\"middle\" align=\"center\">" + cena + "</td>\n"
                    + "     </tr>\n";
        }

        html += "       </table>\n"
                + "    \n"
                + " 	<table width=\"500px\" border=\"0\">\n"
                + "		<tr>\n"
                + "			<td width=\"500px\" valign=\"middle\"><p class=\"ppp\"><b>Razem do zapłaty:</b></br>\n"
                + "                     " + Math.round(faktura.getFakturaKwota() * 100.0) / 100.0 + " PLN</p></td>\n"
                + "        </tr>    \n"
                + "	</table>\n"
                + "    \n"
                + "    <table width=\"800px\" border=\"0\">\n"
                + "		<tr>\n"
                + "			<td width=\"400px\" height=\"100px\" valign=\"bottom\" align=\"center\">Podpis sprzedawcy</td>\n"
                + "            <td width=\"400px\" height=\"100px\" valign=\"bottom\" align=\"center\">Podpis nabywcy</td>\n"
                + "		</tr>\n"
                + "	</table>\n"
                + "    \n"
                + "	</body>\n"
                + "</html>";

        creator.addChapter(html);
        
        creator.createEpub();
        creator.clear();

        try {
            Desktop.getDesktop().open(new File(sciezkaDoPliku));
        } catch (IOException e) {
            System.out.print(e);
        }
    }

    @SuppressWarnings("deprecation")
    private String GenerujFakturaID(Date dataWystawieniaFaktury) {
        Date data = new Date(dataWystawieniaFaktury.getYear(), dataWystawieniaFaktury.getMonth(), dataWystawieniaFaktury.getDate());

        List<FakturaSprzedazyModel> listaFaktur = bazaDanych.znajdzObiekt("BazaFakturSprzedazy.yap", true, new FakturaSprzedazyModel(data));
        if (listaFaktur == null) {
            return null;
        }

        String noweId = listaFaktur.size() + "/" + data.getDate() + "/" + (data.getMonth() + 1) + "/" + (data.getYear() + 1900);
        return noweId;
    }

    private boolean AktualizujIloscTowarow(List<TowarModel> listaTowarow) {
        for (TowarModel towar : listaTowarow) {
            List<TowarModel> lista = bazaDanych.znajdzObiekt("BazaTowarow.yap", false, new TowarModel(towar.getTowarID()));
            if (lista == null) {
                return false;
            }
            TowarModel towarDoZmianyIlosci = lista.get(0);

            towarDoZmianyIlosci.setTowarIlosc(towarDoZmianyIlosci.getTowarIlosc() - towar.getTowarIlosc());

            boolean wynikOperacji = bazaDanych.edytujObiekt("BazaTowarow.yap", false, new TowarModel(towar.getTowarID()), towarDoZmianyIlosci);
            if (!wynikOperacji) {
                return false;
            }
        }
        return true;
    }
}
