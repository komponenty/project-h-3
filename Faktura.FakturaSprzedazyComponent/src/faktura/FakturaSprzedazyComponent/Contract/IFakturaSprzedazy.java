package faktura.FakturaSprzedazyComponent.Contract;

import magazyn.ListaTowarowComponent.Model.TowarModel;
import faktura.FakturaSprzedazyComponent.Model.FakturaSprzedazyModel;
import faktura.ListaKontrahentowComponent.Model.KontrahentModel;

/**
 * @author Katarzyna Romanowska
 * @version 1.0.1.0
 */
public interface IFakturaSprzedazy {

    /**
     * Dodaje nową fakture sprzedaży do bazy danych.
     * <p>
     * Metoda przydziela także każdej fakturze unikalne ID oparte o date i ilość wystawionych w danym dniu faktur.
     * <br>
     * Użytkownik aplikacji musi przydzielić konkretne daty do obiektu.
     * @param nowaFaktura faktura do dodania 
     * @return "Faktura sprzedazy została pomyślnie dodana do bazy danych." - w przypadku pomyślnego dodania do bazy
     *          <br>
     *         "Błąd bazy! Skonsultuj się z dostawcą oprogramowania!" - w przypadku błędu krytycznego
     */
    String dodajFaktureSprzedazy(FakturaSprzedazyModel nowaFaktura);

    /**
     * Dodaje wybranego kontrahenta do danej faktury sprzedaży.
     * @param nowaFaktura faktura sprzedaży, do której dodajemy kontrahenta
     * @param kontrahent kontrahent, którego przypisujemy do faktury sprzedaży
     * @return "Kontrahent został pomyślnie dodany" - w przypadku pomyślnego dodania kontrahenta
     */
    String dodajKontrahentaDoFaktury(FakturaSprzedazyModel nowaFaktura, KontrahentModel kontrahent);

    /**
     * Dodaje wybrany towar oraz jego ilość do danej faktury sprzedaży.
     * <p>
     * Metoda aktualizuje również aktualną wartość faktury.
     * <br>
     * Ilość zawarta w towarze musi być zgodna z aktualną ilością dostępną w magazynie.
     * @param nowaFaktura faktura sprzedaży, do której chcemy dodać towar
     * @param towar towar, który przypisujemy do faktury
     * @param ilosc ilość towaru, który chcemi przypisać do faktury
     * @return "Towar został pomyślnie dodany" - w przypadku pomyślnego dodania towaru do faktury sprzedaży.
     *         <br>
     *         "Liczebność towaru została zaktualizowana." - w przypadku zaktualizowania ilości towaru już znajdującego się w fakturze sprzedaży
     *         <br>
     *         "Błąd! Nie można dodać określonej ilości towaru." - w przypadku, gdy dodanie określonej ilości towaru jest niemożliwe
     */
    String dodajTowarDoFaktury(FakturaSprzedazyModel nowaFaktura, TowarModel towar, double ilosc);

    /**
     * Usuwa wybrany towar z danej faktury sprzedaży.
     * <p>
     * Metoda aktualizuje również aktualną wartość faktury.
     * @param nowaFaktura faktura sprzedaży, z której chcemy usunąc towar
     * @param towar towar, który chcemy usunąć
     * @return "Towar został pomyślnie usunięty" - w przypadku pomyślnego usunięcia towaru z faktury sprzedaży.
     *         <br>
     *         "Błąd! Nie znaleziono towaru." - w przypadku nie znalezienia wybranego towaru na fakturze sprzedaży
     */
    String usunTowarZFaktury(FakturaSprzedazyModel nowaFaktura, TowarModel towar);

    /**
     * Zrzuca daną fakturę sprzedaży do pliku w formacie EPUB
     * @param fakturaDoPliku faktura sprzedaży do zrzutu do pliku
     * @param sciezkaDoPliku scieżka gdzie ma być zapisana faktura sprzedaży
     */
    void doPlikuFaktureSprzedazy(FakturaSprzedazyModel fakturaDoPliku, String sciezkaDoPliku);
}
