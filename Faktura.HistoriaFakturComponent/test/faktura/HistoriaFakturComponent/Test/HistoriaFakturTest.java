package faktura.HistoriaFakturComponent.Test;

import faktura.FakturaSprzedazyComponent.Contract.IFakturaSprzedazy;
import faktura.FakturaSprzedazyComponent.Implementation.FakturaSprzedazy;
import faktura.FakturaSprzedazyComponent.Model.FakturaSprzedazyModel;
import faktura.FakturaZakupuComponent.Contract.IFakturaZakupu;
import faktura.FakturaZakupuComponent.Implementation.FakturaZakupu;
import faktura.FakturaZakupuComponent.Model.FakturaZakupuModel;
import faktura.HistoriaFakturComponent.Contract.IHistoriaFaktur;
import faktura.HistoriaFakturComponent.Implementation.HistoriaFaktur;
import faktura.ListaKontrahentowComponent.Model.KontrahentModel;
import glowne.BazaDanychComponent.Implementation.BazaDanych;
import test.resetBazy.ResetBazy;
import java.util.Date;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;
import pk.epub.Creator.Creator;

/**
 * @author Katarzyna Romanowska
 * @version 1.0.1.1
 */
public class HistoriaFakturTest {

    private static IHistoriaFaktur historiaFaktur = new HistoriaFaktur(new BazaDanych());

    @SuppressWarnings("deprecation")
    private static String GenerujBazeFakturSprzedazy() {
        ResetBazy.usunBaze("BazaFakturSprzedazy.yap", true);
        String wynik = "Faktura sprzedazy została pomyślnie dodana do bazy danych.";
        IFakturaSprzedazy fakturaSprzedazy = new FakturaSprzedazy(new BazaDanych(), new Creator());

        FakturaSprzedazyModel faktura1 = new FakturaSprzedazyModel(new Date(2013, 1, 1), new Date(2013, 1, 1), "Janusz", "Kwiatkowski", 1);
        fakturaSprzedazy.dodajKontrahentaDoFaktury(faktura1, new KontrahentModel(1));
        wynik = fakturaSprzedazy.dodajFaktureSprzedazy(faktura1);

        FakturaSprzedazyModel faktura2 = new FakturaSprzedazyModel(new Date(2013, 1, 1), new Date(2013, 1, 1), "Tadeusz", "Kowalski", 2);
        fakturaSprzedazy.dodajKontrahentaDoFaktury(faktura2, new KontrahentModel(2));
        wynik = fakturaSprzedazy.dodajFaktureSprzedazy(faktura2);

        FakturaSprzedazyModel faktura3 = new FakturaSprzedazyModel(new Date(2013, 1, 3), new Date(2013, 1, 3), "Roman", "Kopytko", 3);
        fakturaSprzedazy.dodajKontrahentaDoFaktury(faktura3, new KontrahentModel(1));
        wynik = fakturaSprzedazy.dodajFaktureSprzedazy(faktura3);

        FakturaSprzedazyModel faktura4 = new FakturaSprzedazyModel(new Date(2013, 4, 3), new Date(2013, 4, 3), "Kapitan", "Kirk", 4);
        fakturaSprzedazy.dodajKontrahentaDoFaktury(faktura4, new KontrahentModel(3));
        wynik = fakturaSprzedazy.dodajFaktureSprzedazy(faktura4);

        return wynik;
    }

    @SuppressWarnings("deprecation")
    private static String GenerujBazeFakturZakupu() {
        ResetBazy.usunBaze("BazaFakturZakupu.yap", true);
        String wynik = "Faktura zakupu została pomyślnie dodana do bazy danych.";
        IFakturaZakupu fakturaZakupu = new FakturaZakupu(new BazaDanych(), new Creator());

        FakturaZakupuModel faktura1 = new FakturaZakupuModel("1", new Date(2013, 1, 1), new Date(2013, 1, 1), "Janusz", "Kwiatkowski", 1);
        fakturaZakupu.dodajKontrahenta(faktura1, new KontrahentModel(1));
        wynik = fakturaZakupu.dodajFaktureZakupu(faktura1);

        FakturaZakupuModel faktura2 = new FakturaZakupuModel("2", new Date(2013, 1, 1), new Date(2013, 1, 1), "Tadeusz", "Kowalski", 2);
        fakturaZakupu.dodajKontrahenta(faktura2, new KontrahentModel(2));
        wynik = fakturaZakupu.dodajFaktureZakupu(faktura2);

        FakturaZakupuModel faktura3 = new FakturaZakupuModel("3", new Date(2013, 1, 3), new Date(2013, 1, 3), "Roman", "Kopytko", 3);
        fakturaZakupu.dodajKontrahenta(faktura3, new KontrahentModel(1));
        wynik = fakturaZakupu.dodajFaktureZakupu(faktura3);

        FakturaZakupuModel faktura4 = new FakturaZakupuModel("4", new Date(2013, 4, 3), new Date(2013, 4, 3), "Kapitan", "Kirk", 4);
        fakturaZakupu.dodajKontrahenta(faktura4, new KontrahentModel(3));
        wynik = fakturaZakupu.dodajFaktureZakupu(faktura4);
        return wynik;
    }

    @SuppressWarnings("deprecation")
    @Test
    public void testFakturaSprzedazy() {
        GenerujBazeFakturSprzedazy();
        List<FakturaSprzedazyModel> lista1 = historiaFaktur.znajdzFaktureSprzedazyPoDacie(new Date(2013, 1, 1), new Date(2013, 1, 3));
        Assert.assertEquals(3, lista1.size());
        Assert.assertEquals("Janusz", lista1.get(0).getFakturaOsobaWystawiajacaImie());
        Assert.assertEquals("Tadeusz", lista1.get(1).getFakturaOsobaWystawiajacaImie());
        Assert.assertEquals("Roman", lista1.get(2).getFakturaOsobaWystawiajacaImie());

        List<FakturaSprzedazyModel> lista2 = historiaFaktur.znajdzFaktureSprzedazyPoKontrahencie(1);
        Assert.assertEquals(2, lista2.size());
        Assert.assertEquals("Janusz", lista2.get(0).getFakturaOsobaWystawiajacaImie());
        Assert.assertEquals("Roman", lista2.get(1).getFakturaOsobaWystawiajacaImie());

        List<FakturaSprzedazyModel> lista3 = historiaFaktur.znajdzFaktureSprzedazyPoNumerzeFaktury("1/1/2/2013");
        Assert.assertEquals(1, lista3.size());
        Assert.assertEquals("Tadeusz", lista3.get(0).getFakturaOsobaWystawiajacaImie());
    }

    @SuppressWarnings("deprecation")
    @Test
    public void testFakturaZakupu() {
        GenerujBazeFakturZakupu();
        List<FakturaZakupuModel> lista1 = historiaFaktur.znajdzFaktureZakupuPoDacie(new Date(2013, 1, 1), new Date(2013, 1, 3));
        Assert.assertEquals(3, lista1.size());
        Assert.assertEquals("Janusz", lista1.get(0).getFakturaOsobaPrzyjmujacaImie());
        Assert.assertEquals("Tadeusz", lista1.get(1).getFakturaOsobaPrzyjmujacaImie());
        Assert.assertEquals("Roman", lista1.get(2).getFakturaOsobaPrzyjmujacaImie());

        List<FakturaZakupuModel> lista2 = historiaFaktur.znajdzFaktureZakupuPoKontrahencie(1);
        Assert.assertEquals(2, lista2.size());
        Assert.assertEquals("Janusz", lista2.get(0).getFakturaOsobaPrzyjmujacaImie());
        Assert.assertEquals("Roman", lista2.get(1).getFakturaOsobaPrzyjmujacaImie());

        List<FakturaZakupuModel> lista3 = historiaFaktur.znajdzFaktureZakupuPoNumerzeFaktury("1");
        Assert.assertEquals(1, lista3.size());
        Assert.assertEquals("Janusz", lista3.get(0).getFakturaOsobaPrzyjmujacaImie());
    }
}
