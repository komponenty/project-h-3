package faktura.HistoriaFakturComponent.Implementation;

import java.util.Date;
import java.util.List;
import faktura.FakturaSprzedazyComponent.Model.FakturaSprzedazyModel;
import faktura.FakturaZakupuComponent.Model.FakturaZakupuModel;
import faktura.HistoriaFakturComponent.Contract.IHistoriaFaktur;
import faktura.HistoriaFakturComponent.Predicate.FakturaSprzedazyPoDaciePredicate;
import faktura.HistoriaFakturComponent.Predicate.FakturaZakupuPoDaciePredicate;
import glowne.BazaDanychComponent.Contract.IBaza;

/**
 * @author Katarzyna Romanowska
 * @version 1.0.2.0
 */
public class HistoriaFaktur implements IHistoriaFaktur {

    private IBaza bazaDanych;

    /**
     * Konstruktor zapewniajacy podstawowe funkcjonalnosci komponentu
     * @param bazaDanych repozytorium używane do przeprowadzania operacji nad bazą
     */
    public HistoriaFaktur(IBaza bazaDanych) {
        this.bazaDanych = bazaDanych;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<FakturaSprzedazyModel> znajdzFaktureSprzedazyPoDacie(Date dataOd, Date dataDo) {
        return bazaDanych.znajdzObiektPredicate("BazaFakturSprzedazy.yap", true, new FakturaSprzedazyPoDaciePredicate(dataOd, dataDo));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<FakturaSprzedazyModel> znajdzFaktureSprzedazyPoNumerzeFaktury(String numerFaktury) {
        return bazaDanych.znajdzObiekt("BazaFakturSprzedazy.yap", true, new FakturaSprzedazyModel(numerFaktury));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<FakturaSprzedazyModel> znajdzFaktureSprzedazyPoKontrahencie(int kontrahentID) {
        return bazaDanych.znajdzObiekt("BazaFakturSprzedazy.yap", true, new FakturaSprzedazyModel(kontrahentID));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<FakturaZakupuModel> znajdzFaktureZakupuPoDacie(Date dataOd, Date dataDo) {
        return bazaDanych.znajdzObiektPredicate("BazaFakturZakupu.yap", true, new FakturaZakupuPoDaciePredicate(dataOd, dataDo));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<FakturaZakupuModel> znajdzFaktureZakupuPoNumerzeFaktury(String numerFaktury) {
        return bazaDanych.znajdzObiekt("BazaFakturZakupu.yap", true, new FakturaZakupuModel(numerFaktury));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<FakturaZakupuModel> znajdzFaktureZakupuPoKontrahencie(int kontrahentID) {
        return bazaDanych.znajdzObiekt("BazaFakturZakupu.yap", true, new FakturaZakupuModel(kontrahentID));
    }
}
