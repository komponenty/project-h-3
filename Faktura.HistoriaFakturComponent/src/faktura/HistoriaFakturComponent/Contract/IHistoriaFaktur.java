package faktura.HistoriaFakturComponent.Contract;

import java.util.Date;
import java.util.List;
import faktura.FakturaSprzedazyComponent.Model.FakturaSprzedazyModel;
import faktura.FakturaZakupuComponent.Model.FakturaZakupuModel;

/**
 * @author Katarzyna Romanowska
 * @version 1.0.2.0
 */
public interface IHistoriaFaktur {

    /**
     * Znajduje faktury sprzedaży wystawione w danym okresie czasu
     * @param dataOd możliwie najmniejsza data wystawienia faktury sprzedaży
     * @param dataDo możliwie największa data wystawienia faktury sprzedaży
     * @return List<FakturaSprzedazyModel> - w przypadku znalezienia lub nie (dlugosc listy = 0) rekordów spełniających dane kryterium
     *         <br>
     *         null -  w przypadku błędu krytycznego
     */
    List<FakturaSprzedazyModel> znajdzFaktureSprzedazyPoDacie(Date dataOd, Date dataDo);

    /**
     * Znajduje faktury sprzedaży o określonym ID
     * @param numerFaktury numer identyfikacyjny faktury sprzedaży
     * @return List<FakturaSprzedazyModel> - w przypadku znalezienia lub nie (dlugosc listy = 0) rekordów spełniających dane kryterium
     *         <br>
     *         null -  w przypadku błędu krytycznego
     */
    List<FakturaSprzedazyModel> znajdzFaktureSprzedazyPoNumerzeFaktury(String numerFaktury);

    /**
     * Znajduje faktury sprzedaży wystawione na danego kontrahenta
     * @param kontrahentID numer identyfikacyjny kontrahenta, na którego wystawione są faktury
     * @return List<FakturaSprzedazyModel> - w przypadku znalezienia lub nie (dlugosc listy = 0) rekordów spełniających dane kryterium
     *         <br>
     *         null -  w przypadku błędu krytycznego
     */
    List<FakturaSprzedazyModel> znajdzFaktureSprzedazyPoKontrahencie(int kontrahentID);

    /**
     * Znajduje faktury zakupu wystawione w danym okresie czasu
     * @param dataOd możliwie najmniejsza data wystawienia faktury zakupu
     * @param dataDo możliwie największa data wystawienia faktury zakupu
     * @return List<FakturaSprzedazyModel> - w przypadku znalezienia lub nie (dlugosc listy = 0) rekordów spełniających dane kryterium
     *         <br>
     *         null -  w przypadku błędu krytycznego
     */
    List<FakturaZakupuModel> znajdzFaktureZakupuPoDacie(Date dataOd, Date dataDo);

    /**
     * Znajduje faktury zakupu o określonym ID
     * @param numerFaktury numer identyfikacyjny faktury zakupu
     * @return List<FakturaSprzedazyModel> - w przypadku znalezienia lub nie (dlugosc listy = 0) rekordów spełniających dane kryterium
     *         <br>
     *         null -  w przypadku błędu krytycznego
     */
    List<FakturaZakupuModel> znajdzFaktureZakupuPoNumerzeFaktury(String numerFaktury);

    /**
     * Znajduje faktury zakupu wystawione przez danego kontrahenta
     * @param kontrahentID numer identyfikacyjny kontrahenta, który wystawił faktury
     * @return List<FakturaSprzedazyModel> - w przypadku znalezienia lub nie (dlugosc listy = 0) rekordów spełniających dane kryterium
     *         <br>
     *         null -  w przypadku błędu krytycznego
     */
    List<FakturaZakupuModel> znajdzFaktureZakupuPoKontrahencie(int kontrahentID);
}
