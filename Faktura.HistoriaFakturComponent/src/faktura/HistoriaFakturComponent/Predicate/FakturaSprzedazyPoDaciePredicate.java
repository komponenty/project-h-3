package faktura.HistoriaFakturComponent.Predicate;

import java.util.Date;
import com.db4o.query.Predicate;
import faktura.FakturaSprzedazyComponent.Model.FakturaSprzedazyModel;

/**
 * @author Katarzyna Romanowska
 * @version 1.0.1.0
 */
public class FakturaSprzedazyPoDaciePredicate extends Predicate<FakturaSprzedazyModel> {

    Date dataOd;
    Date dataDo;

    /** 
     * @param dataOd możliwie najmniejsza data wystawienia faktury sprzedaży
     * @param dataDo możliwie największa data wystawienia faktury sprzedaży
     */
    public FakturaSprzedazyPoDaciePredicate(Date dataOd, Date dataDo) {
        this.dataOd = dataOd;
        this.dataDo = dataDo;
    }

    /**
     * Metoda sprawdza czy dana faktura sprzedaży odpowiada kryteriom
     * @param faktura sprawdzana faktura sprzedaży
     * @return true - faktura sprzedaży jest zgodna z kryteriami
     *         <br>
     *         false - faktura sprzedaży jest niezgodna z kryteriami
     */
    @Override
    public boolean match(FakturaSprzedazyModel faktura) {
        return faktura.getFakuraDataWystawienie().compareTo(dataOd) >= 0
                && faktura.getFakuraDataWystawienie().compareTo(dataDo) <= 0;
    }
}
