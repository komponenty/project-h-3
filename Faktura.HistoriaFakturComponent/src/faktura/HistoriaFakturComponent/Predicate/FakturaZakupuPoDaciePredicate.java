package faktura.HistoriaFakturComponent.Predicate;

import java.util.Date;
import com.db4o.query.Predicate;
import faktura.FakturaZakupuComponent.Model.FakturaZakupuModel;

/**
 * @author Katarzyna Romanowska
 * @version 1.0.1.0
 */
public class FakturaZakupuPoDaciePredicate extends Predicate<FakturaZakupuModel> {

    Date dataOd;
    Date dataDo;

    /** 
     * @param dataOd możliwie najmniejsza data wystawienia faktury zakupu
     * @param dataDo możliwie największa data wystawienia faktury zakupu
     */
    public FakturaZakupuPoDaciePredicate(Date dataOd, Date dataDo) {
        this.dataOd = dataOd;
        this.dataDo = dataDo;
    }

    /**
     * Metoda sprawdza czy dana faktura zakupu odpowiada kryteriom
     * @param faktura sprawdzana faktura zakupu
     * @return true - faktura zakupu jest zgodna z kryteriami
     *         <br>
     *         false - faktura zakupu jest niezgodna z kryteriami
     */
    @Override
    public boolean match(FakturaZakupuModel faktura) {
        return faktura.getFakuraDataWystawienie().compareTo(dataOd) >= 0
                && faktura.getFakuraDataWystawienie().compareTo(dataDo) <= 0;
    }
}
