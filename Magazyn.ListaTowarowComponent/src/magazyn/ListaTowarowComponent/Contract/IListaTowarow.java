package magazyn.ListaTowarowComponent.Contract;

import java.util.List;
import magazyn.ListaTowarowComponent.Model.TowarModel;

/**
 * @author Jakub Dydo
 * @version 1.0.1.1
 */
public interface IListaTowarow {

    /**
     * Znajduje towary odpowiadajacych danemu szablonowi
     * @param szablon szablon towaru
     * @return List<TowarModel> - w przypadku znalezienia lub nie (dlugosc listy = 0) rekordów odpowiadających danemu szablonowi
     *         <br>
     *         null -  w przypadku błędu krytycznego
     */
    List<TowarModel> znajdzTowar(TowarModel szablon);

    /**
     * Dodaje nowy towar do bazy danych
     * @param nowyTowar towar do dodania
     * @return "Towar został pomyślnie dodany do bazy danych." - w przypadku pomyślnego dodania do bazy
     *          <br>
     *         "Błąd bazy! Skonsultuj się z dostawcą oprogramowania!" - w przypadku błędu krytycznego
     */
    String dodajTowar(TowarModel nowyTowar);

    /**
     * Aktualizuje dany towar w bazie danych
     * @param szablon towar przed edycją (stary)
     * @param towarWyedytowany towar po edycji (nowy)
     * @return "Towar został pomyślnie zaktualizowany." - w przypadku pomyślnej aktualizacji rekordu w bazie
     *          <br>
     *         "Błąd bazy! Skonsultuj się z dostawcą oprogramowania!" - w przypadku błędu krytycznego
     */
    String edytujTowar(TowarModel szablon, TowarModel towarWyedytowany);

    /**
     * Usuwa dany towar z bazy danych
     * @param szablon towar do usunięcia
     * @return "Towar został pomyślnie usunięty z bazy danych." - w przypadku pomyślnego usunięcia rekordu z bazy
     *          <br>
     *         "Błąd bazy! Skonsultuj się z dostawcą oprogramowania!" - w przypadku błędu krytycznego
     */
    String usunTowar(TowarModel szablon);

    /**
     * Zmienia wartość podatku VAT dla towarów zgodnych z szablonem
     * @param listaTowarow lista towarów do aktualizacji
     * @param wartoscVAT nowa wartość podatku VAT
     * @return "Wartość VAT została pomyślnie zmieniona dla danych produktów." - w przypadku pomyślnego aktualizacji rekordu z bazy
     *          <br>
     *         "Nie znaleziono danych towarów w bazie danych." - w przypadku nie znalezienia danego rekordu w bazie
     *          <br>
     *         "Błąd bazy! Skonsultuj się z dostawcą oprogramowania!" - w przypadku błędu krytycznego
     */
    String zmienVAT(List<TowarModel> listaTowarow, double wartoscVAT);

    /**
     * Zmienia wartość rabatu dla towarów zgodnych z szablonem
     * @param listaTowarow lista towarów do aktualizacji
     * @param wartoscRabatu nowa wartość rabatu
     * @return "Wartość rabatu została pomyślnie zmieniona dla danych produktów." - w przypadku pomyślnego aktualizacji rekordu z bazy
     *          <br>
     *         "Nie znaleziono danych towarów w bazie danych." - w przypadku nie znalezienia danego rekordu w bazie
     *          <br>
     *         "Błąd bazy! Skonsultuj się z dostawcą oprogramowania!" - w przypadku błędu krytycznego
     */
    String zmienRabat(List<TowarModel> listaTowarow, double wartoscRabatu);
}
