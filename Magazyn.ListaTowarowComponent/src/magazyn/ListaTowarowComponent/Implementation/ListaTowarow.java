package magazyn.ListaTowarowComponent.Implementation;

import java.util.List;
import glowne.BazaDanychComponent.Contract.IBaza;
import magazyn.ListaTowarowComponent.Contract.IListaTowarow;
import magazyn.ListaTowarowComponent.Model.TowarModel;

/**
 * @author Jakub Dydo
 * @version 1.0.1.1
 */
public class ListaTowarow implements IListaTowarow {

    private IBaza bazaDanych;

    /**
     * Konstruktor zapewniajacy podstawowe funkcjonalnosci komponentu
     * @param bazaDanych repozytorium używane do przeprowadzania operacji nad bazą
     */
    public ListaTowarow(IBaza bazaDanych) {
        this.bazaDanych = bazaDanych;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<TowarModel> znajdzTowar(TowarModel szablon) {
        return bazaDanych.znajdzObiekt("BazaTowarow.yap", false, szablon);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String dodajTowar(TowarModel nowyTowar) {
        List<TowarModel> listaTowarow = bazaDanych.znajdzObiekt("BazaTowarow.yap", false, new TowarModel(nowyTowar.getTowarID()));
        if (listaTowarow == null) {
            return "Błąd bazy! Skonsultuj się z dostawcą oprogramowania!";
        } else if (listaTowarow.size() > 0) {
            return "Błąd! Towar o podanym numerze identyfikacyjnym już istnieje!";
        } else {
            boolean wynikOperacji = bazaDanych.dodajObiekt("BazaTowarow.yap", false, nowyTowar);
            if (wynikOperacji) {
                return "Towar został pomyślnie dodany do bazy danych.";
            } else {
                return "Błąd bazy! Skonsultuj się z dostawcą oprogramowania!";
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String edytujTowar(TowarModel szablon, TowarModel towarWyedytowany) {
        List<TowarModel> listaTowarow1 = bazaDanych.znajdzObiekt("BazaTowarow.yap", false, new TowarModel(towarWyedytowany.getTowarID()));
        List<TowarModel> listaTowarow2 = bazaDanych.znajdzObiekt("BazaTowarow.yap", false, szablon);

        if (listaTowarow1 == null) {
            return "Błąd bazy! Skonsultuj się z dostawcą oprogramowania!";
        } else if (listaTowarow1.size() > 0 && towarWyedytowany.getTowarID() != listaTowarow2.get(0).getTowarID()) {
            return "Błąd! Towar o podanym numerze identyfikacyjnym już istnieje!";
        } else {
            boolean wynikOperacji = bazaDanych.edytujObiekt("BazaTowarow.yap", false, szablon, towarWyedytowany);
            if (wynikOperacji) {
                return "Towar został pomyślnie zaktualizowany.";
            } else {
                return "Błąd bazy! Skonsultuj się z dostawcą oprogramowania!";
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String usunTowar(TowarModel szablon) {
        boolean wynikOperacji = bazaDanych.usunObiekt("BazaTowarow.yap", false, szablon);
        if (wynikOperacji) {
            return "Towar został pomyślnie usunięty z bazy danych.";
        } else {
            return "Błąd bazy! Skonsultuj się z dostawcą oprogramowania!";
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String zmienVAT(List<TowarModel> listaTowarow, double wartoscVAT) {
        if (listaTowarow == null) {
            return "Błąd bazy! Skonsultuj się z dostawcą oprogramowania!";
        } else if (listaTowarow.size() == 0) {
            return "Nie znaleziono danych towarów w bazie danych.";
        } else {
            boolean wynikOperacji = true;
            for (TowarModel towarModel : listaTowarow) {
                TowarModel towarZBazy = bazaDanych.znajdzObiekt("BazaTowarow.yap", false, towarModel).get(0);
                towarZBazy.setTowarVAT(wartoscVAT);
                wynikOperacji = bazaDanych.edytujObiekt("BazaTowarow.yap", false, towarModel, towarZBazy);
            }
            if (wynikOperacji) {
                return "Wartość VAT została pomyślnie zmieniona dla danych produktów.";
            } else {
                return "Błąd bazy! Skonsultuj się z dostawcą oprogramowania!";
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String zmienRabat(List<TowarModel> listaTowarow, double wartoscRabatu) {
        if (listaTowarow == null) {
            return "Błąd bazy! Skonsultuj się z dostawcą oprogramowania!";
        } else if (listaTowarow.size() == 0) {
            return "Nie znaleziono danych towarów w bazie danych.";
        } else {
            boolean wynikOperacji = true;
            for (TowarModel towarModel : listaTowarow) {
                TowarModel towarZBazy = bazaDanych.znajdzObiekt("BazaTowarow.yap", false, towarModel).get(0);
                towarZBazy.setTowarRabat(wartoscRabatu);
                wynikOperacji = bazaDanych.edytujObiekt("BazaTowarow.yap", false, towarModel, towarZBazy);
            }
            if (wynikOperacji) {
                return "Wartość rabatu została pomyślnie zmieniona dla danych produktów.";
            } else {
                return "Błąd bazy! Skonsultuj się z dostawcą oprogramowania!";
            }
        }
    }
}
