package magazyn.ListaTowarowComponent.Model;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Date;

/**
 * @author Jakub Dydo
 * @version 1.0.1.2
 */
public class TowarModel {

    //<editor-fold defaultstate="collapsed" desc="Pola">
    private long towarID;
    private String towarnNazwa;
    private String towarProducent;
    private double towarIlosc;
    private double towarCenaNetto;
    private double towarVAT;
    private Date towarTerminWaznosci;
    private double towarRabat;
    private String towarRodzaj;
    private boolean towarUzytekWlasny;
    private boolean towarIgnorowanie;
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Wlasciwosci">
    public void setTowarID(long towarID) {
        long oldValue = this.towarID;
        this.towarID = towarID;
        changeSupport.firePropertyChange("towarID", oldValue, towarID);
    }

    public long getTowarID() {
        return this.towarID;
    }

    public void setTowarnNazwa(String towarnNazwa) {
        String oldValue = this.towarnNazwa;
        this.towarnNazwa = towarnNazwa;
        changeSupport.firePropertyChange("towarnNazwa", oldValue, towarnNazwa);
    }

    public String getTowarnNazwa() {
        return this.towarnNazwa;
    }

    public void setTowarProducent(String towarProducent) {
        String oldValue = this.towarProducent;
        this.towarProducent = towarProducent;
        changeSupport.firePropertyChange("towarProducent", oldValue, towarProducent);
    }

    public String getTowarProducent() {
        return this.towarProducent;
    }

    public void setTowarIlosc(double towarIlosc) {
        double oldValue = this.towarIlosc;
        this.towarIlosc = towarIlosc;
        changeSupport.firePropertyChange("towarIlosc", oldValue, towarIlosc);
    }

    public double getTowarIlosc() {
        return this.towarIlosc;
    }

    public void setTowarCenaNetto(double towarCenaNetto) {
        double oldValue = this.towarCenaNetto;
        this.towarCenaNetto = towarCenaNetto;
        changeSupport.firePropertyChange("towarCenaNetto", oldValue, towarCenaNetto);
    }

    public double getTowarCenaNetto() {
        return this.towarCenaNetto;
    }

    public void setTowarVAT(double towarVAT) {
        double oldValue = this.towarVAT;
        this.towarVAT = towarVAT;
        changeSupport.firePropertyChange("towarVAT", oldValue, towarVAT);
    }

    public double getTowarVAT() {
        return this.towarVAT;
    }

    public void setTowarTerminWaznosci(Date towarTerminWaznosci) {
        Date oldValue = this.towarTerminWaznosci;
        this.towarTerminWaznosci = towarTerminWaznosci;
        changeSupport.firePropertyChange("towarTerminWaznosci", oldValue, towarTerminWaznosci);
    }

    public Date getTowarTerminWaznosci() {
        return this.towarTerminWaznosci;
    }

    public void setTowarRabat(double towarRabat) {
        double oldValue = this.towarRabat;
        this.towarRabat = towarRabat;
        changeSupport.firePropertyChange("towarRabat", oldValue, towarRabat);
    }

    public double getTowarRabat() {
        return this.towarRabat;
    }

    public void setTowarRodzaj(String towarRodzaj) {
        String oldValue = this.towarRodzaj;
        this.towarRodzaj = towarRodzaj;
        changeSupport.firePropertyChange("towarRodzaj", oldValue, towarRodzaj);
    }

    public String getTowarRodzaj() {
        return this.towarRodzaj;
    }

    public void setTowarUzytekWlasny(boolean towarUzytekWlasny) {
        boolean oldValue = this.towarUzytekWlasny;
        this.towarUzytekWlasny = towarUzytekWlasny;
        changeSupport.firePropertyChange("towarUzytekWlasny", oldValue, towarUzytekWlasny);
    }

    public boolean getTowarUzytekWlasny() {
        return this.towarUzytekWlasny;
    }

    public void setTowarIgnorowanie(boolean towarIgnorowanie) {
        boolean oldValue = this.towarIgnorowanie;
        this.towarIgnorowanie = towarIgnorowanie;
        changeSupport.firePropertyChange("towarIgnorowanie", oldValue, towarIgnorowanie);
    }

    public boolean getTowarIgnorowanie() {
        return this.towarIgnorowanie;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Konstruktory">
    public TowarModel() {
    }

    public TowarModel(long towarID) {
        this.towarID = towarID;
    }

    public TowarModel(String towarnNazwa) {
        this.towarnNazwa = towarnNazwa;
    }

    public TowarModel(String towarnNazwa, String towarProducent, String towarRodzaj) {
        this(towarnNazwa);
        this.towarProducent = towarProducent;
        this.towarRodzaj = towarRodzaj;
    }

    public TowarModel(double towarIlosc, boolean towarIgnorowanie, boolean uzytekWlasny) {
        this.towarIlosc = towarIlosc;
        this.towarIgnorowanie = towarIgnorowanie;
        this.towarUzytekWlasny = uzytekWlasny;
    }

    public TowarModel(long towarID, String towarnNazwa, String towarProducent, String towarRodzaj) {
        this(towarnNazwa, towarProducent, towarRodzaj);
        this.towarID = towarID;
    }

    public TowarModel(long towarID, String towarnNazwa, String towarProducent, double towarIlosc, double towarCenaNetto, double towarVAT, Date towarTerminWaznosci, double towarRabat, String towarRodzaj, boolean towarUzytekWlasny) {
        this.towarID = towarID;
        this.towarnNazwa = towarnNazwa;
        this.towarProducent = towarProducent;
        this.towarIlosc = towarIlosc;
        this.towarCenaNetto = towarCenaNetto;
        this.towarVAT = towarVAT;
        this.towarTerminWaznosci = towarTerminWaznosci;
        this.towarRabat = towarRabat;
        this.towarRodzaj = towarRodzaj;
        this.towarUzytekWlasny = towarUzytekWlasny;
    }

    public TowarModel(long towarID, String towarnNazwa, String towarProducent, double towarIlosc, double towarCenaNetto, double towarVAT, Date towarTerminWaznosci, String towarPKWiU, double towarRabat, String towarRodzaj, boolean towarUzytekWlasny, boolean towarIgnorowanie) {
        this(towarID, towarnNazwa, towarProducent, towarIlosc, towarCenaNetto, towarVAT, towarTerminWaznosci, towarRabat, towarRodzaj, towarUzytekWlasny);
        this.towarIgnorowanie = towarIgnorowanie;
    }

    public TowarModel(TowarModel towar) {
        this.towarID = towar.towarID;
        this.towarnNazwa = towar.towarnNazwa;
        this.towarProducent = towar.towarProducent;
        this.towarIlosc = towar.towarIlosc;
        this.towarCenaNetto = towar.towarCenaNetto;
        this.towarVAT = towar.towarVAT;
        this.towarTerminWaznosci = towar.towarTerminWaznosci;
        this.towarRabat = towar.towarRabat;
        this.towarRodzaj = towar.towarRodzaj;
        this.towarUzytekWlasny = towar.towarUzytekWlasny;
        this.towarIgnorowanie = towar.towarIgnorowanie;
    }

    public TowarModel(TowarModel towar, double towarIlosc) {
        this(towar);
        this.towarIlosc = towarIlosc;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="PropertyChange">
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="toString method">
    @Override
    public String toString() {
        return this.towarnNazwa + " - " + this.towarProducent + " - " + this.towarID;
    }
    //</editor-fold>
}
