package magazyn.ListaTowarowComponent.Test;

import java.util.Date;
import java.util.List;
import glowne.BazaDanychComponent.Implementation.BazaDanych;
import magazyn.ListaTowarowComponent.Implementation.ListaTowarow;
import magazyn.ListaTowarowComponent.Model.TowarModel;
import test.resetBazy.ResetBazy;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author Jakub Dydo
 * @version 1.0.1.1
 */
public class ListaTowarowTest {

    @Test
    public void testDodajTowar() {
        ResetBazy.usunBaze("BazaTowarow.yap", false);
        BazaDanych baza = new BazaDanych();
        ListaTowarow listaTowarow = new ListaTowarow(baza);
        String wiadomosc1 = listaTowarow.dodajTowar(new TowarModel(1, "nazwa1", "producent1", 11, 1.11, 0.11, new Date(), 1, "Produkt Testowy1", false));
        String wiadomosc2 = listaTowarow.dodajTowar(new TowarModel(1, "nazwa2", "producent2", 22, 2.22, 0.22, new Date(), 2, "Produkt Testowy2", false));
        String wiadomosc3 = listaTowarow.dodajTowar(new TowarModel(3, "nazwa3", "producent3", 33, 3.33, 0.33, new Date(), 3, "Produkt Testowy3", false));
        Assert.assertEquals("Towar został pomyślnie dodany do bazy danych.", wiadomosc1);
        Assert.assertEquals("Błąd! Towar o podanym numerze identyfikacyjnym już istnieje!", wiadomosc2);
        Assert.assertEquals("Towar został pomyślnie dodany do bazy danych.", wiadomosc3);
    }

    @Test
    public void testWyszukajTowar() {
        ResetBazy.usunBaze("BazaTowarow.yap", false);
        BazaDanych baza = new BazaDanych();
        ListaTowarow listaTowarow = new ListaTowarow(baza);
        String wiadomosc1 = listaTowarow.dodajTowar(new TowarModel(2, "nazwa2", "producent2", 22, 2.22, 0.22, new Date(), 2, "Produkt Testowy4", false));
        String wiadomosc2 = listaTowarow.dodajTowar(new TowarModel(4, "nazwa4", "producent4", 44, 4.44, 0.44, new Date(), 4, "Produkt Testowy4", false));
        List<TowarModel> lista = listaTowarow.znajdzTowar(new TowarModel(null, null, "Produkt Testowy4"));
        Assert.assertEquals("Towar został pomyślnie dodany do bazy danych.", wiadomosc1);
        Assert.assertEquals("Towar został pomyślnie dodany do bazy danych.", wiadomosc2);
        Assert.assertNotNull(lista);
        Assert.assertEquals(2, lista.size());
    }

    @Test
    public void testEdytujTowar() {
        ResetBazy.usunBaze("BazaTowarow.yap", false);
        BazaDanych baza = new BazaDanych();
        ListaTowarow listaTowarow = new ListaTowarow(baza);
        String wiadomosc1 = listaTowarow.dodajTowar(new TowarModel(1, "nazwa1", "producent1", 11, 1.11, 0.11, new Date(), 1, "Produkt Testowy1", false));
        String wiadomosc2 = listaTowarow.dodajTowar(new TowarModel(2, "nazwa2", "producent2", 22, 2.22, 0.22, new Date(), 2, "Produkt Testowy4", false));
        String wiadomosc3 = listaTowarow.dodajTowar(new TowarModel(3, "nazwa3", "producent3", 33, 3.33, 0.33, new Date(), 3, "Produkt Testowy3", false));
        String wiadomosc4 = listaTowarow.dodajTowar(new TowarModel(4, "nazwa4", "producent4", 44, 4.44, 0.44, new Date(), 4, "Produkt Testowy4", false));

        String wiadomosc5 = listaTowarow.edytujTowar(new TowarModel(4), new TowarModel(5, "nazwa5", "producent5", 55, 5.55, 0.55, new Date(), 5, "Produkt Testowy5", true));
        String wiadomosc6 = listaTowarow.edytujTowar(new TowarModel(3), new TowarModel(3, "nazwa6", "producent6", 66, 6.66, 0.66, new Date(), 6, "Produkt Testowy6", true));
        String wiadomosc7 = listaTowarow.edytujTowar(new TowarModel(2), new TowarModel(1, "nazwa7", "producent7", 77, 7.7, 0.77, new Date(), 7, "Produkt Testowy7", true));

        Assert.assertEquals("Towar został pomyślnie dodany do bazy danych.", wiadomosc1);
        Assert.assertEquals("Towar został pomyślnie dodany do bazy danych.", wiadomosc2);
        Assert.assertEquals("Towar został pomyślnie dodany do bazy danych.", wiadomosc3);
        Assert.assertEquals("Towar został pomyślnie dodany do bazy danych.", wiadomosc4);

        Assert.assertEquals("Towar został pomyślnie zaktualizowany.", wiadomosc5);
        Assert.assertEquals("Towar został pomyślnie zaktualizowany.", wiadomosc6);
        Assert.assertEquals("Błąd! Towar o podanym numerze identyfikacyjnym już istnieje!", wiadomosc7);
    }

    @Test
    public void testUsunTowar() {
        ResetBazy.usunBaze("BazaTowarow.yap", false);
        BazaDanych baza = new BazaDanych();
        ListaTowarow listaTowarow = new ListaTowarow(baza);
        String wiadomosc1 = listaTowarow.dodajTowar(new TowarModel(1, "nazwa1", "producent1", 11, 1.11, 0.11, new Date(), 1, "Produkt Testowy1", false));
        String wiadomosc2 = listaTowarow.dodajTowar(new TowarModel(2, "nazwa2", "producent2", 22, 2.22, 0.22, new Date(), 2, "Produkt Testowy4", false));
        List<TowarModel> lista1 = listaTowarow.znajdzTowar(new TowarModel());

        String wiadomosc3 = listaTowarow.usunTowar(new TowarModel(1));
        List<TowarModel> lista2 = listaTowarow.znajdzTowar(new TowarModel());

        Assert.assertEquals("Towar został pomyślnie dodany do bazy danych.", wiadomosc1);
        Assert.assertEquals("Towar został pomyślnie dodany do bazy danych.", wiadomosc2);
        Assert.assertEquals("Towar został pomyślnie usunięty z bazy danych.", wiadomosc3);
        Assert.assertEquals(2, lista1.size());
        Assert.assertEquals(1, lista2.size());
    }

    @Test
    public void testZmienVat() {
        ResetBazy.usunBaze("BazaTowarow.yap", false);
        BazaDanych baza = new BazaDanych();
        ListaTowarow listaTowarow = new ListaTowarow(baza);
        String wiadomosc1 = listaTowarow.dodajTowar(new TowarModel(1, "nazwa1", "producent1", 11, 1.11, 0.11, new Date(), 1, "Produkt Testowy1", false));
        String wiadomosc2 = listaTowarow.dodajTowar(new TowarModel(2, "nazwa2", "producent2", 22, 2.22, 0.22, new Date(), 2, "Produkt Testowy4", false));
        String wiadomosc3 = listaTowarow.dodajTowar(new TowarModel(3, "nazwa3", "producent3", 33, 3.33, 0.33, new Date(), 3, "Produkt Testowy4", false));
        String wiadomosc4 = listaTowarow.dodajTowar(new TowarModel(4, "nazwa4", "producent4", 44, 4.44, 0.44, new Date(), 4, "Produkt Testowy4", false));

        String wiadomosc5 = listaTowarow.zmienVAT(listaTowarow.znajdzTowar(new TowarModel(null, null, "Produkt Testowy4")), 0.50);
        List<TowarModel> lista1 = listaTowarow.znajdzTowar(new TowarModel(null, null, "Produkt Testowy4"));
        boolean sprawdzenie1 = true;
        for (TowarModel towarModel : lista1) {
            if (towarModel.getTowarVAT() != 0.50) {
                sprawdzenie1 = false;
            }
        }

        List<TowarModel> lista2 = listaTowarow.znajdzTowar(new TowarModel());
        boolean sprawdzenie2 = true;
        for (TowarModel towarModel : lista2) {
            if (towarModel.getTowarVAT() != 0.50) {
                sprawdzenie2 = false;
            }
        }

        String wiadomosc6 = listaTowarow.zmienVAT(listaTowarow.znajdzTowar(new TowarModel(10)), 0.60);

        Assert.assertEquals("Towar został pomyślnie dodany do bazy danych.", wiadomosc1);
        Assert.assertEquals("Towar został pomyślnie dodany do bazy danych.", wiadomosc2);
        Assert.assertEquals("Towar został pomyślnie dodany do bazy danych.", wiadomosc3);
        Assert.assertEquals("Towar został pomyślnie dodany do bazy danych.", wiadomosc4);
        Assert.assertEquals("Wartość VAT została pomyślnie zmieniona dla danych produktów.", wiadomosc5);
        Assert.assertEquals("Nie znaleziono danych towarów w bazie danych.", wiadomosc6);
        Assert.assertEquals(3, lista1.size());
        Assert.assertEquals(true, sprawdzenie1);
        Assert.assertEquals(false, sprawdzenie2);
    }

    @Test
    public void testZmienRabat() {
        ResetBazy.usunBaze("BazaTowarow.yap", false);
        BazaDanych baza = new BazaDanych();
        ListaTowarow listaTowarow = new ListaTowarow(baza);
        String wiadomosc1 = listaTowarow.dodajTowar(new TowarModel(1, "nazwa1", "producent1", 11, 1.11, 0.11, new Date(), 1, "Produkt Testowy1", false));
        String wiadomosc2 = listaTowarow.dodajTowar(new TowarModel(2, "nazwa2", "producent2", 22, 2.22, 0.22, new Date(), 2, "Produkt Testowy4", false));
        String wiadomosc3 = listaTowarow.dodajTowar(new TowarModel(3, "nazwa3", "producent3", 33, 3.33, 0.33, new Date(), 3, "Produkt Testowy4", false));
        String wiadomosc4 = listaTowarow.dodajTowar(new TowarModel(4, "nazwa4", "producent4", 44, 4.44, 0.44, new Date(), 4, "Produkt Testowy4", false));

        String wiadomosc5 = listaTowarow.zmienRabat(listaTowarow.znajdzTowar(new TowarModel(null, null, "Produkt Testowy4")), 7.0);
        List<TowarModel> lista1 = listaTowarow.znajdzTowar(new TowarModel(null, null, "Produkt Testowy4"));
        boolean sprawdzenie1 = true;
        for (TowarModel towarModel : lista1) {
            if (towarModel.getTowarRabat() != 7.0) {
                sprawdzenie1 = false;
            }
        }

        List<TowarModel> lista2 = listaTowarow.znajdzTowar(new TowarModel());
        boolean sprawdzenie2 = true;
        for (TowarModel towarModel : lista2) {
            if (towarModel.getTowarRabat() != 7.0) {
                sprawdzenie2 = false;
            }
        }

        String wiadomosc6 = listaTowarow.zmienRabat(listaTowarow.znajdzTowar(new TowarModel(10)), 8.0);

        Assert.assertEquals("Towar został pomyślnie dodany do bazy danych.", wiadomosc1);
        Assert.assertEquals("Towar został pomyślnie dodany do bazy danych.", wiadomosc2);
        Assert.assertEquals("Towar został pomyślnie dodany do bazy danych.", wiadomosc3);
        Assert.assertEquals("Towar został pomyślnie dodany do bazy danych.", wiadomosc4);
        Assert.assertEquals("Wartość rabatu została pomyślnie zmieniona dla danych produktów.", wiadomosc5);
        Assert.assertEquals("Nie znaleziono danych towarów w bazie danych.", wiadomosc6);
        Assert.assertEquals(3, lista1.size());
        Assert.assertEquals(true, sprawdzenie1);
        Assert.assertEquals(false, sprawdzenie2);
    }
}
