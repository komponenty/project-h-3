package eRPFacade.Contract;

/**
 * @author Jakub Dydo
 * @version 2.0.0.0
 */
public interface IERPFacade {

    /**
     * Uruchamia system ERP
     */
    void run();
}
