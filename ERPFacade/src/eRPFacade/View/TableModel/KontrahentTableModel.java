package eRPFacade.View.TableModel;

import faktura.ListaKontrahentowComponent.Model.KontrahentModel;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 * @author Jakub Dydo, Katarzyna Romanowska
 * @version 1.0.0.1
 */
public class KontrahentTableModel extends AbstractTableModel {

    // <editor-fold defaultstate="collapsed" desc="Fields">
    private List<KontrahentModel> listaKontrahentow;
    // </editor-fold> 

    // <editor-fold defaultstate="collapsed" desc="Properties">
    public KontrahentModel getKontrahent(int index) {
        if (index < 0) {
            return null;
        } else {
            return new KontrahentModel(this.listaKontrahentow.get(index));
        }
    }

    public void setKontrahent(KontrahentModel kontrahent, int index) {
        if (index >= 0) {
            this.listaKontrahentow.set(index, kontrahent);
            this.fireTableDataChanged();
        }
    }

    public void removeKontrahent(int index) {
        if (index >= 0) {
            this.listaKontrahentow.remove(index);
            this.fireTableDataChanged();
        }
    }

    public List<KontrahentModel> getListaKontrahentow() {
        return this.listaKontrahentow;
    }

    public void setListaKontrahentow(List<KontrahentModel> listaKontrahentow) {
        this.listaKontrahentow = listaKontrahentow;
        this.fireTableDataChanged();
    }
    // </editor-fold> 

    // <editor-fold defaultstate="collapsed" desc="AbstractTableModel Implementation">
    @Override
    public int getRowCount() {
        if (this.listaKontrahentow == null) {
            return 0;
        }
        return this.listaKontrahentow.size();
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 0:
                return "Firma";
            case 1:
                return "NIP";
            case 2:
                return "Imie";
            case 3:
                return "Nazwisko";
            case 4:
                return "Adres";
        }
        return null;
    }

    @Override
    public int getColumnCount() {
        return 5;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        if (this.listaKontrahentow == null) {
            return null;
        }
        KontrahentModel kontrahent = this.listaKontrahentow.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return kontrahent.getKontrahentNazwaFirmy();
            case 1:
                return kontrahent.getKontrahentNIP();
            case 2:
                return kontrahent.getKontrahentImie();
            case 3:
                return kontrahent.getKontrahentNazwisko();
            case 4:
                return kontrahent.getKontrahentAdres();
        }
        return null;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }
    // </editor-fold> 
}
