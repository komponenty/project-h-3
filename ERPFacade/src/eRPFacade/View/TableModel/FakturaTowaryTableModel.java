package eRPFacade.View.TableModel;

import java.util.List;
import javax.swing.table.AbstractTableModel;
import magazyn.ListaTowarowComponent.Model.TowarModel;

/**
 * @author Jakub Dydo, Katarzyna Romanowska
 * @version 1.0.0.1
 */
public class FakturaTowaryTableModel extends AbstractTableModel {

    // <editor-fold defaultstate="collapsed" desc="Fields">
    private List<TowarModel> listaTowarow;
    // </editor-fold> 

    // <editor-fold defaultstate="collapsed" desc="Properties">
    public TowarModel getTowar(int index) {
        if (index < 0) {
            return null;
        } else {
            return new TowarModel(this.listaTowarow.get(index));
        }
    }

    public List<TowarModel> getListaTowarow() {
        return this.listaTowarow;
    }

    public void setListaTowarow(List<TowarModel> listaTowarow) {
        this.listaTowarow = listaTowarow;
        this.fireTableDataChanged();
    }
    // </editor-fold> 

    // <editor-fold defaultstate="collapsed" desc="AbstractTableModel Implementation">
    @Override
    public int getRowCount() {
        if (this.listaTowarow == null) {
            return 0;
        }
        return this.listaTowarow.size();
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 0:
                return "Nazwa";
            case 1:
                return "Ilość";
            case 2:
                return "Cena za jednostkę";
            case 3:
                return "VAT";
            case 4:
                return "Rabat";
            case 5:
                return "Towar na użytek własny";
        }
        return null;
    }

    @Override
    public int getColumnCount() {
        return 6;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        if (this.listaTowarow == null) {
            return null;
        }
        TowarModel towar = this.listaTowarow.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return towar.getTowarnNazwa();
            case 1:
                return Math.round(towar.getTowarIlosc() * 100.0) / 100.0;
            case 2:
                return Math.round(towar.getTowarCenaNetto() * 100.0) / 100.0;
            case 3:
                return Math.round(towar.getTowarVAT() * 100.0) / 100.0;
            case 4:
                return Math.round(towar.getTowarRabat() * 100.0) / 100.0;
            case 5:
                if (towar.getTowarUzytekWlasny()) {
                    return "Tak";
                } else {
                    return "Nie";
                }
        }
        return null;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }
    // </editor-fold> 
}
