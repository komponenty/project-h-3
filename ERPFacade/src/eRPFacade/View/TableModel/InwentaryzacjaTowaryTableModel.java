package eRPFacade.View.TableModel;

import java.util.List;
import javax.swing.table.AbstractTableModel;
import magazyn.ListaTowarowComponent.Model.TowarModel;

/**
 * @author Jakub Dydo
 * @version 1.0.0.1
 */
public class InwentaryzacjaTowaryTableModel extends AbstractTableModel {

    // <editor-fold defaultstate="collapsed" desc="Fields">
    List<TowarModel> listaTowarow;
    // </editor-fold> 

    // <editor-fold defaultstate="collapsed" desc="Properties">
    public List<TowarModel> getListaTowarow() {
        return this.listaTowarow;
    }

    public void setListaTowarow(List<TowarModel> listaTowarow) {
        this.listaTowarow = listaTowarow;
        this.fireTableDataChanged();
    }
    
    public TowarModel getTowar(int index) {
        if (index < 0) {
            return null;
        } else {
            return new TowarModel(this.listaTowarow.get(index));
        }
    }
    // </editor-fold> 

    // <editor-fold defaultstate="collapsed" desc="AbstractTableModel Implementation">
    @Override
    public int getRowCount() {
        if (this.listaTowarow == null) {
            return 0;
        }
        return this.listaTowarow.size();
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 0:
                return "Nazwa";
            case 1:
                return "Producent";
            case 2:
                return "Identyfikator";
            case 3:
                return "Ilość";
            case 4:
                return "Cena brutto";
        }
        return null;
    }

    @Override
    public int getColumnCount() {
        return 5;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        if (this.listaTowarow == null) {
            return null;
        }
        TowarModel towar = this.listaTowarow.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return towar.getTowarnNazwa();
            case 1:
                return towar.getTowarProducent();
            case 2:
                return towar.getTowarID();
            case 3:
                return Math.round(towar.getTowarIlosc() * 100.0) / 100.0;
            case 4:
                return Math.round(towar.getTowarCenaNetto() * 100.0) / 100.0;
        }
        return null;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }
    // </editor-fold> 
}
