package eRPFacade.View.TableModel;

import java.util.List;
import javax.swing.table.AbstractTableModel;
import magazyn.InwentaryzacjaComponent.Model.TowarZyskStrataModel;

/**
 * @author Jakub Dydo
 * @version 1.0.0.1
 */
public class TowarZyskStrataTableModel extends AbstractTableModel {

    // <editor-fold defaultstate="collapsed" desc="Fields">
    List<TowarZyskStrataModel> listaTowarow;
    // </editor-fold> 

    // <editor-fold defaultstate="collapsed" desc="Properties">
    public List<TowarZyskStrataModel> getListaTowarow() {
        return this.listaTowarow;
    }

    public void setListaTowarow(List<TowarZyskStrataModel> listaTowarow) {
        this.listaTowarow = listaTowarow;
        this.fireTableDataChanged();
    }
    // </editor-fold> 

    // <editor-fold defaultstate="collapsed" desc="AbstractTableModel Implementation">
    @Override
    public int getRowCount() {
        if (this.listaTowarow == null) {
            return 0;
        }
        return this.listaTowarow.size();
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 0:
                return "Nazwa";
            case 1:
                return "Producent";
            case 2:
                return "Identyfikator";
            case 3:
                return "Ilość";
            case 4:
                return "Kwota";
            case 5:
                return "Uwagi";
        }
        return null;
    }

    @Override
    public int getColumnCount() {
        return 6;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        if (this.listaTowarow == null) {
            return null;
        }
        TowarZyskStrataModel towar = this.listaTowarow.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return towar.getTowarZyskStrataTowar().getTowarnNazwa();
            case 1:
                return towar.getTowarZyskStrataTowar().getTowarProducent();
            case 2:
                return towar.getTowarZyskStrataTowar().getTowarID();
            case 3:
                return Math.round(towar.getTowarZyskStrataIlosc() * 100.0) / 100.0;
            case 4:
                return Math.round(towar.getTowarZyskStrataKwota() * 100.0) / 100.0;
            case 5:
                return towar.getTowarZyskStrataUwagi();
        }
        return null;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }
    // </editor-fold> 
}
