package eRPFacade.View.TableModel;

import faktura.FakturaSprzedazyComponent.Model.FakturaSprzedazyModel;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 * @author Jakub Dydo, Katarzyna Romanowska
 * @version 1.0.0.1
 */
public class FakturaSprzedazyTableModel extends AbstractTableModel {

    // <editor-fold defaultstate="collapsed" desc="Fields">
    List<FakturaSprzedazyModel> listaFaktur;
    // </editor-fold> 

    // <editor-fold defaultstate="collapsed" desc="Properties">
    public List<FakturaSprzedazyModel> getListaFaktur() {
        return this.listaFaktur;
    }

    public void setListaFaktur(List<FakturaSprzedazyModel> listaFaktur) {
        this.listaFaktur = listaFaktur;
        this.fireTableDataChanged();
    }
    // </editor-fold> 

    // <editor-fold defaultstate="collapsed" desc="AbstractTableModel Implementation">
    @Override
    public int getRowCount() {
        if (this.listaFaktur == null) {
            return 0;
        }
        return this.listaFaktur.size();
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 0:
                return "Identyfikator";
            case 1:
                return "Kwota";
            case 2:
                return "Data wystawienia";
        }
        return null;
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        if (this.listaFaktur == null) {
            return null;
        }
        FakturaSprzedazyModel faktura = this.listaFaktur.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return faktura;
            case 1:
                return Math.round(faktura.getFakturaKwota() * 100.0) / 100.0;
            case 2:
                return new SimpleDateFormat("dd-MM-yyyy").format(faktura.getFakuraDataWystawienie());
        }
        return null;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }
    // </editor-fold> 
}
