package eRPFacade.View.TableModel;

import java.util.List;
import javax.swing.table.AbstractTableModel;
import magazyn.ListaTowarowComponent.Model.TowarModel;

/**
 * @author Jakub Dydo
 * @version 1.0.0.0
 */
public class TowaryTableModel extends AbstractTableModel {

    // <editor-fold defaultstate="collapsed" desc="Fields">
    private List<TowarModel> listaTowarow;
    // </editor-fold> 

    // <editor-fold defaultstate="collapsed" desc="Properties">
    public TowarModel getTowar(int index) {
        if (index < 0) {
            return null;
        } else {
            return new TowarModel(this.listaTowarow.get(index));
        }
    }

    public void setTowar(TowarModel towar, int index) {
        if (index >= 0) {
            this.listaTowarow.set(index, towar);
            this.fireTableDataChanged();
        }
    }

    public void removeTowar(int index) {
        if (index >= 0) {
            this.listaTowarow.remove(index);
            this.fireTableDataChanged();
        }
    }

    public List<TowarModel> getListaTowarow() {
        return this.listaTowarow;
    }

    public void setListaTowarow(List<TowarModel> listaTowarow) {
        this.listaTowarow = listaTowarow;
        this.fireTableDataChanged();
    }
    // </editor-fold> 

    // <editor-fold defaultstate="collapsed" desc="AbstractTableModel Implementation">
    @Override
    public int getRowCount() {
        if (this.listaTowarow == null) {
            return 0;
        }
        return this.listaTowarow.size();
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 0:
                return "Nazwa";
            case 1:
                return "Producent";
            case 2:
                return "Identyfikator";
            case 3:
                return "Rodzaj";
        }
        return null;
    }

    @Override
    public int getColumnCount() {
        return 4;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        if (this.listaTowarow == null) {
            return null;
        }
        TowarModel towar = this.listaTowarow.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return towar.getTowarnNazwa();
            case 1:
                return towar.getTowarProducent();
            case 2:
                return towar.getTowarID();
            case 3:
                return towar.getTowarRodzaj();
        }
        return null;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }
    // </editor-fold> 
}
