package eRPFacade.View.TableModel;

import glowne.KonfiguracjaComponent.Model.UzytkownikModel;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 * @author Jakub Dydo, Katarzyna Romanowska
 * @version 1.0.0.0
 */
public class UzytkownikTableModel extends AbstractTableModel {

    // <editor-fold defaultstate="collapsed" desc="Fields">
    List<UzytkownikModel> listaUzytkownikow;
    // </editor-fold> 

    // <editor-fold defaultstate="collapsed" desc="Properties">
    public UzytkownikModel getUzytkownik(int index) {
        if (index < 0) {
            return null;
        } else {
            return new UzytkownikModel(this.listaUzytkownikow.get(index));
        }
    }

    public void setUzytkownik(UzytkownikModel uzytkownik, int index) {
        if (index >= 0) {
            this.listaUzytkownikow.set(index, uzytkownik);
            this.fireTableDataChanged();
        }
    }

    public void removeUzytkownik(int index) {
        if (index >= 0) {
            this.listaUzytkownikow.remove(index);
            this.fireTableDataChanged();
        }
    }
    
    public List<UzytkownikModel> getListaUzytkownikow() {
        return this.listaUzytkownikow;
    }

    public void setListaUzytkownikow(List<UzytkownikModel> listaUzytkownikow) {
        this.listaUzytkownikow = listaUzytkownikow;
        this.fireTableDataChanged();
    }
    // </editor-fold> 

    // <editor-fold defaultstate="collapsed" desc="AbstractTableModel Implementation">
    @Override
    public int getRowCount() {
        if (this.listaUzytkownikow == null) {
            return 0;
        }
        return this.listaUzytkownikow.size();
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 0:
                return "Identyfikator";
            case 1:
                return "Login";
            case 2:
                return "Imię";
            case 3:
                return "Nazwisko";
            case 4:
                return "Status";
        }
        return null;
    }

    @Override
    public int getColumnCount() {
        return 5;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        if (this.listaUzytkownikow == null) {
            return null;
        }
        UzytkownikModel uzytkownik = this.listaUzytkownikow.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return uzytkownik.getUzytkownikID();
            case 1:
                return uzytkownik.getUzytkownikLogin();
            case 2:
                return uzytkownik.getUzytkownikImie();
            case 3:
                return uzytkownik.getUzytkownikNazwisko();
            case 4:
                return uzytkownik.getUzytkownikStatus().toString();
        }
        return null;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }
    // </editor-fold> 
}
