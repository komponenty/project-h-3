package eRPFacade.View.TableModel;

import java.text.SimpleDateFormat;
import java.util.List;
import javax.swing.table.AbstractTableModel;
import magazyn.InwentaryzacjaComponent.Model.SpisZNaturyModel;

/**
 * @author Jakub Dydo
 * @version 1.0.0.0
 */
public class SpisZNaturyTableModel extends AbstractTableModel {

    // <editor-fold defaultstate="collapsed" desc="Fields">
    List<SpisZNaturyModel> listaSpisow;
    // </editor-fold> 

    // <editor-fold defaultstate="collapsed" desc="Properties">
    public List<SpisZNaturyModel> getListaSpisow() {
        return this.listaSpisow;
    }

    public void setListaSpisow(List<SpisZNaturyModel> listaSpisow) {
        this.listaSpisow = listaSpisow;
        this.fireTableDataChanged();
    }
    // </editor-fold> 

    // <editor-fold defaultstate="collapsed" desc="AbstractTableModel Implementation">
    @Override
    public int getRowCount() {
        if (this.listaSpisow == null) {
            return 0;
        }
        return this.listaSpisow.size();
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 0:
                return "Identyfikator";
            case 1:
                return "Imię";
            case 2:
                return "Nazwisko";
            case 3:
                return "Data";
        }
        return null;
    }

    @Override
    public int getColumnCount() {
        return 4;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        if (this.listaSpisow == null) {
            return null;
        }
        SpisZNaturyModel spis = this.listaSpisow.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return spis;
            case 1:
                return spis.getSpisZNaturyOsobaSprawdzajacaImie();
            case 2:
                return spis.getSpisZNaturyOsobaSprawdzajacaNazwisko();
            case 3:
                return new SimpleDateFormat("dd-MM-yyyy").format(spis.getSpisZNaturyDataSpisu());
        }
        return null;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }
    // </editor-fold> 
}
