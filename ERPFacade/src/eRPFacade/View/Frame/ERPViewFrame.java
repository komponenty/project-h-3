package eRPFacade.View.Frame;

import eRPFacade.View.Enum.RodzajFaktury;
import eRPFacade.View.Enum.RodzajRaportu;
import faktura.FakturaSprzedazyComponent.Contract.IFakturaSprzedazy;
import faktura.FakturaSprzedazyComponent.Model.FakturaSprzedazyModel;
import faktura.FakturaZakupuComponent.Contract.IFakturaZakupu;
import faktura.FakturaZakupuComponent.Model.FakturaZakupuModel;
import faktura.HistoriaFakturComponent.Contract.IHistoriaFaktur;
import faktura.ListaKontrahentowComponent.Contract.IListaKontrahentow;
import glowne.KonfiguracjaComponent.Contract.IKonfiguracja;
import glowne.KonfiguracjaComponent.Model.Status;
import glowne.KonfiguracjaComponent.Model.UzytkownikModel;
import glowne.LogowanieComponent.Contract.ILogowanie;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import ksiegowosc.DeklaracjaVAT7Component.Contract.IDeklaracjaVAT7;
import ksiegowosc.KsiazkaPodatkowaComponent.Contract.IKsiazkaPodatkowa;
import ksiegowosc.ListaPracownikowComponent.Contract.IListaPracownikow;
import ksiegowosc.RejestrVATComponent.Contract.IRejestrVAT;
import magazyn.InwentaryzacjaComponent.Contract.IInwentaryzacja;
import magazyn.InwentaryzacjaComponent.Model.DokumentNadwyzekNiedoborowModel;
import magazyn.InwentaryzacjaComponent.Model.RodzajDokumentu;
import magazyn.InwentaryzacjaComponent.Model.SpisZNaturyModel;
import magazyn.ListaTowarowComponent.Contract.IListaTowarow;
import magazyn.ListaTowarowComponent.Model.TowarModel;
import magazyn.RaportyProsteComponent.Contract.IRaportyProste;

/**
 * @author Jakub Dydo, Katarzyna Romanowska
 * @version 1.0.0.5
 */
public class ERPViewFrame extends javax.swing.JFrame {

    // <editor-fold defaultstate="collapsed" desc="Fields">   
    private ActionListener actionListener;

    //Glowne
    private IKonfiguracja konfiguracja;
    private ILogowanie logowanie;

    //Faktura
    private IFakturaSprzedazy fakturaSprzedazy;
    private IFakturaZakupu fakturaZakupu;
    private IHistoriaFaktur historiaFaktur;
    private IListaKontrahentow listaKontrahentow;

    //Ksiegowosc 
    private IDeklaracjaVAT7 deklaracjaVAT7;
    private IKsiazkaPodatkowa ksiazkaPodatkowa;
    private IListaPracownikow listaPracownikow;
    private IRejestrVAT rejestrVAT;

    //Magazyn
    private IInwentaryzacja inwentaryzacja;
    private IListaTowarow listaTowarow;
    private IRaportyProste raportyProste;
    // </editor-fold>  

    // <editor-fold defaultstate="collapsed" desc="Constructor">  
    public ERPViewFrame(IKonfiguracja konfiguracja, ILogowanie logowanie,
            IFakturaSprzedazy fakturaSprzedazy, IFakturaZakupu fakturaZakupu, IHistoriaFaktur historiaFaktur, IListaKontrahentow listaKontrahentow,
            IDeklaracjaVAT7 deklaracjaVAT7, IKsiazkaPodatkowa ksiazkaPodatkowa, IListaPracownikow listaPracownikow, IRejestrVAT rejestrVAT,
            IInwentaryzacja inwentaryzacja, IListaTowarow listaTowaro, IRaportyProste raportyProste) {

        initComponents();

        this.konfiguracja = konfiguracja;
        this.logowanie = logowanie;
        this.fakturaSprzedazy = fakturaSprzedazy;
        this.fakturaZakupu = fakturaZakupu;
        this.historiaFaktur = historiaFaktur;
        this.listaKontrahentow = listaKontrahentow;
        this.deklaracjaVAT7 = deklaracjaVAT7;
        this.ksiazkaPodatkowa = ksiazkaPodatkowa;
        this.listaPracownikow = listaPracownikow;
        this.rejestrVAT = rejestrVAT;
        this.inwentaryzacja = inwentaryzacja;
        this.listaTowarow = listaTowaro;
        this.raportyProste = raportyProste;

        InstActionListenersMethod();
        
    }
    // </editor-fold> 

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel5 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        jPanel8 = new javax.swing.JPanel();
        jButton2 = new javax.swing.JButton();
        jPanel9 = new javax.swing.JPanel();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jPanel10 = new javax.swing.JPanel();
        jButton5 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        jPanel26 = new javax.swing.JPanel();
        dodajKontrahentaPanel1 = new eRPFacade.View.Panel.Faktura.Kontrahent.DodajKontrahentaPanel();
        znajdzKontrahentaPanel1 = new eRPFacade.View.Panel.Faktura.Kontrahent.ZnajdzKontrahentaPanel();
        znajdzFakturePanel1 = new eRPFacade.View.Panel.Faktura.HistoriaFaktur.ZnajdzFakturePanel();
        wynikZnajdzKontrahentaPanel1 = new eRPFacade.View.Panel.Faktura.Kontrahent.WynikZnajdzKontrahentaPanel();
        wynikZnajdzFaktureSprzedazyPanel1 = new eRPFacade.View.Panel.Faktura.HistoriaFaktur.WynikZnajdzFaktureSprzedazyPanel();
        wynikZnajdzFaktureZakupuPanel1 = new eRPFacade.View.Panel.Faktura.HistoriaFaktur.WynikZnajdzFaktureZakupuPanel();
        dodajFaktureSprzedazyPanel1 = new eRPFacade.View.Panel.Faktura.FakturaSprzedazy.DodajFaktureSprzedazyPanel();
        dodajFaktureZakupuPanel1 = new eRPFacade.View.Panel.Faktura.FakturaZakupu.DodajFaktureZakupuPanel();
        jPanel2 = new javax.swing.JPanel();
        jPanel15 = new javax.swing.JPanel();
        jPanel12 = new javax.swing.JPanel();
        jButton7 = new javax.swing.JButton();
        jButton14 = new javax.swing.JButton();
        jPanel11 = new javax.swing.JPanel();
        jButton8 = new javax.swing.JButton();
        jButton9 = new javax.swing.JButton();
        jButton11 = new javax.swing.JButton();
        jButton10 = new javax.swing.JButton();
        jPanel13 = new javax.swing.JPanel();
        jButton15 = new javax.swing.JButton();
        jButton18 = new javax.swing.JButton();
        jPanel23 = new javax.swing.JPanel();
        dodajTowarPanel1 = new eRPFacade.View.Panel.Magazyn.Towary.DodajTowarPanel();
        znajdzTowarPanel1 = new eRPFacade.View.Panel.Magazyn.Towary.ZnajdzTowarPanel();
        wynikZnajdzTowarPanel1 = new eRPFacade.View.Panel.Magazyn.Towary.WynikZnajdzTowarPanel();
        raportPanel1 = new eRPFacade.View.Panel.Magazyn.Raporty.RaportPanel();
        raportWybierzMiesiacPanel1 = new eRPFacade.View.Panel.Magazyn.Raporty.RaportWybierzMiesiacPanel();
        dodajSpisZNaturyPanel1 = new eRPFacade.View.Panel.Magazyn.Inwentaryzacja.DodajSpisZNaturyPanel();
        znajdzSpisZNaturyPanel1 = new eRPFacade.View.Panel.Magazyn.Inwentaryzacja.ZnajdzSpisZNaturyPanel();
        wynikZnajdzSpisZNaturyPanel1 = new eRPFacade.View.Panel.Magazyn.Inwentaryzacja.WynikZnajdzSpisZNaturyPanel();
        wygenerowaneNadwyzkiNiedobory1 = new eRPFacade.View.Panel.Magazyn.Inwentaryzacja.WygenerowaneNadwyzkiNiedobory();
        jPanel3 = new javax.swing.JPanel();
        jPanel18 = new javax.swing.JPanel();
        jPanel19 = new javax.swing.JPanel();
        jButton25 = new javax.swing.JButton();
        jButton26 = new javax.swing.JButton();
        jButton27 = new javax.swing.JButton();
        jPanel20 = new javax.swing.JPanel();
        jButton28 = new javax.swing.JButton();
        jButton29 = new javax.swing.JButton();
        jPanel21 = new javax.swing.JPanel();
        jButton30 = new javax.swing.JButton();
        jPanel22 = new javax.swing.JPanel();
        jButton32 = new javax.swing.JButton();
        jButton31 = new javax.swing.JButton();
        jPanel24 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        jPanel16 = new javax.swing.JPanel();
        jPanel14 = new javax.swing.JPanel();
        jButton21 = new javax.swing.JButton();
        jButton22 = new javax.swing.JButton();
        jPanel17 = new javax.swing.JPanel();
        jButton23 = new javax.swing.JButton();
        jButton24 = new javax.swing.JButton();
        jPanel25 = new javax.swing.JPanel();
        dodajUzytkownikaPanel1 = new eRPFacade.View.Panel.Glowne.Uzytkownik.DodajUzytkownikaPanel();
        zmienDanePanel1 = new eRPFacade.View.Panel.Glowne.DanePrzedsiebiorstwa.ZmienDanePanel();
        pokażDanePanel1 = new eRPFacade.View.Panel.Glowne.DanePrzedsiebiorstwa.PokażDanePanel();
        przegladajUzytkownikow1 = new eRPFacade.View.Panel.Glowne.Uzytkownik.PrzegladajUzytkownikow();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(119, 198, 251));
        setMinimumSize(new java.awt.Dimension(1365, 720));
        setName("mainFrame"); // NOI18N
        setPreferredSize(new java.awt.Dimension(1365, 720));
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        jPanel5.setBackground(new java.awt.Color(190, 219, 246));
        jPanel5.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(109, 109, 109))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 33, Short.MAX_VALUE)
        );

        jTabbedPane1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        jPanel6.setBackground(new java.awt.Color(190, 219, 246));
        jPanel6.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jPanel7.setBackground(new java.awt.Color(190, 219, 246));
        jPanel7.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Faktura sprzedaży", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 14))); // NOI18N
        jPanel7.setFont(jPanel7.getFont());

        jButton1.setFont(new java.awt.Font("Tahoma", 0, 20)); // NOI18N
        jButton1.setText("Dodaj");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, 108, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel8.setBackground(new java.awt.Color(190, 219, 246));
        jPanel8.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Faktura zakupu", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 14))); // NOI18N

        jButton2.setFont(new java.awt.Font("Tahoma", 0, 20)); // NOI18N
        jButton2.setText("Dodaj");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jButton2, javax.swing.GroupLayout.DEFAULT_SIZE, 108, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel9.setBackground(new java.awt.Color(190, 219, 246));
        jPanel9.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Historia faktur", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 14))); // NOI18N

        jButton3.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        jButton3.setText("Znajdź fakturę sprzedaży");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jButton4.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        jButton4.setText("Znajdź fakturę zakupu");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jButton3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton4, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel10.setBackground(new java.awt.Color(190, 219, 246));
        jPanel10.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Kontrahenci", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 14))); // NOI18N

        jButton5.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        jButton5.setText("Dodaj");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });

        jButton6.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        jButton6.setText("Znajdź");
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jButton5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton6, javax.swing.GroupLayout.DEFAULT_SIZE, 105, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jButton5, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton6, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 654, Short.MAX_VALUE))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jPanel26Layout = new javax.swing.GroupLayout(jPanel26);
        jPanel26.setLayout(jPanel26Layout);
        jPanel26Layout.setHorizontalGroup(
            jPanel26Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(wynikZnajdzKontrahentaPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
            .addComponent(wynikZnajdzFaktureSprzedazyPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
            .addComponent(wynikZnajdzFaktureZakupuPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
            .addGroup(jPanel26Layout.createSequentialGroup()
                .addGroup(jPanel26Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(dodajKontrahentaPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(znajdzKontrahentaPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(znajdzFakturePanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, Short.MAX_VALUE))
            .addComponent(dodajFaktureSprzedazyPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
            .addComponent(dodajFaktureZakupuPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel26Layout.setVerticalGroup(
            jPanel26Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel26Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(dodajKontrahentaPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(znajdzKontrahentaPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(znajdzFakturePanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(wynikZnajdzKontrahentaPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addGap(0, 0, 0)
                .addComponent(wynikZnajdzFaktureSprzedazyPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addGap(0, 0, 0)
                .addComponent(wynikZnajdzFaktureZakupuPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addGap(0, 0, 0)
                .addComponent(dodajFaktureSprzedazyPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addGap(0, 0, 0)
                .addComponent(dodajFaktureZakupuPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel26, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jPanel26, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );

        jTabbedPane1.addTab("FAKTURA", jPanel1);

        jPanel15.setBackground(new java.awt.Color(190, 219, 246));
        jPanel15.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jPanel12.setBackground(new java.awt.Color(190, 219, 246));
        jPanel12.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Towary", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 14))); // NOI18N
        jPanel12.setFont(jPanel12.getFont());

        jButton7.setFont(new java.awt.Font("Tahoma", 0, 20)); // NOI18N
        jButton7.setText("Dodaj");
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });

        jButton14.setFont(new java.awt.Font("Tahoma", 0, 20)); // NOI18N
        jButton14.setText("Znajdź");
        jButton14.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton14ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel12Layout = new javax.swing.GroupLayout(jPanel12);
        jPanel12.setLayout(jPanel12Layout);
        jPanel12Layout.setHorizontalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jButton7, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jButton14, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel12Layout.setVerticalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton7, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton14, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, Short.MAX_VALUE))
        );

        jPanel11.setBackground(new java.awt.Color(190, 219, 246));
        jPanel11.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Raporty produktów", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 14))); // NOI18N

        jButton8.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        jButton8.setText("Przeterminowane");
        jButton8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton8ActionPerformed(evt);
            }
        });

        jButton9.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        jButton9.setText("Bliskie przeterminowania");
        jButton9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton9ActionPerformed(evt);
            }
        });

        jButton11.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        jButton11.setText("Na wyczerpaniu");
        jButton11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton11ActionPerformed(evt);
            }
        });

        jButton10.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        jButton10.setText("Wyczerpane");
        jButton10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton10ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel11Layout = new javax.swing.GroupLayout(jPanel11);
        jPanel11.setLayout(jPanel11Layout);
        jPanel11Layout.setHorizontalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jButton9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton11, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton10, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel11Layout.setVerticalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jButton8, javax.swing.GroupLayout.DEFAULT_SIZE, 42, Short.MAX_VALUE)
                    .addComponent(jButton10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel11Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jButton9, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jButton11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        jPanel13.setBackground(new java.awt.Color(190, 219, 246));
        jPanel13.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Inwentaryzacja", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 14))); // NOI18N

        jButton15.setFont(new java.awt.Font("Tahoma", 0, 20)); // NOI18N
        jButton15.setText("Dodaj");
        jButton15.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton15ActionPerformed(evt);
            }
        });

        jButton18.setFont(new java.awt.Font("Tahoma", 0, 20)); // NOI18N
        jButton18.setText("Znajdź");
        jButton18.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton18ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel13Layout = new javax.swing.GroupLayout(jPanel13);
        jPanel13.setLayout(jPanel13Layout);
        jPanel13Layout.setHorizontalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jButton15, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jButton18, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel13Layout.setVerticalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton15, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton18, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(14, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel15Layout = new javax.swing.GroupLayout(jPanel15);
        jPanel15.setLayout(jPanel15Layout);
        jPanel15Layout.setHorizontalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel15Layout.createSequentialGroup()
                .addComponent(jPanel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel15Layout.setVerticalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        jPanel23.setMinimumSize(new java.awt.Dimension(0, 0));

        javax.swing.GroupLayout jPanel23Layout = new javax.swing.GroupLayout(jPanel23);
        jPanel23.setLayout(jPanel23Layout);
        jPanel23Layout.setHorizontalGroup(
            jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(wynikZnajdzTowarPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
            .addGroup(jPanel23Layout.createSequentialGroup()
                .addComponent(raportPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addGap(0, 0, 0)
                .addComponent(raportWybierzMiesiacPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addComponent(dodajSpisZNaturyPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 0, Short.MAX_VALUE)
            .addComponent(wynikZnajdzSpisZNaturyPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
            .addGroup(jPanel23Layout.createSequentialGroup()
                .addGroup(jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(dodajTowarPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(znajdzTowarPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(znajdzSpisZNaturyPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, Short.MAX_VALUE))
            .addComponent(wygenerowaneNadwyzkiNiedobory1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
        );
        jPanel23Layout.setVerticalGroup(
            jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel23Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(dodajTowarPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(znajdzTowarPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(wynikZnajdzTowarPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addGap(0, 0, 0)
                .addGroup(jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(raportPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(jPanel23Layout.createSequentialGroup()
                        .addComponent(raportWybierzMiesiacPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(dodajSpisZNaturyPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 0, Short.MAX_VALUE)
                        .addGap(0, 0, 0)
                        .addComponent(znajdzSpisZNaturyPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(wynikZnajdzSpisZNaturyPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                        .addGap(0, 0, 0)
                        .addComponent(wygenerowaneNadwyzkiNiedobory1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))))
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel15, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel23, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jPanel15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jPanel23, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );

        jTabbedPane1.addTab("MAGAZYN", jPanel2);

        jPanel18.setBackground(new java.awt.Color(190, 219, 246));
        jPanel18.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jPanel19.setBackground(new java.awt.Color(190, 219, 246));
        jPanel19.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Książka podatkowa", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 14))); // NOI18N

        jButton25.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jButton25.setText("Zestawienie przychodów");
        jButton25.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton25ActionPerformed(evt);
            }
        });

        jButton26.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jButton26.setText("Zestawienie rozchodów");
        jButton26.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton26ActionPerformed(evt);
            }
        });

        jButton27.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jButton27.setText("Zestawienie przychodów i rozchodów");
        jButton27.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton27ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel19Layout = new javax.swing.GroupLayout(jPanel19);
        jPanel19.setLayout(jPanel19Layout);
        jPanel19Layout.setHorizontalGroup(
            jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel19Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jButton27, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton26, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton25, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel19Layout.setVerticalGroup(
            jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel19Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jButton25)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton26)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton27)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel20.setBackground(new java.awt.Color(190, 219, 246));
        jPanel20.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Rejestr VAT", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 14))); // NOI18N

        jButton28.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        jButton28.setText("Wyświetl otwarty");
        jButton28.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton28ActionPerformed(evt);
            }
        });

        jButton29.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        jButton29.setText("Znajdź zamknięte");
        jButton29.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton29ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel20Layout = new javax.swing.GroupLayout(jPanel20);
        jPanel20.setLayout(jPanel20Layout);
        jPanel20Layout.setHorizontalGroup(
            jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel20Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jButton28, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton29, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel20Layout.setVerticalGroup(
            jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel20Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jButton28, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton29, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel21.setBackground(new java.awt.Color(190, 219, 246));
        jPanel21.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Deklaracja VAT7", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 14))); // NOI18N

        jButton30.setFont(new java.awt.Font("Tahoma", 0, 20)); // NOI18N
        jButton30.setText("Generuj");
        jButton30.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton30ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel21Layout = new javax.swing.GroupLayout(jPanel21);
        jPanel21.setLayout(jPanel21Layout);
        jPanel21Layout.setHorizontalGroup(
            jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel21Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jButton30, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel21Layout.setVerticalGroup(
            jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel21Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jButton30, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel22.setBackground(new java.awt.Color(190, 219, 246));
        jPanel22.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Pracownicy", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 14))); // NOI18N

        jButton32.setFont(new java.awt.Font("Tahoma", 0, 20)); // NOI18N
        jButton32.setText("Dodaj");
        jButton32.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton32ActionPerformed(evt);
            }
        });

        jButton31.setFont(new java.awt.Font("Tahoma", 0, 20)); // NOI18N
        jButton31.setText("Znajdź");
        jButton31.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton31ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel22Layout = new javax.swing.GroupLayout(jPanel22);
        jPanel22.setLayout(jPanel22Layout);
        jPanel22Layout.setHorizontalGroup(
            jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel22Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jButton32, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jButton31, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel22Layout.setVerticalGroup(
            jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel22Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton32, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton31, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel18Layout = new javax.swing.GroupLayout(jPanel18);
        jPanel18.setLayout(jPanel18Layout);
        jPanel18Layout.setHorizontalGroup(
            jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel18Layout.createSequentialGroup()
                .addComponent(jPanel19, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel20, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel21, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel22, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 430, Short.MAX_VALUE))
        );
        jPanel18Layout.setVerticalGroup(
            jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel19, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel20, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel21, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel22, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        jPanel24.setPreferredSize(new java.awt.Dimension(0, 0));

        javax.swing.GroupLayout jPanel24Layout = new javax.swing.GroupLayout(jPanel24);
        jPanel24.setLayout(jPanel24Layout);
        jPanel24Layout.setHorizontalGroup(
            jPanel24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1341, Short.MAX_VALUE)
        );
        jPanel24Layout.setVerticalGroup(
            jPanel24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 478, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel18, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel24, javax.swing.GroupLayout.DEFAULT_SIZE, 1341, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jPanel18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jPanel24, javax.swing.GroupLayout.DEFAULT_SIZE, 478, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );

        jTabbedPane1.addTab("KSIĘGOWOŚĆ", jPanel3);

        jPanel16.setBackground(new java.awt.Color(190, 219, 246));
        jPanel16.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jPanel14.setBackground(new java.awt.Color(190, 219, 246));
        jPanel14.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Użytkownicy", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 14))); // NOI18N

        jButton21.setFont(new java.awt.Font("Tahoma", 0, 20)); // NOI18N
        jButton21.setText("Dodaj");
        jButton21.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton21ActionPerformed(evt);
            }
        });

        jButton22.setFont(new java.awt.Font("Tahoma", 0, 20)); // NOI18N
        jButton22.setText("Przeglądaj");
        jButton22.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton22ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel14Layout = new javax.swing.GroupLayout(jPanel14);
        jPanel14.setLayout(jPanel14Layout);
        jPanel14Layout.setHorizontalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel14Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jButton21, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jButton22)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel14Layout.setVerticalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel14Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton21, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton22, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel17.setBackground(new java.awt.Color(190, 219, 246));
        jPanel17.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Dane przedsiębiorstwa", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 14))); // NOI18N

        jButton23.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        jButton23.setText("Pokaż");
        jButton23.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton23ActionPerformed(evt);
            }
        });

        jButton24.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        jButton24.setText("Zmień");
        jButton24.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton24ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel17Layout = new javax.swing.GroupLayout(jPanel17);
        jPanel17.setLayout(jPanel17Layout);
        jPanel17Layout.setHorizontalGroup(
            jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel17Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jButton23, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton24, javax.swing.GroupLayout.DEFAULT_SIZE, 130, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel17Layout.setVerticalGroup(
            jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel17Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jButton23, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton24, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel16Layout = new javax.swing.GroupLayout(jPanel16);
        jPanel16.setLayout(jPanel16Layout);
        jPanel16Layout.setHorizontalGroup(
            jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel16Layout.createSequentialGroup()
                .addComponent(jPanel14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel16Layout.setVerticalGroup(
            jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel14, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel17, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jPanel25Layout = new javax.swing.GroupLayout(jPanel25);
        jPanel25.setLayout(jPanel25Layout);
        jPanel25Layout.setHorizontalGroup(
            jPanel25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel25Layout.createSequentialGroup()
                .addGroup(jPanel25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(dodajUzytkownikaPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(zmienDanePanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(pokażDanePanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(przegladajUzytkownikow1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addGap(0, 0, 0))
        );
        jPanel25Layout.setVerticalGroup(
            jPanel25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel25Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(dodajUzytkownikaPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(zmienDanePanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(pokażDanePanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(przegladajUzytkownikow1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel16, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel25, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addComponent(jPanel16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jPanel25, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );

        jTabbedPane1.addTab("KONFIGURACJA", jPanel4);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1)
            .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(jTabbedPane1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    // <editor-fold defaultstate="collapsed" desc="ButtonEvents"> 
    private void jButton21ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton21ActionPerformed
        HideKonfiguracja();
        this.dodajUzytkownikaPanel1.resetComponentState();
        this.dodajUzytkownikaPanel1.setVisible(true);
    }//GEN-LAST:event_jButton21ActionPerformed

    private void jButton24ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton24ActionPerformed
        HideKonfiguracja();
        this.zmienDanePanel1.setPrzedsiebiorstwo(this.konfiguracja.zwrocDaneFirmy());
        this.zmienDanePanel1.setVisible(true);
    }//GEN-LAST:event_jButton24ActionPerformed

    private void jButton23ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton23ActionPerformed
        HideKonfiguracja();
        this.pokażDanePanel1.setPrzedsiebiorstwo(this.konfiguracja.zwrocDaneFirmy());
        this.pokażDanePanel1.setVisible(true);
    }//GEN-LAST:event_jButton23ActionPerformed

    private void jButton22ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton22ActionPerformed
        HideKonfiguracja();
        this.przegladajUzytkownikow1.resetComponentState();
        this.przegladajUzytkownikow1.setListaUzytkownikow(konfiguracja.znajdzUzytkownikow(new UzytkownikModel()));
        this.przegladajUzytkownikow1.setVisible(true);
    }//GEN-LAST:event_jButton22ActionPerformed

    private void jButton25ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton25ActionPerformed
        HideKsiegowosc();
    }//GEN-LAST:event_jButton25ActionPerformed

    private void jButton26ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton26ActionPerformed
        HideKsiegowosc();
    }//GEN-LAST:event_jButton26ActionPerformed

    private void jButton27ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton27ActionPerformed
        HideKsiegowosc();
    }//GEN-LAST:event_jButton27ActionPerformed

    private void jButton28ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton28ActionPerformed
        HideKsiegowosc();
    }//GEN-LAST:event_jButton28ActionPerformed

    private void jButton29ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton29ActionPerformed
        HideKsiegowosc();
    }//GEN-LAST:event_jButton29ActionPerformed

    private void jButton30ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton30ActionPerformed
        HideKsiegowosc();
    }//GEN-LAST:event_jButton30ActionPerformed

    private void jButton32ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton32ActionPerformed
        HideKsiegowosc();
    }//GEN-LAST:event_jButton32ActionPerformed

    private void jButton31ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton31ActionPerformed
        HideKsiegowosc();
    }//GEN-LAST:event_jButton31ActionPerformed

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        HideFaktura();
        znajdzKontrahentaPanel1.resetComponentState();
        wynikZnajdzKontrahentaPanel1.resetComponentState();
        znajdzKontrahentaPanel1.setVisible(true);
    }//GEN-LAST:event_jButton6ActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        HideFaktura();
        this.dodajKontrahentaPanel1.resetComponentState();
        this.dodajKontrahentaPanel1.setVisible(true);
    }//GEN-LAST:event_jButton5ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        HideFaktura();
        this.znajdzFakturePanel1.resetComponentState();
        this.wynikZnajdzFaktureZakupuPanel1.resetComponentState();
        this.znajdzFakturePanel1.setRodzajFaktury(RodzajFaktury.FAKTURA_ZAKUPU);
        this.znajdzFakturePanel1.setVisible(true);
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        HideFaktura();
        this.znajdzFakturePanel1.resetComponentState();
        this.wynikZnajdzFaktureSprzedazyPanel1.resetComponentState();
        this.znajdzFakturePanel1.setRodzajFaktury(RodzajFaktury.FAKTURA_SPRZEDAZY);
        this.znajdzFakturePanel1.setVisible(true);
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        HideFaktura();
        setEnableFakturaMenuPanel(false);
        this.dodajFaktureZakupuPanel1.resetComponentState();
        this.dodajFaktureZakupuPanel1.setVisible(true);
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        HideFaktura();
        setEnableFakturaMenuPanel(false);
        this.dodajFaktureSprzedazyPanel1.resetComponentState();
        this.dodajFaktureSprzedazyPanel1.setVisible(true);
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton18ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton18ActionPerformed
        HideMagazyn();
        this.znajdzSpisZNaturyPanel1.resetComponentState();
        this.wynikZnajdzSpisZNaturyPanel1.resetComponentState();
        this.znajdzSpisZNaturyPanel1.setVisible(true);
    }//GEN-LAST:event_jButton18ActionPerformed

    private void jButton15ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton15ActionPerformed
        HideMagazyn();
        this.setEnableMagazynMenuPanel(false);
        this.dodajSpisZNaturyPanel1.resetComponentState();
        this.dodajSpisZNaturyPanel1.setVisible(true);
    }//GEN-LAST:event_jButton15ActionPerformed

    private void jButton10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton10ActionPerformed
        HideMagazyn();
        raportPanel1.setRodzajRaportu(RodzajRaportu.TOWARY_WYCZERPANE);
        raportPanel1.setVisible(true);
        raportPanel1.setListaTowarow(this.raportyProste.raportProduktyWyczerpane());
    }//GEN-LAST:event_jButton10ActionPerformed

    private void jButton11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton11ActionPerformed
        HideMagazyn();
        raportPanel1.setRodzajRaportu(RodzajRaportu.TOWARY_BLISKIE_WYCZERPANIU);
        raportPanel1.setVisible(true);
        raportPanel1.setListaTowarow(this.raportyProste.raportProduktyNaWyczerpaniu());
    }//GEN-LAST:event_jButton11ActionPerformed

    private void jButton9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton9ActionPerformed
        HideMagazyn();
        raportPanel1.resetComponentState();
        raportWybierzMiesiacPanel1.resetComponentState();
        raportPanel1.setRodzajRaportu(RodzajRaportu.TOWARY_BLISKIE_PRZETERMINOWANIA);
        raportPanel1.setVisible(true);
        raportWybierzMiesiacPanel1.setVisible(true);
        raportWybierzMiesiacPanel1.getGenerujButton().doClick();
    }//GEN-LAST:event_jButton9ActionPerformed

    private void jButton8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton8ActionPerformed
        HideMagazyn();
        raportPanel1.resetComponentState();
        raportWybierzMiesiacPanel1.resetComponentState();
        raportPanel1.setRodzajRaportu(RodzajRaportu.TOWARY_PRZETERMINOWANE);
        raportPanel1.setVisible(true);
        raportWybierzMiesiacPanel1.setVisible(true);
        raportWybierzMiesiacPanel1.getGenerujButton().doClick();
    }//GEN-LAST:event_jButton8ActionPerformed

    private void jButton14ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton14ActionPerformed
        HideMagazyn();
        znajdzTowarPanel1.resetComponentState();
        wynikZnajdzTowarPanel1.resetComponentState();
        znajdzTowarPanel1.setVisible(true);
    }//GEN-LAST:event_jButton14ActionPerformed

    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton7ActionPerformed
        HideMagazyn();
        this.dodajTowarPanel1.resetComponentState();
        this.dodajTowarPanel1.setVisible(true);
    }//GEN-LAST:event_jButton7ActionPerformed

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        if (logowanie.getUzytkownik().getUzytkownikStatus() == Status.Administrator) {
            this.jTabbedPane1.remove(2);
        } else if (logowanie.getUzytkownik().getUzytkownikStatus() == Status.Sprzedawca) {
            this.jTabbedPane1.remove(2);
            this.jTabbedPane1.remove(2);
        } else if (logowanie.getUzytkownik().getUzytkownikStatus() == Status.Ksiegowy) {
            this.jTabbedPane1.remove(0);
            this.jTabbedPane1.remove(0);
            this.jTabbedPane1.remove(1);
            /////////////////////////////////
            this.jTabbedPane1.remove(0);
            /////////////////////////////////
        } else {
            this.jTabbedPane1.remove(0);
            this.jTabbedPane1.remove(0);
            this.jTabbedPane1.remove(0);
            this.jTabbedPane1.remove(0);
        }
    }//GEN-LAST:event_formWindowOpened
    // </editor-fold> 

    // <editor-fold defaultstate="collapsed" desc="Variables declaration">  
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private eRPFacade.View.Panel.Faktura.FakturaSprzedazy.DodajFaktureSprzedazyPanel dodajFaktureSprzedazyPanel1;
    private eRPFacade.View.Panel.Faktura.FakturaZakupu.DodajFaktureZakupuPanel dodajFaktureZakupuPanel1;
    private eRPFacade.View.Panel.Faktura.Kontrahent.DodajKontrahentaPanel dodajKontrahentaPanel1;
    private eRPFacade.View.Panel.Magazyn.Inwentaryzacja.DodajSpisZNaturyPanel dodajSpisZNaturyPanel1;
    private eRPFacade.View.Panel.Magazyn.Towary.DodajTowarPanel dodajTowarPanel1;
    private eRPFacade.View.Panel.Glowne.Uzytkownik.DodajUzytkownikaPanel dodajUzytkownikaPanel1;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton10;
    private javax.swing.JButton jButton11;
    private javax.swing.JButton jButton14;
    private javax.swing.JButton jButton15;
    private javax.swing.JButton jButton18;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton21;
    private javax.swing.JButton jButton22;
    private javax.swing.JButton jButton23;
    private javax.swing.JButton jButton24;
    private javax.swing.JButton jButton25;
    private javax.swing.JButton jButton26;
    private javax.swing.JButton jButton27;
    private javax.swing.JButton jButton28;
    private javax.swing.JButton jButton29;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton30;
    private javax.swing.JButton jButton31;
    private javax.swing.JButton jButton32;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton7;
    private javax.swing.JButton jButton8;
    private javax.swing.JButton jButton9;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel15;
    private javax.swing.JPanel jPanel16;
    private javax.swing.JPanel jPanel17;
    private javax.swing.JPanel jPanel18;
    private javax.swing.JPanel jPanel19;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel20;
    private javax.swing.JPanel jPanel21;
    private javax.swing.JPanel jPanel22;
    private javax.swing.JPanel jPanel23;
    private javax.swing.JPanel jPanel24;
    private javax.swing.JPanel jPanel25;
    private javax.swing.JPanel jPanel26;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JTabbedPane jTabbedPane1;
    private eRPFacade.View.Panel.Glowne.DanePrzedsiebiorstwa.PokażDanePanel pokażDanePanel1;
    private eRPFacade.View.Panel.Glowne.Uzytkownik.PrzegladajUzytkownikow przegladajUzytkownikow1;
    private eRPFacade.View.Panel.Magazyn.Raporty.RaportPanel raportPanel1;
    private eRPFacade.View.Panel.Magazyn.Raporty.RaportWybierzMiesiacPanel raportWybierzMiesiacPanel1;
    private eRPFacade.View.Panel.Magazyn.Inwentaryzacja.WygenerowaneNadwyzkiNiedobory wygenerowaneNadwyzkiNiedobory1;
    private eRPFacade.View.Panel.Faktura.HistoriaFaktur.WynikZnajdzFaktureSprzedazyPanel wynikZnajdzFaktureSprzedazyPanel1;
    private eRPFacade.View.Panel.Faktura.HistoriaFaktur.WynikZnajdzFaktureZakupuPanel wynikZnajdzFaktureZakupuPanel1;
    private eRPFacade.View.Panel.Faktura.Kontrahent.WynikZnajdzKontrahentaPanel wynikZnajdzKontrahentaPanel1;
    private eRPFacade.View.Panel.Magazyn.Inwentaryzacja.WynikZnajdzSpisZNaturyPanel wynikZnajdzSpisZNaturyPanel1;
    private eRPFacade.View.Panel.Magazyn.Towary.WynikZnajdzTowarPanel wynikZnajdzTowarPanel1;
    private eRPFacade.View.Panel.Glowne.DanePrzedsiebiorstwa.ZmienDanePanel zmienDanePanel1;
    private eRPFacade.View.Panel.Faktura.HistoriaFaktur.ZnajdzFakturePanel znajdzFakturePanel1;
    private eRPFacade.View.Panel.Faktura.Kontrahent.ZnajdzKontrahentaPanel znajdzKontrahentaPanel1;
    private eRPFacade.View.Panel.Magazyn.Inwentaryzacja.ZnajdzSpisZNaturyPanel znajdzSpisZNaturyPanel1;
    private eRPFacade.View.Panel.Magazyn.Towary.ZnajdzTowarPanel znajdzTowarPanel1;
    // End of variables declaration//GEN-END:variables
    // </editor-fold>  

    // <editor-fold defaultstate="collapsed" desc="ActionListeners">  
    // <editor-fold defaultstate="collapsed" desc="FAKTURA"> 
    // <editor-fold defaultstate="collapsed" desc="Faktura sprzedazy">
    private void DodajTowarDoFakturySprzedazy() {

        actionListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (dodajFaktureSprzedazyPanel1.valid()) {
                    List<TowarModel> lista = listaTowarow.znajdzTowar(dodajFaktureSprzedazyPanel1.getTowar());
                    if (!lista.isEmpty()) {
                        TowarModel towar = lista.get(0);
                        FakturaSprzedazyModel faktura = dodajFaktureSprzedazyPanel1.getFaktura();
                        jLabel1.setText(fakturaSprzedazy.dodajTowarDoFaktury(faktura, towar, dodajFaktureSprzedazyPanel1.getIlosc()));
                        dodajFaktureSprzedazyPanel1.setFaktura(faktura);
                    } else {
                        jLabel1.setText("Błąd! Towaru nie ma w bazie.");
                    }
                } else {
                    jLabel1.setText("Błąd! Popraw oznaczone pola.");
                }
            }
        };
        dodajFaktureSprzedazyPanel1.getDodajTowarButton().addActionListener(actionListener);
    }

    private void UsunTowarDoFakturySprzedazy() {

        actionListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                TowarModel towar = dodajFaktureSprzedazyPanel1.getSelectedTowar();
                if (towar != null) {
                    FakturaSprzedazyModel faktura = dodajFaktureSprzedazyPanel1.getFaktura();
                    jLabel1.setText(fakturaSprzedazy.usunTowarZFaktury(faktura, towar));
                    dodajFaktureSprzedazyPanel1.setFaktura(faktura);
                }
            }
        };
        dodajFaktureSprzedazyPanel1.getUsunTowarButton().addActionListener(actionListener);
    }

    private void WyszukajKontrahentaDoFakturySprzedazy() {

        actionListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (dodajFaktureSprzedazyPanel1.validWyszukaj()) {
                    dodajFaktureSprzedazyPanel1.setListaKontrahentow(listaKontrahentow.znajdzKontrahenta(dodajFaktureSprzedazyPanel1.getKontrahentSzukaj()));
                } else {
                    jLabel1.setText("Błąd! Popraw oznaczone pola.");
                }
            }
        };
        dodajFaktureSprzedazyPanel1.getSzukajKontrahentaButton().addActionListener(actionListener);
    }

    private void DodajKontrahentaDoFakturySprzedazy() {

        actionListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (dodajFaktureSprzedazyPanel1.getSelectedKontrahent() != null) {
                    FakturaSprzedazyModel faktura = dodajFaktureSprzedazyPanel1.getFaktura();
                    jLabel1.setText(fakturaSprzedazy.dodajKontrahentaDoFaktury(faktura, dodajFaktureSprzedazyPanel1.getSelectedKontrahent()));
                    dodajFaktureSprzedazyPanel1.setFaktura(faktura);
                } else {
                    jLabel1.setText("Błąd! Wybierz kontrahenta.");
                }
            }
        };
        dodajFaktureSprzedazyPanel1.getDodajKontrahentaDoFakturyButton().addActionListener(actionListener);
    }

    private void DodajKontrahentaDoFakturyIBazySprzedazy() {

        actionListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (dodajFaktureSprzedazyPanel1.validDodaj()) {
                    listaKontrahentow.dodajKontrahenta(dodajFaktureSprzedazyPanel1.getKontrahentDodaj());
                    FakturaSprzedazyModel faktura = dodajFaktureSprzedazyPanel1.getFaktura();
                    jLabel1.setText(fakturaSprzedazy.dodajKontrahentaDoFaktury(faktura, listaKontrahentow.znajdzKontrahenta(dodajFaktureSprzedazyPanel1.getKontrahentDodaj()).get(0)));
                    dodajFaktureSprzedazyPanel1.setFaktura(faktura);
                } else {
                    jLabel1.setText("Błąd! Popraw oznaczone pola.");
                }
            }
        };
        dodajFaktureSprzedazyPanel1.getDodajKontrahentaDoFakturyIBazyButton().addActionListener(actionListener);
    }

    private void FinalizujFaktureSprzedazy() {

        actionListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (dodajFaktureSprzedazyPanel1.validFaktura()) {
                    FakturaSprzedazyModel faktura = dodajFaktureSprzedazyPanel1.getFaktura();
                    faktura.setFakturaOsobaWystawiajacaID(logowanie.getUzytkownik().getUzytkownikID());
                    faktura.setFakturaOsobaWystawiajacaImie(logowanie.getUzytkownik().getUzytkownikImie());
                    faktura.setFakturaOsobaWystawiajacaNazwisko(logowanie.getUzytkownik().getUzytkownikNazwisko());

                    jLabel1.setText(fakturaSprzedazy.dodajFaktureSprzedazy(faktura));
                    setEnableFakturaMenuPanel(true);
                    //tutaj dodac rejestracje faktur do modulu ksiegowosc
                } else {
                    jLabel1.setText("Błąd! Popraw oznaczone pola, dodaj towary, dodaj kontrahenta.");
                }
            }
        };
        dodajFaktureSprzedazyPanel1.getFinalizujButton().addActionListener(actionListener);
    }

    private void AnulujFaktureSprzedazy() {

        actionListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                dodajFaktureSprzedazyPanel1.setVisible(false);
                dodajFaktureSprzedazyPanel1.resetComponentState();
                setEnableFakturaMenuPanel(true);
            }
        };
        dodajFaktureSprzedazyPanel1.getAnulujButton().addActionListener(actionListener);
    }

    private void DoPlikuFaktureSprzedazy() {

        actionListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                fakturaSprzedazy.doPlikuFaktureSprzedazy(dodajFaktureSprzedazyPanel1.getFaktura(), "EPUB");
            }
        };
        dodajFaktureSprzedazyPanel1.getZrzutDoPlikuButton().addActionListener(actionListener);
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Faktura zakupu">
    private void DodajTowarDoFakturyZakupu() {

        actionListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (dodajFaktureZakupuPanel1.valid()) {
                    List<TowarModel> lista = listaTowarow.znajdzTowar(dodajFaktureZakupuPanel1.getTowarDoSzukania());
                    if (!lista.isEmpty()) {
                        TowarModel towar = lista.get(0);
                        towar.setTowarIlosc(dodajFaktureZakupuPanel1.getIlosc());
                        FakturaZakupuModel faktura = dodajFaktureZakupuPanel1.getFaktura();
                        jLabel1.setText(fakturaZakupu.dodajTowarDoFaktury(faktura, towar));
                        dodajFaktureZakupuPanel1.setFaktura(faktura);
                    } else {
                        jLabel1.setText("Błąd! Towaru nie ma w bazie.");
                    }
                } else {
                    jLabel1.setText("Błąd! Popraw oznaczone pola.");
                }
            }
        };
        dodajFaktureZakupuPanel1.getDodajTowarButton().addActionListener(actionListener);
    }

    private void UsunTowarDoFakturyZakupu() {

        actionListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                TowarModel towar = dodajFaktureZakupuPanel1.getSelectedTowarT1();
                if (towar != null) {
                    FakturaZakupuModel faktura = dodajFaktureZakupuPanel1.getFaktura();
                    jLabel1.setText(fakturaZakupu.usunTowarZFaktury(faktura, towar));
                    dodajFaktureZakupuPanel1.setFaktura(faktura);
                }
            }
        };
        dodajFaktureZakupuPanel1.getUsunTowarButton().addActionListener(actionListener);
    }

    private void DodajTowarDoBazyIFakturyZakupu() {

        actionListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (dodajFaktureZakupuPanel1.validTowarDoDodania()) {
                    TowarModel towar = dodajFaktureZakupuPanel1.getTowarDoDodania();
                    FakturaZakupuModel faktura = dodajFaktureZakupuPanel1.getFaktura();
                    jLabel1.setText(fakturaZakupu.dodajTowarDoFaktury(faktura, towar));
                    dodajFaktureZakupuPanel1.setFaktura(faktura);
                } else {
                    jLabel1.setText("Błąd! Popraw oznaczone pola.");
                }
            }
        };
        dodajFaktureZakupuPanel1.getDodajTowarDoBazyIFakturyButton().addActionListener(actionListener);
    }

    private void UsunTowarDoBazyIFakturyZakupu() {

        actionListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                TowarModel towar = dodajFaktureZakupuPanel1.getSelectedTowarT2();
                if (towar != null) {
                    FakturaZakupuModel faktura = dodajFaktureZakupuPanel1.getFaktura();
                    jLabel1.setText(fakturaZakupu.usunTowarZFaktury(faktura, towar));
                    dodajFaktureZakupuPanel1.setFaktura(faktura);
                }
            }
        };
        dodajFaktureZakupuPanel1.getUsunTowarDoBazyIFakturyButton().addActionListener(actionListener);
    }

    private void WyszukajKontrahentaDoFakturyZakupu() {

        actionListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (dodajFaktureZakupuPanel1.validWyszukaj()) {
                    dodajFaktureZakupuPanel1.setListaKontrahentow(listaKontrahentow.znajdzKontrahenta(dodajFaktureZakupuPanel1.getKontrahentSzukaj()));
                } else {
                    jLabel1.setText("Błąd! Popraw oznaczone pola.");
                }
            }
        };
        dodajFaktureZakupuPanel1.getSzukajKontrahentaButton().addActionListener(actionListener);
    }

    private void DodajKontrahentaDoFakturyZakupu() {

        actionListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (dodajFaktureZakupuPanel1.getSelectedKontrahent() != null) {
                    FakturaZakupuModel faktura = dodajFaktureZakupuPanel1.getFaktura();
                    jLabel1.setText(fakturaZakupu.dodajKontrahenta(faktura, dodajFaktureZakupuPanel1.getSelectedKontrahent()));
                    dodajFaktureZakupuPanel1.setFaktura(faktura);
                } else {
                    jLabel1.setText("Błąd! Wybierz kontrahenta.");
                }
            }
        };
        dodajFaktureZakupuPanel1.getDodajKontrahentaDoFakturyButton().addActionListener(actionListener);
    }

    private void DodajKontrahentaDoFakturyZakupuIBazy() {

        actionListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (dodajFaktureZakupuPanel1.validDodaj()) {
                    listaKontrahentow.dodajKontrahenta(dodajFaktureZakupuPanel1.getKontrahentDodaj());
                    FakturaZakupuModel faktura = dodajFaktureZakupuPanel1.getFaktura();
                    jLabel1.setText(fakturaZakupu.dodajKontrahenta(faktura, listaKontrahentow.znajdzKontrahenta(dodajFaktureZakupuPanel1.getKontrahentDodaj()).get(0)));
                    dodajFaktureZakupuPanel1.setFaktura(faktura);
                } else {
                    jLabel1.setText("Błąd! Popraw oznaczone pola.");
                }
            }
        };
        dodajFaktureZakupuPanel1.getDodajKontrahentaDoFakturyIBazyButton().addActionListener(actionListener);
    }

    private void FinalizujFaktureZakupu() {

        actionListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (dodajFaktureZakupuPanel1.validFaktura()) {
                    FakturaZakupuModel faktura = dodajFaktureZakupuPanel1.getFaktura();
                    jLabel1.setText(fakturaZakupu.dodajFaktureZakupu(faktura));
                    setEnableFakturaMenuPanel(true);
                    //tutaj dodac rejestracje faktur do modulu ksiegowosc
                } else {
                    jLabel1.setText("Błąd! Popraw oznaczone pola, dodaj towary, dodaj kontrahenta.");
                }
            }
        };
        dodajFaktureZakupuPanel1.getFinalizujButton().addActionListener(actionListener);
    }

    private void AnulujFaktureZakupu() {

        actionListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                dodajFaktureZakupuPanel1.setVisible(false);
                dodajFaktureZakupuPanel1.resetComponentState();
                setEnableFakturaMenuPanel(true);
            }
        };
        dodajFaktureZakupuPanel1.getAnulujButton().addActionListener(actionListener);
    }

    private void DoPlikuFaktureZakupu() {

        actionListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                fakturaZakupu.doPlikuFaktureZakupu(dodajFaktureZakupuPanel1.getFaktura(), "EPUB");
            }
        };
        dodajFaktureZakupuPanel1.getZrzutDoPlikuButton().addActionListener(actionListener);
    }
     // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Historia faktur"> 
    private void ZnajdzFakturePoDacie() {

        actionListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (znajdzFakturePanel1.validDate()) {
                    znajdzFakturePanel1.setVisible(false);
                    if (znajdzFakturePanel1.getRodzajFaktury() == RodzajFaktury.FAKTURA_SPRZEDAZY) {
                        wynikZnajdzFaktureSprzedazyPanel1.setListaFaktur(historiaFaktur.znajdzFaktureSprzedazyPoDacie(znajdzFakturePanel1.getOkresPoczatek(), znajdzFakturePanel1.getOkresKoniec()));
                        wynikZnajdzFaktureSprzedazyPanel1.setVisible(true);
                    } else if (znajdzFakturePanel1.getRodzajFaktury() == RodzajFaktury.FAKTURA_ZAKUPU) {
                        wynikZnajdzFaktureZakupuPanel1.setListaFaktur(historiaFaktur.znajdzFaktureZakupuPoDacie(znajdzFakturePanel1.getOkresPoczatek(), znajdzFakturePanel1.getOkresKoniec()));
                        wynikZnajdzFaktureZakupuPanel1.setVisible(true);
                    }
                } else {
                    jLabel1.setText("Błąd! Popraw pola okresu.");
                }
            }
        };
        znajdzFakturePanel1.getZnajdzDataButton().addActionListener(actionListener);
    }

    private void ZnajdzFakturePoID() {

        actionListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                znajdzFakturePanel1.setVisible(false);
                if (znajdzFakturePanel1.getRodzajFaktury() == RodzajFaktury.FAKTURA_SPRZEDAZY) {
                    wynikZnajdzFaktureSprzedazyPanel1.setListaFaktur(historiaFaktur.znajdzFaktureSprzedazyPoNumerzeFaktury(znajdzFakturePanel1.getIDFaktury()));
                    wynikZnajdzFaktureSprzedazyPanel1.setVisible(true);
                } else if (znajdzFakturePanel1.getRodzajFaktury() == RodzajFaktury.FAKTURA_ZAKUPU) {
                    wynikZnajdzFaktureZakupuPanel1.setListaFaktur(historiaFaktur.znajdzFaktureZakupuPoNumerzeFaktury(znajdzFakturePanel1.getIDFaktury()));
                    wynikZnajdzFaktureZakupuPanel1.setVisible(true);
                }
            }
        };
        znajdzFakturePanel1.getZnajdzIDFakturaButton().addActionListener(actionListener);
    }

    private void ZnajdzFakturePoIDKontrahenta() {

        actionListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                znajdzFakturePanel1.setVisible(false);
                if (znajdzFakturePanel1.getRodzajFaktury() == RodzajFaktury.FAKTURA_SPRZEDAZY) {
                    wynikZnajdzFaktureSprzedazyPanel1.setListaFaktur(historiaFaktur.znajdzFaktureSprzedazyPoKontrahencie(znajdzFakturePanel1.getIDKontrahenta()));
                    wynikZnajdzFaktureSprzedazyPanel1.setVisible(true);
                } else if (znajdzFakturePanel1.getRodzajFaktury() == RodzajFaktury.FAKTURA_ZAKUPU) {
                    wynikZnajdzFaktureZakupuPanel1.setListaFaktur(historiaFaktur.znajdzFaktureZakupuPoKontrahencie(znajdzFakturePanel1.getIDKontrahenta()));
                    wynikZnajdzFaktureZakupuPanel1.setVisible(true);
                }
            }
        };
        znajdzFakturePanel1.getZnajdzIDKontrahentButton().addActionListener(actionListener);
    }

    private void DoPlikuFaktureSprzedazyH() {

        actionListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                fakturaSprzedazy.doPlikuFaktureSprzedazy(wynikZnajdzFaktureSprzedazyPanel1.getFaktura(), "EPUB");
                //do zmiany gdy bedzie komponent
            }
        };
        wynikZnajdzFaktureSprzedazyPanel1.getZrzutDoPlikuButton().addActionListener(actionListener);
    }

    private void DoPlikuFaktureZakupuH() {

        actionListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                fakturaZakupu.doPlikuFaktureZakupu(wynikZnajdzFaktureZakupuPanel1.getFaktura(), "EPUB");
                //do zmiany gdy bedzie komponent
            }
        };
        wynikZnajdzFaktureZakupuPanel1.getZrzutDoPlikuButton().addActionListener(actionListener);
    }
    // </editor-fold> 

    // <editor-fold defaultstate="collapsed" desc="Kontrahent">
    private void DodajKontrahenta() {

        actionListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (dodajKontrahentaPanel1.valid()) {
                    jLabel1.setText(listaKontrahentow.dodajKontrahenta(dodajKontrahentaPanel1.getKontrahent()));
                } else {
                    jLabel1.setText("Błąd! Popraw oznaczone pola.");
                }
            }
        };
        dodajKontrahentaPanel1.getDodajButton().addActionListener(actionListener);
    }

    private void ZnajdzKontrahenta() {

        actionListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (znajdzKontrahentaPanel1.valid()) {
                    wynikZnajdzKontrahentaPanel1.setListaKontrahentow(listaKontrahentow.znajdzKontrahenta(znajdzKontrahentaPanel1.getKontrahent()));
                    znajdzKontrahentaPanel1.setVisible(false);
                    wynikZnajdzKontrahentaPanel1.setVisible(true);
                } else {
                    jLabel1.setText("Błąd! Popraw oznaczone pola.");
                }
            }
        };
        znajdzKontrahentaPanel1.getZnajdzButton().addActionListener(actionListener);
    }

    private void EdytujKontrahenta() {

        actionListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (wynikZnajdzKontrahentaPanel1.valid()) {
                    jLabel1.setText(listaKontrahentow.edytujKontrahenta(wynikZnajdzKontrahentaPanel1.getSelectedKontrahent(), wynikZnajdzKontrahentaPanel1.getKontrahent()));
                    if (jLabel1.getText() == "Kontrahent został pomyślnie zaktualizowany.") {
                        wynikZnajdzKontrahentaPanel1.setSelectedKontrahent(wynikZnajdzKontrahentaPanel1.getKontrahent());
                    }
                } else {
                    jLabel1.setText("Błąd! Popraw oznaczone pola.");
                }
            }
        };
        wynikZnajdzKontrahentaPanel1.getZapiszButton().addActionListener(actionListener);
    }

    private void UsunKontrahenta() {

        actionListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                jLabel1.setText(listaKontrahentow.usunKontrahenta(wynikZnajdzKontrahentaPanel1.getSelectedKontrahent()));
                if (jLabel1.getText() == "Kontrahent został pomyślnie usunięty z bazy danych.") {
                    wynikZnajdzKontrahentaPanel1.removeSelectedKontrahent();
                }
            }
        };
        wynikZnajdzKontrahentaPanel1.getUsunButton().addActionListener(actionListener);
    }
    // </editor-fold> 
    // </editor-fold> 

    // <editor-fold defaultstate="collapsed" desc="MAGAZYN"> 
    // <editor-fold defaultstate="collapsed" desc="Towary"> 
    private void DodajTowar() {

        actionListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (dodajTowarPanel1.valid()) {
                    jLabel1.setText(listaTowarow.dodajTowar(dodajTowarPanel1.getTowar()));
                } else {
                    jLabel1.setText("Błąd! Popraw oznaczone pola.");
                }
            }
        };
        dodajTowarPanel1.getDodajButton().addActionListener(actionListener);
    }

    private void ZnajdzTowar() {

        actionListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (znajdzTowarPanel1.valid()) {
                    wynikZnajdzTowarPanel1.setListaTowarow(listaTowarow.znajdzTowar(znajdzTowarPanel1.getTowar()));
                    znajdzTowarPanel1.setVisible(false);
                    wynikZnajdzTowarPanel1.setVisible(true);
                } else {
                    jLabel1.setText("Błąd! Popraw oznaczone pola.");
                }
            }
        };
        znajdzTowarPanel1.getDodajButton().addActionListener(actionListener);
    }

    private void EdytujTowar() {

        actionListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (wynikZnajdzTowarPanel1.valid()) {
                    jLabel1.setText(listaTowarow.edytujTowar(wynikZnajdzTowarPanel1.getSelectedTowar(), wynikZnajdzTowarPanel1.getTowar()));
                    if (jLabel1.getText() == "Towar został pomyślnie zaktualizowany.") {
                        wynikZnajdzTowarPanel1.setSelectedTowar(wynikZnajdzTowarPanel1.getTowar());
                    }
                } else {
                    jLabel1.setText("Błąd! Popraw oznaczone pola.");
                }
            }
        };
        wynikZnajdzTowarPanel1.getZapiszButton().addActionListener(actionListener);
    }

    private void UsunTowar() {

        actionListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                jLabel1.setText(listaTowarow.usunTowar(wynikZnajdzTowarPanel1.getSelectedTowar()));
                wynikZnajdzTowarPanel1.removeSelectedTowar();
            }
        };
        wynikZnajdzTowarPanel1.getUsunButton().addActionListener(actionListener);
    }

    private void ZmienVAT() {

        actionListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                jLabel1.setText(listaTowarow.zmienVAT(wynikZnajdzTowarPanel1.getListaTowarow(), wynikZnajdzTowarPanel1.getVAT()));
                wynikZnajdzTowarPanel1.setListaTowarow(listaTowarow.znajdzTowar(znajdzTowarPanel1.getTowar()));
            }
        };
        wynikZnajdzTowarPanel1.getVATButton().addActionListener(actionListener);
    }

    private void ZmienRabat() {

        actionListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                jLabel1.setText(listaTowarow.zmienRabat(wynikZnajdzTowarPanel1.getListaTowarow(), wynikZnajdzTowarPanel1.getRabat()));
                wynikZnajdzTowarPanel1.setListaTowarow(listaTowarow.znajdzTowar(znajdzTowarPanel1.getTowar()));
            }
        };
        wynikZnajdzTowarPanel1.getRabatButton().addActionListener(actionListener);
    }
    // </editor-fold> 

    // <editor-fold defaultstate="collapsed" desc="Raporty"> 
    private void ZrzutDoPlikuRaport() {

        actionListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (raportPanel1.getRodzajRaportu() == RodzajRaportu.TOWARY_BLISKIE_PRZETERMINOWANIA) {
                    raportyProste.doPlikuRaport(raportPanel1.getListaTowarow(), "Towary bliskie przeterminowania", raportWybierzMiesiacPanel1.getData(), "EPUB");
                } else if (raportPanel1.getRodzajRaportu() == RodzajRaportu.TOWARY_PRZETERMINOWANE) {
                    raportyProste.doPlikuRaport(raportPanel1.getListaTowarow(), "Towary przeterminowane", raportWybierzMiesiacPanel1.getData(), "EPUB");
                } else if (raportPanel1.getRodzajRaportu() == RodzajRaportu.TOWARY_BLISKIE_WYCZERPANIU) {
                    raportyProste.doPlikuRaport(raportPanel1.getListaTowarow(), "Towary bliskie wyczerpaniu", null, "EPUB");
                } else if (raportPanel1.getRodzajRaportu() == RodzajRaportu.TOWARY_WYCZERPANE) {
                    raportyProste.doPlikuRaport(raportPanel1.getListaTowarow(), "Towary wyczerpane", null, "EPUB");
                }
            }
        };
        raportPanel1.getZrzutDoPlikuButton().addActionListener(actionListener);
    }

    private void GenerujRaportZData() {

        actionListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (raportWybierzMiesiacPanel1.validDate()) {
                    if (raportPanel1.getRodzajRaportu() == RodzajRaportu.TOWARY_BLISKIE_PRZETERMINOWANIA) {
                        raportPanel1.setListaTowarow(raportyProste.raportProduktyBliskiePrzeterminowania(raportWybierzMiesiacPanel1.getData()));
                    } else {
                        raportPanel1.setListaTowarow(raportyProste.raportProduktyPrzeterminowane(raportWybierzMiesiacPanel1.getData()));
                    }
                } else {
                    jLabel1.setText("Błąd! Popraw oznaczone pola.");
                }
            }
        };
        raportWybierzMiesiacPanel1.getGenerujButton().addActionListener(actionListener);
    }
     // </editor-fold> 

    // <editor-fold defaultstate="collapsed" desc="Inwentaryzacja"> 
    private void DodajTowarDoSpisu() {

        actionListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (dodajSpisZNaturyPanel1.validTowar()) {
                    SpisZNaturyModel spis = dodajSpisZNaturyPanel1.getSpisZNatury();
                    jLabel1.setText(inwentaryzacja.dodajTowarDoSpisu(spis, dodajSpisZNaturyPanel1.getTowar()));
                    dodajSpisZNaturyPanel1.setSpisZNatury(spis);
                } else {
                    jLabel1.setText("Błąd! Popraw oznaczone pola.");
                }
            }
        };
        dodajSpisZNaturyPanel1.getDodajTowarButton().addActionListener(actionListener);
    }

    private void UsunTowarZeSpisu() {

        actionListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                TowarModel towar = dodajSpisZNaturyPanel1.getTowar();
                if (towar != null) {
                    SpisZNaturyModel spis = dodajSpisZNaturyPanel1.getSpisZNatury();
                    jLabel1.setText(inwentaryzacja.usunTowarZeSpisu(spis, dodajSpisZNaturyPanel1.getSelectedTowar()));
                    dodajSpisZNaturyPanel1.setSpisZNatury(spis);
                }
            }
        };
        dodajSpisZNaturyPanel1.getUsunTowarButton().addActionListener(actionListener);
    }

    private void DodajSpis() {

        actionListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (dodajSpisZNaturyPanel1.validSpis()) {
                    jLabel1.setText(inwentaryzacja.dodajSpisZNatury(dodajSpisZNaturyPanel1.getSpisZNatury()));
                    setEnableMagazynMenuPanel(true);
                } else {
                    jLabel1.setText("Błąd! Popraw oznaczone pola, dodaj towary.");
                }
            }
        };
        dodajSpisZNaturyPanel1.getDodajSpisButton().addActionListener(actionListener);
    }

    private void AnulujSpis() {

        actionListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                dodajSpisZNaturyPanel1.setVisible(false);
                dodajSpisZNaturyPanel1.resetComponentState();
                setEnableMagazynMenuPanel(true);
            }
        };
        dodajSpisZNaturyPanel1.getAnulujSpisButton().addActionListener(actionListener);
    }

    private void DoPlikuSpis() {

        actionListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                inwentaryzacja.doPlikuSpisZNatury(dodajSpisZNaturyPanel1.getSpisZNatury(), "EPUB");
            }
        };
        dodajSpisZNaturyPanel1.getZrzutDoPlikuButton().addActionListener(actionListener);
    }

    private void ZnajdzSpisPoDacie() {

        actionListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (znajdzSpisZNaturyPanel1.validDate()) {
                    znajdzSpisZNaturyPanel1.setVisible(false);
                    wynikZnajdzSpisZNaturyPanel1.setListaSpisow(inwentaryzacja.znajdzSpisZNatury(znajdzSpisZNaturyPanel1.getOkresPoczatek(), znajdzSpisZNaturyPanel1.getOkresKoniec()));
                    wynikZnajdzSpisZNaturyPanel1.setVisible(true);
                } else {
                    jLabel1.setText("Błąd! Popraw pola okresu.");
                }
            }
        };
        znajdzSpisZNaturyPanel1.getZnajdzDataButton().addActionListener(actionListener);
    }

    private void ZnajdzSpisPoOsobie() {

        actionListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (znajdzSpisZNaturyPanel1.validOsoba()) {
                    znajdzSpisZNaturyPanel1.setVisible(false);
                    wynikZnajdzSpisZNaturyPanel1.setListaSpisow(inwentaryzacja.znajdzSpisZNatury(znajdzSpisZNaturyPanel1.getSpisZOsoba()));
                    wynikZnajdzSpisZNaturyPanel1.setVisible(true);
                } else {
                    jLabel1.setText("Błąd! Popraw pola okresu.");
                }
            }
        };
        znajdzSpisZNaturyPanel1.getZnajdzOsobaButton().addActionListener(actionListener);
    }

    private void DoPlikuSpisH() {

        actionListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                inwentaryzacja.doPlikuSpisZNatury(wynikZnajdzSpisZNaturyPanel1.getSpis(), "EPUB");
            }
        };
        wynikZnajdzSpisZNaturyPanel1.getZrzutDoPlikuButton().addActionListener(actionListener);
    }

    private void GenerujNadwyzki() {

        actionListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                DokumentNadwyzekNiedoborowModel dokument = new DokumentNadwyzekNiedoborowModel();
                dokument.setDokumentNadwyzekNiedoborowRodzaj(RodzajDokumentu.Nadwyzki);
                jLabel1.setText(inwentaryzacja.generujDokumentNadwyzek(dokument, wynikZnajdzSpisZNaturyPanel1.getSpis()));
                wygenerowaneNadwyzkiNiedobory1.setDokument(dokument);
                wynikZnajdzSpisZNaturyPanel1.setVisible(false);
                wygenerowaneNadwyzkiNiedobory1.setVisible(true);
            }
        };
        wynikZnajdzSpisZNaturyPanel1.getGenerujNadwyzkiButton().addActionListener(actionListener);
    }

    private void GenerujNiedobory() {

        actionListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                DokumentNadwyzekNiedoborowModel dokument = new DokumentNadwyzekNiedoborowModel();
                dokument.setDokumentNadwyzekNiedoborowRodzaj(RodzajDokumentu.Niedobor);
                jLabel1.setText(inwentaryzacja.generujDokumentNiedoborow(dokument, wynikZnajdzSpisZNaturyPanel1.getSpis()));
                wygenerowaneNadwyzkiNiedobory1.setDokument(dokument);
                wynikZnajdzSpisZNaturyPanel1.setVisible(false);
                wygenerowaneNadwyzkiNiedobory1.setVisible(true);
            }
        };
        wynikZnajdzSpisZNaturyPanel1.getGenerujNiedoboryButton().addActionListener(actionListener);
    }

    private void DoPlikuDokument() {

        actionListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                inwentaryzacja.doPlikuDokument(wygenerowaneNadwyzkiNiedobory1.getDokument(), "EPUB");
            }
        };
        wygenerowaneNadwyzkiNiedobory1.getZrzutDoPlikuButton().addActionListener(actionListener);
    }

    private void CofnijDoWyszukania() {

        actionListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                wynikZnajdzSpisZNaturyPanel1.setVisible(true);
                wygenerowaneNadwyzkiNiedobory1.setVisible(false);
            }
        };
        wygenerowaneNadwyzkiNiedobory1.getWrocButton().addActionListener(actionListener);
    }
     // </editor-fold> 
    // </editor-fold> 

    // <editor-fold defaultstate="collapsed" desc="KONFIGURACJA"> 
    private void DodajUzytkownika() {

        actionListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (dodajUzytkownikaPanel1.valid()) {
                    jLabel1.setText(konfiguracja.dodajUzytkownika(dodajUzytkownikaPanel1.getUzytkownik()));
                } else {
                    jLabel1.setText("Błąd! Popraw oznaczone pola.");
                }
            }
        };
        dodajUzytkownikaPanel1.getDodajButton().addActionListener(actionListener);
    }

    private void EdytujUzytkownika() {

        actionListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (przegladajUzytkownikow1.valid()) {
                    jLabel1.setText(konfiguracja.edytujUzytkownika(przegladajUzytkownikow1.getSelectedUzytkownik(), przegladajUzytkownikow1.getUzytkownik()));
                    if (jLabel1.getText() == "Użytkownik został pomyślnie zaktualizowany.") {
                        przegladajUzytkownikow1.setSelectedUzytkownik(przegladajUzytkownikow1.getUzytkownik());
                    }

                } else {
                    jLabel1.setText("Błąd! Popraw oznaczone pola.");
                }
            }
        };
        przegladajUzytkownikow1.getZapiszButton().addActionListener(actionListener);
    }

    //pilna zmiana - potrzebny id uzytkownika aktualnie zalogowanego
    private void UsunUzytkownika() {

        actionListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                jLabel1.setText(konfiguracja.usunUzytkownika(przegladajUzytkownikow1.getSelectedUzytkownik(), logowanie.getUzytkownik().getUzytkownikID()));
                if (jLabel1.getText() == "Użytkownik został pomyślnie usunięty z bazy danych.") {
                    przegladajUzytkownikow1.removeSelectedUzytkownik();
                }
            }
        };
        przegladajUzytkownikow1.getUsunButton().addActionListener(actionListener);
    }

    private void ZapiszPrzedsiebiorstwo() {

        actionListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (zmienDanePanel1.valid()) {
                    jLabel1.setText(konfiguracja.zmienDaneFirmy(zmienDanePanel1.getPrzedsiebiorstwo()));
                } else {
                    jLabel1.setText("Błąd! Popraw oznaczone pola.");
                }
            }
        };
        zmienDanePanel1.getDodajButton().addActionListener(actionListener);
    }
    // </editor-fold> 
    // </editor-fold> 

    // <editor-fold defaultstate="collapsed" desc="InstActionListenersMethod"> 
    private void InstActionListenersMethod() {
        //Faktura
        this.DodajTowarDoFakturySprzedazy();
        this.UsunTowarDoFakturySprzedazy();
        this.WyszukajKontrahentaDoFakturySprzedazy();
        this.DodajKontrahentaDoFakturySprzedazy();
        this.DodajKontrahentaDoFakturyIBazySprzedazy();
        this.FinalizujFaktureSprzedazy();
        this.AnulujFaktureSprzedazy();
        this.DoPlikuFaktureSprzedazy();

        this.DodajTowarDoBazyIFakturyZakupu();
        this.UsunTowarDoBazyIFakturyZakupu();
        this.DodajTowarDoFakturyZakupu();
        this.UsunTowarDoFakturyZakupu();
        this.WyszukajKontrahentaDoFakturyZakupu();
        this.DodajKontrahentaDoFakturyZakupu();
        this.DodajKontrahentaDoFakturyZakupuIBazy();
        this.FinalizujFaktureZakupu();
        this.AnulujFaktureZakupu();
        this.DoPlikuFaktureZakupu();

        this.ZnajdzFakturePoDacie();
        this.ZnajdzFakturePoID();
        this.ZnajdzFakturePoIDKontrahenta();
        this.DoPlikuFaktureSprzedazyH();
        this.DoPlikuFaktureZakupuH();

        this.DodajKontrahenta();
        this.ZnajdzKontrahenta();
        this.EdytujKontrahenta();
        this.UsunKontrahenta();

        //Magazyn
        this.DodajTowar();
        this.ZnajdzTowar();
        this.EdytujTowar();
        this.UsunTowar();
        this.ZmienVAT();
        this.ZmienRabat();

        this.ZrzutDoPlikuRaport();
        this.GenerujRaportZData();

        this.DodajTowarDoSpisu();
        this.UsunTowarZeSpisu();
        this.DodajSpis();
        this.AnulujSpis();
        this.DoPlikuSpis();
        this.ZnajdzSpisPoDacie();
        this.ZnajdzSpisPoOsobie();
        this.DoPlikuSpisH();
        this.GenerujNadwyzki();
        this.GenerujNiedobory();
        this.DoPlikuDokument();
        this.CofnijDoWyszukania();

        //Konfiguracja
        this.DodajUzytkownika();
        this.EdytujUzytkownika();
        this.UsunUzytkownika();
        this.ZapiszPrzedsiebiorstwo();
    }
    // </editor-fold> 

    // <editor-fold defaultstate="collapsed" desc="HideMethods"> 
    private void HideFaktura() {
        this.jLabel1.setText(null);
        this.dodajKontrahentaPanel1.setVisible(false);
        this.znajdzKontrahentaPanel1.setVisible(false);
        this.wynikZnajdzKontrahentaPanel1.setVisible(false);
        this.znajdzFakturePanel1.setVisible(false);
        this.wynikZnajdzFaktureSprzedazyPanel1.setVisible(false);
        this.wynikZnajdzFaktureZakupuPanel1.setVisible(false);
        this.dodajFaktureSprzedazyPanel1.setVisible(false);
        this.dodajFaktureZakupuPanel1.setVisible(false);
    }

    private void HideMagazyn() {
        this.jLabel1.setText(null);
        this.dodajTowarPanel1.setVisible(false);
        this.znajdzTowarPanel1.setVisible(false);
        this.wynikZnajdzTowarPanel1.setVisible(false);
        this.raportPanel1.setVisible(false);
        this.raportWybierzMiesiacPanel1.setVisible(false);
        this.dodajSpisZNaturyPanel1.setVisible(false);
        this.znajdzSpisZNaturyPanel1.setVisible(false);
        this.wynikZnajdzSpisZNaturyPanel1.setVisible(false);
        this.wygenerowaneNadwyzkiNiedobory1.setVisible(false);
    }

    private void HideKsiegowosc() {
        this.jLabel1.setText(null);

    }

    private void HideKonfiguracja() {
        this.jLabel1.setText(null);
        this.dodajUzytkownikaPanel1.setVisible(false);
        this.przegladajUzytkownikow1.setVisible(false);
        this.zmienDanePanel1.setVisible(false);
        this.pokażDanePanel1.setVisible(false);
    }
    // </editor-fold>  

    // <editor-fold defaultstate="collapsed" desc="EnableMethods"> 
    private void setEnableFakturaMenuPanel(boolean enabled) {
        jPanel6.setEnabled(enabled);
        jPanel7.setEnabled(enabled);
        jPanel8.setEnabled(enabled);
        jPanel9.setEnabled(enabled);
        jPanel10.setEnabled(enabled);
        jButton1.setEnabled(enabled);
        jButton2.setEnabled(enabled);
        jButton3.setEnabled(enabled);
        jButton4.setEnabled(enabled);
        jButton5.setEnabled(enabled);
        jButton6.setEnabled(enabled);
    }

    private void setEnableMagazynMenuPanel(boolean enabled) {
        jPanel11.setEnabled(enabled);
        jPanel12.setEnabled(enabled);
        jPanel13.setEnabled(enabled);
        jPanel15.setEnabled(enabled);
        jButton7.setEnabled(enabled);
        jButton8.setEnabled(enabled);
        jButton9.setEnabled(enabled);
        jButton10.setEnabled(enabled);
        jButton11.setEnabled(enabled);
        jButton14.setEnabled(enabled);
        jButton15.setEnabled(enabled);
        jButton18.setEnabled(enabled);
    }
    // </editor-fold>  
}
