package eRPFacade.View.Panel.Glowne.Uzytkownik;

import glowne.KonfiguracjaComponent.Model.Status;
import glowne.KonfiguracjaComponent.Model.UzytkownikModel;
import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;

/**
 * @author Jakub Dydo, Katarzyna Romanowska
 * @version 1.0.0.0
 */
public class PrzegladajUzytkownikow extends javax.swing.JPanel {

    // <editor-fold defaultstate="collapsed" desc="Constructor">
    public PrzegladajUzytkownikow() {
        initComponents();
        this.setVisible(false);
        setUpTable();
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Properties">
    public List<UzytkownikModel> getListaUzytkownikow() {
        return this.uzytkownikTableModel1.getListaUzytkownikow();
    }

    public void setListaUzytkownikow(List<UzytkownikModel> listaUzytkownikow) {
        this.uzytkownikTableModel1.setListaUzytkownikow(listaUzytkownikow);
    }

    public UzytkownikModel getUzytkownik() {
        return new UzytkownikModel(this.uzytkownikModel1);
    }

    public void setUzytkownik(UzytkownikModel uzytkownikModel) {
        this.uzytkownikModel1.setUzytkownikID(uzytkownikModel.getUzytkownikID());
        this.uzytkownikModel1.setUzytkownikHaslo(uzytkownikModel.getUzytkownikHaslo());
        this.uzytkownikModel1.setUzytkownikStatus(uzytkownikModel.getUzytkownikStatus());
        this.uzytkownikModel1.setUzytkownikImie(uzytkownikModel.getUzytkownikImie());
        this.uzytkownikModel1.setUzytkownikNazwisko(uzytkownikModel.getUzytkownikNazwisko());
        this.uzytkownikModel1.setUzytkownikLogin(uzytkownikModel.getUzytkownikLogin());
    }

    public UzytkownikModel getSelectedUzytkownik() {
        return this.uzytkownikTableModel1.getUzytkownik(jXTable1.getSelectedRow());
    }
    
    public void setSelectedUzytkownik(UzytkownikModel uzytkownikModel) {
        this.uzytkownikTableModel1.setUzytkownik(uzytkownikModel, jXTable1.getSelectedRow());
    }
    
    public void removeSelectedUzytkownik() {
        this.uzytkownikTableModel1.removeUzytkownik(jXTable1.getSelectedRow());
    }

    public javax.swing.JButton getZapiszButton() {
        return this.jButton1;
    }

    public javax.swing.JButton getUsunButton() {
        return this.jButton2;
    }
    // </editor-fold> 
    
    // <editor-fold defaultstate="collapsed" desc="JTable methods">
    private void setUpTable() {
        jXTable1.getTableHeader().setReorderingAllowed(false);
        jXTable1.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        jXTable1.setRowSorter(null);

        DefaultTableCellRenderer center = new DefaultTableCellRenderer();
        center.setHorizontalAlignment(DefaultTableCellRenderer.CENTER);

        for (TableColumn column : jXTable1.getColumns()) {
            column.setCellRenderer(center);
        }
        
        jXTable1.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent event) {
                if (jXTable1.getSelectedRow() == -1) {
                    setUzytkownik(new UzytkownikModel());
                    jButton1.setEnabled(false);
                    jButton2.setEnabled(false);
                } else {
                    UzytkownikModel uzytkownik = uzytkownikTableModel1.getUzytkownik(jXTable1.getSelectedRow());
                    setUzytkownik(uzytkownik);
                    jButton1.setEnabled(true);
                    jButton2.setEnabled(true);
                }
            }
        });
    }
    // </editor-fold> 

    // <editor-fold defaultstate="collapsed" desc="Validation"> 
   public boolean valid() {
        boolean validValue = true;

        if (!jTextField1.getText().isEmpty()) {
            jTextField1.setBackground(Color.white);
        } else {
            jTextField1.setBackground(Color.red);
            validValue = false;
        }

        if (!jTextField2.getText().isEmpty()) {
            jTextField2.setBackground(Color.white);
        } else {
            jTextField2.setBackground(Color.red);
            validValue = false;
        }

        if (!jTextField3.getText().isEmpty()) {
            jTextField3.setBackground(Color.white);
        } else {
            jTextField3.setBackground(Color.red);
            validValue = false;
        }

        if (!jTextField4.getText().isEmpty()) {
            jTextField4.setBackground(Color.white);
        } else {
            jTextField4.setBackground(Color.red);
            validValue = false;
        }

        if (jComboBox1.getSelectedItem() != null) {
            jComboBox1.setBackground(Color.white);
        } else {
            jComboBox1.setBackground(Color.red);
            validValue = false;
        }

        return validValue;
    }
    // </editor-fold>   

    // <editor-fold defaultstate="collapsed" desc="ResetComponentState method"> 
    public void resetComponentState() {
        this.setUzytkownik(new UzytkownikModel());
        jTextField1.setBackground(Color.white);
        jTextField2.setBackground(Color.white);
        jTextField3.setBackground(Color.white);
        jTextField4.setBackground(Color.white);
        jComboBox1.setBackground(Color.white);
        jButton1.setEnabled(false);
        jButton2.setEnabled(false);
        this.setListaUzytkownikow(new ArrayList<UzytkownikModel>());
    }
    // </editor-fold> 

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        bindingGroup = new org.jdesktop.beansbinding.BindingGroup();

        uzytkownikModel1 = new glowne.KonfiguracjaComponent.Model.UzytkownikModel();
        uzytkownikTableModel1 = new eRPFacade.View.TableModel.UzytkownikTableModel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jXTable1 = new org.jdesktop.swingx.JXTable();
        jPanel1 = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jComboBox1 = new javax.swing.JComboBox();
        jLabel1 = new javax.swing.JLabel();
        jTextField4 = new javax.swing.JTextField();
        jTextField3 = new javax.swing.JTextField();
        jTextField2 = new javax.swing.JTextField();
        jTextField1 = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();

        setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Zapisani użytkownicy", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 18))); // NOI18N
        setMinimumSize(new java.awt.Dimension(716, 448));
        setPreferredSize(new java.awt.Dimension(716, 448));

        jXTable1.setModel(uzytkownikTableModel1);
        jScrollPane1.setViewportView(jXTable1);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Podgląd użytkownika", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 15))); // NOI18N

        jLabel9.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel9.setText("Nazwisko");

        jLabel10.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel10.setText("Status");

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel3.setText("Hasło");

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel2.setText("Imię");

        jComboBox1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jComboBox1.setModel(new DefaultComboBoxModel(Status.values()));

        org.jdesktop.beansbinding.Binding binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, uzytkownikModel1, org.jdesktop.beansbinding.ELProperty.create("${uzytkownikStatus}"), jComboBox1, org.jdesktop.beansbinding.BeanProperty.create("selectedItem"));
        bindingGroup.addBinding(binding);

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel1.setText("Login");

        jTextField4.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, uzytkownikModel1, org.jdesktop.beansbinding.ELProperty.create("${uzytkownikNazwisko}"), jTextField4, org.jdesktop.beansbinding.BeanProperty.create("text"));
        bindingGroup.addBinding(binding);

        jTextField3.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, uzytkownikModel1, org.jdesktop.beansbinding.ELProperty.create("${uzytkownikImie}"), jTextField3, org.jdesktop.beansbinding.BeanProperty.create("text"));
        bindingGroup.addBinding(binding);

        jTextField2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, uzytkownikModel1, org.jdesktop.beansbinding.ELProperty.create("${uzytkownikHaslo}"), jTextField2, org.jdesktop.beansbinding.BeanProperty.create("text"));
        bindingGroup.addBinding(binding);

        jTextField1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jTextField1.setEnabled(false);

        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, uzytkownikModel1, org.jdesktop.beansbinding.ELProperty.create("${uzytkownikLogin}"), jTextField1, org.jdesktop.beansbinding.BeanProperty.create("text"));
        bindingGroup.addBinding(binding);

        jButton1.setText("Zapisz");
        jButton1.setEnabled(false);

        jButton2.setText("Usuń");
        jButton2.setEnabled(false);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(47, 47, 47)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel10)
                            .addComponent(jLabel9)
                            .addComponent(jLabel3)
                            .addComponent(jLabel1)
                            .addComponent(jLabel2))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jTextField1)
                            .addComponent(jTextField2)
                            .addComponent(jTextField3)
                            .addComponent(jTextField4)
                            .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(53, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jTextField1)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 39, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(47, 47, 47))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 333, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        bindingGroup.bind();
    }// </editor-fold>//GEN-END:initComponents

    // <editor-fold defaultstate="collapsed" desc="Variables declaration"> 
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JComboBox jComboBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField2;
    private javax.swing.JTextField jTextField3;
    private javax.swing.JTextField jTextField4;
    private org.jdesktop.swingx.JXTable jXTable1;
    private glowne.KonfiguracjaComponent.Model.UzytkownikModel uzytkownikModel1;
    private eRPFacade.View.TableModel.UzytkownikTableModel uzytkownikTableModel1;
    private org.jdesktop.beansbinding.BindingGroup bindingGroup;
    // End of variables declaration//GEN-END:variables
    // </editor-fold>
}
