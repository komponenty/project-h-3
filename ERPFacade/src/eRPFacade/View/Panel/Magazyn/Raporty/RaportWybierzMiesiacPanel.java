package eRPFacade.View.Panel.Magazyn.Raporty;

import java.awt.Color;
import java.util.Date;

/**
 * @author Jakub Dydo
 * @version 1.0.0.1
 */
public class RaportWybierzMiesiacPanel extends javax.swing.JPanel {

    // <editor-fold defaultstate="collapsed" desc="Field"> 
    private Date zatwierdzonaData;
    // </editor-fold>  
    
    // <editor-fold defaultstate="collapsed" desc="Constructor">  
    public RaportWybierzMiesiacPanel() {
        this.zatwierdzonaData = new Date();
        initComponents();
        this.setVisible(false);
    }
    // </editor-fold>  

    // <editor-fold defaultstate="collapsed" desc="Properties">  
    public Date getData() {
        //return this.jXDatePicker1.getDate();
        return zatwierdzonaData;
    }

    public javax.swing.JButton getGenerujButton() {
        return this.jButton1;
    }
    // </editor-fold>  

    // <editor-fold defaultstate="collapsed" desc="ResetComponentState method"> 
    public void resetComponentState() {
        this.jXDatePicker1.setDate(new Date());
    }
    // </editor-fold> 

    // <editor-fold defaultstate="collapsed" desc="Validation">
    public boolean validDate() {
        boolean validValue = true;

        if (jXDatePicker1.getDate() != null) {
            jXDatePicker1.getEditor().setBackground(Color.white);
        } else {
            jXDatePicker1.getEditor().setBackground(Color.red);
            validValue = false;
        }

        return validValue;
    }
    // </editor-fold> 
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton1 = new javax.swing.JButton();
        jXDatePicker1 = new org.jdesktop.swingx.JXDatePicker();
        jLabel3 = new javax.swing.JLabel();

        setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Wybierz datę", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 15))); // NOI18N

        jButton1.setText("Generuj raport");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel3.setText("Data");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(36, 36, 36)
                .addComponent(jLabel3)
                .addGap(18, 18, 18)
                .addComponent(jXDatePicker1, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(38, 38, 38)
                .addComponent(jButton1)
                .addContainerGap(21, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jXDatePicker1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(28, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        zatwierdzonaData = this.jXDatePicker1.getDate();
    }//GEN-LAST:event_jButton1ActionPerformed

    // <editor-fold defaultstate="collapsed" desc="Variables declaration">  
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel3;
    private org.jdesktop.swingx.JXDatePicker jXDatePicker1;
    // End of variables declaration//GEN-END:variables
    // </editor-fold>  
}
