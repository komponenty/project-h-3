package eRPFacade.View.Panel.Magazyn.Raporty;

import eRPFacade.View.Enum.RodzajRaportu;
import java.util.ArrayList;
import java.util.List;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;
import magazyn.ListaTowarowComponent.Model.TowarModel;

/**
 * @author Jakub Dydo
 * @version 1.0.0.0
 */
public class RaportPanel extends javax.swing.JPanel {

    // <editor-fold defaultstate="collapsed" desc="Fields">
    RodzajRaportu rodzajRaportu;
    // </editor-fold> 

    // <editor-fold defaultstate="collapsed" desc="Constructors">
    public RaportPanel() {
        initComponents();    
        this.setVisible(false);
    }
    // </editor-fold> 

    // <editor-fold defaultstate="collapsed" desc="Properties">
    public List<TowarModel> getListaTowarow() {
        return this.raportIloscTableModel1.getListaTowarow();
    }

    public void setListaTowarow(List<TowarModel> listaTowarow) {
        this.raportIloscTableModel1.setListaTowarow(listaTowarow);
    }

    public RodzajRaportu getRodzajRaportu() {
        return this.rodzajRaportu;
    }

    public void setRodzajRaportu(RodzajRaportu rodzajRaportu) {
        this.rodzajRaportu = rodzajRaportu;
        setUpTable();
        setUpBorderTitle();
    }
    
    public javax.swing.JButton getZrzutDoPlikuButton() {
        return this.jButton2;
    }
    // </editor-fold> 

    // <editor-fold defaultstate="collapsed" desc="JTable methods">
    private void setUpTable() {
        jXTable1.getTableHeader().setReorderingAllowed(false);

        DefaultTableCellRenderer center = new DefaultTableCellRenderer();
        center.setHorizontalAlignment(DefaultTableCellRenderer.CENTER);

        for (TableColumn column : jXTable1.getColumns()) {
            column.setCellRenderer(center);
        }

        if (this.rodzajRaportu == RodzajRaportu.TOWARY_WYCZERPANE
                || this.rodzajRaportu == RodzajRaportu.TOWARY_BLISKIE_WYCZERPANIU) {
            jXTable1.getColumnExt("Ilość").setVisible(true);
            jXTable1.getColumnExt("Data ważności").setVisible(false);
        } else if (this.rodzajRaportu == RodzajRaportu.TOWARY_PRZETERMINOWANE
                || this.rodzajRaportu == RodzajRaportu.TOWARY_BLISKIE_PRZETERMINOWANIA) {
            jXTable1.getColumnExt("Data ważności").setVisible(true);
            jXTable1.getColumnExt("Ilość").setVisible(false);
        }

    }
    // </editor-fold> 

    // <editor-fold defaultstate="collapsed" desc="Border title method">
    private void setUpBorderTitle() {
        if (this.rodzajRaportu == RodzajRaportu.TOWARY_WYCZERPANE) {
            ((TitledBorder) this.getBorder()).setTitle("Raport - towary wyczerpane");
        } else if (this.rodzajRaportu == RodzajRaportu.TOWARY_BLISKIE_WYCZERPANIU) {
            ((TitledBorder) this.getBorder()).setTitle("Raport - towary bliskie wyczerpaniu");
        } else if (this.rodzajRaportu == RodzajRaportu.TOWARY_PRZETERMINOWANE) {
            ((TitledBorder) this.getBorder()).setTitle("Raport - towary przeterminowane");
        } else if (this.rodzajRaportu == RodzajRaportu.TOWARY_BLISKIE_PRZETERMINOWANIA) {
            ((TitledBorder) this.getBorder()).setTitle("Raport - towary bliskie przeterminowaniu");
        } else {
            ((TitledBorder) this.getBorder()).setTitle("Raport");
        }
    }
    // </editor-fold> 
    
    // <editor-fold defaultstate="collapsed" desc="ResetComponentState method"> 
    public void resetComponentState() {
        this.setListaTowarow(new ArrayList<TowarModel>());
        this.rodzajRaportu = null;
    }
    // </editor-fold> 

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        raportIloscTableModel1 = new eRPFacade.View.TableModel.RaportTableModel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jXTable1 = new org.jdesktop.swingx.JXTable();
        jButton2 = new javax.swing.JButton();

        setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Raport ilościowy", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 18))); // NOI18N
        setPreferredSize(new java.awt.Dimension(1341, 478));

        jXTable1.setModel(raportIloscTableModel1);
        jScrollPane1.setViewportView(jXTable1);

        jButton2.setText("Do pliku");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 785, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 264, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    // <editor-fold defaultstate="collapsed" desc="Variables declaration">
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton2;
    private javax.swing.JScrollPane jScrollPane1;
    private org.jdesktop.swingx.JXTable jXTable1;
    private eRPFacade.View.TableModel.RaportTableModel raportIloscTableModel1;
    // End of variables declaration//GEN-END:variables
    // </editor-fold> 
}
