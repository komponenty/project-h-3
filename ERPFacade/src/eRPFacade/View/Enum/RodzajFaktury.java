package eRPFacade.View.Enum;

/**
 * @author Jakub Dydo, Katarzyna Romanowska
 * @version 1.0.0.0
 */
public enum RodzajFaktury {
    FAKTURA_SPRZEDAZY,
    FAKTURA_ZAKUPU
}
