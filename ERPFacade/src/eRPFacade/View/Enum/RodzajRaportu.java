package eRPFacade.View.Enum;

/**
 * @author Jakub Dydo
 * @version 1.0.0.0
 */
public enum RodzajRaportu {
    TOWARY_WYCZERPANE,
    TOWARY_BLISKIE_WYCZERPANIU,
    TOWARY_PRZETERMINOWANE,
    TOWARY_BLISKIE_PRZETERMINOWANIA
}
