package eRPFacade.Implementation;

import eRPFacade.Contract.IERPFacade;
import eRPFacade.View.Frame.ERPViewFrame;
import faktura.FakturaSprzedazyComponent.Contract.IFakturaSprzedazy;
import faktura.FakturaZakupuComponent.Contract.IFakturaZakupu;
import faktura.HistoriaFakturComponent.Contract.IHistoriaFaktur;
import faktura.ListaKontrahentowComponent.Contract.IListaKontrahentow;
import glowne.KonfiguracjaComponent.Contract.IKonfiguracja;
import glowne.LogowanieComponent.Contract.ILogowanie;
import javax.swing.JFrame;
import ksiegowosc.DeklaracjaVAT7Component.Contract.IDeklaracjaVAT7;
import ksiegowosc.KsiazkaPodatkowaComponent.Contract.IKsiazkaPodatkowa;
import ksiegowosc.ListaPracownikowComponent.Contract.IListaPracownikow;
import ksiegowosc.RejestrVATComponent.Contract.IRejestrVAT;
import magazyn.InwentaryzacjaComponent.Contract.IInwentaryzacja;
import magazyn.ListaTowarowComponent.Contract.IListaTowarow;
import magazyn.RaportyProsteComponent.Contract.IRaportyProste;

/**
 * @author Jakub Dydo
 * @version 1.0.0.0
 */
public class ERPFacade implements IERPFacade {

    private JFrame frame;

    private ILogowanie logowanie;

    public ERPFacade(IKonfiguracja konfiguracja, ILogowanie logowanie,
            IFakturaSprzedazy fakturaSprzedazy, IFakturaZakupu fakturaZakupu, IHistoriaFaktur historiaFaktur, IListaKontrahentow listaKontrahentow,
            IDeklaracjaVAT7 deklaracjaVAT7, IKsiazkaPodatkowa ksiazkaPodatkowa, IListaPracownikow listaPracownikow, IRejestrVAT rejestrVAT,
            IInwentaryzacja inwentaryzacja, IListaTowarow listaTowaro, IRaportyProste raportyProste) {
        
        this.logowanie = logowanie;
        
        this.frame = new ERPViewFrame(konfiguracja, logowanie,
                fakturaSprzedazy, fakturaZakupu, historiaFaktur, listaKontrahentow,
                deklaracjaVAT7, ksiazkaPodatkowa, listaPracownikow, rejestrVAT,
                inwentaryzacja, listaTowaro, raportyProste);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void run() {
        this.logowanie.setMainFrame(frame);
    }
}
