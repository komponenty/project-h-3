package ksiegowosc.RejestrVATComponent.Model;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Date;
import java.util.List;
import magazyn.ListaTowarowComponent.Model.TowarModel;

/**
 * @author Paweł Żurawik
 * @version 1.0.0.1
 */
public class RejestrVATModel {

    //<editor-fold defaultstate="collapsed" desc="Pola">
    private Stan rejestrVATStan;
    private TypRozliczenia rejestrVATTyp;
    private double rejestrVATKwotaPodatku;
    private List<TowarModel> rejestrVATPrzedmiotOpodatkowania;
    private double rejestrVATPodatekNaliczonyKwota;
    private double rejestrVATPodatekNaleznyKwota;
    private double rejestrVATPodatekNaliczonyMinusNalezny;
    private double rejestrVATKwotaDoZaplatyLubZwrotu;
    private Date rejestrVATDataOtwarcia;
    private Date rejestrVATDataZamkniecia;
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Wlasciwosci">
    public void setRejestrVATStan(Stan rejestrVATStan) {
        Stan oldValue = this.rejestrVATStan;
        this.rejestrVATStan = rejestrVATStan;
        changeSupport.firePropertyChange("rejestrVATStan", oldValue, rejestrVATStan);
    }

    public Stan getRejestrVATStan() {
        return this.rejestrVATStan;
    }

    public void setRejestrVATTyp(TypRozliczenia rejestrVATTyp) {
        TypRozliczenia oldValue = this.rejestrVATTyp;
        this.rejestrVATTyp = rejestrVATTyp;
        changeSupport.firePropertyChange("rejestrVATTyp", oldValue, rejestrVATTyp);
    }

    public TypRozliczenia getRejestrVATTyp() {
        return this.rejestrVATTyp;
    }

    public void setRejestrVATKwotaPodatku(double rejestrVATKwotaPodatku) {
        double oldValue = this.rejestrVATKwotaPodatku;
        this.rejestrVATKwotaPodatku = rejestrVATKwotaPodatku;
        changeSupport.firePropertyChange("rejestrVATKwotaPodatku", oldValue, rejestrVATKwotaPodatku);
    }

    public double getRejestrVATKwotaPodatku() {
        return this.rejestrVATKwotaPodatku;
    }

    public void setRejestrVATPrzedmiotOpodatkowania(List<TowarModel> rejestrVATPrzedmiotOpodatkowania) {
        List<TowarModel> oldValue = this.rejestrVATPrzedmiotOpodatkowania;
        this.rejestrVATPrzedmiotOpodatkowania = rejestrVATPrzedmiotOpodatkowania;
        changeSupport.firePropertyChange("rejestrVATPrzedmiotOpodatkowania", oldValue, rejestrVATPrzedmiotOpodatkowania);
    }

    public List<TowarModel> getRejestrVATPrzedmiotOpodatkowania() {
        return this.rejestrVATPrzedmiotOpodatkowania;
    }

    public void setRejestrVATPodatekNaliczonyKwota(double rejestrVATPodatekNaliczonyKwota) {
        double oldValue = this.rejestrVATPodatekNaliczonyKwota;
        this.rejestrVATPodatekNaliczonyKwota = rejestrVATPodatekNaliczonyKwota;
        changeSupport.firePropertyChange("rejestrVATPodatekNaliczonyKwota", oldValue, rejestrVATPodatekNaliczonyKwota);
    }

    public double getRejestrVATPodatekNaliczonyKwota() {
        return this.rejestrVATPodatekNaliczonyKwota;
    }

    public void setRejestrVATPodatekNaleznyKwota(double rejestrVATPodatekNaleznyKwota) {
        double oldValue = this.rejestrVATPodatekNaleznyKwota;
        this.rejestrVATPodatekNaleznyKwota = rejestrVATPodatekNaleznyKwota;
        changeSupport.firePropertyChange("rejestrVATPodatekNaleznyKwota", oldValue, rejestrVATPodatekNaleznyKwota);
    }

    public double getRejestrVATPodatekNaleznyKwota() {
        return this.rejestrVATPodatekNaleznyKwota;
    }

    public void setRejestrVATPodatekNaliczonyMinusNalezny(double rejestrVATPodatekNaliczonyMinusNalezny) {
        double oldValue = this.rejestrVATPodatekNaliczonyMinusNalezny;
        this.rejestrVATPodatekNaliczonyMinusNalezny = rejestrVATPodatekNaliczonyMinusNalezny;
        changeSupport.firePropertyChange("rejestrVATPodatekNaliczonyMinusNalezny", oldValue, rejestrVATPodatekNaliczonyMinusNalezny);
    }

    public double getRejestrVATPodatekNaliczonyMinusNalezny() {
        return this.rejestrVATPodatekNaliczonyMinusNalezny;
    }

    public void setRejestrVATKwotaDoZaplatyLubZwrotu(double rejestrVATKwotaDoZaplatyLubZwrotu) {
        double oldValue = this.rejestrVATKwotaDoZaplatyLubZwrotu;
        this.rejestrVATKwotaDoZaplatyLubZwrotu = rejestrVATKwotaDoZaplatyLubZwrotu;
        changeSupport.firePropertyChange("rejestrVATKwotaDoZaplatyLubZwrotu", oldValue, rejestrVATKwotaDoZaplatyLubZwrotu);
    }

    public double getRejestrVATKwotaDoZaplatyLubZwrotu() {
        return this.rejestrVATKwotaDoZaplatyLubZwrotu;
    }

    public void setRejestrVATDataOtwarcia(Date rejestrVATDataOtwarcia) {
        Date oldValue = this.rejestrVATDataOtwarcia;
        this.rejestrVATDataOtwarcia = rejestrVATDataOtwarcia;
        changeSupport.firePropertyChange("rejestrVATDataOtwarcia", oldValue, rejestrVATDataOtwarcia);
    }

    public Date getRejestrVATDataOtwarcia() {
        return this.rejestrVATDataOtwarcia;
    }

    public void setRejestrVATDataZamkniecia(Date rejestrVATDataZamkniecia) {
        Date oldValue = this.rejestrVATDataZamkniecia;
        this.rejestrVATDataZamkniecia = rejestrVATDataZamkniecia;
        changeSupport.firePropertyChange("rejestrVATDataZamkniecia", oldValue, rejestrVATDataZamkniecia);
    }

    public Date getRejestrVATDataZamkniecia() {
        return this.rejestrVATDataZamkniecia;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Konstruktory">
    public RejestrVATModel() {
    }

    public RejestrVATModel(Stan rejestrVATStan, TypRozliczenia rejestrVATTyp, double rejestrVATKwotaPodatku, List<TowarModel> rejestrVATPrzedmiotOpodatkowania, double rejestrVATPodatekNaliczonyKwota, double rejestrVATPodatekNaleznyKwota, double rejestrVATPodatekNaliczonyMinusNalezny, double rejestrVATKwotaDoZaplatyLubZwrotu, Date rejestrVATDataOtwarcia, Date rejestrVATDataZamkniecia) {
        this.rejestrVATStan = rejestrVATStan;
        this.rejestrVATTyp = rejestrVATTyp;
        this.rejestrVATKwotaPodatku = rejestrVATKwotaPodatku;
        this.rejestrVATPrzedmiotOpodatkowania = rejestrVATPrzedmiotOpodatkowania;
        this.rejestrVATPodatekNaliczonyKwota = rejestrVATPodatekNaliczonyKwota;
        this.rejestrVATPodatekNaleznyKwota = rejestrVATPodatekNaleznyKwota;
        this.rejestrVATPodatekNaliczonyMinusNalezny = rejestrVATPodatekNaliczonyMinusNalezny;
        this.rejestrVATKwotaDoZaplatyLubZwrotu = rejestrVATKwotaDoZaplatyLubZwrotu;
        this.rejestrVATDataOtwarcia = rejestrVATDataOtwarcia;
        this.rejestrVATDataZamkniecia = rejestrVATDataZamkniecia;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="PropertyChange">
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }
    //</editor-fold>
}
