package ksiegowosc.RejestrVATComponent.Model;

/**
 * @author Paweł Żurawik
 * @version 1.0.0.0
 */
public enum Stan {

    Otwarty,
    Zamknięty
}
