package ksiegowosc.RejestrVATComponent.Implementation;

import faktura.FakturaSprzedazyComponent.Model.FakturaSprzedazyModel;
import faktura.FakturaZakupuComponent.Model.FakturaZakupuModel;
import ksiegowosc.RejestrVATComponent.Contract.IRejestrVAT;
import ksiegowosc.RejestrVATComponent.Model.RejestrVATModel;
import ksiegowosc.RejestrVATComponent.Model.TypRozliczenia;
import java.util.Date;
import java.util.List;

/**
 * @author Paweł Żurawik
 * @version 1.0.0.0
 */
public class RejestrVAT implements IRejestrVAT {

    @Override
    public String otworzRejestr(TypRozliczenia typ) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String zamknijRejestr(TypRozliczenia typ) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String aktualizujRejestrSprzedazy(FakturaSprzedazyModel fakturaSprzedazy) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String aktualizujRejestrZakupu(FakturaZakupuModel fakturaZakupu) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<RejestrVATModel> znajdzRejestr(Date dataOd, Date dataDo, TypRozliczenia typ) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String zwrocAktualnyRejestr(RejestrVATModel rejestr, TypRozliczenia typ) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void doPlikuRejestrVAT(RejestrVATModel rejestrVATDoPliku, String tytul, String informacje, String sciezkaDoPliku) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
