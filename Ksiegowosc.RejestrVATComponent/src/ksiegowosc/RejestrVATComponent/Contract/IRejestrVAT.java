package ksiegowosc.RejestrVATComponent.Contract;

import java.util.Date;
import java.util.List;
import faktura.FakturaSprzedazyComponent.Model.FakturaSprzedazyModel;
import faktura.FakturaZakupuComponent.Model.FakturaZakupuModel;
import ksiegowosc.RejestrVATComponent.Model.RejestrVATModel;
import ksiegowosc.RejestrVATComponent.Model.TypRozliczenia;

/**
 * @author Paweł Żurawik
 * @version 1.0.0.0
 */
public interface IRejestrVAT {

    String otworzRejestr(TypRozliczenia typ);

    String zamknijRejestr(TypRozliczenia typ);

    String aktualizujRejestrSprzedazy(FakturaSprzedazyModel fakturaSprzedazy);

    String aktualizujRejestrZakupu(FakturaZakupuModel fakturaZakupu);

    List<RejestrVATModel> znajdzRejestr(Date dataOd, Date dataDo, TypRozliczenia typ);

    String zwrocAktualnyRejestr(RejestrVATModel rejestr, TypRozliczenia typ);

    void doPlikuRejestrVAT(RejestrVATModel rejestrVATDoPliku, String tytul, String informacje, String sciezkaDoPliku);
}
