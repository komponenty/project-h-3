package test.resetBazy;

import xtea_db4o.XTeaEncryptionStorage;
import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import com.db4o.config.EmbeddedConfiguration;

/**
 * @author Jakub Dydo
 * @version 1.0.1.0
 */
public class ResetBazy {

    public static boolean usunBaze(String nazwaBazy, boolean czyBazaSzyfrowana) {
        String haslo = "a2v5m2x9f7";
        EmbeddedConfiguration config = Db4oEmbedded.newConfiguration();
        config.common().activationDepth(Integer.MAX_VALUE);

        if (czyBazaSzyfrowana) {
            config.file().storage(new XTeaEncryptionStorage(haslo));
        }

        ObjectContainer db = Db4oEmbedded.openFile(config, nazwaBazy);
        try {
            ObjectSet<Object> wynik = db.queryByExample(new Object());
            for (Object object : wynik) {
                db.delete(object);
            }
            return true;
        } catch (Exception e) {
            return false;
        } finally {
            db.close();
        }
    }
}
