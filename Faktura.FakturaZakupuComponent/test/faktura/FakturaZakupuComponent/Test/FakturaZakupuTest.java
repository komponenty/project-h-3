package faktura.FakturaZakupuComponent.Test;

import faktura.FakturaZakupuComponent.Contract.IFakturaZakupu;
import faktura.FakturaZakupuComponent.Implementation.FakturaZakupu;
import faktura.FakturaZakupuComponent.Model.FakturaZakupuModel;
import faktura.ListaKontrahentowComponent.Model.AdresModel;
import faktura.ListaKontrahentowComponent.Model.KontrahentModel;
import glowne.BazaDanychComponent.Implementation.BazaDanych;
import magazyn.ListaTowarowComponent.Contract.IListaTowarow;
import magazyn.ListaTowarowComponent.Implementation.ListaTowarow;
import magazyn.ListaTowarowComponent.Model.TowarModel;
import test.resetBazy.ResetBazy;
import java.util.Date;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;
import pk.epub.Creator.Creator;

/**
 * @author Katarzyna Romanowska
 * @version 1.0.1.0
 */
public class FakturaZakupuTest {

    @SuppressWarnings("deprecation")
    private static String GenerujBazeTowarow() {
        ResetBazy.usunBaze("BazaTowarow.yap", false);
        ResetBazy.usunBaze("BazaFakturZakupu.yap", true);
        String wynik = "Towar został pomyślnie dodany do bazy danych.";
        IListaTowarow listaTowarow = new ListaTowarow(new BazaDanych());
        wynik = listaTowarow.dodajTowar(new TowarModel(1, "nazwa1", "producent1", 11, 1.00, 0.1, new Date(2013, 0, 31), "pkwiu1", 0.1, "Produkt Testowy1", false, false));
        wynik = listaTowarow.dodajTowar(new TowarModel(2, "nazwa2", "producent2", 11, 1.00, 0.1, new Date(2013, 0, 31), "pkwiu1", 0.1, "Produkt Testowy1", false, false));
        //wynik = listaTowarow.dodajTowar(new TowarModel(3, "nazwa3", "producent3", 7, 1.00, 0.1, new Date(2013, 0, 12), "pkwiu1", 0.1, "Produkt Testowy1", false, false));		
        wynik = listaTowarow.dodajTowar(new TowarModel(4, "nazwa4", "producent4", 5, 1.00, 0.1, new Date(2013, 0, 14), "pkwiu1", 0.1, "Produkt Testowy1", false, false));
        wynik = listaTowarow.dodajTowar(new TowarModel(5, "nazwa5", "producent5", 8, 1.00, 0.1, new Date(2013, 0, 31), "pkwiu1", 0.1, "Produkt Testowy1", false, false));
        wynik = listaTowarow.dodajTowar(new TowarModel(6, "nazwa6", "producent6", 4, 1.00, 0.1, new Date(2013, 1, 1), "pkwiu1", 0.1, "Produkt Testowy1", false, false));
        wynik = listaTowarow.dodajTowar(new TowarModel(7, "nazwa7", "producent7", 78, 1.00, 0.1, new Date(2013, 1, 28), "pkwiu1", 0.1, "Produkt Testowy1", false, false));
        wynik = listaTowarow.dodajTowar(new TowarModel(8, "nazwa8", "producent8", 43, 1.00, 0.1, new Date(2013, 1, 15), "pkwiu1", 0.1, "Produkt Testowy1", false, false));
        wynik = listaTowarow.dodajTowar(new TowarModel(9, "nazwa9", "producent9", 0, 1.00, 0.1, new Date(2013, 1, 27), "pkwiu1", 0.1, "Produkt Testowy1", false, false));
        wynik = listaTowarow.dodajTowar(new TowarModel(10, "nazwa10", "producent10", 0, 1.00, 0.1, new Date(2013, 2, 1), "pkwiu1", 0.1, "Produkt Testowy1", false, false));
        wynik = listaTowarow.dodajTowar(new TowarModel(11, "nazwa11", "producent11", 0, 1.00, 0.1, new Date(2013, 2, 31), "pkwiu1", 0.1, "Produkt Testowy1", false, false));
        wynik = listaTowarow.dodajTowar(new TowarModel(12, "nazwa12", "producent12", 0, 1.00, 0.1, new Date(2013, 2, 12), "pkwiu1", 0.1, "Produkt Testowy1", false, false));
        wynik = listaTowarow.dodajTowar(new TowarModel(13, "nazwa13", "producent13", 5, 1.00, 0.1, new Date(2013, 2, 18), "pkwiu1", 0.1, "Produkt Testowy1", false, false));
        wynik = listaTowarow.dodajTowar(new TowarModel(14, "nazwa14", "producent14", 10, 1.00, 0.1, new Date(2013, 2, 21), "pkwiu1", 0.1, "Produkt Testowy1", false, false));
        wynik = listaTowarow.dodajTowar(new TowarModel(15, "nazwa15", "producent15", 0, 1.00, 0.1, new Date(2013, 0, 1), "pkwiu1", 0.1, "Produkt Testowy1", false, true));
        wynik = listaTowarow.dodajTowar(new TowarModel(16, "nazwa16", "producent16", 1, 1.00, 0.1, new Date(2013, 0, 31), "pkwiu1", 0.1, "Produkt Testowy1", true, false));
        wynik = listaTowarow.dodajTowar(new TowarModel(17, "nazwa17", "producent17", 1, 1.00, 0.1, new Date(2013, 0, 16), "pkwiu1", 0.1, "Produkt Testowy1", true, true));
        return wynik;
    }

    @SuppressWarnings("deprecation")
    @Test
    public void testKomponentu() {
        GenerujBazeTowarow();
        IFakturaZakupu fakturaZakupu = new FakturaZakupu(new BazaDanych(), new Creator());

        //faktura
        FakturaZakupuModel faktura = new FakturaZakupuModel("1", new Date(2013, 10, 30), new Date(2013, 10, 29), "Jan", "Kowalski", 1);

        //kontrahent
        KontrahentModel kontrahent = new KontrahentModel("Firma1", "1", "Tadeusz", "Nowak", new AdresModel("Ladna", "22", "2", "22-222", "Poznan", "Polska"));
        kontrahent.setKontrahentID(1);

        String wiadomosc1 = fakturaZakupu.dodajKontrahenta(faktura, kontrahent);
        Assert.assertEquals(1, faktura.getKontrahentID());
        Assert.assertEquals("Kontrahent został pomyślnie dodany", wiadomosc1);

        //towar
        TowarModel towar1 = new TowarModel(1, "nazwa1", "producent1", 11, 1.00, 0.1, new Date(2013, 0, 31), "pkwiu1", 0, "Produkt Testowy1", false, false);
        TowarModel towar2 = new TowarModel(20, "nazwa20", "producent20", 7, 1.00, 0.1, new Date(2013, 0, 31), "pkwiu1", 0, "Produkt Testowy1", false, false);
        TowarModel towar3 = new TowarModel(3, "nazwa3", "producent3", 8, 1.00, 0.1, new Date(2013, 0, 12), "pkwiu1", 0, "Produkt Testowy1", true, false);
        TowarModel towar4 = new TowarModel(4, "nazwa4", "producent4", 4, 1.00, 0.1, new Date(2013, 0, 12), "pkwiu1", 0, "Produkt Testowy1", false, false);

        //dodawanie
        String wiadomosc2 = fakturaZakupu.dodajTowarDoFaktury(faktura, towar1);
        Assert.assertEquals(12.1, faktura.getFakturaKwota(), 0.0000001);
        Assert.assertEquals(1, faktura.getFakturaTowary().size());
        Assert.assertEquals("nazwa1", faktura.getFakturaTowary().get(0).getTowarnNazwa());
        Assert.assertEquals(11, faktura.getFakturaTowary().get(0).getTowarIlosc(), 0);
        Assert.assertEquals("Towar został pomyślnie dodany", wiadomosc2);

        String wiadomosc3 = fakturaZakupu.dodajTowarDoFaktury(faktura, towar1);
        Assert.assertEquals(24.2, faktura.getFakturaKwota(), 0.0000001);
        Assert.assertEquals(1, faktura.getFakturaTowary().size());
        Assert.assertEquals("nazwa1", faktura.getFakturaTowary().get(0).getTowarnNazwa());
        Assert.assertEquals(22, faktura.getFakturaTowary().get(0).getTowarIlosc(), 0);
        Assert.assertEquals("Liczebność towaru została zaktualizowana.", wiadomosc3);

        String wiadomosc4 = fakturaZakupu.dodajTowarDoFaktury(faktura, towar2);
        Assert.assertEquals(31.9, faktura.getFakturaKwota(), 0.0000001);
        Assert.assertEquals(2, faktura.getFakturaTowary().size());
        Assert.assertEquals("nazwa1", faktura.getFakturaTowary().get(0).getTowarnNazwa());
        Assert.assertEquals("nazwa20", faktura.getFakturaTowary().get(1).getTowarnNazwa());
        Assert.assertEquals(7, faktura.getFakturaTowary().get(1).getTowarIlosc(), 0);
        Assert.assertEquals("Towar został pomyślnie dodany", wiadomosc4);

        String wiadomosc5 = fakturaZakupu.dodajTowarDoFaktury(faktura, towar3);
        Assert.assertEquals(40.7, faktura.getFakturaKwota(), 0.0000001);
        Assert.assertEquals(3, faktura.getFakturaTowary().size());
        Assert.assertEquals("nazwa3", faktura.getFakturaTowary().get(2).getTowarnNazwa());
        Assert.assertEquals(8, faktura.getFakturaTowary().get(2).getTowarIlosc(), 0);
        Assert.assertEquals("Towar został pomyślnie dodany", wiadomosc5);

        String wiadomosc6 = fakturaZakupu.dodajTowarDoFaktury(faktura, towar4);
        Assert.assertEquals(45.1, faktura.getFakturaKwota(), 0.0000001);
        Assert.assertEquals(4, faktura.getFakturaTowary().size());
        Assert.assertEquals("nazwa4", faktura.getFakturaTowary().get(3).getTowarnNazwa());
        Assert.assertEquals(4, faktura.getFakturaTowary().get(3).getTowarIlosc(), 0);
        Assert.assertEquals("Towar został pomyślnie dodany", wiadomosc6);

        //usuwanie
        String wiadomosc7 = fakturaZakupu.usunTowarZFaktury(faktura, towar4);
        Assert.assertEquals(40.7, faktura.getFakturaKwota(), 0.0000001);
        Assert.assertEquals(3, faktura.getFakturaTowary().size());
        Assert.assertEquals("Towar został pomyślnie usunięty", wiadomosc7);

        String wiadomosc8 = fakturaZakupu.usunTowarZFaktury(faktura, towar4);
        Assert.assertEquals(40.7, faktura.getFakturaKwota(), 0.0000001);
        Assert.assertEquals(3, faktura.getFakturaTowary().size());
        Assert.assertEquals("Błąd! Nie znaleziono towaru.", wiadomosc8);

        //sprawdzenie towarow
        Assert.assertEquals(11, towar1.getTowarIlosc(), 0);
        Assert.assertEquals(7, towar2.getTowarIlosc(), 0);
        Assert.assertEquals(8, towar3.getTowarIlosc(), 0);
        Assert.assertEquals(4, towar4.getTowarIlosc(), 0);

        //dodanie faktury
        String wiadomosc9 = fakturaZakupu.dodajFaktureZakupu(new FakturaZakupuModel("2", new Date(2013, 9, 1), new Date(2013, 9, 1), "Janusz", "Kwiatkowski", 1));
        String wiadomosc10 = fakturaZakupu.dodajFaktureZakupu(new FakturaZakupuModel("3", new Date(2013, 10, 30), new Date(2013, 10, 29), "Janek", "Kowalski", 1));
        String wiadomosc11 = fakturaZakupu.dodajFaktureZakupu(faktura);
        List<FakturaZakupuModel> lista = new BazaDanych().znajdzObiekt("BazaFakturZakupu.yap", true, new FakturaZakupuModel());

        Assert.assertEquals(3, lista.size());
        Assert.assertEquals(3, faktura.getFakturaTowary().size());
        Assert.assertEquals("nazwa1", faktura.getFakturaTowary().get(0).getTowarnNazwa());
        Assert.assertEquals("nazwa3", faktura.getFakturaTowary().get(2).getTowarnNazwa());
        Assert.assertEquals("Faktura zakupu została pomyślnie dodana do bazy danych.", wiadomosc9);
        Assert.assertEquals("Faktura zakupu została pomyślnie dodana do bazy danych.", wiadomosc10);
        Assert.assertEquals("Faktura zakupu została pomyślnie dodana do bazy danych.", wiadomosc11);
    }
}
