package faktura.FakturaZakupuComponent.Implementation;

import java.util.List;
import magazyn.ListaTowarowComponent.Model.TowarModel;
import faktura.FakturaZakupuComponent.Contract.IFakturaZakupu;
import faktura.FakturaZakupuComponent.Model.FakturaZakupuModel;
import faktura.ListaKontrahentowComponent.Model.KontrahentModel;
import glowne.BazaDanychComponent.Contract.IBaza;
import glowne.KonfiguracjaComponent.Model.PrzedsiebiorstwoModel;
import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import pk.epub.Creator.Contracts.ICreator;

/**
 * @author Katarzyna Romanowska
 * @version 1.0.1.2
 */
public class FakturaZakupu implements IFakturaZakupu {

    private IBaza bazaDanych;
    private ICreator creator;

    /**
     * Konstruktor zapewniajacy podstawowe funkcjonalnosci komponentu
     *
     * @param bazaDanych repozytorium używane do przeprowadzania operacji nad
     * bazą
     * @param creator komponent używany do zrzutu faktury do pliku
     */
    public FakturaZakupu(IBaza bazaDanych, ICreator creator) {
        this.bazaDanych = bazaDanych;
        this.creator = creator;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String dodajFaktureZakupu(FakturaZakupuModel nowaFaktura) {
        //usuwanie danej ilosci towaru z magazynu
        if (!AktualizujIloscTowarow(nowaFaktura.getFakturaTowary())) {
            return "Błąd bazy! Skonsultuj się z dostawcą oprogramowania!";
        }

        //dodawanie faktury do bazy danych
        boolean wynikOperacji = bazaDanych.dodajObiekt("BazaFakturZakupu.yap", true, nowaFaktura);
        if (wynikOperacji) {
            return "Faktura zakupu została pomyślnie dodana do bazy danych.";
        } else {
            return "Błąd bazy! Skonsultuj się z dostawcą oprogramowania!";
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String dodajKontrahenta(FakturaZakupuModel nowaFaktura, KontrahentModel kontrahent) {
        nowaFaktura.setKontrahentID(kontrahent.getKontrahentID());
        return "Kontrahent został pomyślnie dodany";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String dodajTowarDoFaktury(FakturaZakupuModel nowaFaktura, TowarModel towar) {
        for (TowarModel towarZFaktury : nowaFaktura.getFakturaTowary()) {
            if (towarZFaktury.getTowarID() == towar.getTowarID() && towarZFaktury.getTowarUzytekWlasny() == towar.getTowarUzytekWlasny()) {
                double cenaZRabatem = towarZFaktury.getTowarCenaNetto() - towarZFaktury.getTowarCenaNetto() * towarZFaktury.getTowarRabat();
                double cenaZVATem = nowaFaktura.getFakturaKwota() + towar.getTowarIlosc() * (cenaZRabatem + cenaZRabatem * towarZFaktury.getTowarVAT());
                double cena = Math.round(cenaZVATem * 100.0) / 100.0;
                nowaFaktura.setFakturaKwota(cena);
                towarZFaktury.setTowarIlosc(towarZFaktury.getTowarIlosc() + towar.getTowarIlosc());
                return "Liczebność towaru została zaktualizowana.";
            }
        }

        List<TowarModel> listaTowarow = nowaFaktura.getFakturaTowary();
        listaTowarow.add(new TowarModel(towar));
        nowaFaktura.setFakturaTowary(listaTowarow);
        double cenaZRabatem = towar.getTowarCenaNetto() - towar.getTowarCenaNetto() * towar.getTowarRabat();
        double cenaZVATem = nowaFaktura.getFakturaKwota() + towar.getTowarIlosc() * (cenaZRabatem + cenaZRabatem * towar.getTowarVAT());
        double cena = Math.round(cenaZVATem * 100.0) / 100.0;
        nowaFaktura.setFakturaKwota(cena);
        return "Towar został pomyślnie dodany";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String usunTowarZFaktury(FakturaZakupuModel nowaFaktura, TowarModel towar) {
        for (TowarModel towarZFaktury : nowaFaktura.getFakturaTowary()) {
            if (towarZFaktury.getTowarID() == towar.getTowarID() && towarZFaktury.getTowarUzytekWlasny() == towar.getTowarUzytekWlasny()) {
                double cenaZRabatem = towarZFaktury.getTowarCenaNetto() - towarZFaktury.getTowarCenaNetto() * towarZFaktury.getTowarRabat();
                double nowaKwota = nowaFaktura.getFakturaKwota() - towarZFaktury.getTowarIlosc() * (cenaZRabatem + cenaZRabatem * towar.getTowarVAT());
                double cena = Math.round(nowaKwota * 100.0) / 100.0;
                nowaFaktura.setFakturaKwota(cena);
                nowaFaktura.getFakturaTowary().remove(towarZFaktury);
                return "Towar został pomyślnie usunięty";
            }
        }
        return "Błąd! Nie znaleziono towaru.";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void doPlikuFaktureZakupu(FakturaZakupuModel fakturaDoPliku, String sciezkaDoPliku) {
        FakturaZakupuModel faktura = bazaDanych.znajdzObiekt("BazaFakturZakupu.yap", true, new FakturaZakupuModel(fakturaDoPliku)).get(0);
        KontrahentModel kontrahent = bazaDanych.znajdzObiekt("BazaKontrahentow.yap", true, new KontrahentModel(fakturaDoPliku.getKontrahentID())).get(0);
        PrzedsiebiorstwoModel przedsiebiorstwo;

        List<PrzedsiebiorstwoModel> lista = bazaDanych.znajdzObiekt("DanePrzedsiebiorstwa.yap", false, new PrzedsiebiorstwoModel());

        if (lista == null || lista.isEmpty()) {
            przedsiebiorstwo = new PrzedsiebiorstwoModel();
        }
        else {
            przedsiebiorstwo = lista.get(0);
        }

        creator.setDir(sciezkaDoPliku);
        creator.setTitle("Faktura zakupu");
        creator.setLanguage("pl");
        creator.addIdentifier("faktura_zakupu", faktura.getFakturaID());

        String style = "@charset \"utf-8\";\n"
                + "body { width: 100%; }\n"
                + ".pp { margin: 20px; }\n"
                + "table { margin-bottom:50px; }\n"
                + ".ppp { font-size:22px; margin: 20px; }";

        creator.setStyle(style);

        String nazwaFirmy = "";
        String nIP = "";
        if (kontrahent.getKontrahentNazwaFirmy() == null) {
            nazwaFirmy = kontrahent.getKontrahentImie() + " " + kontrahent.getKontrahentNazwisko();
            nIP = "";
        } else {
            nazwaFirmy = kontrahent.getKontrahentNazwaFirmy();
            nIP = kontrahent.getKontrahentNIP();
        }

        String html = "<html>\n"
                + "	<head>\n"
                + "		<link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\" />\n"
                + "	</head>\n"
                + "	<body>\n"
                + "    <table width=\"800px\" border=\"1\">\n"
                + "		<tr>\n"
                + "			<td width=\"400px\" height=\"150px\" colspan=\"2\" valign=\"bottom\" align=\"center\"></td>\n"
                + "                     <td width=\"400px\" colspan=\"2\"><p class=\"pp\"><b>FAKTURA SPRZEDAŻY</b></br>\n"
                + "        		" + faktura.getFakturaID() + "</br></br>\n"
                + "        		Data wystawienia: " + new SimpleDateFormat("dd-MM-yyyy").format(faktura.getFakuraDataZaplata()) + "</br>\n"
                + "                     Data zapłaty: " + new SimpleDateFormat("dd-MM-yyyy").format(faktura.getFakuraDataWystawienie()) + "</p></td>\n"
                + "		</tr>\n"
                + "             <tr>\n"
                + "            <td width=\"400px\" colspan=\"2\"><p class=\"pp\"><b>SPRZEDAWCA</b></br>\n"
                + "        		" + nazwaFirmy + "</br>\n"
                + "        		Adres: " + kontrahent.getKontrahentAdres() + "</br></br>\n"
                + "        		NIP: " + nIP + "</p></td>\n"
                + "             <td width=\"400px\" colspan=\"2\"><p class=\"pp\"><b>NABYWCA</b></br>\n"
                + "        		" + przedsiebiorstwo.getPrzedsiebiorstwoNazwa() + "</br>\n"
                + "        		Adres: " + przedsiebiorstwo.getPrzedsiebiorstwoAdres() + "</br></br>\n"
                + "        		NIP: " + przedsiebiorstwo.getPrzedsiebiorstwoNIP() + "</p></td>\n"
                + "			\n"
                + "		</tr>\n"
                + "	</table>\n"
                + " \n"
                + "	<table width=\"800px\" border=\"1\">\n"
                + "		<tr>\n"
                + "			<td width=\"300px\" valign=\"middle\" align=\"center\"><b>Nazwa towaru</b></td>\n"
                + "            <td width=\"100px\" valign=\"middle\" align=\"center\"><b>Ilosć</b></td>\n"
                + "            <td  width=\"125px\" valign=\"middle\" align=\"center\"><b>Cena netto</b></td>\n"
                + "            <td width=\"50px\"valign=\"middle\" align=\"center\"><b>VAT</b></td>\n"
                + "            <td width=\"50px\" valign=\"middle\" align=\"center\"><b>Rabat</b></td>\n"
                + "            <td width=\"125px\" valign=\"middle\" align=\"center\"><b>Cena brutto</b></td>\n"
                + "            <td width=\"50px\" valign=\"middle\" align=\"center\"><b>Użytek własny</b></td>\n"
                + "		</tr>\n";

        for (TowarModel towar : faktura.getFakturaTowary()) {

            String uzytekWlasny = "NIE";
            if (towar.getTowarUzytekWlasny()) {
                uzytekWlasny = "TAK";
            } else {
                uzytekWlasny = "NIE";
            }

            double cenaZRabatem = towar.getTowarCenaNetto() - towar.getTowarCenaNetto() * towar.getTowarRabat();
            double cenaZVATem = towar.getTowarIlosc() * (cenaZRabatem + cenaZRabatem * towar.getTowarVAT());
            double cena = Math.round(cenaZVATem * 100.0) / 100.0;

            html += "       <tr>\n"
                    + "            <td width=\"300px\" valign=\"middle\" align=\"center\">" + towar.getTowarnNazwa() + "</td>\n"
                    + "            <td width=\"100px\" valign=\"middle\" align=\"center\">" + Math.round(towar.getTowarIlosc() * 100.0) / 100.0 + "</td>\n"
                    + "            <td  width=\"125px\" valign=\"middle\" align=\"center\">" + Math.round((towar.getTowarIlosc() * towar.getTowarCenaNetto()) * 100.0) / 100.0 + "</td>\n"
                    + "            <td width=\"50px\"valign=\"middle\" align=\"center\">" + Math.round(towar.getTowarVAT() * 100.0) / 100.0 + "</td>\n"
                    + "            <td width=\"50px\" valign=\"middle\" align=\"center\">" + Math.round(towar.getTowarRabat() * 100.0) / 100.0 + "</td>\n"
                    + "            <td width=\"125px\" valign=\"middle\" align=\"center\">" + cena + "</td>\n"
                    + "            <td width=\"125px\" valign=\"middle\" align=\"center\">" + uzytekWlasny + "</td>\n"
                    + "     </tr>\n";
        }

        html += "       </table>\n"
                + "    \n"
                + " 	<table width=\"500px\" border=\"0\">\n"
                + "		<tr>\n"
                + "			<td width=\"500px\" valign=\"middle\"><p class=\"ppp\"><b>Razem do zapłaty:</b></br>\n"
                + "                     " + Math.round(faktura.getFakturaKwota() * 100.0) / 100.0 + " PLN</p></td>\n"
                + "        </tr>    \n"
                + "	</table>\n"
                + "    \n"
                + "    <table width=\"800px\" border=\"0\">\n"
                + "		<tr>\n"
                + "            <td width=\"400px\" height=\"100px\" valign=\"bottom\" align=\"right\">Podpis nabywcy</td>\n"
                + "		</tr>\n"
                + "	</table>\n"
                + "    \n"
                + "	</body>\n"
                + "</html>";

        creator.addChapter(html);

        creator.createEpub();
        creator.clear();

        try {
            Desktop.getDesktop().open(new File(sciezkaDoPliku));
        } catch (IOException e) {
            System.out.print(e);
        }
    }

    private boolean AktualizujIloscTowarow(List<TowarModel> listaTowarow) {
        for (TowarModel towar : listaTowarow) {
            if (!towar.getTowarUzytekWlasny()) {
                List<TowarModel> lista = bazaDanych.znajdzObiekt("BazaTowarow.yap", false, new TowarModel(towar.getTowarID()));
                if (lista == null) {
                    return false;
                } else if (lista.size() == 0) {
                    boolean wynikOperacji = bazaDanych.dodajObiekt("BazaTowarow.yap", false, towar);
                    if (!wynikOperacji) {
                        return false;
                    }
                } else {
                    TowarModel towarDoZmianyIlosci = lista.get(0);

                    towarDoZmianyIlosci.setTowarIlosc(towarDoZmianyIlosci.getTowarIlosc() + towar.getTowarIlosc());

                    boolean wynikOperacji = bazaDanych.edytujObiekt("BazaTowarow.yap", false, new TowarModel(towar.getTowarID()), towarDoZmianyIlosci);
                    if (!wynikOperacji) {
                        return false;
                    }
                }
            }
        }
        return true;
    }
}
