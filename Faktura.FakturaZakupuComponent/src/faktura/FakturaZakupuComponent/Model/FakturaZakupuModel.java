package faktura.FakturaZakupuComponent.Model;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import magazyn.ListaTowarowComponent.Model.TowarModel;

/**
 * @author Katarzyna Romanowska
 * @version 1.0.1.2
 */
public class FakturaZakupuModel {

    //<editor-fold defaultstate="collapsed" desc="Pola">
    private String fakturaID;
    private Date fakuraDataWystawienie;
    private Date fakuraDataZaplata;
    private double fakturaKwota;
    private String fakturaOsobaPrzyjmujacaImie;
    private String fakturaOsobaPrzyjmujacaNazwisko;
    private int fakturaOsobaPrzyjmujacaID;
    private int kontrahentID;
    private List<TowarModel> fakturaTowary;
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Wlasciwosci">
    public void setFakturaID(String fakturaID) {
        String oldValue = this.fakturaID;
        this.fakturaID = fakturaID;
        changeSupport.firePropertyChange("fakturaID", oldValue, fakturaID);
    }

    public String getFakturaID() {
        return this.fakturaID;
    }

    public void setFakuraDataWystawienie(Date fakuraDataWystawienie) {
        Date oldValue = this.fakuraDataWystawienie;
        this.fakuraDataWystawienie = fakuraDataWystawienie;
        changeSupport.firePropertyChange("fakuraDataWystawienie", oldValue, fakuraDataWystawienie);
    }

    public Date getFakuraDataWystawienie() {
        return this.fakuraDataWystawienie;
    }

    public void setFakuraDataZaplata(Date fakuraDataZaplata) {
        Date oldValue = this.fakuraDataZaplata;
        this.fakuraDataZaplata = fakuraDataZaplata;
        changeSupport.firePropertyChange("fakuraDataZaplata", oldValue, fakuraDataZaplata);
    }

    public Date getFakuraDataZaplata() {
        return this.fakuraDataZaplata;
    }

    public void setFakturaKwota(double fakturaKwota) {
        double oldValue = this.fakturaKwota;
        this.fakturaKwota = fakturaKwota;
        changeSupport.firePropertyChange("fakturaKwota", oldValue, fakturaKwota);
    }

    public double getFakturaKwota() {
        return this.fakturaKwota;
    }

    public void setFakturaOsobaPrzyjmujacaImie(String fakturaOsobaPrzyjmujacaImie) {
        String oldValue = this.fakturaOsobaPrzyjmujacaImie;
        this.fakturaOsobaPrzyjmujacaImie = fakturaOsobaPrzyjmujacaImie;
        changeSupport.firePropertyChange("fakturaOsobaPrzyjmujacaImie", oldValue, fakturaOsobaPrzyjmujacaImie);
    }

    public String getFakturaOsobaPrzyjmujacaImie() {
        return this.fakturaOsobaPrzyjmujacaImie;
    }

    public void setFakturaOsobaPrzyjmujacaNazwisko(String fakturaOsobaPrzyjmujacaNazwisko) {
        String oldValue = this.fakturaOsobaPrzyjmujacaNazwisko;
        this.fakturaOsobaPrzyjmujacaNazwisko = fakturaOsobaPrzyjmujacaNazwisko;
        changeSupport.firePropertyChange("fakturaOsobaPrzyjmujacaNazwisko", oldValue, fakturaOsobaPrzyjmujacaNazwisko);
    }

    public String getFakturaOsobaPrzyjmujacaNazwisko() {
        return this.fakturaOsobaPrzyjmujacaNazwisko;
    }

    public void setFakturaOsobaPrzyjmujacaID(int fakturaOsobaPrzyjmujacaID) {
        int oldValue = this.fakturaOsobaPrzyjmujacaID;
        this.fakturaOsobaPrzyjmujacaID = fakturaOsobaPrzyjmujacaID;
        changeSupport.firePropertyChange("fakturaOsobaPrzyjmujacaID", oldValue, fakturaOsobaPrzyjmujacaID);
    }

    public int getFakturaOsobaPrzyjmujacaID() {
        return this.fakturaOsobaPrzyjmujacaID;
    }

    public void setKontrahentID(int kontrahentID) {
        int oldValue = this.kontrahentID;
        this.kontrahentID = kontrahentID;
        changeSupport.firePropertyChange("kontrahentID", oldValue, kontrahentID);
    }

    public int getKontrahentID() {
        return this.kontrahentID;
    }

    public void setFakturaTowary(List<TowarModel> fakturaTowary) {
        List<TowarModel> oldValue = this.fakturaTowary;
        this.fakturaTowary = fakturaTowary;
        changeSupport.firePropertyChange("fakturaTowary", oldValue, fakturaTowary);
    }

    public List<TowarModel> getFakturaTowary() {
        return this.fakturaTowary;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Konstruktory">
    public FakturaZakupuModel() {
        this.fakturaTowary = new ArrayList<TowarModel>();
    }

    public FakturaZakupuModel(String fakturaID) {
        this.fakturaID = fakturaID;
        this.fakturaTowary = new ArrayList<TowarModel>();
    }

    public FakturaZakupuModel(int kontrahentID) {
        this.kontrahentID = kontrahentID;
        this.fakturaTowary = new ArrayList<TowarModel>();
    }

    public FakturaZakupuModel(String fakturaID, Date fakuraDataWystawienie, Date fakuraDataZaplata, String fakturaOsobaPrzyjmujacaImie, String fakturaOsobaPrzyjmujacaNazwisko, int fakturaOsobaPrzyjmujacaID) {
        this(fakturaID);
        this.fakuraDataWystawienie = fakuraDataWystawienie;
        this.fakuraDataZaplata = fakuraDataZaplata;
        this.fakturaOsobaPrzyjmujacaImie = fakturaOsobaPrzyjmujacaImie;
        this.fakturaOsobaPrzyjmujacaNazwisko = fakturaOsobaPrzyjmujacaNazwisko;
        this.fakturaOsobaPrzyjmujacaID = fakturaOsobaPrzyjmujacaID;
    }

    public FakturaZakupuModel(FakturaZakupuModel fakturaZakupuModel) {
        this.fakuraDataWystawienie = fakturaZakupuModel.getFakuraDataWystawienie();
        this.fakuraDataZaplata = fakturaZakupuModel.getFakuraDataZaplata();
        this.fakturaOsobaPrzyjmujacaID = fakturaZakupuModel.getFakturaOsobaPrzyjmujacaID();
        this.fakturaOsobaPrzyjmujacaImie = fakturaZakupuModel.getFakturaOsobaPrzyjmujacaImie();
        this.fakturaOsobaPrzyjmujacaNazwisko = fakturaZakupuModel.getFakturaOsobaPrzyjmujacaNazwisko();
        this.fakturaTowary = fakturaZakupuModel.getFakturaTowary();
        this.fakturaID = fakturaZakupuModel.getFakturaID();
        this.kontrahentID = fakturaZakupuModel.getKontrahentID();
        this.fakturaKwota = fakturaZakupuModel.getFakturaKwota();
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="PropertyChange">
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="toString method">
    @Override
    public String toString() {
        return this.fakturaID;
    }
    //</editor-fold>

}
