package faktura.FakturaZakupuComponent.Contract;

import magazyn.ListaTowarowComponent.Model.TowarModel;
import faktura.FakturaZakupuComponent.Model.FakturaZakupuModel;
import faktura.ListaKontrahentowComponent.Model.KontrahentModel;

/**
 * @author Katarzyna Romanowska
 * @version 1.0.1.0
 */
public interface IFakturaZakupu {

    /**
     * Dodaje nową fakture zakupu do bazy danych.
     * @param nowaFaktura faktura do dodania
     * @return "Faktura zakupu została pomyślnie dodana do bazy danych." - w przypadku pomyślnego dodania do bazy
     *          <br>
     *         "Błąd bazy! Skonsultuj się z dostawcą oprogramowania!" - w przypadku błędu krytycznego
     */
    String dodajFaktureZakupu(FakturaZakupuModel nowaFaktura);

   /**
     * Dodaje wybranego kontrahenta do danej faktury zakupu.
     * @param nowaFaktura faktura zakupu, do której dodajemy kontrahenta
     * @param kontrahent kontrahent, którego przypisujemy do faktury zakupu
     * @return "Kontrahent został pomyślnie dodany" - w przypadku pomyślnego dodania kontrahenta
     */
    String dodajKontrahenta(FakturaZakupuModel nowaFaktura, KontrahentModel kontrahent);

    /**
     * Dodaje wybrany towar oraz jego ilość do danej faktury sprzedaży.
     * <p>
     * Metoda aktualizuje również aktualną wartość faktury.
     * <br>
     * Ilość zawarta w towarze stanowi również ilość towaru na fakturze.
     * @param nowaFaktura faktura zakupu, do której chcemy dodać towar
     * @param towar towar, który przypisujemy do faktury zakupu
     * @return "Towar został pomyślnie dodany" - w przypadku pomyślnego dodania towaru do faktury zakupu.
     *         <br>
     *         "Liczebność towaru została zaktualizowana." - w przypadku zaktualizowania ilości towaru już znajdującego się w fakturze zakupu
     */
    String dodajTowarDoFaktury(FakturaZakupuModel nowaFaktura, TowarModel towar);

    /**
     * Usuwa wybrany towar z danej faktury zakupu.
     * <p>
     * Metoda aktualizuje również aktualną wartość faktury.
     * @param nowaFaktura faktura zakupu, z której chcemy usunąc towar
     * @param towar towar, który chcemy usunąć
     * @return "Towar został pomyślnie usunięty" - w przypadku pomyślnego usunięcia towaru z faktury zakupu.
     *         <br>
     *         "Błąd! Nie znaleziono towaru." - w przypadku nie znalezienia wybranego towaru na fakturze zakupu
     */
    String usunTowarZFaktury(FakturaZakupuModel nowaFaktura, TowarModel towar);

    /**
     * Zrzuca daną fakturę zakupu do pliku w formacie EPUB
     * @param fakturaDoPliku faktura zakupu do zrzutu do pliku
     * @param sciezkaDoPliku scieżka gdzie ma być zapisana faktura zakupu
     */
    void doPlikuFaktureZakupu(FakturaZakupuModel fakturaDoPliku, String sciezkaDoPliku);
}
