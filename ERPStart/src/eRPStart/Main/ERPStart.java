package eRPStart.Main;

import eRPFacade.Contract.IERPFacade;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author Jakub Dydo
 * @version 1.0.0.1
 */
public class ERPStart {

    public static void main(String[] args) {

        final ApplicationContext context = new ClassPathXmlApplicationContext("eRPStart/BeansConfig/SpringBeans.xml");

        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                IERPFacade facade = (IERPFacade) context.getBean("eRPFacade");
                facade.run();
            }
        });
    }
}
