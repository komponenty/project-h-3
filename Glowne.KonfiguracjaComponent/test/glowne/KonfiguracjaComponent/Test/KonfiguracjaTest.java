package glowne.KonfiguracjaComponent.Test;

import glowne.KonfiguracjaComponent.Contract.IKonfiguracja;
import glowne.KonfiguracjaComponent.Implementation.Konfiguracja;
import glowne.KonfiguracjaComponent.Model.PrzedsiebiorstwoModel;
import glowne.KonfiguracjaComponent.Model.Status;
import glowne.KonfiguracjaComponent.Model.UzytkownikModel;
import glowne.BazaDanychComponent.Contract.IBaza;
import glowne.BazaDanychComponent.Implementation.BazaDanych;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;
import test.resetBazy.ResetBazy;

/**
 * @author Katarzyna Romanowska
 * @version 1.0.1.0
 */
public class KonfiguracjaTest {

    private static String generujBazeUzytkownikow() {
        IBaza baza = new BazaDanych();
        IKonfiguracja konfiguracja = new Konfiguracja(baza);
        ResetBazy.usunBaze("BazaUzytkownikow.yap", true);
        String wynik = "Użytkownik został pomyślnie dodany do bazy danych.";
        wynik = konfiguracja.dodajUzytkownika(new UzytkownikModel("Janek123", "12345", Status.Administrator, "Jan", "Kowalski"));
        wynik = konfiguracja.dodajUzytkownika(new UzytkownikModel("Tadek123", "54321", Status.Sprzedawca, "Tadeusz", "Nowak"));
        wynik = konfiguracja.dodajUzytkownika(new UzytkownikModel("Maria123", "67890", Status.Sprzedawca, "Maria", "Kwiatkowska"));
        return wynik;
    }

    private static String generujPrzedsiebiorstwo() {
        IBaza baza = new BazaDanych();
        IKonfiguracja konfiguracja = new Konfiguracja(baza);
        ResetBazy.usunBaze("DanePrzedsiebiorstwa.yap", false);
        String wynik = "Dane przedsiębiorstwa zostały wprowadzone pomyślnie.";
        wynik = konfiguracja.zmienDaneFirmy(new PrzedsiebiorstwoModel("Firma", "Jan", "Kowalski", "Kwiatowa 11/23, 11-222 Warszawa", 1234567890));
        return wynik;
    }

    @Test
    public void testDodawanie() {
        IBaza baza = new BazaDanych();
        IKonfiguracja konfiguracja = new Konfiguracja(baza);
        String wiadomosc1 = generujBazeUzytkownikow();
        String wiadomosc2 = konfiguracja.dodajUzytkownika(new UzytkownikModel("Janek123", "1111", Status.Administrator, "Janusz", "Magiera"));
        List<UzytkownikModel> listaUzytkownikow = konfiguracja.znajdzUzytkownikow(new UzytkownikModel());

        Assert.assertEquals("Użytkownik został pomyślnie dodany do bazy danych.", wiadomosc1);
        Assert.assertEquals("Błąd! Istnieje już użytkownik o podanym loginie.", wiadomosc2);
        Assert.assertNotNull(listaUzytkownikow);
        Assert.assertEquals(3, listaUzytkownikow.size());
        Assert.assertEquals(Status.Administrator, listaUzytkownikow.get(0).getUzytkownikStatus());
        Assert.assertEquals(1, listaUzytkownikow.get(0).getUzytkownikID());
        Assert.assertEquals(2, listaUzytkownikow.get(1).getUzytkownikID());
        Assert.assertEquals(3, listaUzytkownikow.get(2).getUzytkownikID());
    }

    @Test
    public void testEdycja() {
        IBaza baza = new BazaDanych();
        IKonfiguracja konfiguracja = new Konfiguracja(baza);
        String wiadomosc1 = generujBazeUzytkownikow();

        UzytkownikModel szablon = new UzytkownikModel(1);
        UzytkownikModel wyedytowanyUzytkownik = new UzytkownikModel("Janek1235555555", "11111", Status.Ksiegowy, "Jan1111", "Kowalski1111");

        String wiadomosc2 = konfiguracja.edytujUzytkownika(szablon, wyedytowanyUzytkownik);

        List<UzytkownikModel> listaUzytkownikow1 = konfiguracja.znajdzUzytkownikow(new UzytkownikModel(1));
        List<UzytkownikModel> listaUzytkownikow2 = konfiguracja.znajdzUzytkownikow(new UzytkownikModel());

        Assert.assertEquals("Użytkownik został pomyślnie dodany do bazy danych.", wiadomosc1);
        Assert.assertEquals("Użytkownik został pomyślnie zaktualizowany.", wiadomosc2);
        Assert.assertNotNull(listaUzytkownikow2);
        Assert.assertEquals(3, listaUzytkownikow2.size());
        Assert.assertNotNull(listaUzytkownikow1);
        Assert.assertEquals(1, listaUzytkownikow1.size());
        Assert.assertEquals("Janek123", listaUzytkownikow1.get(0).getUzytkownikLogin());
        Assert.assertEquals("11111", listaUzytkownikow1.get(0).getUzytkownikHaslo());
        Assert.assertEquals(Status.Ksiegowy, listaUzytkownikow1.get(0).getUzytkownikStatus());
        Assert.assertEquals("Jan1111", listaUzytkownikow1.get(0).getUzytkownikImie());
        Assert.assertEquals("Kowalski1111", listaUzytkownikow1.get(0).getUzytkownikNazwisko());
    }

    @Test
    public void testUsuwanie() {
        IBaza baza = new BazaDanych();
        IKonfiguracja konfiguracja = new Konfiguracja(baza);
        String wiadomosc1 = generujBazeUzytkownikow();

        String wiadomosc2 = konfiguracja.usunUzytkownika(new UzytkownikModel(1), 2);
        List<UzytkownikModel> listaUzytkownikow1 = konfiguracja.znajdzUzytkownikow(new UzytkownikModel());

        String wiadomosc3 = konfiguracja.usunUzytkownika(new UzytkownikModel(2), 2);
        List<UzytkownikModel> listaUzytkownikow2 = konfiguracja.znajdzUzytkownikow(new UzytkownikModel());

        Assert.assertEquals("Użytkownik został pomyślnie dodany do bazy danych.", wiadomosc1);
        Assert.assertEquals("Użytkownik został pomyślnie usunięty z bazy danych.", wiadomosc2);
        Assert.assertEquals("Błąd! Nie możesz usunąć własnego konta.", wiadomosc3);

        Assert.assertNotNull(listaUzytkownikow1);
        Assert.assertEquals(2, listaUzytkownikow1.size());
        Assert.assertNotNull(listaUzytkownikow2);
        Assert.assertEquals(2, listaUzytkownikow2.size());

        Assert.assertEquals("Tadek123", listaUzytkownikow2.get(0).getUzytkownikLogin());
        Assert.assertEquals("Maria123", listaUzytkownikow2.get(1).getUzytkownikLogin());
    }

    @Test
    public void testPrzedsiebiorstwo() {
        IBaza baza = new BazaDanych();
        IKonfiguracja konfiguracja = new Konfiguracja(baza);
        String wiadomosc1 = generujPrzedsiebiorstwo();
        String wiadomosc2 = konfiguracja.zmienDaneFirmy(new PrzedsiebiorstwoModel("Firma2", "Jan2", "Kowalski2", "Kwiatowa 11/23, 11-222 Warszawa2", 67890));

        PrzedsiebiorstwoModel przedsiebiorstwo = konfiguracja.zwrocDaneFirmy();
        Assert.assertEquals("Dane przedsiębiorstwa zostały wprowadzone pomyślnie.", wiadomosc1);
        Assert.assertEquals("Dane przedsiębiorstwa zostały wprowadzone pomyślnie.", wiadomosc2);
        Assert.assertEquals("Firma2", przedsiebiorstwo.getPrzedsiebiorstwoNazwa());
    }
}
