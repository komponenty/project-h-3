package glowne.KonfiguracjaComponent.Implementation;

import java.util.List;
import glowne.BazaDanychComponent.Contract.IBaza;
import glowne.KonfiguracjaComponent.Contract.IKonfiguracja;
import glowne.KonfiguracjaComponent.Model.PrzedsiebiorstwoModel;
import glowne.KonfiguracjaComponent.Model.Status;
import glowne.KonfiguracjaComponent.Model.UzytkownikModel;

/**
 * @author Katarzyna Romanowska
 * @version 1.0.1.2
 */
public class Konfiguracja implements IKonfiguracja {

    private IBaza bazaDanych;

    /**
     * Konstruktor zapewniajacy podstawowe funkcjonalnosci komponentu
     * @param bazaDanych repozytorium używane do przeprowadzania operacji nad bazą
     */
    public Konfiguracja(IBaza bazaDanych) {
        this.bazaDanych = bazaDanych;
        
        List<UzytkownikModel> uzytkownicy = znajdzUzytkownikow(new UzytkownikModel(Status.Administrator));
        if (uzytkownicy == null || uzytkownicy.isEmpty()) {
            dodajUzytkownika(new UzytkownikModel("admin", "admin", Status.Administrator, "admin", "admin"));
        }
        
        zwrocDaneFirmy();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<UzytkownikModel> znajdzUzytkownikow(UzytkownikModel szablon) {
        return bazaDanych.znajdzObiekt("BazaUzytkownikow.yap", true, szablon);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String dodajUzytkownika(UzytkownikModel nowyUzytkownik) {
        List<UzytkownikModel> listaUzytkownikow = bazaDanych.znajdzObiekt("BazaUzytkownikow.yap", true, new UzytkownikModel());
        if (listaUzytkownikow == null) {
            return "Błąd bazy! Skonsultuj się z dostawcą oprogramowania!";
        }

        int uzytkownikID = 1;
        for (UzytkownikModel uzytkownikModel : listaUzytkownikow) {
            if (uzytkownikModel.getUzytkownikLogin().equalsIgnoreCase(nowyUzytkownik.getUzytkownikLogin())) {
                return "Błąd! Istnieje już użytkownik o podanym loginie.";
            }
            if (uzytkownikModel.getUzytkownikID() >= uzytkownikID) {
                uzytkownikID = uzytkownikModel.getUzytkownikID() + 1;
            }
        }
        nowyUzytkownik.setUzytkownikID(uzytkownikID);

        boolean wynikOperacji = bazaDanych.dodajObiekt("BazaUzytkownikow.yap", true, nowyUzytkownik);
        if (wynikOperacji) {
            return "Użytkownik został pomyślnie dodany do bazy danych.";
        } else {
            return "Błąd bazy! Skonsultuj się z dostawcą oprogramowania!";
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String edytujUzytkownika(UzytkownikModel szablon, UzytkownikModel uzytkownikWyedytowany) {
        List<UzytkownikModel> listaUzytkownikow = bazaDanych.znajdzObiekt("BazaUzytkownikow.yap", true, szablon);

        if (listaUzytkownikow == null) {
            return "Błąd bazy! Skonsultuj się z dostawcą oprogramowania!";
        }
        uzytkownikWyedytowany.setUzytkownikID(listaUzytkownikow.get(0).getUzytkownikID());
        uzytkownikWyedytowany.setUzytkownikLogin(listaUzytkownikow.get(0).getUzytkownikLogin());

        boolean wynikOperacji = bazaDanych.edytujObiekt("BazaUzytkownikow.yap", true, szablon, uzytkownikWyedytowany);
        if (wynikOperacji) {
            return "Użytkownik został pomyślnie zaktualizowany.";
        } else {
            return "Błąd bazy! Skonsultuj się z dostawcą oprogramowania!";
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String usunUzytkownika(UzytkownikModel szablon, int uzytkownikAktualnieZalogowanyID) {
        if (szablon.getUzytkownikID() == uzytkownikAktualnieZalogowanyID) {
            return "Błąd! Nie możesz usunąć własnego konta.";
        }

        boolean wynikOperacji = bazaDanych.usunObiekt("BazaUzytkownikow.yap", true, szablon);
        if (wynikOperacji) {
            return "Użytkownik został pomyślnie usunięty z bazy danych.";
        } else {
            return "Błąd bazy! Skonsultuj się z dostawcą oprogramowania!";
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PrzedsiebiorstwoModel zwrocDaneFirmy() {
        List<PrzedsiebiorstwoModel> przedsiebiorstwoModelList = bazaDanych.znajdzObiekt("DanePrzedsiebiorstwa.yap", false, new PrzedsiebiorstwoModel());
        if (przedsiebiorstwoModelList == null) {
            PrzedsiebiorstwoModel dane = new PrzedsiebiorstwoModel("Wprowadz nazwe przedsiebiorstwa", "Wprowadz imie własciciela", "Wprowadz nazwisko wlasciciela", "Wprowadz adres przedsiebiorstwa", 0);
            zmienDaneFirmy(dane);
            return dane; 
        }
        else if (przedsiebiorstwoModelList.isEmpty()) {
            PrzedsiebiorstwoModel dane = new PrzedsiebiorstwoModel("Wprowadz nazwe przedsiebiorstwa", "Wprowadz imie własciciela", "Wprowadz nazwisko wlasciciela", "Wprowadz adres przedsiebiorstwa", 0);
            zmienDaneFirmy(dane);
            return dane; 
        }
        else {
            return przedsiebiorstwoModelList.get(0);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String zmienDaneFirmy(PrzedsiebiorstwoModel noweDane) {
        List<PrzedsiebiorstwoModel> lista = bazaDanych.znajdzObiekt("DanePrzedsiebiorstwa.yap", false, new PrzedsiebiorstwoModel());
        boolean wynikOperacji;

        if (lista == null) {
            return "Błąd bazy! Skonsultuj się z dostawcą oprogramowania!";
        } else if (lista.size() == 0) {
            wynikOperacji = bazaDanych.dodajObiekt("DanePrzedsiebiorstwa.yap", false, noweDane);
        } else {
            wynikOperacji = bazaDanych.edytujObiekt("DanePrzedsiebiorstwa.yap", false, new PrzedsiebiorstwoModel(), noweDane);
        }

        if (wynikOperacji) {
            return "Dane przedsiębiorstwa zostały wprowadzone pomyślnie.";
        } else {
            return "Błąd bazy! Skonsultuj się z dostawcą oprogramowania!";
        }
    }
}
