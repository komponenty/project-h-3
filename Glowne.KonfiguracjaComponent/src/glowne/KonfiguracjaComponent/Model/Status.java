package glowne.KonfiguracjaComponent.Model;

/**
 * @author Katarzyna Romanowska
 * @version 1.0.1.0
 */
public enum Status {

    Administrator,
    Ksiegowy,
    Sprzedawca
}
