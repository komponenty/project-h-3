package glowne.KonfiguracjaComponent.Model;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

/**
 * @author Katarzyna Romanowska
 * @version 1.0.1.2
 */
public class UzytkownikModel {

    //<editor-fold defaultstate="collapsed" desc="Pola">
    private int uzytkownikID;
    private String uzytkownikLogin;
    private String uzytkownikHaslo;
    private Status uzytkownikStatus;
    private String uzytkownikImie;
    private String uzytkownikNazwisko;
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Wlasciwosci">
    public void setUzytkownikID(int uzytkownikID) {
        int oldValue = this.uzytkownikID;
        this.uzytkownikID = uzytkownikID;
        changeSupport.firePropertyChange("uzytkownikID", oldValue, uzytkownikID);
    }

    public int getUzytkownikID() {
        return this.uzytkownikID;
    }

    public void setUzytkownikLogin(String uzytkownikLogin) {
        String oldValue = this.uzytkownikLogin;
        this.uzytkownikLogin = uzytkownikLogin;
        changeSupport.firePropertyChange("uzytkownikLogin", oldValue, uzytkownikLogin);
    }

    public String getUzytkownikLogin() {
        return this.uzytkownikLogin;
    }

    public void setUzytkownikHaslo(String uzytkownikHaslo) {
        String oldValue = this.uzytkownikHaslo;
        this.uzytkownikHaslo = uzytkownikHaslo;
        changeSupport.firePropertyChange("uzytkownikHaslo", oldValue, uzytkownikHaslo);
    }

    public String getUzytkownikHaslo() {
        return this.uzytkownikHaslo;
    }

    public void setUzytkownikStatus(Status uzytkownikStatus) {
        Status oldValue = this.uzytkownikStatus;
        this.uzytkownikStatus = uzytkownikStatus;
        changeSupport.firePropertyChange("uzytkownikStatus", oldValue, uzytkownikStatus);
    }

    public Status getUzytkownikStatus() {
        return this.uzytkownikStatus;
    }

    public void setUzytkownikImie(String uzytkownikImie) {
        String oldValue = this.uzytkownikImie;
        this.uzytkownikImie = uzytkownikImie;
        changeSupport.firePropertyChange("uzytkownikImie", oldValue, uzytkownikImie);
    }

    public String getUzytkownikImie() {
        return this.uzytkownikImie;
    }

    public void setUzytkownikNazwisko(String uzytkownikNazwisko) {
        String oldValue = this.uzytkownikNazwisko;
        this.uzytkownikNazwisko = uzytkownikNazwisko;
        changeSupport.firePropertyChange("uzytkownikNazwisko", oldValue, uzytkownikNazwisko);
    }

    public String getUzytkownikNazwisko() {
        return this.uzytkownikNazwisko;
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Konstruktory">
    public UzytkownikModel() {
    }

    public UzytkownikModel(int uzytkownikID) {
        this.uzytkownikID = uzytkownikID;
    }
    
    public UzytkownikModel(Status uzytkownikStatus) {
        this.uzytkownikStatus = uzytkownikStatus;
    }
    
    public UzytkownikModel(String uzytkownikLogin, String uzytkownikHaslo) {
        this.uzytkownikLogin = uzytkownikLogin;
        this.uzytkownikHaslo = uzytkownikHaslo;
    }

    public UzytkownikModel(String uzytkownikHaslo, Status uzytkownikStatus, String uzytkownikImie, String uzytkownikNazwisko) {
        this.uzytkownikHaslo = uzytkownikHaslo;
        this.uzytkownikStatus = uzytkownikStatus;
        this.uzytkownikImie = uzytkownikImie;
        this.uzytkownikNazwisko = uzytkownikNazwisko;
    }

    public UzytkownikModel(String uzytkownikLogin, String uzytkownikHaslo, Status uzytkownikStatus, String uzytkownikImie, String uzytkownikNazwisko) {
        this(uzytkownikHaslo, uzytkownikStatus, uzytkownikImie, uzytkownikNazwisko);
        this.uzytkownikLogin = uzytkownikLogin;
    }

    public UzytkownikModel(int uzytkownikID, String uzytkownikLogin, String uzytkownikHaslo, Status uzytkownikStatus, String uzytkownikImie, String uzytkownikNazwisko) {
        this(uzytkownikLogin, uzytkownikHaslo, uzytkownikStatus, uzytkownikImie, uzytkownikNazwisko);
        this.uzytkownikID = uzytkownikID;
    }
    
    public UzytkownikModel(UzytkownikModel uzytkownikModel) {
        this.uzytkownikID = uzytkownikModel.getUzytkownikID();
        this.uzytkownikHaslo = uzytkownikModel.getUzytkownikHaslo();
        this.uzytkownikStatus = uzytkownikModel.getUzytkownikStatus();
        this.uzytkownikImie = uzytkownikModel.getUzytkownikImie();
        this.uzytkownikNazwisko = uzytkownikModel.getUzytkownikNazwisko();
        this.uzytkownikLogin = uzytkownikModel.getUzytkownikLogin();
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="PropertyChange">
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="toString method">
    @Override
    public String toString() {
        return this.uzytkownikLogin + " - " + this.uzytkownikImie + " " + this.uzytkownikNazwisko;
    }
    //</editor-fold>
}
