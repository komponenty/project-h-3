package glowne.KonfiguracjaComponent.Model;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

/**
 * @author Katarzyna Romanowska
 * @version 1.0.1.1
 */
public class PrzedsiebiorstwoModel {

    //<editor-fold defaultstate="collapsed" desc="Pola">
    private String przedsiebiorstwoNazwa;
    private String przedsiebiorstwoWlascicielImie;
    private String przedsiebiorstwoWlascicielNazwisko;
    private String przedsiebiorstwoAdres;
    private long przedsiebiorstwoNIP;
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Wlasciwosci">
    public void setPrzedsiebiorstwoNazwa(String przedsiebiorstwoNazwa) {
        String oldValue = this.przedsiebiorstwoNazwa;
        this.przedsiebiorstwoNazwa = przedsiebiorstwoNazwa;
        changeSupport.firePropertyChange("przedsiebiorstwoNazwa", oldValue, przedsiebiorstwoNazwa);
    }

    public String getPrzedsiebiorstwoNazwa() {
        return this.przedsiebiorstwoNazwa;
    }

    public void setPrzedsiebiorstwoWlascicielImie(String przedsiebiorstwoWlascicielImie) {
        String oldValue = this.przedsiebiorstwoWlascicielImie;
        this.przedsiebiorstwoWlascicielImie = przedsiebiorstwoWlascicielImie;
        changeSupport.firePropertyChange("przedsiebiorstwoWlascicielImie", oldValue, przedsiebiorstwoWlascicielImie);
    }

    public String getPrzedsiebiorstwoWlascicielImie() {
        return this.przedsiebiorstwoWlascicielImie;
    }

    public void setPrzedsiebiorstwoWlascicielNazwisko(String przedsiebiorstwoWlascicielNazwisko) {
        String oldValue = this.przedsiebiorstwoWlascicielNazwisko;
        this.przedsiebiorstwoWlascicielNazwisko = przedsiebiorstwoWlascicielNazwisko;
        changeSupport.firePropertyChange("przedsiebiorstwoWlascicielNazwisko", oldValue, przedsiebiorstwoWlascicielNazwisko);
    }

    public String getPrzedsiebiorstwoWlascicielNazwisko() {
        return this.przedsiebiorstwoWlascicielNazwisko;
    }

    public void setPrzedsiebiorstwoAdres(String przedsiebiorstwoAdres) {
        String oldValue = this.przedsiebiorstwoAdres;
        this.przedsiebiorstwoAdres = przedsiebiorstwoAdres;
        changeSupport.firePropertyChange("przedsiebiorstwoAdres", oldValue, przedsiebiorstwoAdres);
    }

    public String getPrzedsiebiorstwoAdres() {
        return this.przedsiebiorstwoAdres;
    }

    public void setPrzedsiebiorstwoNIP(long przedsiebiorstwoNIP) {
        long oldValue = this.przedsiebiorstwoNIP;
        this.przedsiebiorstwoNIP = przedsiebiorstwoNIP;
        changeSupport.firePropertyChange("przedsiebiorstwoNIP", oldValue, przedsiebiorstwoNIP);
    }

    public long getPrzedsiebiorstwoNIP() {
        return this.przedsiebiorstwoNIP;
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Konstruktory">
    public PrzedsiebiorstwoModel() {
    }

    public PrzedsiebiorstwoModel(String przedsiebiorstwoNazwa, String przedsiebiorstwoWlascicielImie, String przedsiebiorstwoWlascicielNazwisko, String przedsiebiorstwoAdres, long przedsiebiorstwoNIP) {
        this.przedsiebiorstwoNazwa = przedsiebiorstwoNazwa;
        this.przedsiebiorstwoWlascicielImie = przedsiebiorstwoWlascicielImie;
        this.przedsiebiorstwoWlascicielNazwisko = przedsiebiorstwoWlascicielNazwisko;
        this.przedsiebiorstwoAdres = przedsiebiorstwoAdres;
        this.przedsiebiorstwoNIP = przedsiebiorstwoNIP;
    }
    
    public PrzedsiebiorstwoModel(PrzedsiebiorstwoModel przedsiebiorstwoModel) {
        this.przedsiebiorstwoNazwa = przedsiebiorstwoModel.getPrzedsiebiorstwoNazwa();
        this.przedsiebiorstwoWlascicielImie = przedsiebiorstwoModel.getPrzedsiebiorstwoWlascicielImie();
        this.przedsiebiorstwoWlascicielNazwisko = przedsiebiorstwoModel.getPrzedsiebiorstwoWlascicielNazwisko();
        this.przedsiebiorstwoAdres = przedsiebiorstwoModel.getPrzedsiebiorstwoAdres();
        this.przedsiebiorstwoNIP = przedsiebiorstwoModel.getPrzedsiebiorstwoNIP();
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="PropertyChange">
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }
    //</editor-fold>
}
