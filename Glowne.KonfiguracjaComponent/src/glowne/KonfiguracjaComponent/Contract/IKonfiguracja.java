package glowne.KonfiguracjaComponent.Contract;

import java.util.List;
import glowne.KonfiguracjaComponent.Model.PrzedsiebiorstwoModel;
import glowne.KonfiguracjaComponent.Model.UzytkownikModel;

/**
 * @author Katarzyna Romanowska
 * @version 1.0.1.0
 */
public interface IKonfiguracja {

    /**
     * Znajduje użytkowników odpowiadających danemu szablonowi
     * @param szablon szablon użytkownika
     * @return List<UzytkownikModel> - w przypadku znalezienia lub nie (dlugosc listy = 0) rekordów odpowiadających danemu szablonowi
     *         <br>
     *         null -  w przypadku błędu krytycznego
     */
    List<UzytkownikModel> znajdzUzytkownikow(UzytkownikModel szablon);

    /**
     * Dodaje nowego użytkownika do bazy danych
     * @param nowyUzytkownik użytkownik do dodania
     * @return "Użytkownik został pomyślnie dodany do bazy danych." - w przypadku pomyślnego dodania do bazy
     *          <br>
     *         "Błąd bazy! Skonsultuj się z dostawcą oprogramowania!" - w przypadku błędu krytycznego
     */
    String dodajUzytkownika(UzytkownikModel nowyUzytkownik);

    /**
     * Aktualizuje danego użytkownika w bazie danych
     * @param szablon użytkownik przed edycją (stary)
     * @param uzytkownikWyedytowany kontrahent po edycji (nowy)
     * @return "Użytkownik został pomyślnie zaktualizowany." - w przypadku pomyślnej aktualizacji rekordu w bazie
     *          <br>
     *         "Błąd bazy! Skonsultuj się z dostawcą oprogramowania!" - w przypadku błędu krytycznego
     */
    String edytujUzytkownika(UzytkownikModel szablon, UzytkownikModel uzytkownikWyedytowany);

    /**
     * Usuwa danego kontrahenta z bazy danych
     * @param szablon użytkownik do usunięcia
     * @param uzytkownikAktualnieZalogowanyID numer identyfikacyjny aktualnie zalogowanego użytkownika
     * @return "Użytkownik został pomyślnie usunięty z bazy danych." - w przypadku pomyślnego usunięcia rekordu z bazy
     *          <br>
     *         "Błąd! Nie możesz usunąć własnego konta." - w przypadku próby usunięcia własnego konta użytkownika
     *          <br>
     *         "Błąd bazy! Skonsultuj się z dostawcą oprogramowania!" - w przypadku błędu krytycznego
     */
    String usunUzytkownika(UzytkownikModel szablon, int uzytkownikAktualnieZalogowanyID);

    /**
     * Znajduje dane firmy zapisane w bazie
     * @return PrzedsiebiorstwoModel - dane firmy
     */
    PrzedsiebiorstwoModel zwrocDaneFirmy();

    /**
     * Aktualizuje dane firmy w bazie
     * @param noweDane nowe dane firmy
     * @return "Dane przedsiębiorstwa zostały wprowadzone pomyślnie." - w przypadku pomyślnej aktualizacji rekordu w bazie
     *          <br>
     *         "Błąd bazy! Skonsultuj się z dostawcą oprogramowania!" - w przypadku błędu krytycznego
     */
    String zmienDaneFirmy(PrzedsiebiorstwoModel noweDane);
}
