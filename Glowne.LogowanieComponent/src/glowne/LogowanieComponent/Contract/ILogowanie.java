package glowne.LogowanieComponent.Contract;

import glowne.KonfiguracjaComponent.Model.UzytkownikModel;
import javax.swing.JFrame;

/**
 * @author Jakub Dydo
 * @version 2.0.0.0
 */
public interface ILogowanie {

    /**
     * Zwraca dane aktualnie zalogowanego użytkownika (ID, imię, nazwisko, status)
     * @return dane aktualnie zalogowanego użytkownika
     */
    UzytkownikModel getUzytkownik();

    /**
     * Ustawia JFrame, który ma się uaktywnić po pomyślnym zalogowaniu
     * @param frame JFrame, który ma się uaktywnić po pomyślnym zalogowaniu
     */
    void setMainFrame(JFrame frame);

    /**
     * Sprawdza czy użytkownik podał dobre dane dostępowe, jeżeli tak - uaktywnia JFrame
     * @param login wprowadzony login użytkownika
     * @param password wprowdzone hasło uytkownika
     * @return true - dane dostępowe poprawne</br>
     *         false - dane dostępowe nie poprawne
     */
    boolean zaloguj(String login, String password);

}
