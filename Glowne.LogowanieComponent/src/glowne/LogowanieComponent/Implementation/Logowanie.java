package glowne.LogowanieComponent.Implementation;

import glowne.BazaDanychComponent.Contract.IBaza;
import glowne.KonfiguracjaComponent.Model.UzytkownikModel;
import glowne.LogowanieComponent.Contract.ILogowanie;
import glowne.LogowanieComponent.Frame.LogowanieFrame;
import java.util.List;
import javax.swing.JFrame;

/**
 * @author Jakub Dydo
 * @version 2.0.0.0
 */
public class Logowanie implements ILogowanie {

    private UzytkownikModel uzytkownik;
    private boolean zalogowanie = false;

    private JFrame loginFrame;
    private JFrame mainFrame;

    private IBaza bazaDanych;

    /**
     * Konstruktor zapewniajacy podstawowe funkcjonalnosci komponentu
     *
     * @param bazaDanych repozytorium używane do przeprowadzania operacji nad
     * bazą
     */
    public Logowanie(IBaza bazaDanych) {
        this.uzytkownik = new UzytkownikModel();
        this.bazaDanych = bazaDanych;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UzytkownikModel getUzytkownik() {
        return new UzytkownikModel(this.uzytkownik);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setMainFrame(JFrame frame) {
        this.mainFrame = frame;
        this.mainFrame.setVisible(false);
        this.loginFrame = new LogowanieFrame(this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean zaloguj(String login, String password) {
        List<UzytkownikModel> lista = bazaDanych.znajdzObiekt("BazaUzytkownikow.yap", true, new UzytkownikModel(login, password));

        if (lista == null || lista.isEmpty()) {
            loginFrame.setVisible(true);
            mainFrame.setVisible(false);
            return false;
        } else {
            this.uzytkownik.setUzytkownikID(lista.get(0).getUzytkownikID());
            this.uzytkownik.setUzytkownikImie(lista.get(0).getUzytkownikImie());
            this.uzytkownik.setUzytkownikNazwisko(lista.get(0).getUzytkownikNazwisko());
            this.uzytkownik.setUzytkownikStatus(lista.get(0).getUzytkownikStatus());
            loginFrame.setVisible(false);
            mainFrame.setVisible(true);
            this.mainFrame.validate();
            this.mainFrame.repaint();
            return true;
        }
    }
}
