package magazyn.RaportyProsteComponent.Implementation;

import java.util.List;
import glowne.BazaDanychComponent.Contract.IBaza;
import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import magazyn.ListaTowarowComponent.Model.TowarModel;
import magazyn.RaportyProsteComponent.Contract.IRaportyProste;
import magazyn.RaportyProsteComponent.Predicate.ProduktyBliskiePrzeterminowaniuPredicate;
import magazyn.RaportyProsteComponent.Predicate.ProduktyNaWyczerpaniuPredicate;
import magazyn.RaportyProsteComponent.Predicate.ProduktyPrzeterminowanePredicate;
import magazyn.RaportyProsteComponent.Predicate.ProduktyWyczerpanePredicate;
import pk.epub.Creator.Contracts.ICreator;

/**
 * @author Jakub Dydo
 * @version 1.0.2.0
 */
public class RaportyProste implements IRaportyProste {

    private IBaza bazaDanych;
    private ICreator creator;

    /**
     * Konstruktor zapewniajacy podstawowe funkcjonalnosci komponentu
     *
     * @param bazaDanych repozytorium używane do przeprowadzania operacji nad
     * bazą
     * @param creator komponent potrzebny do zrzutu raportu do pliku
     */
    public RaportyProste(IBaza bazaDanych, ICreator creator) {
        this.bazaDanych = bazaDanych;
        this.creator = creator;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<TowarModel> raportProduktyWyczerpane() {
        List<TowarModel> listaTowarow = bazaDanych.znajdzObiektPredicate("BazaTowarow.yap", false, new ProduktyWyczerpanePredicate());
        return listaTowarow;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<TowarModel> raportProduktyNaWyczerpaniu() {
        List<TowarModel> listaTowarow = bazaDanych.znajdzObiektPredicate("BazaTowarow.yap", false, new ProduktyNaWyczerpaniuPredicate());
        return listaTowarow;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<TowarModel> raportProduktyPrzeterminowane(Date data) {
        List<TowarModel> listaTowarow = bazaDanych.znajdzObiektPredicate("BazaTowarow.yap", false, new ProduktyPrzeterminowanePredicate(data));
        return listaTowarow;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<TowarModel> raportProduktyBliskiePrzeterminowania(Date data) {
        List<TowarModel> listaTowarow = bazaDanych.znajdzObiektPredicate("BazaTowarow.yap", false, new ProduktyBliskiePrzeterminowaniuPredicate(data));
        return listaTowarow;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void doPlikuRaport(List<TowarModel> listaTowarow, String tytul, Date data, String sciezkaDoPliku) {
        String dataString = "";

        if (data != null) {
            dataString = new SimpleDateFormat("dd-MM-yyyy").format(data);
        } else {
            dataString = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
        }

        creator.setDir(sciezkaDoPliku);
        creator.setTitle(tytul);
        creator.setLanguage("pl");
        creator.addIdentifier(tytul, tytul);

        String style = "@charset \"utf-8\";\n"
                + "body { width: 100%; }\n"
                + ".pp { margin: 20px; }\n"
                + "table { margin-bottom:20px; }\n"
                + ".ppp { font-size:22px; margin: 20px; }\n"
                + ".pppp { font-size:18px; }";

        creator.setStyle(style);

        String html = "<html>\n"
                + "	<head>\n"
                + "		<link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\" />\n"
                + "	</head>\n"
                + "	<body>\n"
                + "    <table width=\"800px\" border=\"0\">\n"
                + "		<tr>\n"
                + "			<td width=\"400px\" height=\"100px\" colspan=\"5\" valign=\"middle\" align=\"center\"><p class=\"ppp\"><b>" + tytul.toUpperCase() + "</b></td>\n"
                + "		</tr>\n"
                + "        <tr>\n"
                + "			<td width=\"400px\" colspan=\"5\"><p class=\"pp\">\n"
                + "                     Raport na dzień: <b>" + dataString + "</b></p></td>\n"
                + "		</tr>\n"
                + "	</table>\n"
                + "    \n"
                + "    <table width=\"800px\" border=\"1\">\n"
                + "        <tr>\n"
                + "            <td width=\"300px\" valign=\"middle\" align=\"center\"><b>Nazwa towaru</b></td>\n"
                + "            <td width=\"150px\" valign=\"middle\" align=\"center\"><b>Producent</b></td>\n"
                + "            <td width=\"150px\" valign=\"middle\" align=\"center\"><b>Identifikator</b></td>\n";

        if (data != null) {
            html += "       <td width=\"100px\" valign=\"middle\" align=\"center\"><b>Data ważnosci</b></td>\n"
                    + "		</tr>\n";
        } else {
            html += "       <td width=\"100px\"valign=\"middle\" align=\"center\"><b>Ilosć</b></td>\n"
                    + "		</tr>\n";
        }

        for (TowarModel towar : listaTowarow) {

            html += "        <tr>\n"
                    + "            <td width=\"300px\" valign=\"middle\" align=\"center\">" + towar.getTowarnNazwa() + "</td>\n"
                    + "            <td width=\"150px\" valign=\"middle\" align=\"center\">" + towar.getTowarProducent() + "</td>\n"
                    + "            <td  width=\"150px\" valign=\"middle\" align=\"center\">" + towar.getTowarID() + "</td>\n";

            if (data != null) {
                html += "       <td width=\"100px\"valign=\"middle\" align=\"center\">" + new SimpleDateFormat("dd-MM-yyyy").format(towar.getTowarTerminWaznosci()) + "</td>\n"
                        + "		</tr>\n";
            } else {
                html += "       <td width=\"100px\" valign=\"middle\" align=\"center\">" + (Math.round(towar.getTowarIlosc()* 100.0) / 100.0) + "</td>\n"
                        + "		</tr>\n";
            }
        }

        html += "	</body>\n"
                + "</html>";

        creator.addChapter(html);
        
        creator.createEpub();
        creator.clear();

        try {
            Desktop.getDesktop().open(new File(sciezkaDoPliku));
        } catch (IOException e) {
            System.out.print(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String ignorujTowar(TowarModel towarDoZignorowania) {
        TowarModel szablon = (TowarModel) bazaDanych.znajdzObiekt("BazaTowarow.yap", false, towarDoZignorowania).get(0);
        towarDoZignorowania.setTowarIgnorowanie(true);
        if (bazaDanych.edytujObiekt("BazaTowarow.yap", false, szablon, towarDoZignorowania)) {
            return "Obiekt będzie ignorowany przy generowaniu przyszłych raportów.";
        } else {
            return "Błąd bazy! Skonsultuj się z dostawcą oprogramowania!";
        }
    }
}
