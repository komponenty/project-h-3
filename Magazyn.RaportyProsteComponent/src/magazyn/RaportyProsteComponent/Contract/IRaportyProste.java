package magazyn.RaportyProsteComponent.Contract;

import java.util.Date;
import java.util.List;
import magazyn.ListaTowarowComponent.Model.TowarModel;

/**
 * @author Jakub Dydo
 * @version 1.0.2.0
 */
public interface IRaportyProste {

    /**
     * Znajduje towary, które są na wyczerpaniu ( < 10 )
     * @return List<KontrahentModel> - w przypadku znalezienia lub nie (dlugosc listy = 0) rekordów odpowiadających danemu kryterium
     *         <br>
     *         null -  w przypadku błędu krytycznego
     */
    List<TowarModel> raportProduktyWyczerpane();

    /**
     * Znajduje towary, których zapas już się wyczerpał
     * @return List<KontrahentModel> - w przypadku znalezienia lub nie (dlugosc listy = 0) rekordów odpowiadających danemu kryterium
     *         <br>
     *         null -  w przypadku błędu krytycznego
     */
    List<TowarModel> raportProduktyNaWyczerpaniu();

    /**
     * Znajduje towary, które są przeterminowane
     * @param data data, przed która produkty ulegną przeterminowaniu
     * @return List<KontrahentModel> - w przypadku znalezienia lub nie (dlugosc listy = 0) rekordów odpowiadających danemu kryterium
     *         <br>
     *         null -  w przypadku błędu krytycznego
     */
    List<TowarModel> raportProduktyPrzeterminowane(Date data);

    /**
     * Znajduje towary, które są bliskie przeterminowania (termin przydatności upływa w podanym miesiący i roku)
     * @param data data, po której produkty będą blisko przeterminowania
     * @return List<KontrahentModel> - w przypadku znalezienia lub nie (dlugosc listy = 0) rekordów odpowiadających danemu kryterium
     *         <br>
     *         null -  w przypadku błędu krytycznego
     */
    List<TowarModel> raportProduktyBliskiePrzeterminowania(Date data);

    /**
     * Zrzuca raport towarów do pliku w formacie EPUB
     * @param listaTowarow lista towarów składająca się na raport
     * @param tytul tytuł raportu
     * @param data data raportu
     * @param sciezkaDoPliku sciezka gdzie ma być zapisany plik z raportem
     */
    void doPlikuRaport(List<TowarModel> listaTowarow, String tytul, Date data, String sciezkaDoPliku);

    /**
     * Zaznacza towar jako 'ignorowany'. W przyszłych raportach nię będzie on brany pod uwagę.
     * @param towarDoZignorowania towar do zignorowania
     * @return "Obiekt będzie ignorowany przy generowaniu przyszłych raportów." - w przypadku pomyślnej aktualizacji rekordu w bazie
     *          <br>
     *         "Błąd bazy! Skonsultuj się z dostawcą oprogramowania!" - w przypadku błędu krytycznego
     */
    String ignorujTowar(TowarModel towarDoZignorowania);
}
