package magazyn.RaportyProsteComponent.Predicate;

import com.db4o.query.Predicate;
import magazyn.ListaTowarowComponent.Model.TowarModel;

/**
 * @author Jakub Dydo
 * @version 1.0.1.0
 */
public class ProduktyWyczerpanePredicate extends Predicate<TowarModel> {

    /**
     * Metoda sprawdza czy dany towar odpowiada kryteriom
     * @param towar sprawdzany towar
     * @return true - towar jest zgodny z kryteriami
     *         <br>
     *         false - towar jest niezgodny z kryteriami
     */
    @Override
    public boolean match(TowarModel towar) {
        TowarModel towarModel = towar;
        return towarModel.getTowarIlosc() == 0.0
                && !towarModel.getTowarIgnorowanie()
                && !towarModel.getTowarUzytekWlasny();
    }

}
