package magazyn.RaportyProsteComponent.Predicate;

import java.util.Date;
import com.db4o.query.Predicate;
import magazyn.ListaTowarowComponent.Model.TowarModel;

/**
 * @author Jakub Dydo
 * @version 1.0.1.1
 */
public class ProduktyBliskiePrzeterminowaniuPredicate extends Predicate<TowarModel> {

    Date data1;
    Date data2;

    /**
     * @param data data, po której produkty będą blisko przeterminowania
     */
    public ProduktyBliskiePrzeterminowaniuPredicate(Date data) {
       this.data1 = data;
       this.data2 = new Date(this.data1.getTime());
       this.data2.setTime(this.data2.getTime() + 604800017);
    }

    /**
     * Metoda sprawdza czy dany towar odpowiada kryteriom
     * @param towar sprawdzany towar
     * @return true - towar jest zgodny z kryteriami
     *         <br>
     *         false - towar jest niezgodny z kryteriami
     */
    @SuppressWarnings("deprecation")
    @Override
    public boolean match(TowarModel towar) {
        Date kalendarzDol = this.data1;
        Date kalendarzGora = this.data2;
        TowarModel towarModel = towar;

        return towarModel.getTowarTerminWaznosci() != null
                && towarModel.getTowarTerminWaznosci().compareTo(kalendarzDol) > 0
                && towarModel.getTowarTerminWaznosci().compareTo(kalendarzGora) < 0
                && !towarModel.getTowarIgnorowanie()
                && !towarModel.getTowarUzytekWlasny()
                && towarModel.getTowarIlosc() > 0.0;
    }
}
