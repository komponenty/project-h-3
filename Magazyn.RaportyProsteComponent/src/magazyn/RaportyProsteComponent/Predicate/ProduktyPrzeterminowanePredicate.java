package magazyn.RaportyProsteComponent.Predicate;

import java.util.Date;
import com.db4o.query.Predicate;
import magazyn.ListaTowarowComponent.Model.TowarModel;

/**
 * @author Jakub Dydo
 * @version 1.0.1.1
 */
public class ProduktyPrzeterminowanePredicate extends Predicate<TowarModel> {

    Date data;
 
    /**
     * @param data data, przed która produkty ulegną przeterminowaniu
     */
    public ProduktyPrzeterminowanePredicate(Date data) {
        this.data = data;
    }

    /**
     * Metoda sprawdza czy dany towar odpowiada kryteriom
     * @param towar sprawdzany towar
     * @return true - towar jest zgodny z kryteriami
     *         <br>
     *         false - towar jest niezgodny z kryteriami
     */
    @SuppressWarnings("deprecation")
    @Override
    public boolean match(TowarModel towar) {
        Date kalendarzGora = this.data;
        TowarModel towarModel = towar;

        return towarModel.getTowarTerminWaznosci() != null
                && towarModel.getTowarTerminWaznosci().compareTo(kalendarzGora) <= 0
                && !towarModel.getTowarIgnorowanie()
                && !towarModel.getTowarUzytekWlasny()
                && towarModel.getTowarIlosc() > 0.0;
    }
}
