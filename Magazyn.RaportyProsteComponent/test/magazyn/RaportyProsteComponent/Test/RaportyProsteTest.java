package magazyn.RaportyProsteComponent.Test;

import java.util.Date;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;
import glowne.BazaDanychComponent.Implementation.BazaDanych;
import test.resetBazy.ResetBazy;
import magazyn.ListaTowarowComponent.Contract.IListaTowarow;
import magazyn.ListaTowarowComponent.Implementation.ListaTowarow;
import magazyn.ListaTowarowComponent.Model.TowarModel;
import magazyn.RaportyProsteComponent.Contract.IRaportyProste;
import magazyn.RaportyProsteComponent.Implementation.RaportyProste;
import pk.epub.Creator.Creator;

/**
 * @author Jakub Dydo
 * @version 1.0.1.0
 */
public class RaportyProsteTest {

    @SuppressWarnings("deprecation")
    public static String GenerujBaze() {
        ResetBazy.usunBaze("BazaTowarow.yap", false);
        String wynik = "Towar został pomyślnie dodany do bazy danych.";
        IListaTowarow listaTowarow = new ListaTowarow(new BazaDanych());
        wynik = listaTowarow.dodajTowar(new TowarModel(1, "nazwa1", "producent1", 11, 1.11, 0.11, new Date(2013, 0, 31), "pkwiu1", 1, "Produkt Testowy1", false, false));
        wynik = listaTowarow.dodajTowar(new TowarModel(2, "nazwa2", "producent2", 11, 1.11, 0.11, new Date(2013, 0, 31), "pkwiu1", 1, "Produkt Testowy1", false, false));
        wynik = listaTowarow.dodajTowar(new TowarModel(3, "nazwa3", "producent3", 0, 1.11, 0.11, new Date(2013, 0, 12), "pkwiu1", 1, "Produkt Testowy1", false, false));
        wynik = listaTowarow.dodajTowar(new TowarModel(4, "nazwa4", "producent4", 5, 1.11, 0.11, new Date(2013, 0, 14), "pkwiu1", 1, "Produkt Testowy1", false, false));
        wynik = listaTowarow.dodajTowar(new TowarModel(5, "nazwa5", "producent5", 8, 1.11, 0.11, new Date(2013, 0, 31), "pkwiu1", 1, "Produkt Testowy1", false, false));
        wynik = listaTowarow.dodajTowar(new TowarModel(6, "nazwa6", "producent6", 4, 1.11, 0.11, new Date(2013, 1, 1), "pkwiu1", 1, "Produkt Testowy1", false, false));
        wynik = listaTowarow.dodajTowar(new TowarModel(7, "nazwa7", "producent7", 78, 1.11, 0.11, new Date(2013, 1, 28), "pkwiu1", 1, "Produkt Testowy1", false, false));
        wynik = listaTowarow.dodajTowar(new TowarModel(8, "nazwa8", "producent8", 43, 1.11, 0.11, new Date(2013, 1, 15), "pkwiu1", 1, "Produkt Testowy1", false, false));
        wynik = listaTowarow.dodajTowar(new TowarModel(9, "nazwa9", "producent9", 0, 1.11, 0.11, new Date(2013, 1, 27), "pkwiu1", 1, "Produkt Testowy1", false, false));
        wynik = listaTowarow.dodajTowar(new TowarModel(10, "nazwa10", "producent10", 0, 1.11, 0.11, new Date(2013, 2, 1), "pkwiu1", 1, "Produkt Testowy1", false, false));
        wynik = listaTowarow.dodajTowar(new TowarModel(11, "nazwa11", "producent11", 0, 1.11, 0.11, new Date(2013, 2, 31), "pkwiu1", 1, "Produkt Testowy1", false, false));
        wynik = listaTowarow.dodajTowar(new TowarModel(12, "nazwa12", "producent12", 0, 1.11, 0.11, new Date(2013, 2, 12), "pkwiu1", 1, "Produkt Testowy1", false, false));
        wynik = listaTowarow.dodajTowar(new TowarModel(13, "nazwa13", "producent13", 5, 1.11, 0.11, new Date(2013, 2, 18), "pkwiu1", 1, "Produkt Testowy1", false, false));
        wynik = listaTowarow.dodajTowar(new TowarModel(14, "nazwa14", "producent14", 10, 1.11, 0.11, new Date(2013, 2, 21), "pkwiu1", 1, "Produkt Testowy1", false, false));
        wynik = listaTowarow.dodajTowar(new TowarModel(15, "nazwa15", "producent15", 0, 1.11, 0.11, new Date(2013, 0, 1), "pkwiu1", 1, "Produkt Testowy1", false, true));
        wynik = listaTowarow.dodajTowar(new TowarModel(16, "nazwa16", "producent16", 1, 1.11, 0.11, new Date(2013, 0, 31), "pkwiu1", 1, "Produkt Testowy1", true, false));
        wynik = listaTowarow.dodajTowar(new TowarModel(17, "nazwa17", "producent17", 1, 1.11, 0.11, new Date(2013, 0, 16), "pkwiu1", 1, "Produkt Testowy1", true, true));
        return wynik;
    }

    @Test
    public void testRaportProduktyWyczerpane() {
        String wynik = GenerujBaze();
        IRaportyProste raportyProste = new RaportyProste(new BazaDanych(), new Creator());
        List<TowarModel> raport = raportyProste.raportProduktyWyczerpane();

        Assert.assertEquals("Towar został pomyślnie dodany do bazy danych.", wynik);
        Assert.assertNotNull(raport);
        Assert.assertEquals(5, raport.size());
        for (TowarModel towarModel : raport) {
            Assert.assertEquals(0, towarModel.getTowarIlosc(), 0);
        }
    }

    @Test
    public void testRaportProduktyNaWyczerpaniu() {
        String wynik = GenerujBaze();
        IRaportyProste raportyProste = new RaportyProste(new BazaDanych(), new Creator());
        List<TowarModel> raport = raportyProste.raportProduktyNaWyczerpaniu();

        Assert.assertEquals("Towar został pomyślnie dodany do bazy danych.", wynik);
        Assert.assertNotNull(raport);
        Assert.assertEquals(4, raport.size());
        for (TowarModel towarModel : raport) {
            Assert.assertTrue(towarModel.getTowarIlosc() > 0 && towarModel.getTowarIlosc() < 10);
        }
    }

    @SuppressWarnings("deprecation")
    @Test
    public void testRaportProduktyPrzeterminowane() {
        String wynik = GenerujBaze();
        IRaportyProste raportyProste = new RaportyProste(new BazaDanych(), new Creator());
        //List<TowarModel> raport = raportyProste.raportProduktyPrzeterminowane(2, 2013);

        //Assert.assertEquals("Towar został pomyślnie dodany do bazy danych.", wynik);
        //Assert.assertNotNull(raport);
        //Assert.assertEquals(7, raport.size());
        //for (TowarModel towarModel : raport) {
        //    Assert.assertTrue(towarModel.getTowarTerminWaznosci().compareTo(new Date(2013, 2, 1)) < 0 && towarModel.getTowarIlosc() > 0);
        //}
    }

    @SuppressWarnings("deprecation")
    @Test
    public void testRaportProduktyBliskiePrzeterminowania() {
        String wynik = GenerujBaze();
        IRaportyProste raportyProste = new RaportyProste(new BazaDanych(), new Creator());
        //List<TowarModel> raport = raportyProste.raportProduktyBliskiePrzeterminowania(0, 2013);
        
        //Assert.assertEquals("Towar został pomyślnie dodany do bazy danych.", wynik);
        //Assert.assertNotNull(raport);
        //Assert.assertEquals(4, raport.size());
        //for (TowarModel towarModel : raport) {
        ///    Assert.assertTrue(towarModel.getTowarTerminWaznosci().compareTo(new Date(2013, 0, 1)) >= 0 && towarModel.getTowarTerminWaznosci().compareTo(new Date(2013, 0, 31)) <= 0 && towarModel.getTowarIlosc() > 0);
        //}
    }

    @SuppressWarnings("deprecation")
    @Test
    public void testIgnorowanie() {
        String wynik = GenerujBaze();
        IRaportyProste raportyProste = new RaportyProste(new BazaDanych(), new Creator());
        TowarModel towar = new TowarModel(1, "nazwa1", "producent1", 11, 1.11, 0.11, new Date(2013, 0, 31), "pkwiu1", 1, "Produkt Testowy1", false, false);
        String wynik2 = raportyProste.ignorujTowar(towar);
        towar.setTowarIgnorowanie(true);
        boolean wynik3 = ((TowarModel) new BazaDanych().znajdzObiekt("BazaTowarow.yap", false, towar).get(0)).getTowarIgnorowanie();

        Assert.assertEquals("Towar został pomyślnie dodany do bazy danych.", wynik);
        Assert.assertEquals("Obiekt będzie ignorowany przy generowaniu przyszłych raportów.", wynik2);
        Assert.assertTrue(wynik3);
    }

    @Test
    public void testDoPlikuRaport() {
        //String wynik = GenerujBaze();
        //IRaportyProste raportyProste = new RaportyProste(new BazaDanych());
        // TODO Auto-generated method stub	
    }
}
