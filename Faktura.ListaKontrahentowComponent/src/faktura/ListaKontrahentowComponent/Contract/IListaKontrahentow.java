package faktura.ListaKontrahentowComponent.Contract;

import java.util.List;
import faktura.ListaKontrahentowComponent.Model.KontrahentModel;

/**
 * @author Katarzyna Romanowska
 * @version 1.0.1.0
 */
public interface IListaKontrahentow {

    /**
     * Znajduje kontrahentów odpowiadajacych danemu szablonowi
     * @param szablon szablon kontrahenta
     * @return List<KontrahentModel> - w przypadku znalezienia lub nie (dlugosc listy = 0) rekordów odpowiadających danemu szablonowi
     *         <br>
     *         null -  w przypadku błędu krytycznego
     */
    List<KontrahentModel> znajdzKontrahenta(KontrahentModel szablon);

    /**
     * Dodaje nowego kontrahenta do bazy danych
     * @param nowyKontrahent kontrahent do dodania
     * @return "Kontrahent został pomyślnie dodany do bazy danych." - w przypadku pomyślnego dodania do bazy
     *          <br>
     *         "Błąd bazy! Skonsultuj się z dostawcą oprogramowania!" - w przypadku błędu krytycznego
     */
    String dodajKontrahenta(KontrahentModel nowyKontrahent);

    /**
     * Aktualizuje danego kontrahenta w bazie danych
     * @param szablon kontrahent przed edycją (stary)
     * @param kontrahentWyedytowany kontrahent po edycji (nowy)
     * @return "Kontrahent został pomyślnie zaktualizowany." - w przypadku pomyślnej aktualizacji rekordu w bazie
     *          <br>
     *         "Błąd bazy! Skonsultuj się z dostawcą oprogramowania!" - w przypadku błędu krytycznego
     */
    String edytujKontrahenta(KontrahentModel szablon, KontrahentModel kontrahentWyedytowany);

    /**
     * Usuwa danego kontrahenta z bazy danych
     * @param szablon kontrahent do usunięcia
     * @return "Kontrahent został pomyślnie usunięty z bazy danych." - w przypadku pomyślnego usunięcia rekordu z bazy
     *          <br>
     *         "Błąd! Nie można usunąc kontrahenta." - w przypadku, gdy w bazie faktur znajdują się faktury wystawione na lub przez kontrahenta
     *         <br>
     *         "Błąd bazy! Skonsultuj się z dostawcą oprogramowania!" - w przypadku błędu krytycznego
     */
    String usunKontrahenta(KontrahentModel szablon);
}
