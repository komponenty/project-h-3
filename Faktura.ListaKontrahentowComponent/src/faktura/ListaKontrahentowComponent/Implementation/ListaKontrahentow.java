package faktura.ListaKontrahentowComponent.Implementation;

import faktura.FakturaSprzedazyComponent.Model.FakturaSprzedazyModel;
import faktura.FakturaZakupuComponent.Model.FakturaZakupuModel;
import java.util.List;
import faktura.ListaKontrahentowComponent.Contract.IListaKontrahentow;
import faktura.ListaKontrahentowComponent.Model.KontrahentModel;
import glowne.BazaDanychComponent.Contract.IBaza;

/**
 * @author Katarzyna Romanowska
 * @version 1.0.1.1
 */
public class ListaKontrahentow implements IListaKontrahentow {

    private IBaza bazaDanych;

    /**
     * Konstruktor zapewniajacy podstawowe funkcjonalnosci komponentu
     * @param bazaDanych repozytorium używane do przeprowadzania operacji nad bazą
     */
    public ListaKontrahentow(IBaza bazaDanych) {
        this.bazaDanych = bazaDanych;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public List<KontrahentModel> znajdzKontrahenta(KontrahentModel szablon) {
        return bazaDanych.znajdzObiekt("BazaKontrahentow.yap", true, szablon);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String dodajKontrahenta(KontrahentModel nowyKontrahent) {
        List<KontrahentModel> listaKontrahentow = bazaDanych.znajdzObiekt("BazaKontrahentow.yap", true, new KontrahentModel());
        if (listaKontrahentow == null) {
            return "Błąd bazy! Skonsultuj się z dostawcą oprogramowania!";
        }

        int kontrahentID = 1;
        for (KontrahentModel kontrahentModel : listaKontrahentow) {
            if (kontrahentModel.getKontrahentID() >= kontrahentID) {
                kontrahentID = kontrahentModel.getKontrahentID() + 1;
            }
        }
        nowyKontrahent.setKontrahentID(kontrahentID);

        boolean wynikOperacji = bazaDanych.dodajObiekt("BazaKontrahentow.yap", true, nowyKontrahent);
        if (wynikOperacji) {
            return "Kontrahent został pomyślnie dodany do bazy danych.";
        } else {
            return "Błąd bazy! Skonsultuj się z dostawcą oprogramowania!";
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String edytujKontrahenta(KontrahentModel szablon, KontrahentModel kontrahentWyedytowany) {
        List<KontrahentModel> listaKontrahentow = bazaDanych.znajdzObiekt("BazaKontrahentow.yap", true, szablon);

        if (listaKontrahentow == null) {
            return "Błąd bazy! Skonsultuj się z dostawcą oprogramowania!";
        }
        kontrahentWyedytowany.setKontrahentID(listaKontrahentow.get(0).getKontrahentID());

        boolean wynikOperacji = bazaDanych.edytujObiekt("BazaKontrahentow.yap", true, szablon, kontrahentWyedytowany);
        if (wynikOperacji) {
            return "Kontrahent został pomyślnie zaktualizowany.";
        } else {
            return "Błąd bazy! Skonsultuj się z dostawcą oprogramowania!";
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String usunKontrahenta(KontrahentModel szablon) {
        List<FakturaSprzedazyModel> listaFakturSprzedazy = bazaDanych.znajdzObiekt("BazaFakturSprzedazy.yap", true, new FakturaSprzedazyModel(szablon.getKontrahentID()));
        List<FakturaZakupuModel> listaFakturZakupu = bazaDanych.znajdzObiekt("BazaFakturZakupu.yap", true, new FakturaZakupuModel(szablon.getKontrahentID()));
        
        if ((listaFakturSprzedazy != null && listaFakturSprzedazy.size() > 0) || (listaFakturZakupu != null && listaFakturZakupu.size() > 0)) {
            return "Błąd! Nie można usunąc kontrahenta.";
        }
        
        boolean wynikOperacji = bazaDanych.usunObiekt("BazaKontrahentow.yap", true, szablon);
        if (wynikOperacji) {
            return "Kontrahent został pomyślnie usunięty z bazy danych.";
        } else {
            return "Błąd bazy! Skonsultuj się z dostawcą oprogramowania!";
        }
    }
}
