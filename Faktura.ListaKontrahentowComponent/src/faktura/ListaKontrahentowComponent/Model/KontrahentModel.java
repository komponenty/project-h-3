package faktura.ListaKontrahentowComponent.Model;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

/**
 * @author Katarzyna Romanowska
 * @version 1.0.1.2
 */
public class KontrahentModel {

    //<editor-fold defaultstate="collapsed" desc="Pola">
    private int kontrahentID;
    private String kontrahentNazwaFirmy;
    private String kontrahentNIP;
    private String kontrahentImie;
    private String kontrahentNazwisko;
    private AdresModel kontrahentAdres;
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Wlasciwosci">
    public void setKontrahentID(int kontrahentID) {
        int oldValue = this.kontrahentID;
        this.kontrahentID = kontrahentID;
        changeSupport.firePropertyChange("kontrahentID", oldValue, kontrahentID);
    }

    public int getKontrahentID() {
        return this.kontrahentID;
    }

    public void setKontrahentNazwaFirmy(String kontrahentNazwaFirmy) {
        String oldValue = this.kontrahentNazwaFirmy;
        this.kontrahentNazwaFirmy = kontrahentNazwaFirmy;
        changeSupport.firePropertyChange("kontrahentNazwaFirmy", oldValue, kontrahentNazwaFirmy);
    }

    public String getKontrahentNazwaFirmy() {
        return this.kontrahentNazwaFirmy;
    }

    public void setKontrahentNIP(String kontrahentNIP) {
        String oldValue = this.kontrahentNIP;
        this.kontrahentNIP = kontrahentNIP;
        changeSupport.firePropertyChange("kontrahentNIP", oldValue, kontrahentNIP);
    }

    public String getKontrahentNIP() {
        return this.kontrahentNIP;
    }

    public void setKontrahentImie(String kontrahentImie) {
        String oldValue = this.kontrahentImie;
        this.kontrahentImie = kontrahentImie;
        changeSupport.firePropertyChange("kontrahentImie", oldValue, kontrahentImie);
    }

    public String getKontrahentImie() {
        return this.kontrahentImie;
    }

    public void setKontrahentNazwisko(String kontrahentNazwisko) {
        String oldValue = this.kontrahentNazwisko;
        this.kontrahentNazwisko = kontrahentNazwisko;
        changeSupport.firePropertyChange("kontrahentNazwisko", oldValue, kontrahentNazwisko);
    }

    public String getKontrahentNazwisko() {
        return this.kontrahentNazwisko;
    }

    public void setKontrahentAdres(AdresModel kontrahentAdres) {
        AdresModel oldValue = this.kontrahentAdres;
        this.kontrahentAdres = kontrahentAdres;
        changeSupport.firePropertyChange("kontrahentAdres", oldValue, kontrahentAdres);
    }

    public AdresModel getKontrahentAdres() {
        return this.kontrahentAdres;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Konstruktory">
    public KontrahentModel() {
        this.kontrahentAdres = new AdresModel();
    }

    public KontrahentModel(int kontrahentID) {
        this.kontrahentID = kontrahentID;
        this.kontrahentAdres = new AdresModel();
    }

    public KontrahentModel(String kontrahentNazwaFirmy, String kontrahentNIP, String kontrahentImie, String kontrahentNazwisko) {
        this.kontrahentNazwaFirmy = kontrahentNazwaFirmy;
        this.kontrahentNIP = kontrahentNIP;
        this.kontrahentImie = kontrahentImie;
        this.kontrahentNazwisko = kontrahentNazwisko;
        this.kontrahentAdres = new AdresModel();
    }

    public KontrahentModel(String kontrahentNazwaFirmy, String kontrahentNIP, String kontrahentImie, String kontrahentNazwisko, AdresModel kontrahentAdres) {
        this(kontrahentNazwaFirmy, kontrahentNIP, kontrahentImie, kontrahentNazwisko);
        this.kontrahentAdres = kontrahentAdres;
    }

    public KontrahentModel(int kontrahentID, String kontrahentNazwaFirmy, String kontrahentNIP, String kontrahentImie, String kontrahentNazwisko, AdresModel kontrahentAdres) {
        this(kontrahentNazwaFirmy, kontrahentNIP, kontrahentImie, kontrahentNazwisko, kontrahentAdres);
        this.kontrahentID = kontrahentID;
    }
    
    public KontrahentModel(KontrahentModel kontrahentModel) {
        this.kontrahentNazwaFirmy = kontrahentModel.kontrahentNazwaFirmy;
        this.kontrahentNIP = kontrahentModel.getKontrahentNIP();
        this.kontrahentImie = kontrahentModel.getKontrahentImie();
        this.kontrahentNazwisko = kontrahentModel.getKontrahentNazwisko();
        this.kontrahentAdres = new AdresModel(kontrahentModel.getKontrahentAdres());
        this.kontrahentID = kontrahentModel.getKontrahentID();
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="PropertyChange">
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="toString method">
    @Override
    public String toString() {
        if (this.kontrahentNazwaFirmy == null) {
            return this.kontrahentImie + " " + this.kontrahentNazwisko + " - " + this.kontrahentAdres.getAdresMiejscowosc();
        }
        else {
            return this.kontrahentNazwaFirmy + " - " + this.kontrahentImie + " " + this.kontrahentNazwisko + " - " + this.kontrahentAdres.getAdresMiejscowosc();
        }   
    }
    //</editor-fold>
}
