package faktura.ListaKontrahentowComponent.Model;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

/**
 * @author Katarzyna Romanowska
 * @version 1.0.1.1
 */
public class AdresModel {

    //<editor-fold defaultstate="collapsed" desc="Pola">
    private String adresUlica;
    private String adresNumerUlicy;
    private String adresNumerMieszkania;
    private String adresKodPocztowy;
    private String adresMiejscowosc;
    private String adresPanstwo;
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Wlasciwosci">
    public void setAdresUlica(String adresUlica) {
        String oldValue = this.adresUlica;
        this.adresUlica = adresUlica;
        changeSupport.firePropertyChange("adresUlica", oldValue, adresUlica);
    }

    public String getAdresUlica() {
        return this.adresUlica;
    }

    public void setAdresNumerUlicy(String adresNumerUlicy) {
        String oldValue = this.adresNumerUlicy;
        this.adresNumerUlicy = adresNumerUlicy;
        changeSupport.firePropertyChange("adresNumerUlicy", oldValue, adresNumerUlicy);
    }

    public String getAdresNumerUlicy() {
        return this.adresNumerUlicy;
    }

    public void setAdresNumerMieszkania(String adresNumerMieszkania) {
        String oldValue = this.adresNumerMieszkania;
        this.adresNumerMieszkania = adresNumerMieszkania;
        changeSupport.firePropertyChange("adresNumerMieszkania", oldValue, adresNumerMieszkania);
    }

    public String getAdresNumerMieszkania() {
        return this.adresNumerMieszkania;
    }

    public void setAdresKodPocztowy(String adresKodPocztowy) {
        String oldValue = this.adresKodPocztowy;
        this.adresKodPocztowy = adresKodPocztowy;
        changeSupport.firePropertyChange("adresKodPocztowy", oldValue, adresKodPocztowy);
    }

    public String getAdresKodPocztowy() {
        return this.adresKodPocztowy;
    }

    public void setAdresMiejscowosc(String adresMiejscowosc) {
        String oldValue = this.adresMiejscowosc;
        this.adresMiejscowosc = adresMiejscowosc;
        changeSupport.firePropertyChange("adresMiejscowosc", oldValue, adresMiejscowosc);
    }

    public String getAdresMiejscowosc() {
        return this.adresMiejscowosc;
    }

    public void setAdresPanstwo(String adresPanstwo) {
        String oldValue = this.adresPanstwo;
        this.adresPanstwo = adresPanstwo;
        changeSupport.firePropertyChange("adresPanstwo", oldValue, adresPanstwo);
    }

    public String getAdresPanstwo() {
        return this.adresPanstwo;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Konstruktory">
    public AdresModel() {
    }

    public AdresModel(String adresUlica, String adresNumerUlicy, String adresNumerMieszkania, String adresKodPocztowy, String adresMiejscowosc, String adresPanstwo) {
        this.adresUlica = adresUlica;
        this.adresNumerUlicy = adresNumerUlicy;
        this.adresNumerMieszkania = adresNumerMieszkania;
        this.adresKodPocztowy = adresKodPocztowy;
        this.adresMiejscowosc = adresMiejscowosc;
        this.adresPanstwo = adresPanstwo;
    }

    public AdresModel(AdresModel adresModel) {
        this.adresUlica = adresModel.getAdresUlica();
        this.adresNumerUlicy = adresModel.getAdresNumerUlicy();
        this.adresNumerMieszkania = adresModel.getAdresNumerMieszkania();
        this.adresKodPocztowy = adresModel.getAdresKodPocztowy();
        this.adresMiejscowosc = adresModel.getAdresMiejscowosc();
        this.adresPanstwo = adresModel.getAdresPanstwo();
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="PropertyChange">
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="toString method">
    @Override
    public String toString() {
        if (this.adresNumerMieszkania == null) {
            return this.adresUlica + " " + this.adresNumerUlicy + ", " + this.adresKodPocztowy + " " + this.adresMiejscowosc + ", " + this.adresPanstwo;
        } else {
            return this.adresUlica + " " + this.adresNumerUlicy + "/" + this.adresNumerMieszkania + ", " + this.adresKodPocztowy + " " + this.adresMiejscowosc + ", " + this.adresPanstwo;
        }
    }
    //</editor-fold>
}
