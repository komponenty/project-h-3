package faktura.ListaKontrahentowComponent.Test;

import java.util.List;
import org.junit.Assert;
import org.junit.Test;
import faktura.ListaKontrahentowComponent.Contract.IListaKontrahentow;
import faktura.ListaKontrahentowComponent.Implementation.ListaKontrahentow;
import faktura.ListaKontrahentowComponent.Model.AdresModel;
import faktura.ListaKontrahentowComponent.Model.KontrahentModel;
import glowne.BazaDanychComponent.Implementation.BazaDanych;
import test.resetBazy.ResetBazy;

/**
 * @author Katarzyna Romanowska
 * @version 1.0.1.1
 */
public class ListaKontrahentowTest {

    private static String GenerujBazeKontrahetow() {
        ResetBazy.usunBaze("BazaKontrahentow.yap", true);
        String wynik = "Kontrahent został pomyślnie dodany do bazy danych.";
        IListaKontrahentow listaKontrahentow = new ListaKontrahentow(new BazaDanych());

        wynik = listaKontrahentow.dodajKontrahenta(new KontrahentModel(null, "1", "Jan", "Kowalski", new AdresModel("Kwiatowa", "11", "1", "11-111", "Warszawa", "Polska")));
        wynik = listaKontrahentow.dodajKontrahenta(new KontrahentModel("Firma2", "2", "Tadeusz", "Nowak", new AdresModel("Ladna", "22", "2", "22-222", "Poznan", "Polska")));
        wynik = listaKontrahentow.dodajKontrahenta(new KontrahentModel(null, "3", "Maria", "Kwiatkowska", new AdresModel("Bystra", "33", "3", "33-333", "Wroclaw", "Polska")));
        wynik = listaKontrahentow.dodajKontrahenta(new KontrahentModel("Firma4", "4", "Adam", "Lewandowski", new AdresModel("Kolorowa", "44", "4", "44-444", "Gdansk", "Polska")));
        wynik = listaKontrahentow.dodajKontrahenta(new KontrahentModel("Firma5", "5", "Janusz", "Knot", new AdresModel("Pstrokata", "55", "5", "55-555", "Zakopane", "Polska")));
        return wynik;
    }

    @Test
    public void testDodajKontrahentaIWyszukajKontrahenta() {
        String wiadomosc1 = GenerujBazeKontrahetow();
        IListaKontrahentow listaKontrahentow = new ListaKontrahentow(new BazaDanych());

        KontrahentModel kontrahent1 = (KontrahentModel) listaKontrahentow.znajdzKontrahenta(new KontrahentModel(4)).get(0);
        KontrahentModel kontrahent2 = (KontrahentModel) listaKontrahentow.znajdzKontrahenta(new KontrahentModel(null, null, null, null, new AdresModel("Bystra", "33", "3", "33-333", "Wroclaw", "Polska"))).get(0);
        List<KontrahentModel> lista = listaKontrahentow.znajdzKontrahenta(new KontrahentModel());

        Assert.assertEquals("Kontrahent został pomyślnie dodany do bazy danych.", wiadomosc1);
        Assert.assertEquals("Firma4", kontrahent1.getKontrahentNazwaFirmy());
        Assert.assertEquals("Kolorowa", kontrahent1.getKontrahentAdres().getAdresUlica());
        Assert.assertEquals(5, lista.size());
        Assert.assertEquals("Maria", kontrahent2.getKontrahentImie());
    }

    @Test
    public void testEdytujKontrahenta() {
        String wiadomosc1 = GenerujBazeKontrahetow();
        IListaKontrahentow listaKontrahentow = new ListaKontrahentow(new BazaDanych());

        String wiadomosc2 = listaKontrahentow.edytujKontrahenta(new KontrahentModel(1), new KontrahentModel("Firma1", "11", "Jan1", "Kowalski1", new AdresModel("Kwiatowa1", "111", "11", "111-111", "Warszawa1", "Polska1")));
        KontrahentModel kontrahent1 = (KontrahentModel) listaKontrahentow.znajdzKontrahenta(new KontrahentModel(1)).get(0);

        Assert.assertEquals("Kontrahent został pomyślnie dodany do bazy danych.", wiadomosc1);
        Assert.assertEquals("Kontrahent został pomyślnie zaktualizowany.", wiadomosc2);
        Assert.assertEquals("Firma1", kontrahent1.getKontrahentNazwaFirmy());
        Assert.assertEquals("Kwiatowa1", kontrahent1.getKontrahentAdres().getAdresUlica());
    }

    @Test
    public void testUsunKontrahenta() {
        String wiadomosc1 = GenerujBazeKontrahetow();
        IListaKontrahentow listaKontrahentow = new ListaKontrahentow(new BazaDanych());

        String wiadomosc2 = listaKontrahentow.usunKontrahenta(new KontrahentModel(1));
        List<KontrahentModel> lista1 = listaKontrahentow.znajdzKontrahenta(new KontrahentModel());

        Assert.assertEquals("Kontrahent został pomyślnie dodany do bazy danych.", wiadomosc1);
        Assert.assertEquals("Kontrahent został pomyślnie usunięty z bazy danych.", wiadomosc2);
        Assert.assertEquals(4, lista1.size());
    }
}
