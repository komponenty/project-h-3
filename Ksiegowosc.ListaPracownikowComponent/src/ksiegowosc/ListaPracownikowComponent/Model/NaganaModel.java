package ksiegowosc.ListaPracownikowComponent.Model;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

/**
 * @author Paweł Żurawik
 * @version 1.0.0.1
 */
public class NaganaModel {

    //<editor-fold defaultstate="collapsed" desc="Pola">
    private double naganaKaraPieniezna;
    private String naganaOpis;
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Wlasciwosci">
    public void setNaganaKaraPieniezna(double naganaKaraPieniezna) {
        double oldValue = this.naganaKaraPieniezna;
        this.naganaKaraPieniezna = naganaKaraPieniezna;
        changeSupport.firePropertyChange("naganaKaraPieniezna", oldValue, naganaKaraPieniezna);
    }

    public double getnaganaKaraPieniezna() {
        return this.naganaKaraPieniezna;
    }

    public void setNaganaOpis(String naganaOpis) {
        String oldValue = this.naganaOpis;
        this.naganaOpis = naganaOpis;
        changeSupport.firePropertyChange("naganaOpis", oldValue, naganaOpis);
    }

    public String getNaganaOpis() {
        return this.naganaOpis;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Konstruktory">
    public NaganaModel() {
    }

    public NaganaModel(double naganaKaraPieniezna, String naganaOpis) {
        this.naganaKaraPieniezna = naganaKaraPieniezna;
        this.naganaOpis = naganaOpis;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="PropertyChange">
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }
    //</editor-fold>
}
