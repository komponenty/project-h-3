package ksiegowosc.ListaPracownikowComponent.Model;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.List;

/**
 * @author Paweł Żurawik
 * @version 1.0.0.1
 */
public class PracownikModel {

    //<editor-fold defaultstate="collapsed" desc="Pola">
    private int pracownikID;
    private String pracownikImie;
    private String pracownikNazwisko;
    private double pracownikWynagrodzenie;
    private Stanowisko pracownikStanowisko;
    private List<NagrodaModel> pracownikNagrody;
    private List<NaganaModel> pracownikNagany;
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Wlasciwosci">
    public void setPracownikID(int pracownikID) {
        int oldValue = this.pracownikID;
        this.pracownikID = pracownikID;
        changeSupport.firePropertyChange("pracownikID", oldValue, pracownikID);
    }

    public int getPracownikID() {
        return this.pracownikID;
    }

    public void setPracownikImie(String pracownikImie) {
        String oldValue = this.pracownikImie;
        this.pracownikImie = pracownikImie;
        changeSupport.firePropertyChange("pracownikImie", oldValue, pracownikImie);
    }

    public String getPracownikImie() {
        return this.pracownikImie;
    }

    public void setPracownikNazwisko(String pracownikNazwisko) {
        String oldValue = this.pracownikNazwisko;
        this.pracownikNazwisko = pracownikNazwisko;
        changeSupport.firePropertyChange("pracownikNazwisko", oldValue, pracownikNazwisko);
    }

    public String getPracownikNazwisko() {
        return this.pracownikNazwisko;
    }

    public void setPracownikWynagrodzenie(double pracownikWynagrodzenie) {
        double oldValue = this.pracownikWynagrodzenie;
        this.pracownikWynagrodzenie = pracownikWynagrodzenie;
        changeSupport.firePropertyChange("pracownikWynagrodzenie", oldValue, pracownikWynagrodzenie);
    }

    public double getPracownikWynagrodzenie() {
        return this.pracownikWynagrodzenie;
    }

    public void setPracownikStanowisko(Stanowisko pracownikStanowisko) {
        Stanowisko oldValue = this.pracownikStanowisko;
        this.pracownikStanowisko = pracownikStanowisko;
        changeSupport.firePropertyChange("pracownikStanowisko", oldValue, pracownikStanowisko);
    }

    public Stanowisko getPracownikStanowisko() {
        return this.pracownikStanowisko;
    }

    public void setPracownikNagrody(List<NagrodaModel> pracownikNagrody) {
        List<NagrodaModel> oldValue = this.pracownikNagrody;
        this.pracownikNagrody = pracownikNagrody;
        changeSupport.firePropertyChange("pracownikNagrody", oldValue, pracownikNagrody);
    }

    public List<NagrodaModel> getPracownikNagrody() {
        return this.pracownikNagrody;
    }

    public void setPracownikNagany(List<NaganaModel> pracownikNagany) {
        List<NaganaModel> oldValue = this.pracownikNagany;
        this.pracownikNagany = pracownikNagany;
        changeSupport.firePropertyChange("pracownikNagany", oldValue, pracownikNagany);
    }

    public List<NaganaModel> getPracownikNagany() {
        return this.pracownikNagany;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Konstruktory">
    public PracownikModel() {
    }

    public PracownikModel(int pracownikID, String pracownikImie, String pracownikNazwisko, double pracownikWynagrodzenie, Stanowisko pracownikStanowisko, List<NagrodaModel> pracownikNagrody, List<NaganaModel> pracownikNagany) {
        this.pracownikID = pracownikID;
        this.pracownikImie = pracownikImie;
        this.pracownikNazwisko = pracownikNazwisko;
        this.pracownikWynagrodzenie = pracownikWynagrodzenie;
        this.pracownikStanowisko = pracownikStanowisko;
        this.pracownikNagrody = pracownikNagrody;
        this.pracownikNagany = pracownikNagany;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="PropertyChange">
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }
    //</editor-fold>
}
