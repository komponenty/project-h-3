package ksiegowosc.ListaPracownikowComponent.Model;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

/**
 * @author Paweł Żurawik
 * @version 1.0.0.1
 */
public class NagrodaModel {

    //<editor-fold defaultstate="collapsed" desc="Pola">
    private String nagrodaPremiaRzeczowa;
    private double nagrodaPremiaPieniezna;
    private String nagrodaOpis;
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Wlasciwosci">
    public void setNagrodaPremiaRzeczowa(String nagrodaPremiaRzeczowa) {
        String oldValue = this.nagrodaPremiaRzeczowa;
        this.nagrodaPremiaRzeczowa = nagrodaPremiaRzeczowa;
        changeSupport.firePropertyChange("nagrodaPremiaRzeczowa", oldValue, nagrodaPremiaRzeczowa);
    }

    public String getNagrodaPremiaRzeczowa() {
        return this.nagrodaPremiaRzeczowa;
    }

    public void setNagrodaPremiaPieniezna(double nagrodaPremiaPieniezna) {
        double oldValue = this.nagrodaPremiaPieniezna;
        this.nagrodaPremiaPieniezna = nagrodaPremiaPieniezna;
        changeSupport.firePropertyChange("nagrodaPremiaPieniezna", oldValue, nagrodaPremiaPieniezna);
    }

    public double getNagrodaPremiaPieniezna() {
        return this.nagrodaPremiaPieniezna;
    }

    public void setNagrodaOpis(String nagrodaOpis) {
        String oldValue = this.nagrodaOpis;
        this.nagrodaOpis = nagrodaOpis;
        changeSupport.firePropertyChange("nagrodaOpis", oldValue, nagrodaOpis);
    }

    public String getNagrodaOpis() {
        return this.nagrodaOpis;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Konstruktory">
    public NagrodaModel() {
    }

    public NagrodaModel(String nagrodaPremiaRzeczowa, double nagrodaPremiaPieniezna, String nagrodaOpis) {
        this.nagrodaPremiaRzeczowa = nagrodaPremiaRzeczowa;
        this.nagrodaPremiaPieniezna = nagrodaPremiaPieniezna;
        this.nagrodaOpis = nagrodaOpis;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="PropertyChange">
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }
    //</editor-fold>
}
