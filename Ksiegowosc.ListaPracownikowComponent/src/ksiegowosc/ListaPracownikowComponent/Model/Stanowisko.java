package ksiegowosc.ListaPracownikowComponent.Model;

/**
 * @author Paweł Żurawik
 * @version 1.0.0.0
 */
public enum Stanowisko {

    Sprzedawca,
    Dostawca,
    Księgowy,
    Magazynier,
    Menadżer
}
