package ksiegowosc.ListaPracownikowComponent.Contract;

import java.util.List;
import ksiegowosc.ListaPracownikowComponent.Model.NaganaModel;
import ksiegowosc.ListaPracownikowComponent.Model.NagrodaModel;
import ksiegowosc.ListaPracownikowComponent.Model.PracownikModel;

/**
 * @author Paweł Żurawik
 * @version 1.0.0.0
 */
public interface IListaPracownikow {

    List<PracownikModel> znajdzPracownika(PracownikModel szablon);

    String dodajPracownika(PracownikModel nowyPracownik);

    String edytujPracownika(PracownikModel szablon, PracownikModel pracownikWyedytowany);

    String usunPracownika(PracownikModel szablon);

    String dodajNagrode(PracownikModel szablon, NagrodaModel nagroda);

    String dodajNagane(PracownikModel szablon, NaganaModel nagana);
}
