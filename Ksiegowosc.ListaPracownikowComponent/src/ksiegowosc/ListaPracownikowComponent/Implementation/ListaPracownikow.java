package ksiegowosc.ListaPracownikowComponent.Implementation;

import java.util.List;
import ksiegowosc.ListaPracownikowComponent.Contract.IListaPracownikow;
import ksiegowosc.ListaPracownikowComponent.Model.NaganaModel;
import ksiegowosc.ListaPracownikowComponent.Model.NagrodaModel;
import ksiegowosc.ListaPracownikowComponent.Model.PracownikModel;

/**
 * @author Paweł Żurawik
 * @version 1.0.0.0
 */
public class ListaPracownikow implements IListaPracownikow {

    @Override
    public List<PracownikModel> znajdzPracownika(PracownikModel szablon) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String dodajPracownika(PracownikModel nowyPracownik) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String edytujPracownika(PracownikModel szablon, PracownikModel pracownikWyedytowany) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String usunPracownika(PracownikModel szablon) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String dodajNagrode(PracownikModel szablon, NagrodaModel nagroda) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String dodajNagane(PracownikModel szablon, NaganaModel nagana) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
