package magazyn.InwentaryzacjaComponent.Model;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Jakub Dydo
 * @version 1.0.1.1
 */
public class DokumentNadwyzekNiedoborowModel {

    //<editor-fold defaultstate="collapsed" desc="Pola">
    private RodzajDokumentu dokumentNadwyzekNiedoborowRodzaj;
    private List<TowarZyskStrataModel> dokumentNadwyzekNiedoborowTowary;
    private double dokumentNadwyzekNiedoborowSumaKwota;
    private int spisZNaturyID;
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Wlasciwosci">
    public void setDokumentNadwyzekNiedoborowRodzaj(RodzajDokumentu dokumentNadwyzekNiedoborowRodzaj) {
        RodzajDokumentu oldValue = this.dokumentNadwyzekNiedoborowRodzaj;
        this.dokumentNadwyzekNiedoborowRodzaj = dokumentNadwyzekNiedoborowRodzaj;
        changeSupport.firePropertyChange("dokumentNadwyzekNiedoborowRodzaj", oldValue, dokumentNadwyzekNiedoborowRodzaj);
    }

    public RodzajDokumentu getDokumentNadwyzekNiedoborowRodzaj() {
        return this.dokumentNadwyzekNiedoborowRodzaj;
    }

    public void setDokumentNadwyzekNiedoborowTowary(List<TowarZyskStrataModel> dokumentNadwyzekNiedoborowTowary) {
        List<TowarZyskStrataModel> oldValue = this.dokumentNadwyzekNiedoborowTowary;
        this.dokumentNadwyzekNiedoborowTowary = dokumentNadwyzekNiedoborowTowary;
        changeSupport.firePropertyChange("dokumentNadwyzekNiedoborowTowary", oldValue, dokumentNadwyzekNiedoborowTowary);
    }

    public List<TowarZyskStrataModel> getDokumentNadwyzekNiedoborowTowary() {
        return this.dokumentNadwyzekNiedoborowTowary;
    }

    public void setDokumentNadwyzekNiedoborowSumaKwota(double dokumentNadwyzekNiedoborowSumaKwota) {
        double oldValue = this.dokumentNadwyzekNiedoborowSumaKwota;
        this.dokumentNadwyzekNiedoborowSumaKwota = dokumentNadwyzekNiedoborowSumaKwota;
        changeSupport.firePropertyChange("dokumentNadwyzekNiedoborowSumaKwota", oldValue, dokumentNadwyzekNiedoborowSumaKwota);
    }

    public double getDokumentNadwyzekNiedoborowSumaKwota() {
        return this.dokumentNadwyzekNiedoborowSumaKwota;
    }

    public void setSpisZNaturyID(int spisZNaturyID) {
        int oldValue = this.spisZNaturyID;
        this.spisZNaturyID = spisZNaturyID;
        changeSupport.firePropertyChange("spisZNaturyID", oldValue, spisZNaturyID);
    }

    public int getSpisZNaturyID() {
        return this.spisZNaturyID;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Konstruktory">
    public DokumentNadwyzekNiedoborowModel() {
        this.dokumentNadwyzekNiedoborowTowary = new ArrayList<TowarZyskStrataModel>();
    }

    public DokumentNadwyzekNiedoborowModel(RodzajDokumentu dokumentNadwyzekNiedoborowRodzaj, int spisZNaturyID) {
        this.dokumentNadwyzekNiedoborowRodzaj = dokumentNadwyzekNiedoborowRodzaj;
        this.dokumentNadwyzekNiedoborowTowary = new ArrayList<TowarZyskStrataModel>();
        this.spisZNaturyID = spisZNaturyID;
    }

    public DokumentNadwyzekNiedoborowModel(RodzajDokumentu dokumentNadwyzekNiedoborowRodzaj, double dokumentNadwyzekNiedoborowSumaKwota, int spisZNaturyID) {
        this(dokumentNadwyzekNiedoborowRodzaj, spisZNaturyID);
        this.dokumentNadwyzekNiedoborowSumaKwota = dokumentNadwyzekNiedoborowSumaKwota;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="PropertyChange">
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }
    //</editor-fold>
}
