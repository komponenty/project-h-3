package magazyn.InwentaryzacjaComponent.Model;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import magazyn.ListaTowarowComponent.Model.TowarModel;

/**
 * @author Jakub Dydo
 * @version 1.0.1.3
 */
public class SpisZNaturyModel {

    //<editor-fold defaultstate="collapsed" desc="Pola">
    private int spisZNaturyID;
    private List<TowarModel> spisZNaturyTowary;
    private String spisZNaturyOsobaSprawdzajacaImie;
    private String spisZNaturyOsobaSprawdzajacaNazwisko;
    private Date spisZNaturyDataSpisu;
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Wlasciwosci">
    public void setSpisZNaturyID(int spisZNaturyID) {
        int oldValue = this.spisZNaturyID;
        this.spisZNaturyID = spisZNaturyID;
        changeSupport.firePropertyChange("spisZNaturyID", oldValue, spisZNaturyID);
    }

    public int getSpisZNaturyID() {
        return this.spisZNaturyID;
    }

    public void setSpisZNaturyTowary(List<TowarModel> spisZNaturyTowary) {
        List<TowarModel> oldValue = this.spisZNaturyTowary;
        this.spisZNaturyTowary = spisZNaturyTowary;
        changeSupport.firePropertyChange("spisZNaturyTowary", oldValue, spisZNaturyTowary);
    }

    public List<TowarModel> getSpisZNaturyTowary() {
        return this.spisZNaturyTowary;
    }

    public void setSpisZNaturyOsobaSprawdzajacaImie(String spisZNaturyOsobaSprawdzajacaImie) {
        String oldValue = this.spisZNaturyOsobaSprawdzajacaImie;
        this.spisZNaturyOsobaSprawdzajacaImie = spisZNaturyOsobaSprawdzajacaImie;
        changeSupport.firePropertyChange("spisZNaturyOsobaSprawdzajacaImie", oldValue, spisZNaturyOsobaSprawdzajacaImie);
    }

    public String getSpisZNaturyOsobaSprawdzajacaImie() {
        return this.spisZNaturyOsobaSprawdzajacaImie;
    }

    public void setSpisZNaturyOsobaSprawdzajacaNazwisko(String spisZNaturyOsobaSprawdzajacaNazwisko) {
        String oldValue = this.spisZNaturyOsobaSprawdzajacaNazwisko;
        this.spisZNaturyOsobaSprawdzajacaNazwisko = spisZNaturyOsobaSprawdzajacaNazwisko;
        changeSupport.firePropertyChange("spisZNaturyOsobaSprawdzajacaNazwisko", oldValue, spisZNaturyOsobaSprawdzajacaNazwisko);
    }

    public String getSpisZNaturyOsobaSprawdzajacaNazwisko() {
        return this.spisZNaturyOsobaSprawdzajacaNazwisko;
    }

    public void setSpisZNaturyDataSpisu(Date spisZNaturyDataSpisu) {
        Date oldValue = this.spisZNaturyDataSpisu;
        this.spisZNaturyDataSpisu = spisZNaturyDataSpisu;
        changeSupport.firePropertyChange("spisZNaturyDataSpisu", oldValue, spisZNaturyDataSpisu);
    }

    public Date getSpisZNaturyDataSpisu() {
        return this.spisZNaturyDataSpisu;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Konstruktory">
    public SpisZNaturyModel() {
        this.spisZNaturyTowary = new ArrayList<TowarModel>();
    }

    public SpisZNaturyModel(int spisZNaturyID) {
        this.spisZNaturyID = spisZNaturyID;
        this.spisZNaturyTowary = new ArrayList<TowarModel>();
    }

    public SpisZNaturyModel(String spisZNaturyOsobaSprawdzajacaImie, String spisZNaturyOsobaSprawdzajacaNazwisko, Date spisZNaturyDataSpisu) {
        this.spisZNaturyTowary = new ArrayList<TowarModel>();
        this.spisZNaturyOsobaSprawdzajacaImie = spisZNaturyOsobaSprawdzajacaImie;
        this.spisZNaturyOsobaSprawdzajacaNazwisko = spisZNaturyOsobaSprawdzajacaNazwisko;
        this.spisZNaturyDataSpisu = spisZNaturyDataSpisu;
    }

    public SpisZNaturyModel(SpisZNaturyModel spisZNaturyModel) {
        this.spisZNaturyID = spisZNaturyModel.getSpisZNaturyID();
        this.spisZNaturyTowary = spisZNaturyModel.getSpisZNaturyTowary();
        this.spisZNaturyOsobaSprawdzajacaImie = spisZNaturyModel.getSpisZNaturyOsobaSprawdzajacaImie();
        this.spisZNaturyOsobaSprawdzajacaNazwisko = spisZNaturyModel.getSpisZNaturyOsobaSprawdzajacaNazwisko();
        this.spisZNaturyDataSpisu = spisZNaturyModel.getSpisZNaturyDataSpisu();
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="PropertyChange">
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="toString method">
    @Override
    public String toString() {
        return String.valueOf(this.spisZNaturyID);
    }
    //</editor-fold>
}
