package magazyn.InwentaryzacjaComponent.Model;

/**
 * @author Jakub Dydo
 * @version 1.0.1.0
 */
public enum RodzajDokumentu {

    Nadwyzki,
    Niedobor
}
