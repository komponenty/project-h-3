package magazyn.InwentaryzacjaComponent.Model;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import magazyn.ListaTowarowComponent.Model.TowarModel;

/**
 * @author Jakub Dydo
 * @version 1.0.1.2
 */
public class TowarZyskStrataModel {

    //<editor-fold defaultstate="collapsed" desc="Pola">
    private TowarModel towarZyskStrataTowar;
    private double towarZyskStrataIlosc;
    private double towarZyskStrataKwota;
    private String towarZyskStrataUwagi;
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Wlasciwosci">
    public void setTowarZyskStrataTowar(TowarModel towarZyskStrataTowar) {
        TowarModel oldValue = this.towarZyskStrataTowar;
        this.towarZyskStrataTowar = towarZyskStrataTowar;
        changeSupport.firePropertyChange("towarZyskStrataTowar", oldValue, towarZyskStrataTowar);
    }

    public TowarModel getTowarZyskStrataTowar() {
        return this.towarZyskStrataTowar;
    }

    public void setTowarZyskStrataIlosc(double towarZyskStrataIlosc) {
        double oldValue = this.towarZyskStrataIlosc;
        this.towarZyskStrataIlosc = towarZyskStrataIlosc;
        changeSupport.firePropertyChange("towarZyskStrataIlosc", oldValue, towarZyskStrataIlosc);
    }

    public double getTowarZyskStrataIlosc() {
        return this.towarZyskStrataIlosc;
    }

    public void setTowarZyskStrataKwota(double towarZyskStrataKwota) {
        double oldValue = this.towarZyskStrataKwota;
        this.towarZyskStrataKwota = towarZyskStrataKwota;
        changeSupport.firePropertyChange("towarZyskStrataKwota", oldValue, towarZyskStrataKwota);
    }

    public double getTowarZyskStrataKwota() {
        return this.towarZyskStrataKwota;
    }
    
     public void setTowarZyskStrataUwagi(String towarZyskStrataUwagi) {
        String oldValue = this.towarZyskStrataUwagi;
        this.towarZyskStrataUwagi = towarZyskStrataUwagi;
        changeSupport.firePropertyChange("towarZyskStrataUwagi", oldValue, towarZyskStrataUwagi);
    }

    public String getTowarZyskStrataUwagi() {
        return this.towarZyskStrataUwagi;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Konstruktory">
    public TowarZyskStrataModel() {
    }

    public TowarZyskStrataModel(TowarModel towarZyskStrataTowar, double towarZyskStrataIlosc, double towarZyskStrataKwota) {
        this.towarZyskStrataTowar = towarZyskStrataTowar;
        this.towarZyskStrataIlosc = towarZyskStrataIlosc;
        this.towarZyskStrataKwota = towarZyskStrataKwota;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="PropertyChange">
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }
    //</editor-fold>
}
