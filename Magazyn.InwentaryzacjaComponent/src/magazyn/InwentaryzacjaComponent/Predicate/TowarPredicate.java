package magazyn.InwentaryzacjaComponent.Predicate;

import com.db4o.query.Predicate;
import magazyn.ListaTowarowComponent.Model.TowarModel;

/**
 * @author Jakub Dydo
 * @version 1.0.0.0
 */
public class TowarPredicate extends Predicate<TowarModel> {

    /**
     * Metoda sprawdza czy dany towar odpowiada kryteriom
     *
     * @param towar sprawdzany towar
     * @return true - spis z natury jest zgodny z kryteriami
     * <br>
     * false - spis z natury jest niezgodny z kryteriami
     */
    @Override
        public boolean match(TowarModel towar) {
        return !towar.getTowarIgnorowanie() && !towar.getTowarUzytekWlasny() && towar.getTowarIlosc() != 0;
    }
}
