package magazyn.InwentaryzacjaComponent.Predicate;

import com.db4o.query.Predicate;
import java.util.Date;
import magazyn.InwentaryzacjaComponent.Model.SpisZNaturyModel;

/**
 * @author Jakub Dydo
 * @version 1.0.1.0
 */
public class SpisZNaturyPoDaciePredicate extends Predicate<SpisZNaturyModel> {

    Date dataOd;
    Date dataDo;

    /**
     * @param dataOd możliwie najmniejsza data spisu z natury
     * @param dataDo możliwie największa data spisu z natury
     */
    public SpisZNaturyPoDaciePredicate(Date dataOd, Date dataDo) {
        this.dataOd = dataOd;
        this.dataDo = dataDo;
    }

    /**
     * Metoda sprawdza czy dany spis z natury odpowiada kryteriom
     *
     * @param spis sprawdzany spis z natury
     * @return true - spis z natury jest zgodny z kryteriami
     * <br>
     * false - spis z natury jest niezgodny z kryteriami
     */
    @Override
        public boolean match(SpisZNaturyModel spis) {
        return spis.getSpisZNaturyDataSpisu().compareTo(dataOd) >= 0
                && spis.getSpisZNaturyDataSpisu().compareTo(dataDo) <= 0;
    }
}
