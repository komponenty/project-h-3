package magazyn.InwentaryzacjaComponent.Contract;

import java.util.Date;
import java.util.List;
import magazyn.InwentaryzacjaComponent.Model.DokumentNadwyzekNiedoborowModel;
import magazyn.InwentaryzacjaComponent.Model.SpisZNaturyModel;
import magazyn.ListaTowarowComponent.Model.TowarModel;

/**
 * @author Jakub Dydo
 * @version 1.0.1.0
 */
public interface IInwentaryzacja {

    /**
     * Znajduje spis z natury odpowiadający danemu szablonowi
     *
     * @param szablon szablon spisu z natury
     * @return List<SpisZNaturyModel> - w przypadku znalezienia lub nie (dlugosc
     * listy = 0) rekordów odpowiadających danemu szablonowi
     * <br>
     * null - w przypadku błędu krytycznego
     */
    List<SpisZNaturyModel> znajdzSpisZNatury(SpisZNaturyModel szablon);

    /**
     * Znajduje spis z natury z danego okresu
     *
     * @param dataOd możliwie najmniejsza data spisu z natury
     * @param dataDo możliwie największa data spisu z natury
     * @return List<SpisZNaturyModel> - w przypadku znalezienia lub nie (dlugosc
     * listy = 0) rekordów odpowiadających danemu kryteriu
     * <br>
     * null - w przypadku błędu krytycznego
     */
    List<SpisZNaturyModel> znajdzSpisZNatury(Date dataOd, Date dataDo);

    /**
     * Dodaje towar do spisu z natury
     *
     * @param spis spis z natury, do którego ma być dodany towar
     * @param towar towar do dodania
     * @return "Towar został dodany." - w przypadku pomyślnego dodania towaru
     * <br>
     * "Liczebność towaru została zaktualizowana." - w przypadku pomyślnej
     * aktualizacji ilości towaru
     */
    String dodajTowarDoSpisu(SpisZNaturyModel spis, TowarModel towar);

    /**
     * Usuwa towar ze spisu z natury
     *
     * @param spis spis z natury, z którego ma być usunięty towar
     * @param towar towar do usunięcia
     * @return "Towar został pomyślnie usunięty." - w przypadku pomyślnego
     * usunięcia towaru
     * <br>
     * "Błąd! Nie znaleziono towaru." - w przypadku, gdy nie znaleziono w spisie
     * danego towaru
     */
    String usunTowarZeSpisu(SpisZNaturyModel spis, TowarModel towar);

    /**
     * Dodaje spis z natury do bazy danych
     *
     * @param nowySpisZNatury spis z natury do dodania
     * @return "Spis został pomyślnie dodany do bazy danych." - w przypadku
     * pomyślnego dodania do bazy
     * <br>
     * "Błąd bazy! Skonsultuj się z dostawcą oprogramowania!" - w przypadku
     * błędu krytycznego
     */
    String dodajSpisZNatury(SpisZNaturyModel nowySpisZNatury);

    /**
     * Zrzuca dany spis z natury do pliku w formacie EPUB
     *
     * @param spis spis z natury do zrzutu do pliku
     * @param sciezkaDoPliku sciezka gdzie ma być zapisany spis z natury
     */
    void doPlikuSpisZNatury(SpisZNaturyModel spis, String sciezkaDoPliku);

    /**
     * Generuje dokument nadwyżek na podstawie danego spisu z natury
     *
     * @param dokument dokument nadwyżek do wypełnienia
     * @param spisZNatury spis z natury na podstawie, którego tworzony jest
     * dokument nadwyżek
     * @return "Dokument nadwyżek został wygenerowany pomyślnie." - w przypadku
     * pomyślnego wygenerowania dokumentu
     * <br>
     * "Nie stwierdzono nadwyżek." - w przypadku, gdy nie stwierdzono nadwyżek
     */
    String generujDokumentNadwyzek(DokumentNadwyzekNiedoborowModel dokument, SpisZNaturyModel spisZNatury);

    /**
     * Generuje dokument niedoborów na podstawie danego spisu z natury
     *
     * @param dokument dokument niedoborów do wypełnienia
     * @param spisZNatury spis z natury na podstawie, którego tworzony jest
     * dokument niedoborów
     * @return "Dokument niedoborów został wygenerowany pomyślnie." - w
     * przypadku pomyślnego wygenerowania dokumentu
     * <br>
     * "Nie stwierdzono niedoborów." - w przypadku, gdy nie stwierdzono
     * niedoborów
     */
    String generujDokumentNiedoborow(DokumentNadwyzekNiedoborowModel dokument, SpisZNaturyModel spisZNatury);

    /**
     * Zrzuca dany dokument do pliku w formacie EPUB
     *
     * @param dokument dokument do zrzutu do pliku
     * @param sciezkaDoPliku sciezka gdzie ma być zapisany dokument
     */
    void doPlikuDokument(DokumentNadwyzekNiedoborowModel dokument, String sciezkaDoPliku);
}
