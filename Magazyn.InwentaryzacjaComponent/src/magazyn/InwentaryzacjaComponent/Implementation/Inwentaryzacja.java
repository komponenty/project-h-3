package magazyn.InwentaryzacjaComponent.Implementation;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import glowne.BazaDanychComponent.Contract.IBaza;
import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import magazyn.InwentaryzacjaComponent.Contract.IInwentaryzacja;
import magazyn.InwentaryzacjaComponent.Model.DokumentNadwyzekNiedoborowModel;
import magazyn.InwentaryzacjaComponent.Model.RodzajDokumentu;
import magazyn.InwentaryzacjaComponent.Model.SpisZNaturyModel;
import magazyn.InwentaryzacjaComponent.Model.TowarZyskStrataModel;
import magazyn.InwentaryzacjaComponent.Predicate.SpisZNaturyPoDaciePredicate;
import magazyn.InwentaryzacjaComponent.Predicate.TowarPredicate;
import magazyn.ListaTowarowComponent.Model.TowarModel;
import pk.epub.Creator.Contracts.ICreator;

/**
 * @author Jakub Dydo
 * @version 1.0.1.0
 */
public class Inwentaryzacja implements IInwentaryzacja {

    private IBaza bazaDanych;
    private ICreator creator;

    /**
     * Konstruktor zapewniajacy podstawowe funkcjonalnosci komponentu (brak
     * druku i zrzutu do pliku)
     *
     * @param bazaDanych repozytorium używane do przeprowadzania operacji nad
     * bazą
     * @param creator komponent slużący do zrzucania obiektu do pliku
     */
    public Inwentaryzacja(IBaza bazaDanych, ICreator creator) {
        this.bazaDanych = bazaDanych;
        this.creator = creator;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<SpisZNaturyModel> znajdzSpisZNatury(SpisZNaturyModel szablon) {
        List<SpisZNaturyModel> listaSpisow = bazaDanych.znajdzObiekt("BazaSpisowZNatury.yap", false, szablon);
        return listaSpisow;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    //Konieczne jest dokladne deklarowanie daty - new Date() nie przechodzi mimo, ze obiekt instancjonuje sie z aktualna data
    public List<SpisZNaturyModel> znajdzSpisZNatury(Date dataOd, Date dataDo) {
        List<SpisZNaturyModel> listaSpisow = bazaDanych.znajdzObiektPredicate("BazaSpisowZNatury.yap", false, new SpisZNaturyPoDaciePredicate(dataOd, dataDo));
        return listaSpisow;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String dodajTowarDoSpisu(SpisZNaturyModel spis, TowarModel towar) {
        for (TowarModel towarZeSpisu : spis.getSpisZNaturyTowary()) {
            if (towarZeSpisu.getTowarID() == towar.getTowarID()) {
                double ilosc = towarZeSpisu.getTowarIlosc() + towar.getTowarIlosc();
                towarZeSpisu.setTowarIlosc(ilosc);
                return "Liczebność towaru została zaktualizowana.";
            }
        }
        List<TowarModel> listaTowarowZeSpisu = spis.getSpisZNaturyTowary();
        listaTowarowZeSpisu.add(towar);
        spis.setSpisZNaturyTowary(listaTowarowZeSpisu);
        return "Towar został dodany.";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String usunTowarZeSpisu(SpisZNaturyModel spis, TowarModel towar) {
        System.out.println(towar);
        if (spis.getSpisZNaturyTowary().remove(towar)) {
            return "Towar został pomyślnie usunięty.";
        } else {
            for (TowarModel towarZeSpisu : spis.getSpisZNaturyTowary()) {
                if (towarZeSpisu.getTowarID() == towar.getTowarID()) {
                    spis.getSpisZNaturyTowary().remove(towarZeSpisu);
                    return "Towar został pomyślnie usunięty.";
                }
            }
        }
        return "Błąd! Nie znaleziono towaru.";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String dodajSpisZNatury(SpisZNaturyModel nowySpisZNatury) {
        List<SpisZNaturyModel> listaSpisow = bazaDanych.znajdzObiekt("BazaSpisowZNatury.yap", false, new SpisZNaturyModel());
        if (listaSpisow == null) {
            return "Błąd bazy! Skonsultuj się z dostawcą oprogramowania!";
        }

        int spisID = 1;
        for (SpisZNaturyModel spisModel : listaSpisow) {
            if (spisModel.getSpisZNaturyID() >= spisID) {
                spisID = spisModel.getSpisZNaturyID() + 1;
            }
        }
        nowySpisZNatury.setSpisZNaturyID(spisID);

        boolean wynikOperacji = bazaDanych.dodajObiekt("BazaSpisowZNatury.yap", false, nowySpisZNatury);
        if (wynikOperacji) {
            return "Spis został pomyślnie dodany do bazy danych.";
        } else {
            return "Błąd bazy! Skonsultuj się z dostawcą oprogramowania!";
        }
    }

    public String edytujSpisZNatury(SpisZNaturyModel szablon, SpisZNaturyModel spisZNaturyWyedytowany) {
        List<SpisZNaturyModel> listaSpisow1 = bazaDanych.znajdzObiekt("BazaSpisowZNatury.yap", false, new SpisZNaturyModel(spisZNaturyWyedytowany.getSpisZNaturyID()));
        List<SpisZNaturyModel> listaSpisow2 = bazaDanych.znajdzObiekt("BazaSpisowZNatury.yap", false, szablon);

        if (listaSpisow1 == null || listaSpisow2 == null) {
            return "Błąd bazy! Skonsultuj się z dostawcą oprogramowania!";
        }
        spisZNaturyWyedytowany.setSpisZNaturyID(listaSpisow2.get(0).getSpisZNaturyID());

        boolean wynikOperacji = bazaDanych.edytujObiekt("BazaSpisowZNatury.yap", false, szablon, spisZNaturyWyedytowany);
        if (wynikOperacji) {
            return "Spis został pomyślnie zaktualizowany.";
        } else {
            return "Błąd bazy! Skonsultuj się z dostawcą oprogramowania!";
        }
    }

    public String usunSpisZNatury(SpisZNaturyModel szablon) {
        boolean wynikOperacji = bazaDanych.usunObiekt("BazaSpisowZNatury.yap", false, szablon);
        if (wynikOperacji) {
            return "Spis został pomyślnie usunięty z bazy danych.";
        } else {
            return "Błąd bazy! Skonsultuj się z dostawcą oprogramowania!";
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void doPlikuSpisZNatury(SpisZNaturyModel spis, String sciezkaDoPliku) {
        SpisZNaturyModel spisZID = bazaDanych.znajdzObiekt("BazaSpisowZNatury.yap", false, spis).get(0);
        
        creator.setDir(sciezkaDoPliku);
        creator.setTitle("Spis z natury");
        creator.setLanguage("pl");
        creator.addIdentifier("Spis z natury", "" + spisZID.getSpisZNaturyID());

        String style = "@charset \"utf-8\";\n"
                + "body { width: 100%; }\n"
                + ".pp { margin: 20px; }\n"
                + "table { margin-bottom:20px; }\n"
                + ".ppp { font-size:22px; margin: 20px; }\n"
                + ".pppp { font-size:18px; }";

        creator.setStyle(style);

        String html = "<html>\n"
                + "	<head>\n"
                + "		<link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\" />\n"
                + "	</head>\n"
                + "	<body>\n"
                + "    <table width=\"800px\" border=\"0\">\n"
                + "		<tr>\n"
                + "			<td width=\"400px\" height=\"100px\" colspan=\"5\" valign=\"middle\" align=\"center\"><p class=\"ppp\"><b>SPIS Z NATURY</b></td>\n"
                + "		</tr>\n"
                + "        <tr>\n"
                + "			<td width=\"400px\" colspan=\"5\"><p class=\"pp\">\n"
                + "        		Numer identyfikacyjny spisu z natury: <b>" + spisZID.getSpisZNaturyID() + "</b></br>\n"
                + "        		Przeprowadził: <b>" + spisZID.getSpisZNaturyOsobaSprawdzajacaImie() + " " + spisZID.getSpisZNaturyOsobaSprawdzajacaNazwisko() + "</b></br>\n"
                + "        		Data: <b>" + new SimpleDateFormat("dd-MM-yyyy").format(spisZID.getSpisZNaturyDataSpisu()) + "</b></p></td>\n"
                + "		</tr>\n"
                + "	</table>\n"
                + "    \n"
                + "    <table width=\"800px\" border=\"1\">\n"
                + "        <tr>\n"
                + "            <td width=\"300px\" valign=\"middle\" align=\"center\"><b>Nazwa towaru</b></td>\n"
                + "            <td width=\"150px\" valign=\"middle\" align=\"center\"><b>Producent</b></td>\n"
                + "            <td width=\"150px\" valign=\"middle\" align=\"center\"><b>Identifikator</b></td>\n"
                + "            <td width=\"100px\"valign=\"middle\" align=\"center\"><b>Ilosć</b></td>\n"
                + "            <td width=\"100px\" valign=\"middle\" align=\"center\"><b>Cena brutto</b></td>\n"
                + "		</tr>\n";

        for (TowarModel towar : spisZID.getSpisZNaturyTowary()) {

            html += "        <tr>\n"
                    + "            <td width=\"300px\" valign=\"middle\" align=\"center\">" + towar.getTowarnNazwa() + "</td>\n"
                    + "            <td width=\"150px\" valign=\"middle\" align=\"center\">" + towar.getTowarProducent() + "</td>\n"
                    + "            <td  width=\"150px\" valign=\"middle\" align=\"center\">" + towar.getTowarID() + "</td>\n"
                    + "            <td width=\"100px\"valign=\"middle\" align=\"center\">" + (Math.round(towar.getTowarIlosc()* 100.0) / 100.0) + "</td>\n"
                    + "            <td width=\"100px\" valign=\"middle\" align=\"center\">" + (Math.round(towar.getTowarCenaNetto()* 100.0) / 100.0) + "</td>\n"
                    + "		</tr>\n";
        }

        html += "	</table>\n"
                + "\n"
                + "    <table width=\"800px\" border=\"0\">\n"
                + "		<tr>\n"
                + "			<td width=\"400px\" height=\"150px\" valign=\"bottom\" align=\"center\"></td>\n"
                + "            <td width=\"400px\" height=\"150px\" valign=\"bottom\" align=\"center\">Podpis osoby przeprowadzajacej spis z natury</td>\n"
                + "		</tr>\n"
                + "	</table>\n"
                + "    \n"
                + "	</body>\n"
                + "</html>";

        creator.addChapter(html);

        creator.createEpub();
        creator.clear();

        try {
            Desktop.getDesktop().open(new File(sciezkaDoPliku));
        } catch (IOException e) {
            System.out.print(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String generujDokumentNadwyzek(DokumentNadwyzekNiedoborowModel dokument, SpisZNaturyModel spisZNatury) {
        dokument.setDokumentNadwyzekNiedoborowRodzaj(RodzajDokumentu.Nadwyzki);
        dokument.setSpisZNaturyID(spisZNatury.getSpisZNaturyID());

        List<TowarZyskStrataModel> listaTowarowZNadwyzek = new ArrayList<TowarZyskStrataModel>();
        double nadwyzkaKwotaCalkowita = 0;

        //tu popraw
        for (TowarModel towarZeSpisu : spisZNatury.getSpisZNaturyTowary()) {
            List<TowarModel> listaTowarow = bazaDanych.znajdzObiekt("BazaTowarow.yap", false, new TowarModel(towarZeSpisu.getTowarID()));

            if (listaTowarow == null) {
                return "Błąd bazy! Skonsultuj się z dostawcą oprogramowania!";
            } else if (listaTowarow.isEmpty()) { //gdy jest na spisie a nie ma w bazie
                double kwota = towarZeSpisu.getTowarCenaNetto() * towarZeSpisu.getTowarIlosc();
                nadwyzkaKwotaCalkowita += kwota;
                TowarZyskStrataModel nadwyzkaTowar = new TowarZyskStrataModel(towarZeSpisu, towarZeSpisu.getTowarIlosc(), towarZeSpisu.getTowarCenaNetto());
                nadwyzkaTowar.setTowarZyskStrataUwagi("Brak towaru w bazie");
                listaTowarowZNadwyzek.add(nadwyzkaTowar);
            } else { // gdy jest na spisie i jest w bazie
                TowarModel oryginalnyTowar = listaTowarow.get(0);
                if (oryginalnyTowar.getTowarIlosc() < towarZeSpisu.getTowarIlosc()) {
                    double ilosc = towarZeSpisu.getTowarIlosc() - oryginalnyTowar.getTowarIlosc();
                    //double kwota = ilosc * (oryginalnyTowar.getTowarCenaNetto() + oryginalnyTowar.getTowarCenaNetto() * oryginalnyTowar.getTowarVAT());
                    double kwota = towarZeSpisu.getTowarCenaNetto() * ilosc;
                    nadwyzkaKwotaCalkowita += kwota;
                    TowarZyskStrataModel nadwyzkaTowar = new TowarZyskStrataModel(oryginalnyTowar, ilosc, kwota);
                    listaTowarowZNadwyzek.add(nadwyzkaTowar);
                }
            }
        }

        dokument.setDokumentNadwyzekNiedoborowTowary(listaTowarowZNadwyzek);

        double kwota = Math.round(nadwyzkaKwotaCalkowita * 100.0) / 100.0;
        dokument.setDokumentNadwyzekNiedoborowSumaKwota(kwota);

        if (listaTowarowZNadwyzek.size() > 0) {
            return "Dokument nadwyżek został wygenerowany pomyślnie.";
        } else {
            return "Nie stwierdzono nadwyżek.";
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String generujDokumentNiedoborow(DokumentNadwyzekNiedoborowModel dokument, SpisZNaturyModel spisZNatury) {
        dokument.setDokumentNadwyzekNiedoborowRodzaj(RodzajDokumentu.Niedobor);
        dokument.setSpisZNaturyID(spisZNatury.getSpisZNaturyID());

        List<TowarZyskStrataModel> listaTowarowZNiedoborow = new ArrayList<TowarZyskStrataModel>();
        double niedoborKwotaCalkowita = 0;

        List<TowarModel> listaTowarowZBazy = bazaDanych.znajdzObiektPredicate("BazaTowarow.yap", false, new TowarPredicate());

        for (TowarModel towarZBazy : listaTowarowZBazy) {
            boolean straznik = true;
            for (TowarModel towarZeSpisu : spisZNatury.getSpisZNaturyTowary()) {

                if (towarZeSpisu.getTowarID() == towarZBazy.getTowarID() && towarZeSpisu.getTowarIlosc() < towarZBazy.getTowarIlosc()) {
                    double ilosc = towarZBazy.getTowarIlosc() - towarZeSpisu.getTowarIlosc();
                    //double kwota = ilosc * (towarZBazy.getTowarCenaNetto() + towarZBazy.getTowarCenaNetto() * towarZBazy.getTowarVAT());
                    double kwota = towarZeSpisu.getTowarCenaNetto() * ilosc;
                    niedoborKwotaCalkowita += kwota;
                    TowarZyskStrataModel niedoborTowar = new TowarZyskStrataModel(towarZBazy, ilosc, kwota);
                    listaTowarowZNiedoborow.add(niedoborTowar);
                    straznik = false;
                } else if (towarZeSpisu.getTowarID() == towarZBazy.getTowarID()) {
                    straznik = false;
                }
            }
            if (straznik) {
                //double kwota = towarZBazy.getTowarIlosc() * (towarZBazy.getTowarCenaNetto() + towarZBazy.getTowarCenaNetto() * towarZBazy.getTowarVAT());
                double cenaZRabatem = towarZBazy.getTowarCenaNetto() - towarZBazy.getTowarCenaNetto() * towarZBazy.getTowarRabat();
                double kwota = towarZBazy.getTowarIlosc() * (cenaZRabatem + cenaZRabatem * towarZBazy.getTowarVAT());

                niedoborKwotaCalkowita += kwota;
                TowarZyskStrataModel niedoborTowar = new TowarZyskStrataModel(towarZBazy, towarZBazy.getTowarIlosc(), kwota);
                niedoborTowar.setTowarZyskStrataUwagi("Brak towaru w spisie");
                listaTowarowZNiedoborow.add(niedoborTowar);
            }
        }

        dokument.setDokumentNadwyzekNiedoborowTowary(listaTowarowZNiedoborow);

        double kwota = Math.round(niedoborKwotaCalkowita * 100.0) / 100.0;
        dokument.setDokumentNadwyzekNiedoborowSumaKwota(kwota);

        if (listaTowarowZNiedoborow.size() > 0) {
            return "Dokument niedoborów został wygenerowany pomyślnie.";
        } else {
            return "Nie stwierdzono niedoborów.";
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void doPlikuDokument(DokumentNadwyzekNiedoborowModel dokument, String sciezkaDoPliku) {
        String tytul = "";
        String tytulTabeli = "";
        String wartosc = "";
        String tytulEPUBu = "";

        if (dokument.getDokumentNadwyzekNiedoborowRodzaj() == RodzajDokumentu.Nadwyzki) {
            tytul = "DOKUMENT NADWYŻEK";
            tytulTabeli = "Towary z nadwyżki";
            wartosc = "nadwyżki";
            tytulEPUBu = "Dokument nadwyżek";
        } else {
            tytul = "DOKUMENT NIEDOBORÓW";
            tytulTabeli = "Towary z niedoboru";
            wartosc = "niedoboru";
            tytulEPUBu = "Dokument niedoborów";
        }

        creator.setDir(sciezkaDoPliku);
        creator.setTitle(tytulEPUBu);
        creator.setLanguage("pl");
        creator.addIdentifier(tytulEPUBu, tytulEPUBu + dokument.getSpisZNaturyID());

        String style = "@charset \"utf-8\";\n"
                + "body { width: 100%; }\n"
                + ".pp { margin: 20px; }\n"
                + "table { margin-bottom:20px; }\n"
                + ".ppp { font-size:22px; margin: 20px; }\n"
                + ".pppp { font-size:18px; }";

        creator.setStyle(style);

        String html = "<html>\n"
                + "	<head>\n"
                + "		<link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\" />\n"
                + "	</head>\n"
                + "	<body>\n"
                + "    <table width=\"800px\" border=\"0\">\n"
                + "		<tr>\n"
                + "			<td width=\"400px\" height=\"100px\" colspan=\"5\" valign=\"middle\" align=\"center\"><p class=\"ppp\"><b>" + tytul + "</b></td>\n"
                + "		</tr>\n"
                + "        <tr>\n"
                + "			<td width=\"400px\" colspan=\"5\"><p class=\"pp\">\n"
                + "        		Dokument wygenerowany w oparciu o spis z natury o numerze identyfikacyjnym: <b>" + dokument.getSpisZNaturyID() + "</b></br>\n"
                + "                Wartość " + wartosc + ": <b>" + (Math.round(dokument.getDokumentNadwyzekNiedoborowSumaKwota() * 100.0) / 100.0) + " PLN</b></br>\n"
                + "        		Data: <b>" + new SimpleDateFormat("dd-MM-yyyy").format(new Date()) + "</b></p></td>\n"
                + "		</tr>\n"
                + "	</table>\n"
                + "    \n"
                + "    <table width=\"800px\" border=\"1\">\n"
                + "		<tr>\n"
                + "			<td height=\"50px\" colspan=\"6\" valign=\"middle\" align=\"center\"><b>" + tytulTabeli + "</b></td>\n"
                + "		</tr>\n"
                + "        <tr>\n"
                + "            <td width=\"250px\" valign=\"middle\" align=\"center\"><b>Nazwa towaru</b></td>\n"
                + "            <td width=\"110px\" valign=\"middle\" align=\"center\"><b>Producent</b></td>\n"
                + "            <td width=\"150px\" valign=\"middle\" align=\"center\"><b>Identifikator</b></td>\n"
                + "            <td width=\"60px\"valign=\"middle\" align=\"center\"><b>Ilość</b></td>\n"
                + "            <td width=\"60px\" valign=\"middle\" align=\"center\"><b>Kwota</b></td>\n"
                + "            <td width=\"170px\" valign=\"middle\" align=\"center\"><b>Uwagi</b></td>\n"
                + "		</tr>\n";

        for (TowarZyskStrataModel towar : dokument.getDokumentNadwyzekNiedoborowTowary()) {

            String uwagi = "";
            if (towar.getTowarZyskStrataUwagi() != null) {
                uwagi = towar.getTowarZyskStrataUwagi();
            }

            html += "        <tr>\n"
                    + "			<td width=\"240px\" valign=\"middle\" align=\"center\">" + towar.getTowarZyskStrataTowar().getTowarnNazwa() + "</td>\n"
                    + "            <td width=\"110px\" valign=\"middle\" align=\"center\">" + towar.getTowarZyskStrataTowar().getTowarProducent() + "</td>\n"
                    + "            <td  width=\"150px\" valign=\"middle\" align=\"center\">" + towar.getTowarZyskStrataTowar().getTowarID() + "</td>\n"
                    + "            <td width=\"60px\"valign=\"middle\" align=\"center\">" + (Math.round(towar.getTowarZyskStrataIlosc() * 100.0) / 100.0) + "</td>\n"
                    + "            <td width=\"60px\" valign=\"middle\" align=\"center\">" + (Math.round(towar.getTowarZyskStrataKwota() * 100.0) / 100.0) + "</td>\n"
                    + "            <td width=\"180px\" valign=\"middle\" align=\"center\">" + uwagi + "</td>\n"
                    + "		</tr>\n";
        }

        html += "	</table>\n"
                + "\n"
                + "    <table width=\"800px\" border=\"0\">\n"
                + "		<tr>\n"
                + "			<td width=\"400px\" height=\"150px\" valign=\"bottom\" align=\"center\"></td>\n"
                + "            <td width=\"400px\" height=\"150px\" valign=\"bottom\" align=\"center\">Podpis</td>\n"
                + "		</tr>\n"
                + "	</table>\n"
                + "    \n"
                + "	</body>\n"
                + "</html>";

        creator.addChapter(html);

        creator.createEpub();
        creator.clear();

        try {
            Desktop.getDesktop().open(new File(sciezkaDoPliku));
        } catch (IOException e) {
            System.out.print(e);
        }
    }
}
