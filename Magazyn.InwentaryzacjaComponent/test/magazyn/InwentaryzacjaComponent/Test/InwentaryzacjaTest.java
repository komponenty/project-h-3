package magazyn.InwentaryzacjaComponent.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;
import glowne.BazaDanychComponent.Implementation.BazaDanych;
import magazyn.InwentaryzacjaComponent.Contract.IInwentaryzacja;
import magazyn.InwentaryzacjaComponent.Implementation.Inwentaryzacja;
import magazyn.InwentaryzacjaComponent.Model.DokumentNadwyzekNiedoborowModel;
import magazyn.InwentaryzacjaComponent.Model.SpisZNaturyModel;
import magazyn.ListaTowarowComponent.Contract.IListaTowarow;
import magazyn.ListaTowarowComponent.Implementation.ListaTowarow;
import magazyn.ListaTowarowComponent.Model.TowarModel;
import pk.epub.Creator.Creator;
import test.resetBazy.ResetBazy;

/**
 * @author Jakub Dydo
 * @version 1.0.1.0
 */
public class InwentaryzacjaTest {

    @SuppressWarnings("deprecation")
    private static String GenerujBazeTowarow() {
        ResetBazy.usunBaze("BazaTowarow.yap", false);
        String wynik = "Towar został pomyślnie dodany do bazy danych.";
        IListaTowarow listaTowarow = new ListaTowarow(new BazaDanych());
        wynik = listaTowarow.dodajTowar(new TowarModel(1, "nazwa1", "producent1", 11, 1.00, 0.1, new Date(2013, 0, 31), "pkwiu1", 1, "Produkt Testowy1", false, false));
        wynik = listaTowarow.dodajTowar(new TowarModel(2, "nazwa2", "producent2", 11, 1.00, 0.1, new Date(2013, 0, 31), "pkwiu1", 1, "Produkt Testowy1", false, false));
        wynik = listaTowarow.dodajTowar(new TowarModel(3, "nazwa3", "producent3", 0, 1.00, 0.1, new Date(2013, 0, 12), "pkwiu1", 1, "Produkt Testowy1", false, false));
        wynik = listaTowarow.dodajTowar(new TowarModel(4, "nazwa4", "producent4", 5, 1.00, 0.1, new Date(2013, 0, 14), "pkwiu1", 1, "Produkt Testowy1", false, false));
        wynik = listaTowarow.dodajTowar(new TowarModel(5, "nazwa5", "producent5", 8, 1.00, 0.1, new Date(2013, 0, 31), "pkwiu1", 1, "Produkt Testowy1", false, false));
        wynik = listaTowarow.dodajTowar(new TowarModel(6, "nazwa6", "producent6", 4, 1.00, 0.1, new Date(2013, 1, 1), "pkwiu1", 1, "Produkt Testowy1", false, false));
        wynik = listaTowarow.dodajTowar(new TowarModel(7, "nazwa7", "producent7", 78, 1.00, 0.1, new Date(2013, 1, 28), "pkwiu1", 1, "Produkt Testowy1", false, false));
        wynik = listaTowarow.dodajTowar(new TowarModel(8, "nazwa8", "producent8", 43, 1.00, 0.1, new Date(2013, 1, 15), "pkwiu1", 1, "Produkt Testowy1", false, false));
        wynik = listaTowarow.dodajTowar(new TowarModel(9, "nazwa9", "producent9", 0, 1.00, 0.1, new Date(2013, 1, 27), "pkwiu1", 1, "Produkt Testowy1", false, false));
        wynik = listaTowarow.dodajTowar(new TowarModel(10, "nazwa10", "producent10", 0, 1.00, 0.1, new Date(2013, 2, 1), "pkwiu1", 1, "Produkt Testowy1", false, false));
        wynik = listaTowarow.dodajTowar(new TowarModel(11, "nazwa11", "producent11", 0, 1.00, 0.1, new Date(2013, 2, 31), "pkwiu1", 1, "Produkt Testowy1", false, false));
        wynik = listaTowarow.dodajTowar(new TowarModel(12, "nazwa12", "producent12", 0, 1.00, 0.1, new Date(2013, 2, 12), "pkwiu1", 1, "Produkt Testowy1", false, false));
        wynik = listaTowarow.dodajTowar(new TowarModel(13, "nazwa13", "producent13", 5, 1.00, 0.1, new Date(2013, 2, 18), "pkwiu1", 1, "Produkt Testowy1", false, false));
        wynik = listaTowarow.dodajTowar(new TowarModel(14, "nazwa14", "producent14", 10, 1.00, 0.1, new Date(2013, 2, 21), "pkwiu1", 1, "Produkt Testowy1", false, false));
        wynik = listaTowarow.dodajTowar(new TowarModel(15, "nazwa15", "producent15", 0, 1.00, 0.1, new Date(2013, 0, 1), "pkwiu1", 1, "Produkt Testowy1", false, true));
        wynik = listaTowarow.dodajTowar(new TowarModel(16, "nazwa16", "producent16", 1, 1.00, 0.1, new Date(2013, 0, 31), "pkwiu1", 1, "Produkt Testowy1", true, false));
        wynik = listaTowarow.dodajTowar(new TowarModel(17, "nazwa17", "producent17", 1, 1.00, 0.1, new Date(2013, 0, 16), "pkwiu1", 1, "Produkt Testowy1", true, true));
        return wynik;
    }

    @SuppressWarnings("deprecation")
    public static String GenerujBazeSpisow() {
        ResetBazy.usunBaze("BazaSpisowZNatury.yap", false);
        String wynik = "Spis został pomyślnie dodany do bazy danych.";
        IInwentaryzacja inwentaryzacja = new Inwentaryzacja(new BazaDanych(), new Creator());

        SpisZNaturyModel spis1 = new SpisZNaturyModel("Jan", "Kowalski", new Date(2013, 10, 29));
        SpisZNaturyModel spis2 = new SpisZNaturyModel("Janusz", "Kwiatkowski", new Date(2013, 1, 1));

        inwentaryzacja.dodajTowarDoSpisu(spis1, new TowarModel(1, "nazwa1s", "producent1", 15, 1.00, 0.1, new Date(2013, 0, 31), "pkwiu1", 1, "Produkt Testowy1", false, false));
        inwentaryzacja.dodajTowarDoSpisu(spis1, new TowarModel(2, "nazwa2s", "producent2", 2, 1.00, 0.1, new Date(2013, 0, 31), "pkwiu1", 1, "Produkt Testowy1", false, false));
        inwentaryzacja.dodajTowarDoSpisu(spis1, new TowarModel(3, "nazwa3s", "producent3", 0, 1.00, 0.1, new Date(2013, 0, 12), "pkwiu1", 1, "Produkt Testowy1", false, false));
        inwentaryzacja.dodajTowarDoSpisu(spis1, new TowarModel(4, "nazwa4s", "producent4", 5, 1.00, 0.1, new Date(2013, 0, 14), "pkwiu1", 1, "Produkt Testowy1", false, false));
        inwentaryzacja.dodajTowarDoSpisu(spis1, new TowarModel(5, "nazwa5s", "producent5", 12, 1.00, 0.1, new Date(2013, 0, 31), "pkwiu1", 1, "Produkt Testowy1", false, false));
        inwentaryzacja.dodajTowarDoSpisu(spis1, new TowarModel(6, "nazwa6s", "producent6", 2, 1.00, 0.1, new Date(2013, 1, 1), "pkwiu1", 1, "Produkt Testowy1", false, false));

        inwentaryzacja.dodajTowarDoSpisu(spis2, new TowarModel(7, "nazwa7s", "producent7", 79, 1.00, 0.1, new Date(2013, 1, 28), "pkwiu1", 1, "Produkt Testowy1", false, false));
        inwentaryzacja.dodajTowarDoSpisu(spis2, new TowarModel(8, "nazwa8s", "producent8", 45, 1.00, 0.1, new Date(2013, 1, 15), "pkwiu1", 1, "Produkt Testowy1", false, false));
        inwentaryzacja.dodajTowarDoSpisu(spis2, new TowarModel(9, "nazwa9s", "producent9", 0, 1.00, 0.1, new Date(2013, 1, 27), "pkwiu1", 1, "Produkt Testowy1", false, false));

        wynik = inwentaryzacja.dodajSpisZNatury(spis1);
        wynik = inwentaryzacja.dodajSpisZNatury(spis2);

        return wynik;
    }

    @Test
    public void testZnajdzSpisZNatury() {
        GenerujBazeTowarow();
        GenerujBazeSpisow();
        IInwentaryzacja inwentaryzacja = new Inwentaryzacja(new BazaDanych(), new Creator());

        List<SpisZNaturyModel> listaSpisow = inwentaryzacja.znajdzSpisZNatury(new SpisZNaturyModel(1));

        Assert.assertNotNull(listaSpisow);
        Assert.assertEquals(1, listaSpisow.size());
        Assert.assertEquals(6, listaSpisow.get(0).getSpisZNaturyTowary().size());
    }

    @SuppressWarnings("deprecation")
    @Test
    public void testZnajdzSpisZNaturyPredicate() {
        GenerujBazeTowarow();
        GenerujBazeSpisow();
        IInwentaryzacja inwentaryzacja = new Inwentaryzacja(new BazaDanych(), new Creator());

        List<SpisZNaturyModel> listaSpisow1 = inwentaryzacja.znajdzSpisZNatury(new Date(2013, 10, 1), new Date(2013, 10, 31));
        List<SpisZNaturyModel> listaSpisow2 = inwentaryzacja.znajdzSpisZNatury(new Date(2013, 10, 1), new Date(2012, 10, 31));

        Assert.assertNotNull(listaSpisow1);
        Assert.assertEquals(1, listaSpisow1.size());
        Assert.assertEquals(6, listaSpisow1.get(0).getSpisZNaturyTowary().size());
        Assert.assertEquals(0, listaSpisow2.size());
    }

    @SuppressWarnings("deprecation")
    @Test
    public void testDodajUsunTowar() {
        GenerujBazeTowarow();
        GenerujBazeSpisow();
        IInwentaryzacja inwentaryzacja = new Inwentaryzacja(new BazaDanych(), new Creator());

        SpisZNaturyModel spis1 = inwentaryzacja.znajdzSpisZNatury(new SpisZNaturyModel(1)).get(0);
        Assert.assertNotNull(spis1);
        Assert.assertEquals(6, spis1.getSpisZNaturyTowary().size());

        TowarModel towar = new TowarModel(14, "nazwa14", "producent14", 1, 1.00, 0.1, new Date(2013, 0, 16), "pkwiu1", 1, "Produkt Testowy1", false, false);

        String wiadomosc1 = inwentaryzacja.dodajTowarDoSpisu(spis1, towar);
        Assert.assertNotNull(spis1);
        Assert.assertEquals(7, spis1.getSpisZNaturyTowary().size());

        String wiadomosc2 = inwentaryzacja.dodajTowarDoSpisu(spis1, towar);
        Assert.assertNotNull(spis1);
        Assert.assertEquals(7, spis1.getSpisZNaturyTowary().size());
        Assert.assertEquals(2, spis1.getSpisZNaturyTowary().get(6).getTowarIlosc(), 0.0);

        String wiadomosc3 = inwentaryzacja.usunTowarZeSpisu(spis1, towar);
        Assert.assertNotNull(spis1);
        Assert.assertEquals(6, spis1.getSpisZNaturyTowary().size());

        String wiadomosc4 = inwentaryzacja.usunTowarZeSpisu(spis1, towar);
        Assert.assertNotNull(spis1);
        Assert.assertEquals(6, spis1.getSpisZNaturyTowary().size());

        Assert.assertEquals("Towar został dodany.", wiadomosc1);
        Assert.assertEquals("Liczebność towaru została zaktualizowana.", wiadomosc2);
        Assert.assertEquals("Towar został pomyślnie usunięty.", wiadomosc3);
        Assert.assertEquals("Błąd! Nie znaleziono towaru.", wiadomosc4);
    }

    @SuppressWarnings("deprecation")
    @Test
    public void testDodajSpis() {
        GenerujBazeTowarow();
        GenerujBazeSpisow();
        IInwentaryzacja inwentaryzacja = new Inwentaryzacja(new BazaDanych(), new Creator());

        SpisZNaturyModel spis3 = new SpisZNaturyModel("Korneliusz", "Kowalski",  new Date(2013, 11, 1));
        inwentaryzacja.dodajTowarDoSpisu(spis3, new TowarModel(13, "nazwa13s", "producent13", 15, 1.00, 0.1, new Date(2013, 0, 31), "pkwiu1", 1, "Produkt Testowy1", false, false));
        String wynik1 = inwentaryzacja.dodajSpisZNatury(spis3);
        SpisZNaturyModel spis = inwentaryzacja.znajdzSpisZNatury(new SpisZNaturyModel(3)).get(0);

        Assert.assertEquals("Spis został pomyślnie dodany do bazy danych.", wynik1);
        Assert.assertNotNull(spis3);
        Assert.assertEquals(1, spis3.getSpisZNaturyTowary().size());
        Assert.assertEquals("Korneliusz", spis.getSpisZNaturyOsobaSprawdzajacaImie());
    }

    /*
    @SuppressWarnings("deprecation")
    @Test
    public void testEdytujSpis() {
        GenerujBazeTowarow();
        GenerujBazeSpisow();
        IInwentaryzacja inwentaryzacja = new Inwentaryzacja(new BazaDanych());

        SpisZNaturyModel spisNowy1 = new SpisZNaturyModel("Jan", "Kowalski", new Date(2013, 11, 1));
        inwentaryzacja.dodajTowarDoSpisu(spisNowy1, new TowarModel(13, "nazwa13s", "producent13", 15, 1.00, 0.1, new Date(2013, 0, 31), "pkwiu1", 1, "Produkt Testowy1", false, false));

        SpisZNaturyModel spisNowy2 = new SpisZNaturyModel("Jan", "Kowalski", new Date(2013, 11, 1));

        String wiadomosc1 = inwentaryzacja.edytujSpisZNatury(new SpisZNaturyModel(1), spisNowy1);
        String wiadomosc2 = inwentaryzacja.edytujSpisZNatury(new SpisZNaturyModel(2), spisNowy2);

        Assert.assertEquals("Spis został pomyślnie zaktualizowany.", wiadomosc1);
        Assert.assertEquals("Spis został pomyślnie zaktualizowany.", wiadomosc2);

        SpisZNaturyModel spisA = inwentaryzacja.znajdzSpisZNatury(new SpisZNaturyModel(1)).get(0);
        SpisZNaturyModel spisB = inwentaryzacja.znajdzSpisZNatury(new SpisZNaturyModel(2)).get(0);

        Assert.assertEquals(1, spisA.getSpisZNaturyTowary().size());
        Assert.assertEquals(0, spisB.getSpisZNaturyTowary().size());
    }

    @Test
    public void testUsunSpis() {
        GenerujBazeTowarow();
        GenerujBazeSpisow();
        IInwentaryzacja inwentaryzacja = new Inwentaryzacja(new BazaDanych());

        String wiadomosc1 = inwentaryzacja.usunSpisZNatury(new SpisZNaturyModel(1));
        List<SpisZNaturyModel> listaSpisow = inwentaryzacja.znajdzSpisZNatury(new SpisZNaturyModel());

        Assert.assertEquals("Spis został pomyślnie usunięty z bazy danych.", wiadomosc1);
        Assert.assertEquals(1, listaSpisow.size());
        Assert.assertEquals(3, listaSpisow.get(0).getSpisZNaturyTowary().size());
    }
*/
    @Test
    public void testGenerujDokumentNadwyzek() {
        GenerujBazeTowarow();
        GenerujBazeSpisow();
        IInwentaryzacja inwentaryzacja = new Inwentaryzacja(new BazaDanych(), new Creator());

        SpisZNaturyModel spis = inwentaryzacja.znajdzSpisZNatury(new SpisZNaturyModel(1)).get(0);
        DokumentNadwyzekNiedoborowModel dokument = new DokumentNadwyzekNiedoborowModel();
        String wiadomosc1 = inwentaryzacja.generujDokumentNadwyzek(dokument, spis);

        Assert.assertEquals("Dokument nadwyżek został wygenerowany pomyślnie.", wiadomosc1);
        Assert.assertEquals(2, dokument.getDokumentNadwyzekNiedoborowTowary().size());
        Assert.assertEquals(1, dokument.getSpisZNaturyID());
        Assert.assertEquals(8.8, dokument.getDokumentNadwyzekNiedoborowSumaKwota(), 0.0000001);
        Assert.assertEquals(4, dokument.getDokumentNadwyzekNiedoborowTowary().get(0).getTowarZyskStrataIlosc(), 0.000001);
        Assert.assertEquals(4, dokument.getDokumentNadwyzekNiedoborowTowary().get(1).getTowarZyskStrataIlosc(), 0.000001);
        Assert.assertEquals(4.4, dokument.getDokumentNadwyzekNiedoborowTowary().get(0).getTowarZyskStrataKwota(), 0.000001);
        Assert.assertEquals(4.4, dokument.getDokumentNadwyzekNiedoborowTowary().get(1).getTowarZyskStrataKwota(), 0.000001);
        Assert.assertEquals(1, dokument.getDokumentNadwyzekNiedoborowTowary().get(0).getTowarZyskStrataTowar().getTowarID());
        Assert.assertEquals(5, dokument.getDokumentNadwyzekNiedoborowTowary().get(1).getTowarZyskStrataTowar().getTowarID());
    }

    @Test
    public void testGenerujDokumentNiedoborow() {
        GenerujBazeTowarow();
        GenerujBazeSpisow();
        IInwentaryzacja inwentaryzacja = new Inwentaryzacja(new BazaDanych(), new Creator());

        SpisZNaturyModel spis = inwentaryzacja.znajdzSpisZNatury(new SpisZNaturyModel(1)).get(0);
        DokumentNadwyzekNiedoborowModel dokument = new DokumentNadwyzekNiedoborowModel();
        String wiadomosc1 = inwentaryzacja.generujDokumentNiedoborow(dokument, spis);

        Assert.assertEquals("Dokument niedoborów został wygenerowany pomyślnie.", wiadomosc1);
        Assert.assertEquals(2, dokument.getDokumentNadwyzekNiedoborowTowary().size());
        Assert.assertEquals(1, dokument.getSpisZNaturyID());
        Assert.assertEquals(12.1, dokument.getDokumentNadwyzekNiedoborowSumaKwota(), 0.0000001);
        Assert.assertEquals(9, dokument.getDokumentNadwyzekNiedoborowTowary().get(0).getTowarZyskStrataIlosc(), 0.000001);
        Assert.assertEquals(2, dokument.getDokumentNadwyzekNiedoborowTowary().get(1).getTowarZyskStrataIlosc(), 0.000001);
        Assert.assertEquals(9.9, dokument.getDokumentNadwyzekNiedoborowTowary().get(0).getTowarZyskStrataKwota(), 0.000001);
        Assert.assertEquals(2.2, dokument.getDokumentNadwyzekNiedoborowTowary().get(1).getTowarZyskStrataKwota(), 0.000001);
        Assert.assertEquals(2, dokument.getDokumentNadwyzekNiedoborowTowary().get(0).getTowarZyskStrataTowar().getTowarID());
        Assert.assertEquals(6, dokument.getDokumentNadwyzekNiedoborowTowary().get(1).getTowarZyskStrataTowar().getTowarID());
    }

    @Test
    public void testgeneruj() {
        List<TowarModel> lista = new ArrayList<TowarModel>();
        String wiadomosc1 = generuj(lista);
        Assert.assertEquals("OK", wiadomosc1);
        Assert.assertEquals(1, lista.size());
    }

    private String generuj(List<TowarModel> lista) {
        List<TowarModel> lista1 = new ArrayList<TowarModel>();
        lista1.add(new TowarModel());
        lista.addAll(lista1);
        return "OK";
    }

}
