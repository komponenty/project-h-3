package glowne.BazaDanychComponent.Contract;

import com.db4o.query.Predicate;
import java.util.List;

/**
 * @author Jakub Dydo
 * @version 1.1.1.0
 */
public interface IBaza {

    /**
     * Znajduje rekordy odpowiadające danemu szablonowi
     * @param <T> typ szukanego obiektu
     * @param nazwaBazy sciezka do pliku z bazą danych
     * @param czyBazaSzyfrowana true - jeżeli baza ma być szyfrowana
     * @param szablon szablon rekordu do znalezienia
     * @return List<T>  - w przypadku znalezienia lub nie (dlugosc listy = 0) rekordów odpowiadających danemu szablonowi
     *         <br>
     *         null -  w przypadku błędu krytycznego
     */
    <T> List<T> znajdzObiekt(String nazwaBazy, boolean czyBazaSzyfrowana, T szablon);

    /**
     * Znajduje rekordy odpowiadające danemu kryterium z klasy Predicate
     * @param <T> typ szukanego obiektu
     * @param nazwaBazy sciezka do pliku z bazą danych
     * @param czyBazaSzyfrowana true - jeżeli baza ma być szyfrowana
     * @param klasaPredicate klasa dziedzicząca po Predicate, zawierająca odpowiednie kryterium porównawcze
     * @return List<T>  - w przypadku znalezienia lub nie (dlugosc listy = 0) rekordów odpowiadających danemu kryterium z klasy Predicate
     *         <br>
     *         null -  w przypadku błędu krytycznego
     */
    <T> List<T> znajdzObiektPredicate(String nazwaBazy, boolean czyBazaSzyfrowana, Predicate<T> klasaPredicate);

    /**
     * Dodaje nowy rekord do bazy danych
     * @param <T> typ obiektu do dodania
     * @param nazwaBazy sciezka do pliku z bazą danych
     * @param czyBazaSzyfrowana true - jeżeli baza ma być szyfrowana
     * @param nowyObiekt obiekt do dodania
     * @return true -  w przypadku pomyślnego dodania do bazy
     *         <br>
     *         false - w przypadku błędu bazy
     */
    <T> boolean dodajObiekt(String nazwaBazy, boolean czyBazaSzyfrowana, T nowyObiekt);

    /**
     * Aktualizuje rekord odpowiadający danemu szablonowi
     * @param <T> typ rekordu do edycji
     * @param nazwaBazy sciezka do pliku z bazą danych
     * @param czyBazaSzyfrowana true - jeżeli baza ma być szyfrowana
     * @param szablon rekord przed edycją (stary)
     * @param obiektWyedytowany rekord po edycji (nowy)
     * @return true -  w przypadku pomyślnego dodania do bazy
     *         <br>
     *         false - w przypadku błędu bazy
     */
    <T> boolean edytujObiekt(String nazwaBazy, boolean czyBazaSzyfrowana, T szablon, T obiektWyedytowany);

    /**
     * Aktualizuje rekord odpowiadający danemu kryterium z klasy Predicate
     * @param <T> typ rekordu do edycji
     * @param nazwaBazy sciezka do pliku z bazą danych
     * @param czyBazaSzyfrowana true - jeżeli baza ma być szyfrowana
     * @param klasaPredicate klasa dziedzicząca po Predicate, zawierająca odpowiednie kryterium porównawcze
     * @param obiektWyedytowany  rekord po edycji (nowy)
     * @return true -  w przypadku pomyślnej aktualizacji w bazie
     *         <br>
     *         false - w przypadku błędu bazy
     */
    <T> boolean edytujObiektPredicate(String nazwaBazy, boolean czyBazaSzyfrowana, Predicate<T> klasaPredicate, T obiektWyedytowany);

    /**
     * Usuwa rekord odpowiadający danemu szablonowi
     * @param <T> typ rekordu do usunięcia
     * @param nazwaBazy sciezka do pliku z bazą danych
     * @param czyBazaSzyfrowana true - jeżeli baza ma być szyfrowana
     * @param szablon szablon rekordu do usunięcia
     * @return true -  w przypadku pomyślnego usunięcia z bazy
     *         <br>
     *         false - w przypadku błędu bazy
     */
    <T> boolean usunObiekt(String nazwaBazy, boolean czyBazaSzyfrowana, T szablon);

    /**
     * Usuwa rekordy odpowiadające danemu kryterium z klasy Predicate
     * @param <T> typ rekordu do usunięcia
     * @param nazwaBazy sciezka do pliku z bazą danych
     * @param czyBazaSzyfrowana true - jeżeli baza ma być szyfrowana
     * @param klasaPredicate klasa dziedzicząca po Predicate, zawierająca odpowiednie kryterium porównawcze
     * @param szablon dowolna instancja typu, którego obiekty przeznaczone są do usunięcia
     * @return true -  w przypadku pomyślnego dodania do bazy
     *         <br>
     *         false - w przypadku błędu bazy
     */
    <T> boolean usunObiektPredicate(String nazwaBazy, boolean czyBazaSzyfrowana, Predicate<T> klasaPredicate, T szablon);
}
