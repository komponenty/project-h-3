package glowne.BazaDanychComponent.Implementation;

import glowne.BazaDanychComponent.Contract.IBaza;
import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import com.db4o.config.EmbeddedConfiguration;
import com.db4o.query.Predicate;
import java.util.ArrayList;
import java.util.List;
import xtea_db4o.XTeaEncryptionStorage;

/**
 * @author Jakub Dydo
 * @version 1.1.1.0
 */
public class BazaDanych implements IBaza {

    /**
     * {@inheritDoc}
     */
    @Override
    public <T> boolean dodajObiekt(String nazwaBazy, boolean czyBazaSzyfrowana, T nowyObiekt) {
        String haslo = "a2v5m2x9f7";
        EmbeddedConfiguration config = Db4oEmbedded.newConfiguration();
        config.common().activationDepth(Integer.MAX_VALUE);
        config.common().updateDepth(Integer.MAX_VALUE);

        if (czyBazaSzyfrowana) {
            config.file().storage(new XTeaEncryptionStorage(haslo));
        }

        ObjectContainer db = Db4oEmbedded.openFile(config, nazwaBazy);
        try {
            db.store(nowyObiekt);
            return true;
        } catch (Exception e) {
            return false;
        } finally {
            db.close();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <T> boolean edytujObiekt(String nazwaBazy, boolean czyBazaSzyfrowana, T szablon, T obiektWyedytowany) {
        String haslo = "a2v5m2x9f7";
        EmbeddedConfiguration config = Db4oEmbedded.newConfiguration();
        config.common().activationDepth(Integer.MAX_VALUE);
        config.common().objectClass(szablon).cascadeOnDelete(true);

        if (czyBazaSzyfrowana) {
            config.file().storage(new XTeaEncryptionStorage(haslo));
        }

        ObjectContainer db = Db4oEmbedded.openFile(config, nazwaBazy);
        try {
            ObjectSet<T> wynik = db.queryByExample(szablon);
            T znaleziony = wynik.next();
            db.delete(znaleziony);
            db.store(obiektWyedytowany);
            return true;
        } catch (Exception e) {
            return false;
        } finally {
            db.close();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <T> boolean edytujObiektPredicate(String nazwaBazy, boolean czyBazaSzyfrowana, Predicate<T> klasaPredicate, T obiektWyedytowany) {
        String haslo = "a2v5m2x9f7";
        EmbeddedConfiguration config = Db4oEmbedded.newConfiguration();
        config.common().activationDepth(Integer.MAX_VALUE);
        config.common().objectClass(obiektWyedytowany).cascadeOnDelete(true);

        if (czyBazaSzyfrowana) {
            config.file().storage(new XTeaEncryptionStorage(haslo));
        }

        ObjectContainer db = Db4oEmbedded.openFile(config, nazwaBazy);
        try {
            ObjectSet<T> wynik = db.query(klasaPredicate);
            T znaleziony = wynik.next();
            db.delete(znaleziony);
            db.store(obiektWyedytowany);
            return true;
        } catch (Exception e) {
            return false;
        } finally {
            db.close();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <T> boolean usunObiekt(String nazwaBazy, boolean czyBazaSzyfrowana, T szablon) {
        String haslo = "a2v5m2x9f7";
        EmbeddedConfiguration config = Db4oEmbedded.newConfiguration();
        config.common().activationDepth(Integer.MAX_VALUE);
        config.common().objectClass(szablon).cascadeOnDelete(true);

        if (czyBazaSzyfrowana) {
            config.file().storage(new XTeaEncryptionStorage(haslo));
        }

        ObjectContainer db = Db4oEmbedded.openFile(config, nazwaBazy);
        try {
            ObjectSet<T> wynik = db.queryByExample(szablon);
            T znaleziony = (T) wynik.next();
            db.delete(znaleziony);
            return true;
        } catch (Exception e) {
            return false;
        } finally {
            db.close();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <T> boolean usunObiektPredicate(String nazwaBazy, boolean czyBazaSzyfrowana, Predicate<T> klasaPredicate, T szablon) {
        String haslo = "a2v5m2x9f7";
        EmbeddedConfiguration config = Db4oEmbedded.newConfiguration();
        config.common().activationDepth(Integer.MAX_VALUE);
        config.common().objectClass(szablon).cascadeOnDelete(true);

        if (czyBazaSzyfrowana) {
            config.file().storage(new XTeaEncryptionStorage(haslo));
        }

        ObjectContainer db = Db4oEmbedded.openFile(config, nazwaBazy);
        try {
            ObjectSet<T> wynik = db.query(klasaPredicate);
            for (T object : wynik) {
                db.delete(object);
            }
            return true;
        } catch (Exception e) {
            return false;
        } finally {
            db.close();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <T> List<T> znajdzObiekt(String nazwaBazy, boolean czyBazaSzyfrowana, T szablon) {
        String haslo = "a2v5m2x9f7";
        EmbeddedConfiguration config = Db4oEmbedded.newConfiguration();
        config.common().activationDepth(Integer.MAX_VALUE);

        if (czyBazaSzyfrowana) {
            config.file().storage(new XTeaEncryptionStorage(haslo));
        }

        ObjectContainer db = Db4oEmbedded.openFile(config, nazwaBazy);
        try {
            ObjectSet<T> wynik = db.queryByExample(szablon);
            List<T> lista = new ArrayList<T>();
            for (T object : wynik) {
                lista.add(object);
            }
            return lista;
        } catch (Exception e) {
            System.out.println(e);
            return null;
        } finally {
            db.close();
        }
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    @Override
    public <T> List<T> znajdzObiektPredicate(String nazwaBazy, boolean czyBazaSzyfrowana, Predicate<T> klasaPredicate) {
        String haslo = "a2v5m2x9f7";
        EmbeddedConfiguration config = Db4oEmbedded.newConfiguration();
        config.common().activationDepth(Integer.MAX_VALUE);

        if (czyBazaSzyfrowana) {
            config.file().storage(new XTeaEncryptionStorage(haslo));
        }

        ObjectContainer db = Db4oEmbedded.openFile(config, nazwaBazy);
        try {
            List<T> wynik = db.query(klasaPredicate);
            List<T> lista = new ArrayList<T>();
            for (Object object : wynik) {
                lista.add((T) object);
            }
            return lista;
        } catch (Exception e) {
            return null;
        } finally {
            db.close();
        }
    }
}
