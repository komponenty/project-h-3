package glowne.BazaDanychComponent.Test;

import com.db4o.query.Predicate;

/**
 * @author Jakub Dydo
 * @version 1.0.1.0
 */

public class TestPredicate extends Predicate<TestowaDwa> {

    private static final long serialVersionUID = 1L;

    @Override
    public boolean match(TestowaDwa arg0) {
        if ((arg0).liczba < 7) {
            return true;
        } else {
            return false;
        }
    }
}

