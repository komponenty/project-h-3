package glowne.BazaDanychComponent.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Jakub Dydo
 * @version 1.0.1.0
 */
public class TestowaJeden {

    public String nazwa;
    public int liczba;
    public List<TestowaDwa> lista = new ArrayList<TestowaDwa>();
    public TestowaDwa test1;

    public TestowaJeden() {
        this.nazwa = "nazwa1";
        this.liczba = 1;
        this.lista.add(new TestowaDwa());
        this.test1 = new TestowaDwa();
    }

    public TestowaJeden(String nazwa, int liczba) {
        this.nazwa = nazwa;
        this.liczba = liczba;
        this.lista.add(new TestowaDwa("testLista" + nazwa, liczba));
        this.test1 = new TestowaDwa("test" + nazwa, liczba);
    }
}
