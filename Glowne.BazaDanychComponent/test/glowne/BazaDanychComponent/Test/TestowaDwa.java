package glowne.BazaDanychComponent.Test;

/**
 * @author Jakub Dydo
 * @version 1.0.1.0
 */
public class TestowaDwa {

    public String nazwa;
    public int liczba;

    public TestowaDwa() {
        this.nazwa = "nazwa12";
        this.liczba = 1;
    }

    public TestowaDwa(String nazwa, int liczba) {
        this.nazwa = nazwa;
        this.liczba = liczba;
    }
}
