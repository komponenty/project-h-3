package glowne.BazaDanychComponent.Test;

import glowne.BazaDanychComponent.Implementation.BazaDanych;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;
import test.resetBazy.ResetBazy;

/**
 * @author Jakub Dydo
 * @version 1.1.1.1
 */
public class BazaDanychTest {

    @Test
    public void testZapis() {
        ResetBazy.usunBaze("TestowaDodaj.yap", false);
        BazaDanych baza = new BazaDanych();
        boolean wynik = baza.dodajObiekt("TestowaDodaj.yap", false, new TestowaJeden());
        Assert.assertEquals(true, wynik);
    }

    @Test
    public void testEdycja() {
        ResetBazy.usunBaze("TestowaEdycja.yap", false);
        BazaDanych baza = new BazaDanych();
        boolean wynik1 = baza.dodajObiekt("TestowaEdycja.yap", false, new TestowaJeden());
        boolean wynik2 = baza.edytujObiekt("TestowaEdycja.yap", false, new TestowaJeden(), new TestowaJeden("zmiana2", 18));
        Assert.assertEquals(true, wynik1);
        Assert.assertEquals(true, wynik2);
    }

    @Test
    public void testEdycjaPredicate() {
        ResetBazy.usunBaze("TestowaEdycjaPredicate.yap", false);
        BazaDanych baza = new BazaDanych();
        boolean wynik1 = baza.dodajObiekt("TestowaEdycjaPredicate.yap", false, new TestowaDwa("nazwa1", 7));
        boolean wynik2 = baza.dodajObiekt("TestowaEdycjaPredicate.yap", false, new TestowaDwa("nazwa2", 2));
        boolean wynik3 = baza.dodajObiekt("TestowaEdycjaPredicate.yap", false, new TestowaDwa("nazwa3", 3));
        boolean wynik4 = baza.edytujObiektPredicate("TestowaEdycjaPredicate.yap", false, new TestPredicate(), new TestowaDwa("nazwa4", 4));
        Assert.assertEquals(true, wynik1);
        Assert.assertEquals(true, wynik2);
        Assert.assertEquals(true, wynik3);
        Assert.assertEquals(true, wynik4);
    }

    @Test
    public void testUsuwanie() {
        ResetBazy.usunBaze("TestowaUsuwanie.yap", false);
        BazaDanych baza = new BazaDanych();
        boolean wynik1 = baza.dodajObiekt("TestowaUsuwanie.yap", false, new TestowaDwa("nazwa1", 7));
        boolean wynik2 = baza.dodajObiekt("TestowaUsuwanie.yap", false, new TestowaDwa("nazwa2", 2));
        boolean wynik3 = baza.dodajObiekt("TestowaUsuwanie.yap", false, new TestowaDwa("nazwa3", 3));
        boolean wynik4 = baza.usunObiekt("TestowaUsuwanie.yap", false, new TestowaDwa("nazwa1", 7));
        Assert.assertEquals(true, wynik1);
        Assert.assertEquals(true, wynik2);
        Assert.assertEquals(true, wynik3);
        Assert.assertEquals(true, wynik4);
    }

    @Test
    public void testUsuwaniePredicate() {
        ResetBazy.usunBaze("TestowaUsuwaniePredicate.yap", false);
        BazaDanych baza = new BazaDanych();
        boolean wynik1 = baza.dodajObiekt("TestowaUsuwaniePredicate.yap", false, new TestowaDwa("nazwa1", 7));
        boolean wynik2 = baza.dodajObiekt("TestowaUsuwaniePredicate.yap", false, new TestowaDwa("nazwa2", 2));
        boolean wynik3 = baza.dodajObiekt("TestowaUsuwaniePredicate.yap", false, new TestowaDwa("nazwa3", 3));
        boolean wynik4 = baza.usunObiektPredicate("TestowaUsuwaniePredicate.yap", false, new TestPredicate(), new TestowaDwa());
        Assert.assertEquals(true, wynik1);
        Assert.assertEquals(true, wynik2);
        Assert.assertEquals(true, wynik3);
        Assert.assertEquals(true, wynik4);
    }

    @Test
    public void testWyszukiwanie() {
        ResetBazy.usunBaze("TestowaWyszukanie.yap", false);
        BazaDanych baza = new BazaDanych();
        boolean wynik1 = baza.dodajObiekt("TestowaWyszukanie.yap", false, new TestowaDwa("nazwa1", 1));
        boolean wynik2 = baza.dodajObiekt("TestowaWyszukanie.yap", false, new TestowaDwa("nazwa2", 1));
        boolean wynik3 = baza.dodajObiekt("TestowaWyszukanie.yap", false, new TestowaDwa("nazwa3", 2));
        List<TestowaDwa> wynik4 = baza.znajdzObiekt("TestowaWyszukanie.yap", false, new TestowaDwa(null, 1));
        int liczba1 = wynik4.get(0).liczba;
        int liczba2 = wynik4.get(1).liczba;
        int count = wynik4.size();
        Assert.assertEquals(true, wynik1);
        Assert.assertEquals(true, wynik2);
        Assert.assertEquals(true, wynik3);
        Assert.assertEquals(1, liczba1);
        Assert.assertEquals(1, liczba2);
        Assert.assertEquals(2, count);
    }

    @Test
    public void testWyszukiwaniePredicate() {
        ResetBazy.usunBaze("TestowaWyszukaniePredicate.yap", false);
        BazaDanych baza = new BazaDanych();
        boolean wynik1 = baza.dodajObiekt("TestowaWyszukaniePredicate.yap", false, new TestowaDwa("nazwa1", 1));
        boolean wynik2 = baza.dodajObiekt("TestowaWyszukaniePredicate.yap", false, new TestowaDwa("nazwa2", 1));
        boolean wynik3 = baza.dodajObiekt("TestowaWyszukaniePredicate.yap", false, new TestowaDwa("nazwa3", 8));
        List<TestowaDwa> wynik4 = baza.znajdzObiektPredicate("TestowaWyszukaniePredicate.yap", false, new TestPredicate());
        int liczba1 = wynik4.get(0).liczba;
        int liczba2 = wynik4.get(1).liczba;
        int count = wynik4.size();
        Assert.assertEquals(true, wynik1);
        Assert.assertEquals(true, wynik2);
        Assert.assertEquals(true, wynik3);
        Assert.assertEquals(1, liczba1);
        Assert.assertEquals(1, liczba2);
        Assert.assertEquals(2, count);
    }
}
