package ksiegowosc.KsiazkaPodatkowaComponent.Implementation;

import faktura.FakturaSprzedazyComponent.Model.FakturaSprzedazyModel;
import faktura.FakturaZakupuComponent.Model.FakturaZakupuModel;
import ksiegowosc.KsiazkaPodatkowaComponent.Contract.IKsiazkaPodatkowa;
import ksiegowosc.KsiazkaPodatkowaComponent.Model.KsiazkaPodatkowaModel;
import java.util.Date;

/**
 * @author Paweł Żurawik
 * @version 1.0.0.0
 */
public class KsiazkaPodatkowa implements IKsiazkaPodatkowa {

    @Override
    public String zarejestrujPrzychod(FakturaSprzedazyModel fakturaSprzedazy) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String zarejestrujRozchod(FakturaZakupuModel fakturaZakupu) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String zestawPrzychody(KsiazkaPodatkowaModel ksiazka, Date dataOd, Date dataDo) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String zestawRozchody(KsiazkaPodatkowaModel ksiazka, Date dataOd, Date dataDo) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String zestawWszystko(KsiazkaPodatkowaModel ksiazka, Date dataOd, Date dataDo) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void doPlikuKsiazka(KsiazkaPodatkowaModel ksiazkaDoPliku, String tytul, String informacje, String sciezkaDoPliku) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
