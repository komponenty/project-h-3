package ksiegowosc.KsiazkaPodatkowaComponent.Contract;

import java.util.Date;
import faktura.FakturaSprzedazyComponent.Model.FakturaSprzedazyModel;
import faktura.FakturaZakupuComponent.Model.FakturaZakupuModel;
import ksiegowosc.KsiazkaPodatkowaComponent.Model.KsiazkaPodatkowaModel;

/**
 * @author Paweł Żurawik
 * @version 1.0.0.0
 */
public interface IKsiazkaPodatkowa {

    String zarejestrujPrzychod(FakturaSprzedazyModel fakturaSprzedazy);

    String zarejestrujRozchod(FakturaZakupuModel fakturaZakupu);

    String zestawPrzychody(KsiazkaPodatkowaModel ksiazka, Date dataOd, Date dataDo);

    String zestawRozchody(KsiazkaPodatkowaModel ksiazka, Date dataOd, Date dataDo);

    String zestawWszystko(KsiazkaPodatkowaModel ksiazka, Date dataOd, Date dataDo);

    void doPlikuKsiazka(KsiazkaPodatkowaModel ksiazkaDoPliku, String tytul, String informacje, String sciezkaDoPliku);
}
