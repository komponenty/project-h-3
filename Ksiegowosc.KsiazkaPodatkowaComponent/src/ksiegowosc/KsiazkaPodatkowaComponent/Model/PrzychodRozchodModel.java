package ksiegowosc.KsiazkaPodatkowaComponent.Model;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Date;

/**
 * @author Paweł Żurawik
 * @version 1.0.0.1
 */
public class PrzychodRozchodModel {

    //<editor-fold defaultstate="collapsed" desc="Pola">
    private Rodzaj przychodRozchodRodzaj;
    private Date przychodRozchodDataKsiegowania;
    private int przychodRozchodNumerFaktury;
    private String przychodRozchodTresc;
    private double przychodRozchodKwota;
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Wlasciwosci">
    public void setPrzychodRozchodRodzaj(Rodzaj przychodRozchodRodzaj) {
        Rodzaj oldValue = this.przychodRozchodRodzaj;
        this.przychodRozchodRodzaj = przychodRozchodRodzaj;
        changeSupport.firePropertyChange("przychodRozchodRodzaj", oldValue, przychodRozchodRodzaj);
    }

    public Rodzaj getPrzychodRozchodRodzaj() {
        return this.przychodRozchodRodzaj;
    }

    public void setPrzychodRozchodDataKsiegowania(Date przychodRozchodDataKsiegowania) {
        Date oldValue = this.przychodRozchodDataKsiegowania;
        this.przychodRozchodDataKsiegowania = przychodRozchodDataKsiegowania;
        changeSupport.firePropertyChange("przychodRozchodDataKsiegowania", oldValue, przychodRozchodDataKsiegowania);
    }

    public Date getPrzychodRozchodDataKsiegowania() {
        return this.przychodRozchodDataKsiegowania;
    }

    public void setPrzychodRozchodNumerFaktury(int przychodRozchodNumerFaktury) {
        int oldValue = this.przychodRozchodNumerFaktury;
        this.przychodRozchodNumerFaktury = przychodRozchodNumerFaktury;
        changeSupport.firePropertyChange("przychodRozchodNumerFaktury", oldValue, przychodRozchodNumerFaktury);
    }

    public int getPrzychodRozchodNumerFaktury() {
        return this.przychodRozchodNumerFaktury;
    }

    public void setPrzychodRozchodTresc(String przychodRozchodTresc) {
        String oldValue = this.przychodRozchodTresc;
        this.przychodRozchodTresc = przychodRozchodTresc;
        changeSupport.firePropertyChange("przychodRozchodTresc", oldValue, przychodRozchodTresc);
    }

    public String getPrzychodRozchodTresc() {
        return this.przychodRozchodTresc;
    }

    public void setPrzychodRozchodKwota(double przychodRozchodKwota) {
        double oldValue = this.przychodRozchodKwota;
        this.przychodRozchodKwota = przychodRozchodKwota;
        changeSupport.firePropertyChange("przychodRozchodKwota", oldValue, przychodRozchodKwota);
    }

    public double getPrzychodRozchodKwota() {
        return this.przychodRozchodKwota;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Konstruktory">
    public PrzychodRozchodModel() {
    }

    public PrzychodRozchodModel(Rodzaj przychodRozchodRodzaj, Date przychodRozchodDataKsiegowania, int przychodRozchodNumerFaktury, String przychodRozchodTresc, double przychodRozchodKwota) {
        this.przychodRozchodRodzaj = przychodRozchodRodzaj;
        this.przychodRozchodDataKsiegowania = przychodRozchodDataKsiegowania;
        this.przychodRozchodNumerFaktury = przychodRozchodNumerFaktury;
        this.przychodRozchodTresc = przychodRozchodTresc;
        this.przychodRozchodKwota = przychodRozchodKwota;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="PropertyChange">
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }
    //</editor-fold>
}
