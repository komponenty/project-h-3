package ksiegowosc.KsiazkaPodatkowaComponent.Model;

/**
 * @author Paweł Żurawik
 * @version 1.0.0.0
 */
public enum Rodzaj {

    Przychod,
    Rozchod
}
