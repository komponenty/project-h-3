package ksiegowosc.KsiazkaPodatkowaComponent.Model;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.List;
import org.jfree.chart.ChartFrame;

/**
 * @author Paweł Żurawik
 * @version 1.0.0.1
 */
public class KsiazkaPodatkowaModel {

    //<editor-fold defaultstate="collapsed" desc="Pola">
    private double przychodRozchodZysk;
    private double przychodRozchodStrata;
    private double przychodRozchodSaldo;
    private List<PrzychodRozchodModel> przychodRozchodPrzychodyRozchody;
    private ChartFrame przychodRozchodWykres;
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Wlasciwosci">
    public void setPrzychodRozchodZysk(double przychodRozchodZysk) {
        double oldValue = this.przychodRozchodZysk;
        this.przychodRozchodZysk = przychodRozchodZysk;
        changeSupport.firePropertyChange("przychodRozchodZysk", oldValue, przychodRozchodZysk);
    }

    public double getPrzychodRozchodZysk() {
        return this.przychodRozchodZysk;
    }

    public void setPrzychodRozchodStrata(double przychodRozchodStrata) {
        double oldValue = this.przychodRozchodStrata;
        this.przychodRozchodStrata = przychodRozchodStrata;
        changeSupport.firePropertyChange("przychodRozchodStrata", oldValue, przychodRozchodStrata);
    }

    public double getPrzychodRozchodStrata() {
        return this.przychodRozchodStrata;
    }

    public void setPrzychodRozchodSaldo(double przychodRozchodSaldo) {
        double oldValue = this.przychodRozchodSaldo;
        this.przychodRozchodSaldo = przychodRozchodSaldo;
        changeSupport.firePropertyChange("przychodRozchodSaldo", oldValue, przychodRozchodSaldo);
    }

    public double getPrzychodRozchodSaldo() {
        return this.przychodRozchodSaldo;
    }

    public void setPrzychodRozchodPrzychodyRozchody(List<PrzychodRozchodModel> przychodRozchodPrzychodyRozchody) {
        List<PrzychodRozchodModel> oldValue = this.przychodRozchodPrzychodyRozchody;
        this.przychodRozchodPrzychodyRozchody = przychodRozchodPrzychodyRozchody;
        changeSupport.firePropertyChange("przychodRozchodPrzychodyRozchody", oldValue, przychodRozchodPrzychodyRozchody);
    }

    public List<PrzychodRozchodModel> getPrzychodRozchodPrzychodyRozchody() {
        return this.przychodRozchodPrzychodyRozchody;
    }

    public void setPrzychodRozchodWykres(ChartFrame przychodRozchodWykres) {
        ChartFrame oldValue = this.przychodRozchodWykres;
        this.przychodRozchodWykres = przychodRozchodWykres;
        changeSupport.firePropertyChange("przychodRozchodWykres", oldValue, przychodRozchodWykres);
    }

    public ChartFrame getPrzychodRozchodWykres() {
        return this.przychodRozchodWykres;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Konstruktory">
    public KsiazkaPodatkowaModel() {
    }

    public KsiazkaPodatkowaModel(double przychodRozchodZysk, double przychodRozchodStrata, double przychodRozchodSaldo, List<PrzychodRozchodModel> przychodRozchodPrzychodyRozchody, ChartFrame przychodRozchodWykres) {
        this.przychodRozchodZysk = przychodRozchodZysk;
        this.przychodRozchodStrata = przychodRozchodStrata;
        this.przychodRozchodSaldo = przychodRozchodSaldo;
        this.przychodRozchodPrzychodyRozchody = przychodRozchodPrzychodyRozchody;
        this.przychodRozchodWykres = przychodRozchodWykres;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="PropertyChange">
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }
    //</editor-fold>
}
