package ksiegowosc.DeklaracjaVAT7Component.Model;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.List;
import magazyn.ListaTowarowComponent.Model.TowarModel;

/**
 * @author Paweł Żurawik
 * @version 1.0.0.1
 */
public class DeklaracjaVAT7Model {

    //<editor-fold defaultstate="collapsed" desc="Pola">
    private double deklaracjaVAT7KwotaPodatku;
    private List<TowarModel> deklaracjaVAT7PrzedmiotOpodatkowania;
    private double deklaracjaVAT7PodatekNaliczonyKwota;
    private double deklaracjaVAT7PodatekNaleznyKwota;
    private double deklaracjaVAT7PodatekNaliczonyMinusNalezny;
    private double deklaracjaVAT7KwotaDoZaplatyLubZwrotu;
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Wlasciwosci">
    public void setDeklaracjaVAT7KwotaPodatku(double deklaracjaVAT7KwotaPodatku) {
        double oldValue = this.deklaracjaVAT7KwotaPodatku;
        this.deklaracjaVAT7KwotaPodatku = deklaracjaVAT7KwotaPodatku;
        changeSupport.firePropertyChange("deklaracjaVAT7KwotaPodatku", oldValue, deklaracjaVAT7KwotaPodatku);
    }

    public double getDeklaracjaVAT7KwotaPodatku() {
        return this.deklaracjaVAT7KwotaPodatku;
    }

    public void setDeklaracjaVAT7PrzedmiotOpodatkowania(List<TowarModel> deklaracjaVAT7PrzedmiotOpodatkowania) {
        List<TowarModel> oldValue = this.deklaracjaVAT7PrzedmiotOpodatkowania;
        this.deklaracjaVAT7PrzedmiotOpodatkowania = deklaracjaVAT7PrzedmiotOpodatkowania;
        changeSupport.firePropertyChange("deklaracjaVAT7PrzedmiotOpodatkowania", oldValue, deklaracjaVAT7PrzedmiotOpodatkowania);
    }

    public List<TowarModel> getDeklaracjaVAT7PrzedmiotOpodatkowania() {
        return this.deklaracjaVAT7PrzedmiotOpodatkowania;
    }

    public void setDeklaracjaVAT7PodatekNaliczonyKwota(double deklaracjaVAT7PodatekNaliczonyKwota) {
        double oldValue = this.deklaracjaVAT7PodatekNaliczonyKwota;
        this.deklaracjaVAT7PodatekNaliczonyKwota = deklaracjaVAT7PodatekNaliczonyKwota;
        changeSupport.firePropertyChange("deklaracjaVAT7PodatekNaliczonyKwota", oldValue, deklaracjaVAT7PodatekNaliczonyKwota);
    }

    public double getDeklaracjaVAT7PodatekNaliczonyKwota() {
        return this.deklaracjaVAT7PodatekNaliczonyKwota;
    }

    public void setDeklaracjaVAT7PodatekNaleznyKwota(double deklaracjaVAT7PodatekNaleznyKwota) {
        double oldValue = this.deklaracjaVAT7PodatekNaleznyKwota;
        this.deklaracjaVAT7PodatekNaleznyKwota = deklaracjaVAT7PodatekNaleznyKwota;
        changeSupport.firePropertyChange("deklaracjaVAT7PodatekNaleznyKwota", oldValue, deklaracjaVAT7PodatekNaleznyKwota);
    }

    public double getDeklaracjaVAT7PodatekNaleznyKwota() {
        return this.deklaracjaVAT7PodatekNaleznyKwota;
    }

    public void setDeklaracjaVAT7PodatekNaliczonyMinusNalezny(double deklaracjaVAT7PodatekNaliczonyMinusNalezny) {
        double oldValue = this.deklaracjaVAT7PodatekNaliczonyMinusNalezny;
        this.deklaracjaVAT7PodatekNaliczonyMinusNalezny = deklaracjaVAT7PodatekNaliczonyMinusNalezny;
        changeSupport.firePropertyChange("deklaracjaVAT7PodatekNaliczonyMinusNalezny", oldValue, deklaracjaVAT7PodatekNaliczonyMinusNalezny);
    }

    public double getDeklaracjaVAT7PodatekNaliczonyMinusNalezny() {
        return this.deklaracjaVAT7PodatekNaliczonyMinusNalezny;
    }

    public void setDeklaracjaVAT7KwotaDoZaplatyLubZwrotu(double deklaracjaVAT7KwotaDoZaplatyLubZwrotu) {
        double oldValue = this.deklaracjaVAT7KwotaDoZaplatyLubZwrotu;
        this.deklaracjaVAT7KwotaDoZaplatyLubZwrotu = deklaracjaVAT7KwotaDoZaplatyLubZwrotu;
        changeSupport.firePropertyChange("deklaracjaVAT7KwotaDoZaplatyLubZwrotu", oldValue, deklaracjaVAT7KwotaDoZaplatyLubZwrotu);
    }

    public double getDeklaracjaVAT7KwotaDoZaplatyLubZwrotu() {
        return this.deklaracjaVAT7KwotaDoZaplatyLubZwrotu;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Konstruktory">
    public DeklaracjaVAT7Model() {
    }

    public DeklaracjaVAT7Model(double deklaracjaVAT7KwotaPodatku, List<TowarModel> deklaracjaVAT7PrzedmiotOpodatkowania, double deklaracjaVAT7PodatekNaliczonyKwota, double deklaracjaVAT7PodatekNaleznyKwota, double deklaracjaVAT7PodatekNaliczonyMinusNalezny, double deklaracjaVAT7KwotaDoZaplatyLubZwrotu) {
        this.deklaracjaVAT7KwotaPodatku = deklaracjaVAT7KwotaPodatku;
        this.deklaracjaVAT7PrzedmiotOpodatkowania = deklaracjaVAT7PrzedmiotOpodatkowania;
        this.deklaracjaVAT7PodatekNaliczonyKwota = deklaracjaVAT7PodatekNaliczonyKwota;
        this.deklaracjaVAT7PodatekNaleznyKwota = deklaracjaVAT7PodatekNaleznyKwota;
        this.deklaracjaVAT7PodatekNaliczonyMinusNalezny = deklaracjaVAT7PodatekNaliczonyMinusNalezny;
        this.deklaracjaVAT7KwotaDoZaplatyLubZwrotu = deklaracjaVAT7KwotaDoZaplatyLubZwrotu;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="PropertyChange">
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }
    //</editor-fold>
}
