package ksiegowosc.DeklaracjaVAT7Component.Contract;

import ksiegowosc.DeklaracjaVAT7Component.Model.DeklaracjaVAT7Model;

/**
 * @author Paweł Żurawik
 * @version 1.0.0.0
 */
public interface IDeklaracjaVAT7 {

    String zwrocDeklaracjeVAT7(DeklaracjaVAT7Model deklaracja);

    void doPlikuDeklaracjeVAT7(DeklaracjaVAT7Model deklaracja, String sciezkaDoPliku);
}
